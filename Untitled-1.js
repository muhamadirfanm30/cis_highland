$(".btn-submit-reservasi-online").click(function(e){
        e.preventDefault();
        var name = $("input[name=name]").val();
        var id_campsite = $("input[name=id_campsite]").val();
        var telepon = $("input[name=telepon]").val();
        var checkin = $("input[name=checkin]").val();
        var berapa_malam = $("input[name=berapa_malam]").val();
        var pax = $("input[name=pax]").val();
        var id_paket = $('select[name="id_paket"]').val();

        $.ajax({
            type:'POST',
            url: "{{ url('api/admin/reservasi/store-reservasi-online') }}",
            data:{
                "_token": "{{ csrf_token() }}", 
                name:name, 
                telepon:telepon, 
                checkin:checkin, 
                berapa_malam:berapa_malam, 
                pax:pax, 
                id_paket:id_paket, 
                id_campsite:id_campsite
            },
            success:function(data){
                console.log(data.data.generateWaLink)
                var get_name = data.data.data_customer.nama
                    get_phone = data.data.data_customer.telepon
                    get_checkin = data.data.data_reservasi.checkin_date
                    get_checkout = data.data.data_reservasi.checkout_date
                    get_pax = data.data.data_reservasi.pax
                    get_durasi = data.data.data_reservasi.berapa_malam
                    get_package = data.data.data_paket.nama_paket
                    get_mesage = data.data.get_send_message.value

                /* Pengaturan Whatsapp */
                var walink 		= 'https://web.whatsapp.com/send',
                    phone 		= '6285280062254',
                    text 		= 'Halo saya ingin memesan ',
                    text_yes	= 'Pesanan Anda berhasil terkirim.',
                    text_no 	= 'Isilah formulir terlebih dahulu.';

                /* Smartphone Support */
                if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
                    var walink = 'whatsapp://send';
                }

                if(get_name != "" && get_phone != "" && get_package != ""){
                    /* Whatsapp URL */
                    var checkout_whatsapp = walink + '?phone=' + phone + '&text=' + text + '%0A%0A' +
                        '*Nama* : ' + get_name + '%0A' +
                        '*Nomor Kontak / Whatsapp* : ' + get_phone + '%0A' +
                        '*Paket* : ' + get_package + '%0A' +
                        '*Checkin* : ' + get_checkin + '%0A'+
                        '*Checkout* : ' + get_checkout + '%0A'+
                        '*Jumlah Orang* : ' + get_pax + '%0A'+
                        '*Berapa Malam* : ' + get_durasi + '%0A';

                    /* Whatsapp Window Open */
                    window.open(data.data.generateWaLink,'_blank');
                    document.getElementById("text-info").innerHTML = '<div class="alert alert-success">'+text_yes+'</div>';
                } else {
                    document.getElementById("text-info").innerHTML = '<div class="alert alert-danger">'+text_no+'</div>';
                }
                // window.location.href = '/reservasi-success/' + data.data.data_reservasi.id;
                
            }
        });

    });