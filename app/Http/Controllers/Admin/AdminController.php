<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\WebController;
use Illuminate\Http\Request;

class AdminController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.dashboard', [
            'title' => 'Dashboard',
        ]);
    }
}
