<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\ApiController;
use App\Models\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ApiBlogController extends ApiController
{
    public function index(Request $request)
    {
        $keyword = $request->search;

        $query = Blog::where(function ($q) use ($keyword) {
            if (!empty($keyword)) {
                $q->where('title', 'like', '%' . $keyword . '%');
            }
        });

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function destroy($id)
    {
        $getIcon = Blog::where('id', $id)->first();
        $exists = Storage::disk('local')->has('public/blog_image/' . $getIcon->image_blog);
        if ($exists) {
            Storage::delete('public/blog_image/' . $getIcon->image_blog);
        }
        Blog::find($id)->delete();
        return $this->successResponse($getIcon, 'ok');
    }
}
