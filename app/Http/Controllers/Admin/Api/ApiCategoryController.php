<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\ApiController;
use App\Models\Category;
use Illuminate\Http\Request;

class ApiCategoryController extends ApiController
{
    public function index(Request $request)
    {
        $keyword = $request->search;

        $query = Category::where(function ($q) use ($keyword) {
            if (!empty($keyword)) {
                $q->where('name', 'like', '%' . $keyword . '%');
            }
        });

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function store(Request $request)
    {
        $resp = Category::create([
            'name' => $request->name,
        ]);

        return $this->successResponse($resp, 'ok');
    }

    public function update($id, Request $request)
    {
        $find = Category::where('id', $id)->first();
        $find->update([
            'name' => $request->name,
        ]);
        return $this->successResponse($find, 'ok');
    }

    public function destroy($id)
    {
        $row = Category::where('id', $id)->first();
        $row->delete();
        return $this->successResponse($row, 'ok');
    }
}
