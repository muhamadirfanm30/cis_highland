<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\ApiController;
use App\Models\Fasilitas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;
use Illuminate\Support\Facades\Hash;

class ApiFasilitasController extends ApiController
{
    public function index(Request $request)
    {
        $keyword = $request->search;

        $query = Fasilitas::where(function ($q) use ($keyword) {
            if (!empty($keyword)) {
                $q->where('name', 'like', '%' . $keyword . '%');
            }
        });

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function destroy($id)
    {
        $getIcon = Fasilitas::where('id', $id)->first();
        $exists = Storage::disk('local')->has('public/icon_fasilitas/' . $getIcon->icon);
        if ($exists) {
            Storage::delete('public/icon_fasilitas/' . $getIcon->icon);
        }
        Fasilitas::find($id)->delete();
        return $this->successResponse($getIcon, 'ok');
    }

    public function update($id, Request $request)
    {
        $find = Fasilitas::where('id', $id)->first();
        $find->update([
            'name' => $request->name,
        ]);
        return $this->successResponse($find, 'ok');
    }
}
