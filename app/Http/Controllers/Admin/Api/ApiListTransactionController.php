<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\ApiController;
use App\Models\Checkout;
use App\Models\Vanue;
use Illuminate\Http\Request;

class ApiListTransactionController extends ApiController
{
    public function index(Request $request)
    {
        $keyword = $request->search;

        $query = Checkout::where(function ($q) use ($keyword) {
            if (!empty($keyword)) {
                $q->where('first_name', 'like', '%' . $keyword . '%');
            }
        });

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function getFasilitas($id)
    {
        $data = Vanue::with('get_fasilitas')->where('id', $id)->first();
        return $this->successResponse($data, 'ok');
    }

    public function edit($id)
    {
        # code...
    }
}
