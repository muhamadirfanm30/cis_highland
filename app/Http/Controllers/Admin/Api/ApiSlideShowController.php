<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\ApiController;
use App\Models\SlideShow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class ApiSlideShowController extends ApiController
{
    public function index(Request $request)
    {
        $keyword = $request->search;

        $query = SlideShow::where(function ($q) use ($keyword) {
            if (!empty($keyword)) {
                $q->where('image_slider', 'like', '%' . $keyword . '%');
            }
        });

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function destroy($id)
    {
        $getIcon = SlideShow::where('id', $id)->first();
        $exists = Storage::disk('local')->has('public/slide-show/' . $getIcon->images_slider);
        if ($exists) {
            Storage::delete('public/slide-show/' . $getIcon->images_slider);
        }
        SlideShow::find($id)->delete();
        return $this->successResponse($getIcon, 'ok');
    }
}
