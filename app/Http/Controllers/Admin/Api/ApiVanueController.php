<?php

namespace App\Http\Controllers\Admin\Api;

use App\Http\Controllers\ApiController;
use App\Models\FasilitasDetailVanue;
use App\Models\Vanue;
use App\Models\VanueImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use DB;

class ApiVanueController extends ApiController
{
    public function index(Request $request)
    {
        $keyword = $request->search;

        $query = Vanue::where(function ($q) use ($keyword) {
            if (!empty($keyword)) {
                $q->where('villa_name', 'like', '%' . $keyword . '%');
            }
        });

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function destroy($id)
    {
        $getIcon = VanueImage::where('id', $id)->first();
        $exists = Storage::disk('local')->has('public/vanue_image/' . $getIcon->image);
        if ($exists) {
            Storage::delete('public/vanue_image/' . $getIcon->image);
        }
        VanueImage::find($id)->delete();
        return $this->successResponse($getIcon, 'ok');
    }

    public function delete($id)
    {
        $getIcon = VanueImage::where('ms_vanue_id', $id)->get();
        foreach ($getIcon as $icon) {
            $exists = Storage::disk('local')->has('public/vanue_image/' . $icon->image);
            if ($exists) {
                Storage::delete('public/vanue_image/' . $icon->image);
            }
        }
        
        $deleteImage = VanueImage::where('ms_vanue_id', $id)->delete();
        $deleteFasilitas = FasilitasDetailVanue::where('ms_vanue_id', $id)->delete();
        $deleteVanue = Vanue::find($id)->delete();
        return $this->successResponse($getIcon, 'ok');
    }

    public function edit($id, Request $request)
    {
        return $request->all();
        DB::beginTransaction();
        try {
            $soreVanue = Vanue::find($request->id)->update([
                'vanue_name' => $request->vanue_name,
                'harga' => $request->harga,
                'category' => $request->category,
                'location' => $request->location,
                'desc' => $request->desc,
                'information' => $request->information,
            ]);

            $deleteFasilitas = FasilitasDetailVanue::where('ms_vanue_id', $request->id)->delete();

            $data_fasilitas = [];
            foreach ($request->fasilitas_name as $key => $value) {
                $data_fasilitas[] = [
                    'ms_vanue_id' => $request->id,
                    'fasilitas_name' => $value,
                ];
            }

            FasilitasDetailVanue::insert($data_fasilitas);
            DB::commit();
            
            return redirect()->route('villa.read')->with('success', 'ok');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
