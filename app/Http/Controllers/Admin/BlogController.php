<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\WebController;
use App\Models\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class BlogController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.blog.index', [
            'title' => 'Blog List',
        ]);
    }

    public function create()
    {
        return $this->loadView('admin.blog.create', [
            'title' => 'Blog Create',
        ]);
    }

    public function update($id)
    {
        return $this->loadView('admin.blog.update', [
            'title' => 'Blog update',
            'datas' => Blog::find($id),
        ]);
    }

    public function store(Request $request)
    {
        $filename_img_author = null;
        if (!empty($request->image_blog)) {
            if ($image_blog = $request->image_blog) {
                $name_image_blog = $image_blog->getClientOriginalName();
                $img_blog = Image::make($image_blog);
                $img_blog->stream(); // <-- Key point
                Storage::disk('local')->put('public/blog_image/' . $name_image_blog, $img_blog);
                $filename_img_blog = $name_image_blog;
            }
        } else {
            $filename_img_blog = null;
        }

        $filename_img_author = null;
        if (!empty($request->authors_photo)) {
            if ($author_img = $request->authors_photo) {
                $name_image_author = $author_img->getClientOriginalName();
                $img_author = Image::make($author_img);
                $img_author->stream(); // <-- Key point
                Storage::disk('local')->put('public/author_image/' . $name_image_author, $img_author);
                $filename_img_author = $name_image_author;
            }
        } else {
            $filename_img_author = null;
        }
        
        $store = Blog::create([
            'title' => $request->title,
            'date' => str_replace('/', '-', $request->date),
            'image_blog' => $filename_img_blog,
            'writed_by' => $request->writed_by,
            'authors_photo' => $filename_img_author,
            'desc' => $request->desc,
        ]);

        return redirect()->route('blog.read')->with('success', 'ok');
    }

    public function edit($id, Request $request)
    {
        $cek = Blog::where('id', $id)->first();

        if (!empty($request->image_blog)) {
            if ($request->hasFile('image_blog')) {
                $image_blog_spot      = $request->file('image_blog');
                $file_name_blog_spot   = time(). '.' . $image_blog_spot->getClientOriginalExtension();
                $img_blog_spot = Image::make($image_blog_spot);
                $img_blog_spot->stream(); // <-- Key point
                Storage::disk('local')->put('public/blog_image/' . $file_name_blog_spot, $img_blog_spot);
    
                $exists1 = Storage::disk('local')->has('public/blog_image/' . $cek->image_blog);
                if ($exists1) {
                    Storage::delete('public/blog_image/' . $cek->image_blog);
                }
            }
        } else {
            $file_name_blog_spot = $cek->image_blog;
        }

        if (!empty($request->authors_photo)) {
            if ($request->hasFile('authors_photo')) {
                $image_author      = $request->file('authors_photo');
                $file_name_author   = time(). '.' . $image_author->getClientOriginalExtension();
                $img_author = Image::make($image_author);
                $img_author->stream(); // <-- Key point
                Storage::disk('local')->put('public/author_image/' . $file_name_author, $img_author);
    
                $exists1 = Storage::disk('local')->has('public/author_image/' . $cek->authors_photo);
                if ($exists1) {
                    Storage::delete('public/author_image/' . $cek->authors_photo);
                }
            }
        } else {
            $file_name_author = $cek->authors_photo;
        }

        $cek->update([
            'title' => $request->title,
            'date' => str_replace('/', '-', $request->date),
            'image_blog' => $file_name_blog_spot,
            'writed_by' => $request->writed_by,
            'authors_photo' => $file_name_author,
            'desc' => $request->desc,
        ]);

        return redirect()->route('blog.read')->with('success', 'ok');
    }
}
