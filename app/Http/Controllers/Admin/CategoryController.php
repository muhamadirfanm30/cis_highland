<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\WebController;
use Illuminate\Http\Request;

class CategoryController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.category.index', [
            'title' => 'Category List',
        ]);
    }
}
