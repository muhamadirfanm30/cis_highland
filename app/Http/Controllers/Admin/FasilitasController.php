<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\WebController;
use App\Models\Fasilitas;
use Illuminate\Http\Request;
use App\Helpers\ImageUpload;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class FasilitasController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.fasilitas.index', [
            'title' => 'Fasilitas List',
        ]);
    }

    public function create()
    {
        return $this->loadView('admin.fasilitas.create', [
            'title' => 'Fasilitas Create',
        ]);
    }

    public function update($id)
    {
        return $this->loadView('admin.fasilitas.update', [
            'title' => 'Fasilitas update',
            'data' => Fasilitas::find($id)
        ]);
    }

    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'name' => 'required',
        //     'icon' => 'required',
        // ]);

        // $name_file = null;
        // if ($request->hasfile('icon')) {
        //     $file = $request->file('icon');
        //     foreach ($file as $image) {
        //         $filenameWithExt = $image->getClientOriginalName();
        //         $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        //         $extension = $image->getClientOriginalExtension();
        //         $filenameSimpan = $filename .'.'. $extension;
        //         $path = $image->storeAs('public/icon_fasilitas', $filenameSimpan);

        //         Fasilitas::create([
        //             'icon' => $filenameSimpan,
        //             'name' => $request->name,
        //         ]);
        //     }
        // }
        // return back()->with('success', 'Image Has Been Uploaded');

        $value = $request->name;

        foreach ($value as $key => $values) {
            Fasilitas::create([
                'name' => $value[$key],
            ]);
        }

        return redirect()->route('fasilitas.read')->with('success', 'Image Has Been Uploaded');
    }

    public function edit($id, Request $request)
    {
        $cek = Fasilitas::where('id', $id)->first();

        if (!empty($request->icon)) {
            if ($request->hasFile('icon')) {
                $image      = $request->file('icon');
                $file_name   = time(). '.' . $image->getClientOriginalExtension();
                $img = Image::make($image);
                $img->stream(); // <-- Key point
                Storage::disk('local')->put('public/icon_fasilitas/' . $file_name, $img);
    
                $exists1 = Storage::disk('local')->has('public/icon_fasilitas/' . $cek->icon);
                if ($exists1) {
                    Storage::delete('public/icon_fasilitas/' . $cek->icon);
                }
            }
        } else {
            $file_name = $cek->icon;
        }

        $cek->update([
            'icon' => $file_name,
            'name' => $request->name,
        ]);

        return back()->with('success', 'Fasilitas Has Been Uploaded');
    }
}
