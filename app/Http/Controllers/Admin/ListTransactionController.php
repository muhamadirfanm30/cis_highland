<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\WebController;
use App\Models\Checkout;
use Illuminate\Http\Request;

class ListTransactionController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.transaction.index', [
            'title' => 'Transaction List',
        ]);
    }

    public function detail($id)
    {
        Checkout::find($id)->update([
            'read_at' => date('Y-m-d H:i:s'),
        ]);

        return $this->loadView('admin.transaction.detail', [
            'title' => 'Transaction List',
            'data' => Checkout::with('data_vanue.get_fasilitas')->find($id)
        ]);
    }
}
