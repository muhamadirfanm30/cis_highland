<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\WebController;
use App\Models\SlideShow;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class SlideShowController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.slideshow.index', [
            'title' => 'Slider List',
        ]);
    }

    public function create()
    {
        return $this->loadView('admin.slideshow.create', [
            'title' => 'Slider Create',
        ]);
    }

    public function store(Request $request)
    {
        // $this->validate($request, [
        //     'judul' => ['required'],
        //     'deskripsi' => ['required'],
        //     'images' => ['required'],
        //     'is_template' => ['required'],
        // ]);

        // $path = SlideShow::getImagePathUpload();
        $filename = null;

        if ($request->hasFile('images_slider')) {
            foreach ($request->file('images_slider') as $image) {
                $name = $image->getClientOriginalName();
                $img = Image::make($image);
                $img->stream(); // <-- Key point
                Storage::disk('local')->put('public/slide-show/' . $name, $img);
                $filename[] = $name;
            }
        }
        


        for ($i = 0; $i < count($request->header); $i++) {
            $insert[] = [
                'header' => $request->header[$i],
                'desc' => $request->desc[$i],
                'images_slider' => $filename[$i],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
        }

        SlideShow::insert($insert);
        return redirect()->route('slider.read')
                        ->with('success', 'Slide Show created successfully');
    }
}
