<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\WebController;
use App\Models\Category;
use App\Models\Fasilitas;
use App\Models\FasilitasDetailVanue;
use App\Models\Vanue;
use App\Models\VanueImage;
use Illuminate\Http\Request;
use DB;

class VanueController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.villa.index', [
            'title' => 'Vanue List',
        ]);
    }

    public function create()
    {
        return $this->loadView('admin.villa.create', [
            'title' => 'Vanue Create',
            'getCategory' => Category::get(),
            'getFasilitas' => Fasilitas::get(),
        ]);
    }

    public function update($id)
    {
        return $this->loadView('admin.villa.update', [
            'title' => 'Vanue Create',
            'no' => 1,
            'getCategory' => Category::get(),
            'getFasilitas' => Fasilitas::get(),
            'vanue' => Vanue::find($id),
            'get_image' => VanueImage::where('ms_vanue_id', $id)->get()
        ]);
    }

    public function imageStore(Request $request)
    {

        if ($request->hasfile('image')) {
            $file = $request->file('image');
            foreach ($file as $image) {
                $filenameWithExt = $image->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $image->getClientOriginalExtension();
                $filenameSimpan = $filename .'.'. $extension;
                $path = $image->storeAs('public/vanue_image', $filenameSimpan);

                VanueImage::create([
                    'image' => $filenameSimpan,
                    'ms_vanue_id' => $request->id,
                ]);
            }
        }
        return redirect()->back()->with('success', 'ok');
    }

    public function store(Request $request)
    {
        DB::beginTransaction();
        try {
            $soreVanue = Vanue::create([
                'vanue_name' => $request->vanue_name,
                'harga' => $request->harga,
                'category' => $request->category,
                'location' => $request->location,
                'desc' => $request->desc,
                'information' => $request->information,
            ]);

            if ($request->hasfile('image')) {
                $file = $request->file('image');
                foreach ($file as $image) {
                    $filenameWithExt = $image->getClientOriginalName();
                    $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                    $extension = $image->getClientOriginalExtension();
                    $filenameSimpan = $filename .'.'. $extension;
                    $path = $image->storeAs('public/vanue_image', $filenameSimpan);

                    VanueImage::create([
                        'image' => $filenameSimpan,
                        'ms_vanue_id' => $soreVanue->id,
                    ]);
                }
            }

            $data_fasilitas = [];
            foreach ($request->fasilitas_name as $key => $value) {
                $data_fasilitas[] = [
                    'ms_vanue_id' => $soreVanue->id,
                    'fasilitas_name' => $value,
                ];
            }

            FasilitasDetailVanue::insert($data_fasilitas);
            DB::commit();
            
            return redirect()->route('villa.read')->with('success', 'ok');
        } catch (\Exception $e) {
            return $e->getMessage();
        }
    }
}
