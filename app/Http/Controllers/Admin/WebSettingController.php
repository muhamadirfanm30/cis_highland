<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\WebController;
use App\Models\WebSetting;
use Illuminate\Http\Request;

class WebSettingController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.web-setting.index', [
            'title' => 'Web Setting',
            'get_data_setting' => WebSetting::get(),
        ]);
    }

    public function store(Request $request)
    {
        $value = $request->value;
        $ids = $request->id;

        foreach ($value as $key => $values) {
            $id = $ids[$key];
            WebSetting::where('id', $id)->update([
                'value' => $request->value[$key],
            ]);
        }

        return redirect()->back()->with('success', 'ok');
    }
}
