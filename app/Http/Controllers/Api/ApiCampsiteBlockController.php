<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Models\CampsiteBlock;
use Illuminate\Http\Request;

class ApiCampsiteBlockController extends ApiController
{
    public function index(Request $request)
    {
        $keyword = $request->search;

        $query = CampsiteBlock::with('campsite')->where(function ($q) use ($keyword) {
            if (!empty($keyword)) {
                $q->where('block', 'like', '%' . $keyword . '%');
            }
        })->orderBy('campsite_id', 'asc');

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function select2($id)
    {
        $cek = CampsiteBlock::where('campsite_id', $id)->count();
        if ($cek > 0) {
            $blok = CampsiteBlock::where('campsite_id', $id)->get();
            return $this->successResponse($blok, 'ok');
        } else {
            return $this->errorResponse(null, 'Blok Tidak tersedia di Campsite ini.');
        }
    }

    public function store(Request $request)
    {
        $resp = CampsiteBlock::create([
            'campsite_id' => $request->campsite_id,
            'name' => $request->name,
        ]);

        return $this->successResponse($resp, 'ok');
    }

    public function show($id)
    {
        $resp = CampsiteBlock::where('id', $id)->firstOrFail();

        return $this->successResponse($resp, 'ok');
    }

    public function update(Request $request, $id)
    {
        $row = CampsiteBlock::where('id', $id)->firstOrFail();

        $row->update([
            'campsite_id' => $request->campsite_id,
            'name' => $request->name,
        ]);

        return $this->successResponse($row, 'ok');
    }

    public function destroy($id)
    {
        $resp = CampsiteBlock::where('id', $id)->firstOrFail()->delete();

        return $this->successResponse($resp, 'ok');
    }
}
