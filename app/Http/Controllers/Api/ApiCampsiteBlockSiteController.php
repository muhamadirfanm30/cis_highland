<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Models\CampsiteBlock;
use App\Models\CampsiteBlockSite;
use App\Models\CategoryCampsiteBlock;
use Illuminate\Http\Request;

class ApiCampsiteBlockSiteController extends ApiController
{
    public function index(Request $request)
    {
        $keyword = $request->search;

        $query = CampsiteBlockSite::with('block.campsite')
            ->where(function ($q) use ($keyword) {
                if (!empty($keyword)) {
                    $q->where('name', 'like', '%' . $keyword . '%');
                }
            });

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function store(Request $request)
    {
        $get_category = $request->category_campsite;

        $resp = CampsiteBlockSite::create([
            'campsite_block_id' => $request->campsite_block_id,
            'name' => $request->name,
            'capacity' => $request->capacity,
            'note' => $request->note,
            'type_area' => $request->category,
        ]);
        foreach ($get_category as $key => $value) {
            CategoryCampsiteBlock::create([
                'campsite_block_site_id' => $resp->id,
                'category_campsite' => $get_category[$key],
            ]);
        }
        return $this->successResponse($resp, 'ok');
    }

    public function show($id)
    {
        $data = CampsiteBlockSite::with('get_category_camp')->where('id', $id)->first();
        return [
            'success' => true,
            'data'    => $data,
            'message' => 'success',
            'status_code' => '200',
        ];
    }

    public function update(Request $request, $id)
    {
        $row = CampsiteBlockSite::where('id', $id)->firstOrFail();
        $deleteDulu = CategoryCampsiteBlock::where('campsite_block_site_id', $id)->delete();
        $row->update([
            'campsite_block_id' => $request->campsite_block_id,
            'name' => $request->name,
            'capacity' => $request->capacity,
            'note' => $request->note,
            'category' => $request->category,
            'type_area' => $request->type_area,
        ]);

        $category_campsite = [];
        foreach ($request->category_campsite as $key => $value) {
            $category_campsite[] = [
                'campsite_block_site_id' => $row->id,
                'category_campsite' => $request->category_campsite[$key],
            ];
        }

        CategoryCampsiteBlock::insert($category_campsite);
        return $this->successResponse($row, 'ok');
    }

    public function updateMapCoordinate(Request $request)
    {
        $row = CampsiteBlockSite::where('id', $request->id)->firstOrFail();

        $row->update([
            'coordinate_x' => $request->coordinate_x,
            'coordinate_y' => $request->coordinate_y,
        ]);

        return $this->successResponse($row, 'ok');
    }

    public function destroy($id)
    {
        $resp = CampsiteBlockSite::where('id', $id)->firstOrFail()->delete();

        return $this->successResponse($resp, 'ok');
    }
}
