<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Models\Campsite;
use App\Models\CampsiteBlockSite;
use App\Models\CampsiteImage;
use Illuminate\Http\Request;

class ApiCampsiteController extends ApiController
{
    public function index(Request $request)
    {
        $keyword = $request->search;

        $query = Campsite::where(function ($q) use ($keyword) {
            if (!empty($keyword)) {
                $q->where('campsite', 'like', '%' . $keyword . '%');
            }
        });

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function datatablesCampImage(Request $request)
    {
        $keyword = $request->search;

        $query = CampsiteImage::with('campsiteBlockSite.block.campsite')->where(function ($q) use ($keyword) {
            if (!empty($keyword)) {
                $q->where('camp_block_site_id', 'like', '%' . $keyword . '%');
            }
        });

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function listBlock()
    {
        $resp = CampsiteBlockSite::with('block.campsite')->get();

        return $this->successResponse($resp, 'ok');
    }

    public function store(Request $request)
    {
        $resp = Campsite::create([
            'campsite' => $request->campsite,
        ]);

        return $this->successResponse($resp, 'ok');
    }

    public function show($id)
    {
        $resp = Campsite::where('id', $id)->firstOrFail();

        return $this->successResponse($resp, 'ok');
    }

    public function update(Request $request, $id)
    {
        $row = Campsite::where('id', $id)->firstOrFail();

        $row->update([
            'campsite' => $request->campsite,
        ]);

        return $this->successResponse($row, 'ok');
    }

    public function destroy($id)
    {
        $resp = Campsite::where('id', $id)->firstOrFail()->delete();

        return $this->successResponse($resp, 'ok');
    }
}
