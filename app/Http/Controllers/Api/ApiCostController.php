<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Models\Cost;
use App\Models\CostDetail;
use App\Service\QueryService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiCostController extends ApiController
{
    public function index(Request $request)
    {
        $keyword = $request->search;

        $query = Cost::query()
            ->withCount('details')
            ->withSum('details', 'cost_total')
            ->where(function ($q) use ($keyword) {
                if (!empty($keyword)) {
                    $q->where('note', 'like', '%' . $keyword . '%');
                }
            });

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function filter(Request $request)
    {
        $cost = Cost::with('details')->where('date_from', $request->date_from)->where('date_to', $request->date_to)->first();
        $resp = $cost->details;
        return $this->successResponse($resp, 'data cost filter berhasil di load');
    }

    public function store(Request $request)
    {
        $cost_id = QueryService::getLastId('costs');
        $date_from = $request->date_from;
        $date_to = $request->date_to;

        $cost_total = $request->cost_total;
        $cost_note = $request->cost_note;
        $cost_category_id = $request->cost_category_id;

        $data_cost_details = [];
        foreach ($cost_total as $key => $value) {
            if (!empty($cost_total[$key])) {
                $data_cost_details[] = [
                    'cost_id' => $cost_id,
                    'cost_total' => $cost_total[$key],
                    'cost_note' => $cost_note[$key],
                    'cost_category_id' => $cost_category_id[$key],
                ];
            }
        }

        $data_cost = [
            'id' => $cost_id,
            'date_from' => $date_from,
            'date_to' => $date_to,
        ];

        $debug = [
            'data_cost' => $data_cost,
            'data_cost_details' => $data_cost_details,
        ];

        DB::beginTransaction();
        try {
            Cost::create($data_cost);
            CostDetail::insert($data_cost_details);
            DB::commit();
            return $this->successResponse($debug, 'data cost berhasil di input');
        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorResponse($e, $e->getMessage());
        }
    }

    public function show($id)
    {
        $resp = Cost::where('id', $id)->firstOrFail();

        return $this->successResponse($resp, 'ok');
    }

    public function update(Request $request)
    {
        $cost_id = $request->cost_id;

        $cost_detail_id = $request->id;
        $cost_total = $request->cost_total;
        $cost_note = $request->cost_note;
        $cost_category_id = $request->cost_category_id;

        $data_cost_detail_insert = [];
        $data_cost_detail_update = [];
        foreach ($cost_total as $key => $value) {
            if ($cost_detail_id[$key] == 0) {
                if (!empty($cost_total[$key])) {
                    $data_cost_detail_insert[] = [
                        'cost_id' => $cost_id,
                        'cost_total' => $cost_total[$key],
                        'cost_note' => $cost_note[$key],
                        'cost_category_id' => $cost_category_id[$key],
                    ];
                }
            } else {
                if (!empty($cost_total[$key])) {
                    $data_cost_detail_update[$cost_detail_id[$key]] = [
                        'cost_total' => $cost_total[$key],
                        'cost_note' => $cost_note[$key],
                        'cost_category_id' => $cost_category_id[$key],
                    ];
                }
            }
        }

        $debug = [
            'data_cost_detail_update' => $data_cost_detail_update,
            'data_cost_detail_insert' => $data_cost_detail_insert,
        ];

        DB::beginTransaction();
        try {
            if (count($data_cost_detail_insert) > 0) {
                CostDetail::insert($data_cost_detail_insert);
            }
            if (count($data_cost_detail_update) > 0) {
                foreach ($data_cost_detail_update as $id => $data_update) {
                    CostDetail::where('id', $id)->update($data_update);
                }
            }
            DB::commit();
            return $this->successResponse($debug, 'data cost berhasil di update');
        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorResponse($e, $e->getMessage());
        }
    }

    public function destroy($id)
    {
        $resp = Cost::where('id', $id)->firstOrFail()->delete();

        return $this->successResponse($resp, 'data cost berhasil di delete');
    }
}
