<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Models\CampsiteBlockSite;
use App\Models\Reservation;
use App\Service\Reservation\ReservationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiDashboardController extends ApiController
{
    public function todayReservationMap(Request $request)
    {
        $resp = ReservationService::mapResource($request);

        return $this->successResponse($resp, 'ok');
    }

    public function todayReservation(Request $request)
    {
        $keyword = $request->search;

        $query = DB::table('campsite_block_history_transactions')
            ->select([
                DB::raw('customers.nama as customer_name'),
                DB::raw('reservation_statuses.name as reservation_status_name'),
                DB::raw('campsites.campsite as campsites_name'),
                DB::raw('campsite_blocks.name as campsite_block_name'),
                DB::raw('campsite_block_sites.name as campsite_block_sites_name'),
            ])
            ->join('reservations', 'reservations.id', '=', 'campsite_block_history_transactions.reservation_id')
            ->join('customers', 'customers.id', '=', 'reservations.customer_id')
            ->join('reservation_statuses', 'reservation_statuses.id', '=', 'reservations.reservation_status_id')
            ->join('campsite_block_sites', 'campsite_block_sites.id', '=', 'campsite_block_history_transactions.capmsite_block_site_id')
            ->join('campsite_blocks', 'campsite_blocks.id', '=', 'campsite_block_sites.campsite_block_id')
            ->join('campsites', 'campsites.id', '=', 'campsite_blocks.campsite_id')
            ->where(DB::raw('DATE(reservations.checkin_date)'), date('Y-m-d'));

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function availableReservation(Request $request)
    {
        $keyword = $request->search;

        $query = CampsiteBlockSite::query()
            ->where('capacity', '>', 0);

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }
}
