<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Models\Cost;
use App\Models\HistoryItemService;
use App\Service\FinanceService;
use App\Service\HelperService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ApiFinanceController extends ApiController
{
    public function pembiayaan(Request $request)
    {
        $date_from = $request->date_from;
        $date_to = $request->date_to;

        $query = DB::table('view_reservation_summary');

        if (!empty($date_from) || !empty($date_to)) {
            $query->where('reservations.checkin_date', '>=', $date_from)->where('reservations.checkin_date', '<=', $date_to);;
        }

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function penjualan(Request $request)
    {
        $date_from = $request->date_from;
        $date_to = $request->date_to;

        $query = DB::table('view_reservation_summary');

        if (!empty($date_from) || !empty($date_to)) {
            $query->where('reservations.checkin_date', '>=', $date_from)->where('reservations.checkin_date', '<=', $date_to);;
        }

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function vendor(Request $request)
    {
        $date_from = $request->date_from;
        $date_to = $request->date_to;
        $vendor_id = empty($request->vendor_id) ? null : $request->vendor_id;

        if ($vendor_id == null) {
            return $this->successResponse([], 'ok');
        }

        $query = FinanceService::vendor($query = true)
            ->where('vendors.id', $vendor_id);

        if (!empty($date_from) || !empty($date_to)) {
            $query->where('reservations.checkin_date', '>=', $date_from)->where('reservations.checkin_date', '<=', $date_to);;
        }

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function vendorDetail(Request $request)
    {
        $reservation_id = empty($request->reservation_id) ? null : $request->reservation_id;
        $vendor_id = empty($request->vendor_id) ? null : $request->vendor_id;

        if ($vendor_id == null) {
            return $this->successResponse([], 'ok');
        }

        $query = FinanceService::vendorDetail($query = true)
            ->where('vendor_items.vendor_id', $vendor_id)
            ->where('reservations.id', $reservation_id);

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function a3(Request $request)
    {
        $date_from = $request->date_from == null ? HelperService::dateMinMonth(date('Y-m-d'), 3) : $request->date_from;
        $date_to = $request->date_to == null ? HelperService::dateAddMonth(date('Y-m-d'), 1) : $request->date_to;

        $query = FinanceService::a3($query = true)
            ->where('first_week', '>=', $date_from)
            ->where('end_week', '<=', $date_to);

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }
}
