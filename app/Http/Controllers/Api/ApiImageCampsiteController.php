<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Models\ImagesCampBlockSite;
use Illuminate\Http\Request;

class ApiImageCampsiteController extends ApiController
{
    public function index(Request $request)
    {
        $keyword = $request->search;
        $query = ImagesCampBlockSite::with(['customer', 'package', 'payment_status', 'status'])
            ->where(function ($q) use ($keyword) {
            });
        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }
}
