<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Models\KategoryItem;

class ApiKategoriItemController extends ApiController
{
    public function index(Request $request)
    {
        $keyword = $request->search;

        $query = KategoryItem::where(function ($q) use ($keyword) {
            if (!empty($keyword)) {
                $q->where('kategory_name', 'like', '%' . $keyword . '%');
            }
        });

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function store(Request $request)
    {
        $resp = KategoryItem::create([
            'kategory_name' => $request->kategory_name,
        ]);

        return $this->successResponse($resp, 'ok');
    }

    public function show($id)
    {
        $resp = KategoryItem::where('id', $id)->firstOrFail();

        return $this->successResponse($resp, 'ok');
    }

    public function update(Request $request, $id)
    {

        $row = KategoryItem::where('id', $id)->update([
            'kategory_name' => $request->kategory_name,
        ]);

        return $this->successResponse($row, 'ok');
    }

    public function destroy($id)
    {
        $resp = KategoryItem::where('id', $id)->firstOrFail()->delete();

        return $this->successResponse($resp, 'ok');
    }
}
