<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Models\MapConfig;
use Illuminate\Http\Request;

class ApiMapConfigController extends ApiController
{
    public function update(Request $request)
    {
        $row = MapConfig::getActiveMap();
        
        $row->update([
            'map_src' => $request->hasFile('map_src') ? $request->file('map_src')->store('map', 'public') : $row->map_src,
            'map_show' => $request->map_show,
            'map_rate' => $request->map_rate,
            'map_width' => $request->map_width,
            'map_height' => $request->map_height,
            'map_max' => $request->map_max,
            'map_marker_size' => $request->map_marker_size,
            'map_marker_src' => $request->hasFile('map_marker_src') ? $request->file('map_marker_src')->store('map', 'public') : $row->map_marker_src,
            'map_marker_draggable' => $request->map_marker_draggable,
        ]);

        return $this->successResponse($row, 'ok');
    }
}
