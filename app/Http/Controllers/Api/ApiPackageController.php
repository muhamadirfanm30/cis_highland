<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Models\Package;
use App\Models\PackageItem;
use App\Service\QueryService;
use Auth;
use Illuminate\Support\Facades\DB;

class ApiPackageController extends ApiController
{
    public function index(Request $request)
    {
        $keyword = $request->search;

        $query = Package::with(['bussiness'])->where(function ($q) use ($keyword) {
            if (!empty($keyword)) {
                $q->where('nama_paket', 'like', '%' . $keyword . '%');
            }
        });

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function store(Request $request)
    {
        $package_id = QueryService::getLastId('packages');

        $data_package = [
            'id' => $package_id,
            'bussiness_id' => $request->bussiness_id,
            'harga' => $request->harga,
            'nama_paket' => $request->nama_paket,
        ];

        $data_package_item = [];
        foreach ($request->vendor_item_id as $key => $value) {
            $data_package_item[] = [
                'package_id' => $package_id,
                'vendor_item_id' => $value,
            ];
        }

        DB::beginTransaction();
        try {
            $resp = Package::create($data_package);
            PackageItem::insert($data_package_item);
            DB::commit();
            return $this->successResponse($resp, 'ok');
        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorResponse($e, $e->getMessage());
        }
    }

    public function show($id)
    {
        $resp = Package::with('items')->where('id', $id)->firstOrFail();

        return $this->successResponse($resp, 'ok');
    }

    public function update(Request $request, $id)
    {
        $package = Package::where('id', $id)->firstOrFail();

        $data_package = [
            'bussiness_id' => $request->bussiness_id,
            'harga' => $request->harga,
            'nama_paket' => $request->nama_paket,
        ];

        $data_package_item = [];
        foreach ($request->vendor_item_id as $key => $value) {
            $data_package_item[] = [
                'package_id' => $package->id,
                'vendor_item_id' => $value,
            ];
        }

        DB::beginTransaction();
        try {
            $resp = $package->update($data_package);
            PackageItem::where('package_id', $package->id)->delete();
            PackageItem::insert($data_package_item);
            DB::commit();
            return $this->successResponse($resp, 'ok');
        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorResponse($e, $e->getMessage());
        }
    }

    public function deletePaket($id)
    {
        $getCart = PackageItem::where('id_paket', $id)->get();
        $delete = PackageItem::where('id_paket', $id)->get('id');
        $coundTmp = PackageItem::where('id_paket', $id)->count();
        for ($i = 0; $i < $coundTmp; $i++) {
            $deleteTmp = PackageItem::find($getCart[$i]->id)->delete();
        }
    }

    public function destroy($id)
    {
        $resp = Package::where('id', $id)->firstOrFail()->delete();

        return $this->successResponse($resp, 'ok');
    }
}
