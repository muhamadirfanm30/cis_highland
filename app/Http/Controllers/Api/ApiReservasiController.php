<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Models\CampsiteBlockHistoryTransaction;
use App\Models\CampsiteBlockSite;
use Illuminate\Http\Request;
use App\Models\Reservation;
use App\Models\Customer;
use App\Models\GeneralSetting;
use App\Models\HistoryItemService;
use App\Models\HistoryItemStatus;
use App\Models\Package;
use App\Models\Payment;
use App\Models\PaymentStatus;
use App\Models\ReservationStatus;
use App\Models\Ticket;
use App\Models\TicketTransaction;
use App\Models\VendorItem;
use App\Models\WhatsappTemplate;
use App\Service\HelperService;
use App\Service\QueryService;
use App\Service\Reservation\ReservationService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class ApiReservasiController extends ApiController
{
    public function index(Request $request)
    {
        $keyword = $request->search;

        $query = Reservation::where('is_tmp', 0)
            ->withCount('tickets')->with(['customer', 'package', 'payment_status', 'status'])
            ->where(function ($q) use ($keyword) {
            });

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function listStatus()
    {
        return $this->successResponse(ReservationStatus::orderBy('id', 'Desc')->get(), 'oke');
    }

    public function get_item()
    {
        $item = VendorItem::where('stok', '>', 0)->get();
        return $this->successResponse($item, 'Data Berhasil Ditemukan');
    }

    public function getPriceItem($id)
    {
        $item = VendorItem::where('stok', '>', 0)->find($id);
        return $this->successResponse($item, 'Data Berhasil Ditemukan');
    }

    public function itemInclude($id)
    {
        $include_item = Package::where('id', $id)->with('items.vendor_item')->first();
        return $this->successResponse($include_item, 'Data Berhasil Ditemukan');
    }


    public function datatablesItem($id)
    {
        $reservasi_item = HistoryItemService::whereNotNull('vendor_item_id')
            ->with('vendor_item')
            ->where('reservation_id', $id)
            ->get();
        return $this->successResponse($reservasi_item, 'Data Berhasil Ditemukan');
    }

    public function itemNotInclude($id)
    {
        $reservasi_item = HistoryItemService::whereNotNull('vendor_item_id')
            ->with('vendor_item')
            ->where('reservation_id', $id)
            ->where('is_include_paket', '0')
            ->get();
        return $this->successResponse($reservasi_item, 'Data Berhasil Ditemukan');
    }

    public function generateOrderCode()
    {
        $q          = Reservation::count();
        $prefix     = 'O'; //prefix = O-16032020-00001
        $separator  = '-';
        $date       = date('m-d-Y');
        $number     = 1; //format
        $new_number = sprintf("%03s", $number);
        $code       = 'INV-' . ($new_number) . ($separator) .  $date;
        $order_code   = $code;
        if ($q > 0) {
            $last     = Reservation::orderBy('id', 'desc')->value('code_invoice');
            $last     = explode("-", $last);
            $order_code = 'INV-' . (sprintf("%03s", $last[1] + 1)) . ($separator) . $date;
        }
        return $order_code;
    }

    public function storeReservasi(Request $request)
    {
        $customer_id = QueryService::getLastId('customers');
        $reservasi_id = QueryService::getLastId('reservations');
        $package_total_item_pembiayaan = 0;
        $package_total_item_penjualan = 0;
        $additional_total_item = 0;
        $additional_total_item_pembiayaan = 0;

        // item in package
        $data_history_paket = [];
        $data_item_vandor_update = [];
        $data_campsite_transaction = [];
        $data_campsite_blok_site_update = [];


        foreach ($request->include_id as $keys => $values) {
            $qty = $request->include_qty[$keys];

            $data_history_paket[] = [
                'vendor_item_id' => $request->include_id[$keys],
                'reservation_id' => $reservasi_id,
                'customer_id' => $customer_id,
                'history_item_status_id' => HistoryItemStatus::BELUM_DIANTAR,
                'durasi' => $request->berapa_malam,
                'qty' => $qty,
                'item_price' => 0,
                'is_include_paket' => 1,
            ];

            $itemVendor = VendorItem::where('id', $request->include_id[$keys])->firstOrFail();
            $data_item_vandor_update[] = [
                'id' => $itemVendor->id,
                'stok' => $itemVendor->stok - $qty,
            ];

            if ($itemVendor->item_type == 2) {
                $campBlockSite = CampsiteBlockSite::with('block.campsite')->where('id', $request->capmsite_block_site_id)->first();
                if ($qty < $campBlockSite->capacity) {
                    $data_campsite_blok_site_update[] = [
                        'id' => $campBlockSite->id,
                        'capacity' => $campBlockSite->capacity - $qty,
                    ];
                } else {
                    return $this->errorResponse(null, 'Tenda Melebihi kapasitas yg tersedia');
                }
                $package_total_item_pembiayaan += $itemVendor->pembiayaan * $request->pax * $request->berapa_malam * $qty;
                $package_total_item_penjualan += $itemVendor->harga_jual * $request->pax * $request->berapa_malam * $qty;
            } else {
                $package_total_item_pembiayaan += $itemVendor->pembiayaan * $qty;
                $package_total_item_penjualan += $itemVendor->harga_jual * $qty;
            }
        }


        // item additional
        $additional_qty = $request->qty;
        $additional_item_id = $request->unit_id;
        $additional_durasi = $request->durasi;
        foreach ($additional_qty as $k => $v) {
            if ($v > 0) {
                $data_history_paket[] = [
                    'vendor_item_id' => $additional_item_id[$k],
                    'reservation_id' => $reservasi_id,
                    'customer_id' => $customer_id,
                    'history_item_status_id' => HistoryItemStatus::BELUM_DIANTAR,
                    'durasi' => $additional_durasi[$k],
                    'qty' => $additional_qty[$k],
                    'is_include_paket' => 0,
                    'item_price' => $request->item_price[$k]
                ];

                $itemVendor = VendorItem::where('id', $additional_item_id[$k])->firstOrFail();
                $data_item_vandor_update[] = [
                    'id' => $itemVendor->id,
                    'stok' => $itemVendor->stok - $additional_qty[$k],
                ];

                if ($itemVendor->item_type == 2) {
                    $additional_total_item += $itemVendor->harga_jual * $request->pax * $request->berapa_malam * $additional_qty[$k];
                    $additional_total_item_pembiayaan += $itemVendor->pembiayaan * $request->pax * $request->berapa_malam * $additional_qty[$k];
                } else {
                    $additional_total_item += $itemVendor->harga_jual * $additional_qty[$k];
                    $additional_total_item_pembiayaan += $itemVendor->pembiayaan * $additional_qty[$k];
                }
            }
        }

        // item tiket
        $ticket_transaction_selling_price = 0;
        $ticket_transaction_purchase_price = 0;
        $ticket_ids = $request->ticket_id;
        $ticket_selling_price = $request->ticket_selling_price;
        $ticketTransaction = [];
        
        if (count($ticket_ids) > 0) {
            foreach ($ticket_ids as $key => $tiket_id_value) {
                if (!empty($tiket_id_value)) {
                    $ticket = Ticket::where('id', $tiket_id_value)->firstOrFail();
                    $ticketTransaction[] = [
                        'transaction_pax' => $request->pax,
                        'transaction_date' => $request->checkin,
                        'transaction_selling_price' => $ticket_selling_price[$key],
                        'transaction_purchase_price' => $ticket->purchase_price,
                        'transaction_status' => 'success',
                        'customer_id' => $customer_id,
                        'reservation_id' => $reservasi_id,
                        'ticket_id' => $ticket->id,
                        'user_id' => Auth::id(),
                    ];

                    $ticket_transaction_selling_price += $ticket_selling_price[$key];
                    $ticket_transaction_purchase_price += $ticket->purchase_price;
                }                
            }
        }

        $data_customer = [
            'id' => $customer_id,
            'nama' => $request->nama,
            'telepon' => $request->telepon,
        ];

        //block site
        $block_site = $request->capmsite_block_site_id;
        foreach ($block_site as $key => $val) {
            $data_campsite_transaction[] = [
                'reservation_id' => $reservasi_id,
                'capmsite_block_site_id' => $block_site[$key],
            ];
        }

        $package = Package::where('id', $request->id_paket)->first();
        $subtotalProject = $package->harga * $request->pax;
        $nilai_project = ($subtotalProject * $request->berapa_malam) + $additional_total_item + $ticket_transaction_selling_price;
        $total_pembiayaan = $package_total_item_pembiayaan + $additional_total_item_pembiayaan + $ticket_transaction_purchase_price;
        $data_reservasi = [
            'id' => $reservasi_id,
            'customer_id' => $customer_id,
            'id_penanggung_jawab' => $request->id_penanggung_jawab,
            'checkin_date' => $request->checkin,
            'checkout_date' => HelperService::dateAddDays($request->checkin, $request->berapa_malam),
            'berapa_malam' => $request->berapa_malam,
            'note' => $request->note,
            'package_id' => $request->id_paket,
            'pax' => $request->pax,
            // 'capmsite_block_site_id' => $request->capmsite_block_site_id,
            'reservation_status_id' => $request->reservation_status_id,
            'plat_nomor' => $request->plat_nomor,
            'nilai_project' => $nilai_project,
            'total_pembiayaan' => $total_pembiayaan,
            'payment_status_id' => PaymentStatus::BELUM_LUNAS,
            'code_invoice' => $this->generateOrderCode(),
            'flag_new_additional_item' => 0,
            'read_at' => date('Y-m-d'),
            'is_tmp' => 0, // kalo 1 tmp reservasi online dan kalo 0 bukan tmp
        ];

        $debug = [
            'request' => $request->all(),
            'ticketTransaction' => $ticketTransaction,
            'data_customer' => $data_customer,
            'data_reservasi' => $data_reservasi,
            'data_history_paket' => $data_history_paket,
            'data_item_vandor_update' => $data_item_vandor_update,
            'data_campsite_blok_site_update' => $data_campsite_blok_site_update,
            'data_campsite_transaction' => $data_campsite_transaction,
        ];

        // return $this->errorResponse($debug, 'debug');

        DB::beginTransaction();
        try {
            Customer::create($data_customer);
            Reservation::create($data_reservasi);
            if (count($data_history_paket) > 0) {
                HistoryItemService::insert($data_history_paket);
            }
            if (count($data_campsite_transaction) > 0) {
                CampsiteBlockHistoryTransaction::insert($data_campsite_transaction);
            }
            if (count($data_item_vandor_update) > 0) {
                foreach ($data_item_vandor_update as $k1 => $v1) {
                    $itemVendor = VendorItem::where('id', $v1['id'])->update([
                        'stok' => $v1['stok'],
                    ]);
                }
            }
            if (count($data_campsite_blok_site_update) > 0) {
                foreach ($data_campsite_blok_site_update as $k2 => $v2) {
                    $itemVendor = CampsiteBlockSite::where('id', $v2['id'])->update([
                        'capacity' => $v2['capacity'],
                    ]);
                }
            }
            if (count($ticketTransaction) > 0) {
                TicketTransaction::insert($ticketTransaction);
            }
            DB::commit();
            return $this->successResponse($debug, 'ok');
        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorResponse($e, $e->getMessage());
        }
    }

    public function storeItemReservasi(Request $request)
    {
        // item additional
        $additional_qty = $request->qty;
        $additional_item_id = $request->unit_id;
        $additional_durasi = $request->durasi;
        $package_total_item_pembiayaan = 0;
        $additional_total_item = 0;
        $additional_total_item_pembiayaan = 0;
        $getDataReservasi = Reservation::where('id', $request->get_id_order)->first();

        foreach ($additional_qty as $k => $v) {
            if ($v > 0) {
                $data_history_paket[] = [
                    'vendor_item_id' => $additional_item_id[$k],
                    'customer_id' => $getDataReservasi->customer_id,
                    'reservation_id' => $request->get_id_order,
                    'history_item_status_id' => HistoryItemStatus::BELUM_DIANTAR,
                    'durasi' => $additional_durasi[$k],
                    'qty' => $additional_qty[$k],
                    'is_include_paket' => 0,
                    'is_new_additional' => 1,  // 1 tambahan baru, 0 / null data lama
                    'item_price' => $request->item_price[$k]
                ];

                $itemVendor = VendorItem::where('id', $additional_item_id[$k])->firstOrFail();
                $data_item_vandor_update[] = [
                    'id' => $itemVendor->id,
                    'stok' => $itemVendor->stok - $additional_qty[$k],
                ];

                if ($itemVendor->item_type == 2) {
                    $additional_total_item += $itemVendor->harga_jual * $request->pax * $request->durasi * $additional_qty[$k];
                    $additional_total_item_pembiayaan += $itemVendor->pembiayaan * $request->pax * $request->durasi * $additional_qty[$k];
                } else {
                    $additional_total_item += $itemVendor->harga_jual * $additional_qty[$k];
                    $additional_total_item_pembiayaan += $itemVendor->pembiayaan * $additional_qty[$k];
                }
            }
        }


        $nilai_project = $getDataReservasi->nilai_project + $additional_total_item;
        $total_pembiayaan = $getDataReservasi->total_pembiayaan + $additional_total_item_pembiayaan;
        $data_reservasi = [
            'nilai_project' => $nilai_project,
            'total_pembiayaan' => $total_pembiayaan,
            'payment_status_id' => PaymentStatus::BELUM_LUNAS,
            'flag_new_additional_item' => 1,
            'read_at' => null
        ];

        $debug = [
            'data_reservasi' => $data_reservasi,
            'data_history_paket' => $data_history_paket,
            'data_item_vandor_update' => $data_item_vandor_update,
        ];

        DB::beginTransaction();
        try {
            Reservation::where('id', $request->get_id_order)->update($data_reservasi);
            if (count($data_history_paket) > 0) {
                HistoryItemService::insert($data_history_paket);
            }
            if (count($data_item_vandor_update) > 0) {
                foreach ($data_item_vandor_update as $k1 => $v1) {
                    $itemVendor = VendorItem::where('id', $v1['id'])->update([
                        'stok' => $v1['stok'],
                    ]);
                }
            }
            DB::commit();
            return $this->successResponse($debug, 'ok');
        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorResponse($e, $e->getMessage());
        }
    }

    public function updateReservasi($id, Request $request)
    {
        $reservasion = Reservation::with('details.vendor_item')->where('id', $id)->firstOrFail();
        $package_total_item_pembiayaan = 0;
        $package_total_item_penjualan = 0;
        $additional_total_item = 0;
        $additional_total_item_pembiayaan = 0;
        $data_history_paket = [];
        $data_history_paket_update = [];
        $data_item_vandor_update = [];
        $data_campsite_blok_site_update = [];

        // item in package
        $package_item_id = $request->package_item_id;
        $package_item_qty = $request->package_item_qty;
        $package_item_reservation_detail_id = $request->package_item_reservation_detail_id;
        foreach ($package_item_id as $key => $values) {
            $qty = $package_item_qty[$key];
            $history_id = $package_item_reservation_detail_id[$key];

            if ($qty > 0) {
                if ($history_id == 0) {
                    // create new
                    $data_history_paket[] = [
                        'vendor_item_id' => $package_item_id[$key],
                        'reservation_id' => $reservasion->id,
                        'customer_id' => $reservasion->customer_id,
                        'history_item_status_id' => HistoryItemStatus::BELUM_DIANTAR,
                        'durasi' => $request->berapa_malam,
                        'qty' => $qty,
                        'item_price' => 0,
                        'is_include_paket' => 1,
                    ];

                    $itemVendor = VendorItem::where('id', $request->package_item_id[$key])->firstOrFail();
                    $data_item_vandor_update[] = [
                        'id' => $itemVendor->id,
                        'stok' => $itemVendor->stok - $qty,
                    ];

                    if ($itemVendor->item_type == 2) {
                        $package_total_item_pembiayaan += $itemVendor->pembiayaan * $request->pax * $request->berapa_malam * $qty;
                        $package_total_item_penjualan += $itemVendor->harga_jual * $request->pax * $request->berapa_malam * $qty;
                    } else {
                        $package_total_item_pembiayaan += $itemVendor->pembiayaan * $qty;
                        $package_total_item_penjualan += $itemVendor->harga_jual * $qty;
                    }
                } else {
                    // update
                    $data_history_paket_update[] = [
                        'durasi' => $request->berapa_malam,
                        'qty' => $qty,
                        'id' => $history_id,
                    ];

                    $historyItemService = HistoryItemService::where('id', $history_id)->firstOrFail();
                    $itemVendor = VendorItem::where('id', $request->package_item_id[$key])->firstOrFail();
                    $data_item_vandor_update[] = [
                        'id' => $itemVendor->id,
                        'stok' => ($itemVendor->stok + $historyItemService->qty) - $qty,
                    ];

                    if ($itemVendor->item_type == 2) {
                        $package_total_item_pembiayaan += $itemVendor->pembiayaan * $request->pax * $request->berapa_malam * $qty;
                        $package_total_item_penjualan += $itemVendor->harga_jual * $request->pax * $request->berapa_malam * $qty;
                    } else {
                        $package_total_item_pembiayaan += $itemVendor->pembiayaan * $qty;
                        $package_total_item_penjualan += $itemVendor->harga_jual * $qty;
                    }
                }
            }
        }

        // item additional
        $additional_qty = $request->additional_qty;
        $additional_item_id = $request->additional_item_id;
        $additional_durasi = $request->additional_durasi;
        $additional_reservation_detail_id = $request->additional_reservation_detail_id;
        foreach ($additional_qty as $k => $v) {
            if ($v > 0) {
                $history_id = $additional_reservation_detail_id[$k];
                if ($history_id == 0) {
                    $itemVendor = VendorItem::where('id', $additional_item_id[$k])->firstOrFail();

                    $data_history_paket[] = [
                        'vendor_item_id' => $additional_item_id[$k],
                        'reservation_id' => $reservasion->id,
                        'customer_id' => $reservasion->customer_id,
                        'history_item_status_id' => HistoryItemStatus::BELUM_DIANTAR,
                        'durasi' => $additional_durasi[$k],
                        'qty' => $additional_qty[$k],
                        'is_include_paket' => 0,
                        'item_price' => $itemVendor->harga_jual
                    ];

                    $data_item_vandor_update[] = [
                        'id' => $itemVendor->id,
                        'stok' => $itemVendor->stok - $additional_qty[$k],
                    ];

                    if ($itemVendor->item_type == 2) {
                        $additional_total_item += $itemVendor->harga_jual * $request->pax * $request->berapa_malam * $additional_qty[$k];
                        $additional_total_item_pembiayaan += $itemVendor->pembiayaan * $request->pax * $request->berapa_malam * $additional_qty[$k];
                    } else {
                        $additional_total_item += $itemVendor->harga_jual * $additional_qty[$k];
                        $additional_total_item_pembiayaan += $itemVendor->pembiayaan * $additional_qty[$k];
                    }
                } else {
                    $itemVendor = VendorItem::where('id', $additional_item_id[$k])->firstOrFail();
                    $historyItemService = HistoryItemService::where('id', $history_id)->firstOrFail();

                    $data_history_paket_update[] = [
                        'durasi' => $additional_durasi[$k],
                        'qty' => $additional_qty[$k],
                        'id' => $history_id,
                    ];

                    $data_item_vandor_update[] = [
                        'id' => $itemVendor->id,
                        'stok' => ($itemVendor->stok + $historyItemService->qty) - $additional_qty[$k],
                    ];

                    if ($itemVendor->item_type == 2) {
                        $additional_total_item += $itemVendor->harga_jual * $request->pax * $request->berapa_malam * $additional_qty[$k];
                        $additional_total_item_pembiayaan += $itemVendor->pembiayaan * $request->pax * $request->berapa_malam * $additional_qty[$k];
                    } else {
                        $additional_total_item += $itemVendor->harga_jual * $additional_qty[$k];
                        $additional_total_item_pembiayaan += $itemVendor->pembiayaan * $additional_qty[$k];
                    }
                }
            }
        }

        $return_item_qty = [];
        foreach ($reservasion->details as $detail) {
            $return_item_qty[] = [
                'id' => $detail->vendor_item_id,
                'stok' => $detail->qty + $detail->vendor_item->stok,
            ];
        }

        // item tiket
        $ticket_transaction_selling_price = 0;
        $ticket_transaction_purchase_price = 0;
        $ticket_ids = $request->ticket_id;
        $ticket_transaction_id = $request->ticket_transaction_id;
        $ticket_selling_price = $request->ticket_selling_price;
        $ticketTransaction_insert = [];
        $ticketTransaction_update = [];
        if ($ticket_ids != null) {
            foreach ($ticket_ids as $key => $tiket_id_value) {
                $ticket = Ticket::where('id', $tiket_id_value)->firstOrFail();
                if ($ticket_transaction_id[$key] == 0) {
                    $ticketTransaction_insert[] = [
                        'transaction_pax' => $request->pax,
                        'transaction_date' => $request->checkin,
                        'transaction_selling_price' => $ticket_selling_price[$key],
                        'transaction_purchase_price' => $ticket->purchase_price,
                        'transaction_status' => 'success',
                        'customer_id' => $reservasion->customer_id,
                        'reservation_id' => $reservasion->id,
                        'ticket_id' => $ticket->id,
                        'user_id' => Auth::id(),
                    ];
                } else {
                    $ticketTransaction_update[$ticket_transaction_id[$key]] = [
                        'transaction_pax' => $request->pax,
                        'transaction_date' => $request->checkin,
                        'transaction_selling_price' => $ticket_selling_price[$key],
                        'transaction_purchase_price' => $ticket->purchase_price,
                        'transaction_status' => 'success',
                        'customer_id' => $reservasion->customer_id,
                        'reservation_id' => $reservasion->id,
                        'ticket_id' => $ticket->id,
                        'user_id' => Auth::id(),
                    ];
                }
                $ticket_transaction_selling_price += $ticket_selling_price[$key];
                $ticket_transaction_purchase_price += $ticket->purchase_price;
            }
        }

        $data_customer = [
            'nama' => $request->nama,
            'telepon' => $request->telepon,
        ];

        $package = Package::where('id', $request->id_paket)->first();
        $subtotalProject = $package->harga * $request->pax;
        $nilai_project = ($subtotalProject * $request->berapa_malam) + $additional_total_item + $ticket_transaction_selling_price;
        $total_pembiayaan = $package_total_item_pembiayaan + $additional_total_item_pembiayaan + $ticket_transaction_purchase_price;
        $data_reservasi = [
            'id_penanggung_jawab' => $request->id_penanggung_jawab,
            'checkin_date' => $request->checkin,
            'checkout_date' => HelperService::dateAddDays($request->checkin, $request->berapa_malam),
            'berapa_malam' => $request->berapa_malam,
            'note' => $request->note,
            'package_id' => $request->id_paket,
            'pax' => $request->pax,
            'capmsite_block_site_id' => $request->capmsite_block_site_id,
            'reservation_status_id' => $request->reservation_status_id,
            'plat_nomor' => $request->plat_nomor,
            'nilai_project' => $nilai_project,
            'total_pembiayaan' => $total_pembiayaan,
        ];

        $debug = [
            'ticketTransaction_update' => $ticketTransaction_update,
            'ticketTransaction_insert' => $ticketTransaction_insert,
            'data_customer' => $data_customer,
            'data_reservasi' => $data_reservasi,
            'data_history_paket' => $data_history_paket,
            'return_item_qty' => $return_item_qty,
            'data_history_paket_update' => $data_history_paket_update,
            'data_item_vandor_update' => $data_item_vandor_update,
            'data_campsite_blok_site_update' => $data_campsite_blok_site_update,
            'reservasion' => $reservasion,
            'request' => $request->all(),
            'check' => ($reservasion->package_id != $request->id_paket),
            'harga_paket' => $package->harga,
            'pax' => $request->pax,
            'additional' => $additional_total_item,
            'package_total_item_pembiayaan' => $package_total_item_pembiayaan,
            'additional_total_item_pembiayaan' => $additional_total_item_pembiayaan,
        ];

        // return $this->errorResponse($debug, 'debug');

        DB::beginTransaction();
        try {
            if ($reservasion->package_id != $request->id_paket) {
                HistoryItemService::where('reservation_id', $reservasion->id)->delete();
                if (count($return_item_qty) > 0) {
                    foreach ($return_item_qty as $k4 => $v4) {
                        VendorItem::where('id', $v4['id'])->update([
                            'stok' => $v4['stok'],
                        ]);
                    }
                }
                if (count($data_history_paket) > 0) {
                    HistoryItemService::insert($data_history_paket);
                }
            } else {
                if (count($data_history_paket_update) > 0) {
                    foreach ($data_history_paket_update as $k3 => $v3) {
                        HistoryItemService::where('id', $v3['id'])->update([
                            'qty' => $v3['qty'],
                            'durasi' => $v3['durasi'],
                        ]);
                    }
                }
            }

            if (count($data_item_vandor_update) > 0) {
                foreach ($data_item_vandor_update as $k1 => $v1) {
                    VendorItem::where('id', $v1['id'])->update([
                        'stok' => $v1['stok'],
                    ]);
                }
            }
            if (count($data_campsite_blok_site_update) > 0) {
                foreach ($data_campsite_blok_site_update as $k2 => $v2) {
                    CampsiteBlockSite::where('id', $v2['id'])->update([
                        'capacity' => $v2['capacity'],
                    ]);
                }
            }

            if (count($ticketTransaction_update) > 0) {
                foreach ($ticketTransaction_update as $k2 => $v2) {
                    TicketTransaction::where('id', $v2)->update($v2);
                }
            }

            if (count($ticketTransaction_insert) > 0) {
                TicketTransaction::insert($ticketTransaction_insert);
            }

            Customer::where('id', $reservasion->customer_id)->update($data_customer);
            $reservasion->update($data_reservasi);

            DB::commit();
            return $this->successResponse($debug, 'ok');
        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorResponse($e, $e->getMessage());
        }
    }

    public function storeReservasiOnline(Request $request)
    {
        $getCampSite = CampsiteBlockSite::where('id', $request->id_campsite)->with('block')->first();
        $getIncludePaket = Package::where('id', $request->id_paket)->with('items.vendor_item')->first();
        $customer_id = QueryService::getLastId('customers');
        $reservasi_id = QueryService::getLastId('reservations');
        $package_total_item_pembiayaan = 0;
        $additional_total_item = 0;
        $additional_total_item_pembiayaan = 0;

        // item in package
        $data_history_paket = [];
        $data_item_vandor_update = [];
        $data_campsite_blok_site_update = [];
        foreach ($getIncludePaket->items as $keys => $values) {
            $data_history_paket[] = [
                'vendor_item_id' => $values->vendor_item_id,
                'reservation_id' => $reservasi_id,
                'customer_id' => $customer_id,
                'history_item_status_id' => HistoryItemStatus::BELUM_DIANTAR,
                'durasi' => $request->berapa_malam,
                'qty' => 0,
                'item_price' => 0,
                'is_include_paket' => 1,
            ];

            $itemVendor = VendorItem::where('id', $values->vendor_item_id)->firstOrFail();
            $data_item_vandor_update[] = [
                'id' => $itemVendor->id,
                'stok' => $itemVendor->stok - 0,
            ];

            if ($itemVendor->item_type == 2) {
                $campBlockSite = CampsiteBlockSite::with('block.campsite')->where('id', $getCampSite->id)->first();
                $data_campsite_blok_site_update[] = [
                    'id' => $campBlockSite->id,
                    'capacity' => $campBlockSite->capacity - 0,
                ];
                $package_total_item_pembiayaan += $itemVendor->pembiayaan * $request->pax * $request->berapa_malam;
            } else {
                $package_total_item_pembiayaan += $itemVendor->pembiayaan;
            }
        }

        $data_customer = [
            'id' => $customer_id,
            'nama' => $request->name,
            'telepon' => $request->telepon,
        ];

        $package = Package::where('id', $request->id_paket)->first();
        $subtotalProject = $package->harga * $request->pax;
        $nilai_project = $package->harga * $request->pax * $request->berapa_malam + $additional_total_item;
        $total_pembiayaan = $package_total_item_pembiayaan + $additional_total_item_pembiayaan;
        $data_reservasi = [
            'id' => $reservasi_id,
            'customer_id' => $customer_id,
            'id_penanggung_jawab' => 1,
            'checkin_date' => $request->checkin,
            'checkout_date' => HelperService::dateAddDays($request->checkin, $request->berapa_malam),
            'berapa_malam' => $request->berapa_malam,
            'note' => '-',
            'package_id' => $request->id_paket,
            'pax' => $request->pax,
            'capmsite_block_site_id' => $getCampSite->id,
            'reservation_status_id' => 5,
            'plat_nomor' => '-',
            'nilai_project' => $nilai_project,
            'total_pembiayaan' => 0,
            'payment_status_id' => PaymentStatus::BELUM_LUNAS,
            'code_invoice' => $this->generateOrderCode(),
            'flag_new_additional_item' => 0,
            'read_at' => null,
            'is_tmp' => 1, // kalo 1 tmp reservasi online dan kalo 0 bukan tmp
        ];

        $data_campsite_transaction = [
            'reservation_id' => $reservasi_id,
            'capmsite_block_site_id' => $getCampSite->id,
        ];

        $debug = [
            'data_customer' => $data_customer,
            'data_reservasi' => $data_reservasi,
            'data_history_paket' => $data_history_paket,
            'data_item_vandor_update' => $data_item_vandor_update,
            'data_campsite_blok_site_update' => $data_campsite_blok_site_update,
            'harga_satuan' => $package->harga,
            'pax' => $request->pax,
            'berapa_malam' => $request->berapa_malam,
            'satuan_*_pax' => $package->harga * $request->pax,
            'nilai_project' => $package->harga * $request->pax * $request->berapa_malam,
            'data_paket' => Package::where('id', $request->id_paket)->first(),
            'get_send_message' => GeneralSetting::where('name', 'whatsapp_send')->first(),
            'data_campsite_transaction' => $data_campsite_transaction,
            'data_campsite_cust' => CampsiteBlockSite::with('block')->where('id', $getCampSite->id)->first()
        ];

        // return $this->successResponse($debug, 'debug');

        DB::beginTransaction();
        try {
            Customer::create($data_customer);
            $reservasi = Reservation::create($data_reservasi);
            $saveCampsite = CampsiteBlockHistoryTransaction::create($data_campsite_transaction);
            if (count($data_history_paket) > 0) {
                HistoryItemService::insert($data_history_paket);
            }
            if (count($data_item_vandor_update) > 0) {
                foreach ($data_item_vandor_update as $k1 => $v1) {
                    $itemVendor = VendorItem::where('id', $v1['id'])->update([
                        'stok' => $v1['stok'],
                    ]);
                }
            }
            if (count($data_campsite_blok_site_update) > 0) {
                foreach ($data_campsite_blok_site_update as $k2 => $v2) {
                    $itemVendor = CampsiteBlockSite::where('id', $v2['id'])->update([
                        'capacity' => $v2['capacity'],
                    ]);
                }
            }
            DB::commit();
            $debug['generateWaLink'] = $this->generateWaLink($reservasi->id);
            return $this->successResponse($debug, 'ok');
        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorResponse($e, $e->getMessage());
        }
    }

    public function generateWaLink($id)
    {
        $reservasi = Reservation::with(['customer', 'capmsite_block_site.block', 'package'])->where('id', $id)->first();
        $get_phone = GeneralSetting::where('name', 'admin_number_ponsel')->first();
        $link = 'https://web.whatsapp.com/send?phone=' . $get_phone->value . '&text=';
        $waSetting = GeneralSetting::where('name', 'whatsapp_send')->first();
        $text = strip_tags($waSetting->value);

        $nama_user = $reservasi->customer->nama . '%0A';
        $nomor_user = $reservasi->customer->telepon . '%0A';
        $nama_paket = $reservasi->package->nama_paket . '%0A';
        $checkout_date = $reservasi->checkout_date . '%0A';
        $checkin_date = $reservasi->checkin_date . '%0A';
        $pax = $reservasi->pax . '%0A';
        $durasi = $reservasi->berapa_malam . '%0A';
        $block = $reservasi->capmsite_block_site->block->name . '%0A';
        $block_site = $reservasi->capmsite_block_site->name . '%0A';

        $search  = array(
            '[{nama_user}]',
            '[{nomor_whatsapp}]',
            '[{paket}]',
            '[{get_checkin}]',
            '[{get_checkout}]',
            '[{get_pax}]',
            '[{get_durasi}]',
            '[{block}]',
            '[{blocksite}]',
        );
        $replace = array(
            $nama_user,
            $nomor_user,
            $nama_paket,
            $checkout_date,
            $checkin_date,
            $pax,
            $durasi,
            $block,
            $block_site,
        );

        $text_link_desc = str_replace($search, $replace, $text);

        return $link . $text_link_desc;
    }

    public function sendNotification($id, Request $request)
    {
        $getDataOrder = Reservation::with(['customer', 'capmsite_block_site.block', 'package'])->where('id', $id)->first();
        $get_message = WhatsappTemplate::where('id', $request->notification_send)->first();
        $link = 'https://web.whatsapp.com/send?phone=' . $getDataOrder->customer->telepon . '&text=';
        $text = strip_tags($get_message->desc);

        $nama_user = $getDataOrder->customer->nama . '%0A';
        $nomor_user = $getDataOrder->customer->telepon . '%0A';
        $nama_paket = $getDataOrder->package->nama_paket . '%0A';
        $checkout_date = $getDataOrder->checkout_date . '%0A';
        $checkin_date = $getDataOrder->checkin_date . '%0A';
        $pax = $getDataOrder->pax . '%0A';
        $durasi = $getDataOrder->berapa_malam . '%0A';

        $search  = array(
            '[{nama_user}]',
            '[{nomor_whatsapp}]',
            '[{paket}]',
            '[{get_checkin}]',
            '[{get_checkout}]',
            '[{get_pax}]',
            '[{get_durasi}]',
        );
        $replace = array(
            $nama_user,
            $nomor_user,
            $nama_paket,
            $checkout_date,
            $checkin_date,
            $pax,
            $durasi,
        );

        $text_link_desc = str_replace($search, $replace, $text);

        $debug = [
            'data_order' => $getDataOrder,
            'data_message' => $get_message,
            'get_link' => $link,
            'full_link' => $link . $text_link_desc,
        ];
        return $this->successResponse($debug, 'ok');
    }

    public function datatablesPayment($id)
    {
        $data = Payment::where('reservation_id', $id)->orderBy('id', 'Desc')->get();
        return $this->successResponse($data, 'Data Berhasil Ditemukan');
    }

    public function payment($id, Request $request)
    {
        $filename = null;
        if (!empty($request->image)) {
            if ($image = $request->image) {
                $name = $image->getClientOriginalName();
                $img = Image::make($image);
                $img->stream(); // <-- Key point
                Storage::disk('local')->put('public/request_order_image/' . $name, $img);
                $filename = $name;
            }
        } else {
            $filename = null;
        }

        $getDataReservasi = ReservationService::summary($id);
        $sisa = $getDataReservasi->reservation_total_sisa;

        // jika lebih bayar
        if ($request->payment_total > $sisa) {
            return redirect()->back()->with('error', 'Jumlah Pembarayan Melebihi Sisa Pembayaran, Silahkan Periksa Kembali Nominal yang di Input. ');
        }

        // jika bayar pas
        if ($request->payment_total == $sisa) {
            Reservation::where('id', $id)->update([
                'payment_status_id' => PaymentStatus::LUNAS,
            ]);
        }

        Payment::create([
            'reservation_id' => $id,
            'customer_id' => $getDataReservasi->customer_id,
            'payment_date' => date('Y-m-d H:i:s'),
            'payment_metod' => 'bank-transfer',
            'payment_total' => $request->payment_total,
            'payment_description' => $request->payment_description,
            'image' => $filename,
        ]);
        return redirect()->back()->with('success', 'Data Berhasil Disimpan');
    }

    public function addItem(Request $request)
    {

        $getDataReservasi = Reservation::where('id', $request->id_reservasi)->first();
        $findItemPaket = Reservation::where('id', $request->id_reservasi)->first();
        $updatePayment = Payment::where('reservation_id', $request->id_reservasi)->orderBy('id', 'desc')->first();
        $get_qty = $request->qty;
        $get_paket = $request->unit_id;
        $get_durasi = $request->durasi;
        $getProduk = VendorItem::whereIn('id', $get_paket)->get();

        foreach ($get_paket as $key => $value) {
            HistoryItemService::create([
                'customer_id' => $findItemPaket->customer_id,
                'reservation_id' => $findItemPaket->id,
                'history_item_status_id' => 1,
                'qty' => $get_qty[$key],
                'vendor_item_id' => $get_paket[$key],
                'durasi' => $get_durasi[$key],
                // 'item_price' => $request->item_price[$key],
                // 'tgl_penggunaan' => '-',
                // 'tgl_pengembalian' => '-',
                'is_include_paket' => 0,
            ]);
        }
        $sum = 0;
        foreach ($getProduk as $keys => $values) {
            $sum += $values->harga_jual * $get_qty[$keys];
        }
        $findItemPaket->update([
            // 'status_payment' => 'Belum Lunas',
            'nilai_project' => $getDataReservasi->nilai_project + $sum,
        ]);

        // Payment::create([
        //     'customer_id' => $updatePayment->id_customer,
        //     'reservation_id' => $request->id_reservasi,
        //     'payment_total' => $updatePayment->total_pembayaran + $sum,
        //     'payment_description' => 'Tambahan',
        //     // 'sisa_pembayaran' => $sum + $updatePayment->sisa_pembayaran,
        //     // 'dp' => 0,
        //     // 'status_payment' => '1'
        // ]);
    }

    public function deleteItem($id)
    {
        $deleteItem = HistoryItemService::where('id', $id)->delete();
        return $this->successResponse($deleteItem, 'Data Berhasil Dihapus');
    }

    public function changeStatusCheckin($id)
    {
        $findReservasi = Reservation::where('id', $id)->first();
        $findReservasi->update([
            'reservation_status_id' => 1,
        ]);

        return $this->successResponse($findReservasi, 'Status Berhasil Diubah');
    }

    public function changeStatusCheckout($id)
    {
        $findReservasi = Reservation::where('id', $id)->first();
        if ($findReservasi->payment_status_id == 1) {
            return $this->errorResponse(null, 'Mohon Untuk Melunasi Terlebih dahulu Pembayaran yang tersisa.');
        } else {
            $findReservasi->update([
                'reservation_status_id' => 2,
            ]);
            return $this->successResponse($findReservasi, 'Status Berhasil Diubah');
        }
    }
}
