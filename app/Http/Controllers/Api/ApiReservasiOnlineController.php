<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Models\CampsiteBlockSite;
use App\Models\Customer;
use App\Models\HistoryItemService;
use App\Models\HistoryItemStatus;
use App\Models\Package;
use App\Models\Reservation;
use App\Models\VendorItem;
use App\Service\HelperService;
use Illuminate\Http\Request;
use DB;

class ApiReservasiOnlineController extends ApiController
{
    public function index(Request $request)
    {
        $keyword = $request->search;

        $query = Reservation::where('is_tmp', 1)
            ->with(['customer', 'package', 'payment_status', 'status'])
            ->where(function ($q) use ($keyword) {
            });

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function updateReservasi($id, Request $request)
    {
        $reservasion = Reservation::with('details.vendor_item')->where('id', $id)->firstOrFail();
        $package_total_item_pembiayaan = 0;
        $package_total_item_penjualan = 0;
        $additional_total_item = 0;
        $additional_total_item_pembiayaan = 0;
        $data_history_paket = [];
        $data_history_paket_update = [];
        $data_item_vandor_update = [];
        $data_campsite_blok_site_update = [];

        // item in package
        $package_item_id = $request->package_item_id;
        $package_item_qty = $request->package_item_qty;
        $package_item_reservation_detail_id = $request->package_item_reservation_detail_id;
        foreach ($package_item_id as $key => $values) {
            $qty = $package_item_qty[$key];
            $history_id = $package_item_reservation_detail_id[$key];

            if ($qty > 0) {
                if ($history_id == 0) {
                    // create new
                    $data_history_paket[] = [
                        'vendor_item_id' => $package_item_id[$key],
                        'reservation_id' => $reservasion->id,
                        'customer_id' => $reservasion->customer_id,
                        'history_item_status_id' => HistoryItemStatus::BELUM_DIANTAR,
                        'durasi' => $request->berapa_malam,
                        'qty' => $qty,
                        'item_price' => 0,
                        'is_include_paket' => 1,
                    ];

                    $itemVendor = VendorItem::where('id', $request->package_item_id[$key])->firstOrFail();
                    $data_item_vandor_update[] = [
                        'id' => $itemVendor->id,
                        'stok' => $itemVendor->stok - $qty,
                    ];

                    if ($itemVendor->item_type == 2) {
                        $package_total_item_pembiayaan += $itemVendor->pembiayaan * $request->pax * $request->berapa_malam * $qty;
                        $package_total_item_penjualan += $itemVendor->harga_jual * $request->pax * $request->berapa_malam * $qty;
                    } else {
                        $package_total_item_pembiayaan += $itemVendor->pembiayaan * $qty;
                        $package_total_item_penjualan += $itemVendor->harga_jual * $qty;
                    }
                } else {
                    // update
                    $data_history_paket_update[] = [
                        'durasi' => $request->berapa_malam,
                        'qty' => $qty,
                        'id' => $history_id,
                    ];

                    $historyItemService = HistoryItemService::where('id', $history_id)->firstOrFail();
                    $itemVendor = VendorItem::where('id', $request->package_item_id[$key])->firstOrFail();
                    $data_item_vandor_update[] = [
                        'id' => $itemVendor->id,
                        'stok' => ($itemVendor->stok + $historyItemService->qty) - $qty,
                    ];

                    if ($itemVendor->item_type == 2) {
                        $package_total_item_pembiayaan += $itemVendor->pembiayaan * $request->pax * $request->berapa_malam * $qty;
                        $package_total_item_penjualan += $itemVendor->harga_jual * $request->pax * $request->berapa_malam * $qty;
                    } else {
                        $package_total_item_pembiayaan += $itemVendor->pembiayaan * $qty;
                        $package_total_item_penjualan += $itemVendor->harga_jual * $qty;
                    }
                }
            }
        }

        // item additional
        $additional_qty = $request->additional_qty;
        $additional_item_id = $request->additional_item_id;
        $additional_durasi = $request->additional_durasi;
        $additional_reservation_detail_id = $request->additional_reservation_detail_id;
        foreach ($additional_qty as $k => $v) {
            if ($v > 0) {
                $history_id = $additional_reservation_detail_id[$k];
                if ($history_id == 0) {
                    $itemVendor = VendorItem::where('id', $additional_item_id[$k])->firstOrFail();

                    $data_history_paket[] = [
                        'vendor_item_id' => $additional_item_id[$k],
                        'reservation_id' => $reservasion->id,
                        'customer_id' => $reservasion->customer_id,
                        'history_item_status_id' => HistoryItemStatus::BELUM_DIANTAR,
                        'durasi' => $additional_durasi[$k],
                        'qty' => $additional_qty[$k],
                        'is_include_paket' => 0,
                        'item_price' => $itemVendor->harga_jual
                    ];

                    $data_item_vandor_update[] = [
                        'id' => $itemVendor->id,
                        'stok' => $itemVendor->stok - $additional_qty[$k],
                    ];

                    if ($itemVendor->item_type == 2) {
                        $additional_total_item += $itemVendor->harga_jual * $request->pax * $request->berapa_malam * $additional_qty[$k];
                        $additional_total_item_pembiayaan += $itemVendor->pembiayaan * $request->pax * $request->berapa_malam * $additional_qty[$k];
                    } else {
                        $additional_total_item += $itemVendor->harga_jual * $additional_qty[$k];
                        $additional_total_item_pembiayaan += $itemVendor->pembiayaan * $additional_qty[$k];
                    }
                } else {
                    $itemVendor = VendorItem::where('id', $additional_item_id[$k])->firstOrFail();
                    $historyItemService = HistoryItemService::where('id', $history_id)->firstOrFail();

                    $data_history_paket_update[] = [
                        'durasi' => $additional_durasi[$k],
                        'qty' => $additional_qty[$k],
                        'id' => $history_id,
                    ];

                    $data_item_vandor_update[] = [
                        'id' => $itemVendor->id,
                        'stok' => ($itemVendor->stok + $historyItemService->qty) - $additional_qty[$k],
                    ];

                    if ($itemVendor->item_type == 2) {
                        $additional_total_item += $itemVendor->harga_jual * $request->pax * $request->berapa_malam * $additional_qty[$k];
                        $additional_total_item_pembiayaan += $itemVendor->pembiayaan * $request->pax * $request->berapa_malam * $additional_qty[$k];
                    } else {
                        $additional_total_item += $itemVendor->harga_jual * $additional_qty[$k];
                        $additional_total_item_pembiayaan += $itemVendor->pembiayaan * $additional_qty[$k];
                    }
                }
            }
        }

        $return_item_qty = [];
        foreach ($reservasion->details as $detail) {
            $return_item_qty[] = [
                'id' => $detail->vendor_item_id,
                'stok' => $detail->qty + $detail->vendor_item->stok,
            ];
        }

        $data_customer = [
            'nama' => $request->nama,
            'telepon' => $request->telepon,
        ];

        $package = Package::where('id', $request->id_paket)->first();
        $subtotalProject = $package->harga * $request->pax;
        // $nilai_project = ($subtotalProject * $request->berapa_malam) + $additional_total_item + $package_total_item_penjualan;
        $nilai_project = $package->harga * $request->pax * $request->berapa_malam + $additional_total_item;
        $total_pembiayaan = $package_total_item_pembiayaan + $additional_total_item_pembiayaan;
        $data_reservasi = [
            'id_penanggung_jawab' => $request->id_penanggung_jawab,
            'checkin_date' => $request->checkin,
            'checkout_date' => HelperService::dateAddDays($request->checkin, $request->berapa_malam),
            'berapa_malam' => $request->berapa_malam,
            'note' => $request->note,
            'package_id' => $request->id_paket,
            'pax' => $request->pax,
            'capmsite_block_site_id' => $request->capmsite_block_site_id,
            'reservation_status_id' => $request->reservation_status_id,
            'plat_nomor' => $request->plat_nomor,
            'nilai_project' => $nilai_project,
            'total_pembiayaan' => $total_pembiayaan,
            'is_tmp' => 0,
        ];

        $debug = [
            'data_customer' => $data_customer,
            'data_reservasi' => $data_reservasi,
            'data_history_paket' => $data_history_paket,
            'return_item_qty' => $return_item_qty,
            'data_history_paket_update' => $data_history_paket_update,
            'data_item_vandor_update' => $data_item_vandor_update,
            'data_campsite_blok_site_update' => $data_campsite_blok_site_update,
            'reservasion' => $reservasion,
            'request' => $request->all(),
            'check' => ($reservasion->package_id != $request->id_paket),
            'harga_paket' => $package->harga,
            'pax' => $request->pax,
            'additional' => $additional_total_item,
            'package_total_item_pembiayaan' => $package_total_item_pembiayaan,
            'additional_total_item_pembiayaan' => $additional_total_item_pembiayaan,
        ];

        // return $this->errorResponse($debug, 'debug');

        DB::beginTransaction();
        try {
            if ($reservasion->package_id != $request->id_paket) {
                HistoryItemService::where('reservation_id', $reservasion->id)->delete();
                if (count($return_item_qty) > 0) {
                    foreach ($return_item_qty as $k4 => $v4) {
                        VendorItem::where('id', $v4['id'])->update([
                            'stok' => $v4['stok'],
                        ]);
                    }
                }
                if (count($data_history_paket) > 0) {
                    HistoryItemService::insert($data_history_paket);
                }
            } else {
                if (count($data_history_paket_update) > 0) {
                    foreach ($data_history_paket_update as $k3 => $v3) {
                        HistoryItemService::where('id', $v3['id'])->update([
                            'qty' => $v3['qty'],
                            'durasi' => $v3['durasi'],
                        ]);
                    }
                }
            }

            if (count($data_item_vandor_update) > 0) {
                foreach ($data_item_vandor_update as $k1 => $v1) {
                    VendorItem::where('id', $v1['id'])->update([
                        'stok' => $v1['stok'],
                    ]);
                }
            }

            if (count($data_campsite_blok_site_update) > 0) {
                foreach ($data_campsite_blok_site_update as $k2 => $v2) {
                    CampsiteBlockSite::where('id', $v2['id'])->update([
                        'capacity' => $v2['capacity'],
                    ]);
                }
            }

            Customer::where('id', $reservasion->customer_id)->update($data_customer);
            $reservasion->update($data_reservasi);

            DB::commit();
            return $this->successResponse($debug, 'ok');
        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorResponse($e, $e->getMessage());
        }
    }
}
