<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\BussinessRequest;
use App\Http\Requests\TicketRequest;
use Illuminate\Http\Request;
use App\Models\Bussiness;
use App\Models\Customer;
use App\Models\Reservation;
use App\Models\Ticket;
use App\Models\TicketTransaction;
use App\Service\QueryService;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ApiTicketController extends ApiController
{
    public function index(Request $request)
    {
        $keyword = $request->search;

        $query = Ticket::where(function ($q) use ($keyword) {
            if (!empty($keyword)) {
                $q->where('name', 'like', '%' . $keyword . '%');
            }
        });

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function all()
    {
        $tickets = Ticket::all();

        return $this->successResponse($tickets, 'ok');
    }

    public function store(TicketRequest $request)
    {
        $resp = Ticket::create([
            'name' => $request->name,
            'vendor_id' => $request->vendor_id,
            'max_pax' => $request->max_pax,
            'code' => md5($request->name . '-' . $request->location),
            'location' => $request->location,
            'opening_hours' => $request->opening_hours,
            'closing_hours' => $request->closing_hours,
            'description' => $request->description,
            'selling_price' => $request->selling_price,
            'purchase_price' => $request->purchase_price,
        ]);

        return $this->successResponse($resp, 'ok');
    }

    public function show($id)
    {
        $resp = Ticket::where('id', $id)->firstOrFail();

        return $this->successResponse($resp, 'ok');
    }

    public function update(TicketRequest $request, $id)
    {
        $row = Ticket::where('id', $id)->firstOrFail();

        $row->update([
            'name' => $request->name,
            'vendor_id' => $request->vendor_id,
            'max_pax' => $request->max_pax,
            'code' => md5($request->name . '-' . $request->location),
            'location' => $request->location,
            'opening_hours' => $request->opening_hours,
            'closing_hours' => $request->closing_hours,
            'description' => $request->description,
            'selling_price' => $request->selling_price,
            'purchase_price' => $request->purchase_price,
        ]);

        return $this->successResponse($row, 'ok');
    }

    public function destroy($id)
    {
        $resp = Ticket::where('id', $id)->firstOrFail()->delete();

        return $this->successResponse($resp, 'ok');
    }

    public function indexTicketTransantion(Request $request)
    {
        $keyword = $request->search;

        $query = TicketTransaction::with('ticket', 'reservation')->where(function ($q) use ($keyword) {
        });

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function storeTicketTransantion(Request $request)
    {
        $ticket = Ticket::findOrFail($request->ticket_id);
        $customer_id = QueryService::getLastId('customers');
        $reservation_id = null;

        if (!empty($request->reservation_id)) {
            $reservation = Reservation::findOrFail($request->reservation_id);
            $customer_id = $reservation->customer_id;
            $reservation_id = $reservation->id;
        }

        $data_customer = [
            'id' => $customer_id,
            'nama' => $request->customer_name,
            'telepon' => $request->customer_phone,
        ];

        $ticketTransaction = [
            'transaction_pax' => $request->transaction_pax,
            'transaction_date' => $request->transaction_date,
            'transaction_selling_price' => $ticket->selling_price,
            'transaction_purchase_price' => $ticket->purchase_price,
            'transaction_status' => 'success',
            'customer_id' => $customer_id,
            'reservation_id' => $reservation_id,
            'ticket_id' => $request->ticket_id,
            'user_id' => Auth::id(),
        ];

        $debug = [
            'data_customer' => $data_customer,
            'ticketTransaction' => $ticketTransaction,
        ];

        DB::beginTransaction();
        try {
            if ($reservation_id == null) {
                Customer::create($data_customer);
            }
            TicketTransaction::create($ticketTransaction);
            DB::commit();
            return $this->successResponse($debug, 'ok');
        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorResponse($e, $e->getMessage());
        }
    }

    public function destroyTicketTransantion($id)
    {
        $resp = TicketTransaction::where('id', $id)->firstOrFail()->delete();

        return $this->successResponse($resp, 'ok');
    }
}
