<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use App\Http\Requests\UserRequest;
use App\Models\User;
use App\Models\UserBussiness;
use App\Service\Role\AddUserRoleService;
use App\Service\Role\UpdateUserRoleService;
use App\Service\User\UserCreateService;
use App\Service\User\UserDeleteService;
use App\Service\User\UserUpdateService;
use Illuminate\Http\Request;
use Auth;
use Illuminate\Support\Facades\DB;

class ApiUserController extends ApiController
{
    public function index(Request $request)
    {
        $keyword = $request->search;

        $query = User::with(['roles', 'bussiness_users'])
            ->where(function ($q) use ($keyword) {
                if (!empty($keyword)) {
                    $q->where(function ($q2) use ($keyword) {
                        $q2->where('name', 'like', '%' . $keyword . '%')
                            ->orWhere('email', 'like', '%' . $keyword . '%');
                    });
                }
            });

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function store(UserRequest $request, UserCreateService $userCreateService, AddUserRoleService $addUserRoleService)
    {
        DB::beginTransaction();
        try {
            $user = $userCreateService->createWithRequestObject($request);
            $addUserRoleService->assignRole($user, $request->role_id);
            foreach ($request->bussiness_id as $bussiness_id) {
                UserBussiness::create([
                    'bussiness_id' => $bussiness_id,
                    'user_id' => $user->id,
                ]);
            }
            DB::commit();
            return $this->successResponse($user, 'ok');
        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorResponse($e, $e->getMessage());
        }
    }

    public function update(UserRequest $request, $id, UserUpdateService $userUpdateService, UpdateUserRoleService $updateUserRoleService)
    {
        DB::beginTransaction();
        try {
            $user = $userUpdateService->updateByIdWithRequestObject($id, $request);
            $updateUserRoleService->syncRoles($user, $request->role_id);
            UserBussiness::where('user_id', $user->id)->delete();
            foreach ($request->bussiness_id as $bussiness_id) {
                UserBussiness::create([
                    'bussiness_id' => $bussiness_id,
                    'user_id' => $user->id,
                ]);
            }
            DB::commit();
            return $this->successResponse($user, 'ok');
        } catch (\Exception $e) {
            DB::rollback();
            return $this->errorResponse($e, $e->getMessage());
        }
    }

    public function destroy($id, UserDeleteService $userDeleteService)
    {
        return $this->successResponse($userDeleteService->deleteById($id), 'ok');
    }
}
