<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\ApiController;
use Illuminate\Http\Request;
use App\Models\VendorItem;

class ApiVendorItemController extends ApiController
{
    public function index(Request $request)
    {
        $keyword = $request->search;

        $query = VendorItem::where(function ($q) use ($keyword) {
            if (!empty($keyword)) {
                $q->where(function ($q2) use ($keyword) {
                    $q2->where('nama_barang', 'like', '%' . $keyword . '%');
                });
            }
        });

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function store(Request $request)
    {
        $resp = VendorItem::create([
            'vendor_id' => $request->vendor_id,
            'nama_barang' => $request->nama_barang,
            'stok' => $request->stok,
            'harga_jual' => $request->harga_jual,
            'pembiayaan' => $request->pembiayaan,
            'item_type' => $request->item_type,
            'category_item' => $request->category_item,
        ]);

        return $this->successResponse($resp, 'ok');
    }

    public function show($id)
    {
        $resp = VendorItem::where('id', $id)->firstOrFail();

        return $this->successResponse($resp, 'ok');
    }

    public function update(Request $request, $id)
    {
        $row = VendorItem::where('id', $id)->update([
            'vendor_id' => $request->vendor_id,
            'nama_barang' => $request->nama_barang,
            'stok' => $request->stok,
            'harga_jual' => $request->harga_jual,
            'pembiayaan' => $request->pembiayaan,
            'item_type' => $request->item_type,
            'category_item' => $request->category_item,
        ]);

        return $this->successResponse($row, 'ok');
    }

    public function destroy($id)
    {
        $resp = VendorItem::where('id', $id)->firstOrFail()->delete();

        return $this->successResponse($resp, 'ok');
    }
}
