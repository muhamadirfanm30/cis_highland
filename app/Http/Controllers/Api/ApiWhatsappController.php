<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\ApiController;
use App\Models\WhatsappTemplate;
use Illuminate\Http\Request;

class ApiWhatsappController extends ApiController
{
    public function index(Request $request)
    {
        $keyword = $request->search;

        $query = WhatsappTemplate::where(function ($q) use ($keyword) {
            if (!empty($keyword)) {
                $q->where(function ($q2) use ($keyword) {
                    $q2->where('category_name', 'like', '%' . $keyword . '%');
                });
            }
        });

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }
}
