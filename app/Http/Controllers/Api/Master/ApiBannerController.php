<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use App\Http\Requests\BannerRequest;
use App\Models\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class ApiBannerController extends ApiController
{
    public function index(Request $request)
    {
        $keyword = $request->search;

        $query = Banner::where(function ($q) use ($keyword) {
            if (!empty($keyword)) {
                $q->where('title', 'like', '%' . $keyword . '%');
            }
        });

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function store(BannerRequest $request)
    {
        $resp = Banner::create([
            'title' => $request->title,
            'body' => $request->body,
            'image_file' => $request->file('image_file')->store('banners', 'public'),
        ]);

        return $this->successResponse($resp, 'ok');
    }

    public function show($id)
    {
        $resp = Banner::where('id', $id)->firstOrFail();

        return $this->successResponse($resp, 'ok');
    }

    public function update(BannerRequest $request, $id)
    {
        $row = Banner::where('id', $id)->firstOrFail();

        $row->update([
            'title' => $request->title,
            'body' => $request->body,
        ]);

        return $this->successResponse($row, 'ok');
    }

    public function destroy($id)
    {
        $getIcon = Banner::where('id', $id)->first();
        $exists = Storage::disk('local')->has('public/slide-show/' . $getIcon->image_file);
        if ($exists) {
            Storage::delete('public/slide-show/' . $getIcon->image_file);
        }
        Banner::find($id)->delete();
        return $this->successResponse($getIcon, 'ok');
    }
}
