<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use App\Http\Requests\CostCategoryRequest;
use App\Models\CostCategory;
use Illuminate\Http\Request;

class ApiCostCategoryController extends ApiController
{
    public function index(Request $request)
    {
        $keyword = $request->search;

        $query = CostCategory::where(function ($q) use ($keyword) {
            if (!empty($keyword)) {
                $q->where('name', 'like', '%' . $keyword . '%');
            }
        });

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function list()
    {
        return $this->successResponse(CostCategory::all(), 'data cost category list berhasil di load');
    }

    public function store(CostCategoryRequest $request)
    {
        $resp = CostCategory::create([
            'name' => $request->name,
            'note' => $request->note,
        ]);

        return $this->successResponse($resp, 'ok');
    }

    public function show($id)
    {
        $resp = CostCategory::where('id', $id)->firstOrFail();

        return $this->successResponse($resp, 'ok');
    }

    public function update(CostCategoryRequest $request, $id)
    {
        $row = CostCategory::where('id', $id)->firstOrFail();

        $row->update([
            'name' => $request->name,
            'note' => $request->note,
        ]);

        return $this->successResponse($row, 'ok');
    }

    public function destroy($id)
    {
        $resp = CostCategory::where('id', $id)->firstOrFail()->delete();

        return $this->successResponse($resp, 'ok');
    }
}
