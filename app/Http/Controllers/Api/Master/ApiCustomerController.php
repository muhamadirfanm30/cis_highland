<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use App\Http\Requests\BussinessRequest;
use App\Http\Requests\CustomerRequest;
use Illuminate\Http\Request;
use App\Models\Bussiness;
use App\Models\Customer;

class ApiCustomerController extends ApiController
{
    public function index(Request $request)
    {
        $keyword = $request->search;

        $query = Customer::where(function ($q) use ($keyword) {
            if (!empty($keyword)) {
                $q->where('name', 'like', '%' . $keyword . '%');
            }
        });

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function store(CustomerRequest $request)
    {
        $resp = Customer::create([
            'nama' => $request->nama,
            'telepon' => $request->telepon,
            'bussiness_id' => $request->bussiness_id,
        ]);

        return $this->successResponse($resp, 'ok');
    }

    public function show($id)
    {
        $resp = Customer::where('id', $id)->firstOrFail();

        return $this->successResponse($resp, 'ok');
    }

    public function update(CustomerRequest $request, $id)
    {
        $row = Customer::where('id', $id)->firstOrFail();

        $row->update([
            'nama' => $request->nama,
            'telepon' => $request->telepon,
            'bussiness_id' => $request->bussiness_id,
        ]);

        return $this->successResponse($row, 'ok');
    }

    public function destroy($id)
    {
        $resp = Customer::where('id', $id)->firstOrFail()->delete();

        return $this->successResponse($resp, 'ok');
    }
}
