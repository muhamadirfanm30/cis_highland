<?php

namespace App\Http\Controllers\Api\Master;

use App\Http\Controllers\ApiController;
use App\Http\Requests\PostRequest;
use Illuminate\Http\Request;
use App\Models\Post;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class ApiPostController extends ApiController
{
    public function index(Request $request)
    {
        $keyword = $request->search;

        $query = Post::where(function ($q) use ($keyword) {
            if (!empty($keyword)) {
                $q->where('title', 'like', '%' . $keyword . '%');
            }
        });

        return $this->successResponse($this->bootstrapTableFormat($query, $request), 'ok');
    }

    public function store(PostRequest $request)
    {
        $resp = Post::create([
            'title' => $request->title,
            'slug' => Str::slug($request->title, '-'),
            'body' => $request->body,
            'user_id' => Auth::id(),
        ]);

        return $this->successResponse($resp, 'ok');
    }

    public function show($id)
    {
        $resp = Post::where('id', $id)->firstOrFail();

        return $this->successResponse($resp, 'ok');
    }

    public function update(PostRequest $request, $id)
    {
        $row = Post::where('id', $id)->firstOrFail();

        $row->update([
            'title' => $request->title,
            'slug' => Str::slug($request->title, '-'),
            'body' => $request->body,
        ]);

        return $this->successResponse($row, 'ok');
    }

    public function destroy($id)
    {
        $resp = Post::where('id', $id)->firstOrFail()->delete();

        return $this->successResponse($resp, 'ok');
    }
}
