<?php

namespace App\Http\Controllers;

use App\Http\Controllers\WebController;
use App\Models\Blog;
use App\Models\Category;
use App\Models\Checkout;
use App\Models\SlideShow;
use App\Models\Vanue;
use App\Models\WebSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class LandingPageController extends WebController
{
    public function index()
    {
        return $this->loadView('customer.landingpage', [
            'title' => 'Landing Page',
            'showing_slider' => SlideShow::get(),
            'show_category' => Category::get(),
            'get_product' => Vanue::with(['get_image', 'get_fasilitas', 'category_vanue'])->skip(0)->take(8)->get(),
            'blog' => Blog::skip(0)->take(8)->get(),
            'get_email' => WebSetting::where('name', 'email')->first(),
            'get_phone' => WebSetting::where('name', 'phone_number')->first(),
            'get_address' => WebSetting::where('name', 'address')->first(),
        ]);
    }

    public function detail($id)
    {
        $get_id = decrypt($id);
        $findCategory = Vanue::find($get_id);
        return $this->loadView('customer.view_detail', [
            'title' => 'Landing Page',
            'get_email' => WebSetting::where('name', 'email')->first(),
            'get_phone' => WebSetting::where('name', 'phone_number')->first(),
            'get_address' => WebSetting::where('name', 'address')->first(),
            'get_product' => Vanue::with(['get_image', 'get_fasilitas', 'category_vanue'])->where('id', $get_id)->first(),
            'get_related' => Vanue::with(['get_image', 'get_fasilitas', 'category_vanue'])->where('category', $findCategory->category)->where('id', '!=', $get_id)->get(),
        ]);
    }

    public function contactUs()
    {
        return $this->loadView('customer.contact_us', [
            'title' => 'Contact Us',
            'get_email' => WebSetting::where('name', 'email')->first(),
            'get_phone' => WebSetting::where('name', 'phone_number')->first(),
            'get_address' => WebSetting::where('name', 'address')->first(),
        ]);
    }

    public function checkout($id)
    {
        $get_id = decrypt($id);
        return $this->loadView('customer.checkout', [
            'title' => 'Landing Page',
            'get_email' => WebSetting::where('name', 'email')->first(),
            'get_phone' => WebSetting::where('name', 'phone_number')->first(),
            'get_address' => WebSetting::where('name', 'address')->first(),
            'data_checkout' => Vanue::with(['get_image', 'get_fasilitas', 'category_vanue'])->where('id', $get_id)->first(),
        ]);
    }

    public function blog($id)
    {
        $get_id = decrypt($id);
        return $this->loadView('customer.view_detail_blog', [
            'title' => 'Landing Page',
            'get_email' => WebSetting::where('name', 'email')->first(),
            'get_phone' => WebSetting::where('name', 'phone_number')->first(),
            'get_address' => WebSetting::where('name', 'address')->first(),
            'detail_blog' => Blog::find($get_id),
        ]);
    }

    public function gridProduct(Request $request)
    {
        $showProduct = Vanue::with('category_vanue');
        $kategori = $request->kategori;
        if ($kategori) {
            $showProduct = $showProduct->where('category', $kategori);
        }
        $title = 'Grid Product Page';
        $get_email = WebSetting::where('name', 'email')->first();
        $get_phone = WebSetting::where('name', 'phone_number')->first();
        $get_address = WebSetting::where('name', 'address')->first();
        $get_category = Category::get();
        $showProduct = $showProduct->paginate(32);

        return $this->loadView(
            'customer.list_product',
            compact('title', 'get_email', 'get_phone', 'get_address', 'get_category', 'showProduct', 'kategori')
        )->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function gridBlogs(Request $request)
    {
        $title = 'Grid Blogs Page';
        $get_blogs = Blog::paginate(32);
        $get_email = WebSetting::where('name', 'email')->first();
        $get_phone = WebSetting::where('name', 'phone_number')->first();
        $get_address = WebSetting::where('name', 'address')->first();
        return $this->loadView('customer.list_blogs', compact('title', 'get_email', 'get_phone', 'get_address', 'get_blogs'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }

    public function checkoutPost(Request $request)
    {
        $saveData = Checkout::create([
            'first_name' => $request->first_name,
            'phone_number' => $request->phone_number,
            'checkin_date' => $request->checkin_date,
            'checkout_date' => $request->checkout_date,
            'ms_vanue_id' => $request->ms_vanue_id,
            'status' => 'Reservasi',
            'status_payment' => 'Belum Lunas',
        ]);

        return Redirect()
            ->route('landingpage.home')
            ->with('success', 'Order Anda Sudah Kami Terima, Silahkan menunggu info selanjut dari admin kami.');
    }
}
