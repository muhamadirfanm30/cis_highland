<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use App\Models\CampsiteBlockSite;
use App\Models\CampsiteImage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class CampsiteImageController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.campsite.image.index', [
            'title' => 'Galery List',
        ]);
    }

    public function create()
    {
        return $this->loadView('admin.campsite.image.create', [
            'title' => 'Create Image Campsite',
            'camp_block_site' => CampsiteBlockSite::with('block.campsite')->get(),
        ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'camp_block_site_id' => 'required',
            'file_name' => 'required',
            'file_name' => 'image|mimes:jpeg,jpg,png,gif,svg',
        ]);

        $name_file = null;
        if ($request->hasfile('image_campsite')) {
            $file = $request->file('image_campsite');
            foreach ($file as $image) {
                $filenameWithExt = $image->getClientOriginalName();
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                $extension = $image->getClientOriginalExtension();
                $filenameSimpan = $filename . '.' . $extension;
                $path = $image->storeAs('public/campsite_image', $filenameSimpan);

                CampsiteImage::create([
                    'image_campsite' => $filenameSimpan,
                    'camp_block_site_id' => $request->camp_block_site_id,
                ]);
            }
        }
        return back()->with('success', 'Image Has Been Uploaded');
    }
}
