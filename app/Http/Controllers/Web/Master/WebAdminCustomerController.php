<?php

namespace App\Http\Controllers\Web\Master;

use App\Http\Controllers\WebController;

class WebAdminCustomerController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.customer.index', [
            'title' => 'CUSTOMER',
        ]);
    }
}
