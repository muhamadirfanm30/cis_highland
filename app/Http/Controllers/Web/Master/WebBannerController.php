<?php

namespace App\Http\Controllers\Web\Master;

use App\Http\Controllers\WebController;
use App\Models\Banner;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class WebBannerController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.banner.index', [
            'title' => 'banner',
        ]);
    }

    public function create()
    {
        return $this->loadView('admin.banner.create', [
            'title' => 'banner create',
        ]);
    }

    public function store(Request $request)
    {
        $filename = null;

        if ($request->hasFile('image_file')) {
            foreach ($request->file('image_file') as $image) {
                $name = $image->getClientOriginalName();
                $img = Image::make($image);
                $img->stream(); // <-- Key point
                Storage::disk('local')->put('public/slide-show/' . $name, $img);
                $filename[] = $name;
            }
        }

        $title = count($request->title);
        for ($i = 0; $i < $title; $i++) {
            $insert[] = [
                'title' => $request->title[$i],
                'body' => $request->body[$i],
                'image_file' => $filename[$i],
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            ];
        }

        Banner::insert($insert);

        return redirect()
            ->route('banners.list')
            ->with('success', 'oke');
    }

    public function delete($id)
    {
        # code...
    }
}
