<?php

namespace App\Http\Controllers\Web\Master;

use App\Http\Controllers\WebController;

class WebBussinessController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.bussiness.index', [
            'title' => 'Bussiness List',
        ]);
    }
}
