<?php

namespace App\Http\Controllers\Web\Master;

use App\Http\Controllers\WebController;
use App\Models\Campsite;
use App\Models\CampsiteBlock;
use App\Models\CampsiteBlockSite;
use App\Models\CategoryCampsite;
use App\Models\ImagesCampBlockSite;
use App\Models\VendorItem;
use App\Models\MapConfig;
use Illuminate\Http\Request;
use DB;

class WebCampsiteController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.campsite.camp.index', [
            'title' => 'Campsite List',
        ]);
    }

    public function campsiteBlock()
    {
        $getCampsite = Campsite::get();
        return $this->loadView('admin.campsite.blok.index', [
            'title' => 'Campsite Block List',
            'getCampsite' => $getCampsite,
        ]);
    }

    public function campsiteBlockSite()
    {
        return $this->loadView('admin.campsite.site.index', [
            'title' => 'Campsite Block Site List',
            'blocks' => CampsiteBlock::with('campsite')->get(),
            'map' => MapConfig::getActiveMap()
        ]);
    }

    public function campsiteBlockSiteCreate()
    {
        return $this->loadView('admin.campsite.site.create', [
            'title' => 'Campsite Block Site Create',
            'blocks' => CampsiteBlock::with('campsite')->get(),
        ]);
    }

    public function campsiteBlockSiteUpdate($id)
    {
        return $this->loadView('admin.campsite.site.update', [
            'title' => 'Campsite Block Site Update',
            'blocks' => CampsiteBlock::with('campsite')->get(),
            'category' => CategoryCampsite::with('get_categori')->get(),
            'data' => CampsiteBlockSite::find($id),
            'vendor_item' => VendorItem::get(),
            'id' => $id,
        ]);
    }

    public function imagesBlockSite()
    {
        return $this->loadView('admin.campsite.image.index', [
            'title' => 'Image Block Site List',
            'blocks' => CampsiteBlockSite::with('block')->get(),
        ]);
    }

    public function imagesBlockSiteCreate()
    {
        return $this->loadView('admin.campsite.image.create', [
            'title' => 'Image Block Site Create',
            'get_camps_site' => CampsiteBlockSite::with('block.campsite')->get(),
        ]);
    }

    public function imagesBlockSiteStore(Request $request)
    {
        $this->validate($request, [
            'camp_block_site_id' => 'required',
            'image_campsite' => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        if ($request->hasFile('image_campsite')) {
            foreach ($request->file('image_campsite') as $image) {
                $name = $image->getClientOriginalName();
                $image->move(public_path().'/image_campsite/', $name);
                $data[] = $name;
            }
        }

        $create = new ImagesCampBlockSite;
        $create->camp_block_site_id = $request->camp_block_site_id;
        $create->image_campsite = json_encode($data);
        $create->save();
        return back()->with('success', 'image has Been Uploaded');
    }
}
