<?php

namespace App\Http\Controllers\Web\Master;

use App\Http\Controllers\WebController;
use App\Models\Bussiness;
use App\Models\VendorItem;

class WebPackageController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.package.index', [
            'title' => 'package',
            'bussiness' => Bussiness::get(),
            'vendor_item' => VendorItem::get(),
        ]);
    }

    public function create()
    {
        return $this->loadView('admin.package.create', [
            'title' => 'package Create',
            'bussiness' => Bussiness::get(),
            'vendor_item' => VendorItem::get(),
        ]);
    }

    public function edit($id)
    {
        return $this->loadView('admin.package.update', [
            'title' => 'package edit',
            'vendor_item' => VendorItem::get(),
            'id' => $id,
        ]);
    }
}
