<?php

namespace App\Http\Controllers\Web\Master;

use App\Http\Controllers\WebController;
use App\Models\Post;

class WebPostController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.post.index', [
            'title' => 'post',
        ]);
    }

    public function create()
    {
        return $this->loadView('admin.post.create', [
            'title' => 'post Create',
        ]);
    }

    public function edit($id)
    {
        return $this->loadView('admin.post.edit', [
            'title' => 'post Edit',
            'post' => Post::findOrFail($id)
        ]);
    }
}
