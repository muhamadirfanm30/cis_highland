<?php

namespace App\Http\Controllers\Web\Master;

use App\Http\Controllers\WebController;
use App\Models\User;
use App\Models\VendorItem;
use App\Models\Vendors;

class WebVendorController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.vendors.index', [
            'title' => 'Vendor List',
            'user' => User::all()
        ]);
    }

    public function indexVendorItem()
    {
        $hitungTotalVendor = VendorItem::get()->groupBy('vendor_id')->count();
        $hitungTotalProdukHampirHabis = VendorItem::where('stok', '<', 5)->Where('stok', '>', 0)->count();
        $hitungTotalProdukHabis = VendorItem::where('stok', '=', 0)->count();
        $hitungTotalProduk = VendorItem::count();
        return $this->loadView('admin.vendors-item.index', [
            'title' => 'Vendor Item List',
            'vendor' => Vendors::all(),
            'hitungTotalVendor' => $hitungTotalVendor,
            'hitungTotalProdukHampirHabis' => $hitungTotalProdukHampirHabis,
            'hitungTotalProdukHabis' => $hitungTotalProdukHabis,
            'hitungTotalProduk' => $hitungTotalProduk,
        ]);
    }
}
