<?php

namespace App\Http\Controllers\Web\Utility;

use App\Http\Controllers\WebController;
use App\Models\Bussiness;
use Spatie\Permission\Models\Role;

class WebAdminUserController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.user.index', [
            'title' => 'users',
            'roles' => Role::all(),
            'bussinesses' => Bussiness::all(),
        ]);
    }
}
