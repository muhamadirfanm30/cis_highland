<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use App\Models\HistoryItemService;
use App\Models\MapConfig;
use App\Models\Package;
use App\Models\Reservation;
use App\Models\User;
use App\Models\VendorItem;
use Carbon\Carbon;
use Carbon\CarbonImmutable;
use Illuminate\Support\Facades\DB;

class WebAdminDashboardController extends WebController
{
    public function index()
    {
        // data reservasi mingguan
        $en = CarbonImmutable::now()->locale('en_US');
        $senin = $en->startOfWeek(Carbon::MONDAY);
        $minggu = $en->startOfWeek(Carbon::MONDAY)->addDay(7);
        $startDay = date('Y-m-d H:i:s', strtotime($senin));
        $endDay = date('Y-m-d H:i:s', strtotime($minggu));

        $listWeeklyReport = Reservation::with(['customer', 'payment_status'])
            ->whereBetween('created_at', [$startDay, $endDay])
            ->get();
        $weeklyAllReservasi = Reservation::with(['customer', 'payment_status'])
            ->whereBetween('created_at', [$startDay, $endDay])
            ->count();
        $weeklyCheckin = Reservation::with(['customer', 'payment_status'])
            ->where('reservation_status_id', '1')
            ->whereBetween('created_at', [$startDay, $endDay])
            ->count();
        $weeklyCheckout = Reservation::with(['customer', 'payment_status'])
            ->where('reservation_status_id', '2')
            ->whereBetween('created_at', [$startDay, $endDay])
            ->count();

        // ambil data paket
        $getPackage = Package::withCount('hitung_paket')->get();

        // semua data reservasi
        $countReservasi = Reservation::count();
        $countPengunjung = Reservation::get();
        $countnewAdditional = Reservation::where('flag_new_additional_item', '1')->count();

        $totalPengunjung = 0;
        foreach ($countPengunjung as $pengunjung) {
            $totalPengunjung += $pengunjung->pax;
        }

        // cart
        $label = [
            "Januari",
            "Februari",
            "Maret",
            "April",
            "Mei",
            "Juni",
            "Juli",
            "Agustus",
            "September",
            "Oktober",
            "November",
            "Desember",
        ];

        for ($bulan = 1; $bulan < 13; $bulan++) {
            $totalPendapatan = collect(DB::SELECT("SELECT sum(nilai_project) AS jumlah from reservations where month(created_at)='$bulan'"))->first();
            $totalPembiayaan = collect(DB::SELECT("SELECT sum(total_pembiayaan) AS jumlah from reservations where month(created_at)='$bulan'"))->first();
            $averagePendapatan = collect(DB::SELECT("SELECT avg(nilai_project) AS jumlah from reservations where month(created_at)='$bulan'"))->first();
            $averageBulanan[] = $averagePendapatan->jumlah;
            $pembiayaanBulanan[] = $totalPembiayaan->jumlah;
            $pendapatanBulanan[] = $totalPendapatan->jumlah;
        }

        // ambil data vendor item
        $getDataItem = VendorItem::get();


        return $this->loadView('admin.dashboard_admin', [
            'countReservasi' => $countReservasi,
            'totalPengunjung' => $totalPengunjung,
            'listWeeklyReport' => $listWeeklyReport,
            'weeklyAllReservasi' => $weeklyAllReservasi,
            'weeklyCheckin' => $weeklyCheckin,
            'weeklyCheckout' => $weeklyCheckout,
            'averageBulanan' => $averageBulanan,
            'pembiayaanBulanan' => $pembiayaanBulanan,
            'pendapatanBulanan' => $pendapatanBulanan,
            'countnewAdditional' => $countnewAdditional,
            'label' => $label,
            'getPackage' => $getPackage,
            'totalPendapatan' => $totalPendapatan,
            'totalPembiayaan' => $totalPembiayaan,
            'averagePendapatan' => $averagePendapatan,
            'getDataItem' => $getDataItem,
        ]);
    }

    public function map()
    {
        return $this->loadView('admin.dashboard.map', [
            'title' => 'DATA RESERVASI HARI INI',
            'map' => MapConfig::getActiveMap()
        ]);
    }

    public function mapConfig()
    {
        return $this->loadView('admin.dashboard.map_config', [
            'title' => 'MAP CONFIG',
            'map' => MapConfig::getActiveMap()
        ]);
    }
}
