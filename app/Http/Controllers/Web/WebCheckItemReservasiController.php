<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use App\Models\HistoryItemService;
use App\Models\VendorItem;
use Illuminate\Http\Request;

class WebCheckItemReservasiController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.check-item.index', [
            'title' => 'Invoice List',
        ]);
    }

    public function detail($id)
    {
        return $this->loadView('admin.check-item.detail-item', [
            'title' => 'Detail List',
            'id' => $id,
            'getItem' => HistoryItemService::where('reservation_id', $id)->with('vendor_item')->get(),
        ]);
    }

    public function saveChange(Request $request, $id)
    {
         $value = $request->status_barang;
         $ids = $request->ids;

        foreach ($value as $key => $values) {
            $id = $ids[$key];
            HistoryItemService::where('id', $id)->update([
                'history_item_status_id' => $value[$key],
            ]);
        }
        return redirect()->route('check.list')->with('success', 'Data Berhasil Disimpan');
    }
}
