<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use App\Models\Cost;
use Illuminate\Http\Request;

class WebCostController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.cost.index', [
            'title' => 'COST',
        ]);
    }

    public function create(Request $request)
    {
        return $this->loadView('admin.cost.create', [
            'title' => 'COST CREATE',
            'request' => $request,
        ]);
    }

    public function edit($id)
    {
        $cost = Cost::findOrFail($id);

        return $this->loadView('admin.cost.edit', [
            'title' => 'COST EDIT',
            'cost' => $cost,
        ]);
    }

    public function category()
    {
        return $this->loadView('admin.cost.category', [
            'title' => 'COST CATEGORY',
        ]);
    }
}
