<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use App\Models\HistoryItemService;
use App\Models\Reservation;
use App\Models\VendorItem;
use App\Service\FinanceService;
use App\Service\HelperService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class WebFinanceController extends WebController
{
    public function pembiayaan()
    {
        $summary = FinanceService::pembiayaanSummary();

        $getDataReservasiPembiayaan = Reservation::get();

        return $this->loadView('admin.finance.pembiayaan', [
            'title' => 'FINANCE - PEMBIAYAAN',
            'summary' => $summary,
            'getDataReservasiPembiayaan' => $getDataReservasiPembiayaan,
        ]);
    }

    public function penjualan()
    {
        $summary = FinanceService::penjualanSummary();

        return $this->loadView('admin.finance.penjualan', [
            'title' => 'FINANCE - PENJUALAN',
            'summary' => $summary,
        ]);
    }

    public function detailPembiayaan($id)
    {
        $items = FinanceService::vendorDetail($query = true)
            ->where('reservations.id', $id)
            ->get();

        return $this->loadView('admin.finance.detail_penjualan', [
            'title' => 'FINANCE - PENJUALAN DETAIL',
            'items' => $items,
            'total_paket' => 0,
            'total_additional' => 0,
        ]);
    }

    public function vendor(Request $request)
    {
        $date_from = $request->date_from;
        $date_to = $request->date_to;
        $vendor_id = empty($request->vendor_id) ? null : $request->vendor_id;
        if ($vendor_id == null) {
            return $this->loadView('admin.finance.vendor_table', [
                'title' => 'FINANCE - VENDOR',
                'reportVendors' => [],
                'reportVendorHeader' => [],
            ]);
        }
        $items = VendorItem::where('vendor_id', $vendor_id)->get();

        $reservations = Reservation::query()
            ->whereHas('details.vendor_item', function ($query) use ($vendor_id) {
                $query->where('vendor_id', $vendor_id);
            })
            ->with([
                'customer',
                'details.vendor_item' => function ($query) use ($vendor_id) {
                    $query->where('vendor_id', $vendor_id);
                }
            ])->get();

        $reportVendors = [];
        foreach ($reservations as $key => $reservation) {
            $dataPush = [];
            $dataPush['No'] = ($key + 1);
            $dataPush['Customer'] = $reservation->customer->nama;
            $dataPush['Checkin Date'] = date('d F Y', strtotime($reservation->checkin_date));
            $dataPush['Checkout Date'] = date('d F Y', strtotime($reservation->checkout_date));
            
            foreach ($items as $item) {
                $dataPush[$item->nama_barang] = 0;
            }

            foreach ($reservation->details as $detail) {                
                if ($detail->vendor_item != null) {
                    $keyName = $detail->vendor_item->nama_barang;
                    if (array_key_exists($keyName, $dataPush)) {
                        $dataPush[$detail->vendor_item->nama_barang] = $detail->qty;
                        if (!isset($dataPush['total'])) {
                            $dataPush['total'] = 0;
                        }
                        if($detail->vendor_item->item_type == 2){
                            $dataPush['total'] += $detail->vendor_item->pembiayaan * $detail->qty * $reservation->pax  * $reservation->berapa_malam;
                        }else{
                            $dataPush['total'] += $detail->vendor_item->pembiayaan * $detail->qty;
                        }                        
                    }  
                }
            }
            $dataPush['PAX'] = $reservation->pax;
            $dataPush['Keterangan'] = '';
            $reportVendors[] = $dataPush;
        }
        return $this->loadView('admin.finance.vendor_table', [
            'title' => 'FINANCE - VENDOR',
            'reportVendors' => $reportVendors,
            'reportVendorHeader' => array_keys($reportVendors[0]),
        ]);
    }

    public function a3()
    {
        return $this->loadView('admin.finance.a3', [
            'title' => 'FINANCE - A3',
        ]);
    }

    public function a3Print(Request $request)
    {
        $date_from = $request->date_from == null ? HelperService::dateMinMonth(date('Y-m-d'), 1) : $request->date_from;
        $date_to = $request->date_to == null ? date('Y-m-d') : $request->date_to;

        $a3 = DB::table('view_reservation_summary')
            ->where('checkin_date', '>=', $date_from)
            ->where('checkin_date', '<=', $date_to)
            ->get();

        $summary = FinanceService::a3($query = true)
            ->where('first_week', '=', $date_from)
            ->where('end_week', '=', $date_to)
            ->first();

        return $this->loadView('admin.finance.a3_print', [
            'title' => 'FINANCE - A3 PRINT',
            'A3' => $a3,
            'summary' => $summary,
            'bo_persentase' => ($summary->cost_total / $summary->total_project * 100),
        ]);
    }
}
