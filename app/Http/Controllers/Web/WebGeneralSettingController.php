<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\WebController;
use App\Models\GeneralSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Image;

class WebGeneralSettingController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.general-setting.index', [
            'title' => 'General Setting',
            'getSetting' => GeneralSetting::get(),
        ]);
    }

    public function update(Request $request)
    {
        $value = $request->value;
        $name = $request->name;
        $ids = $request->id;

        foreach ($value as $key => $values) {
            $id = $ids[$key];
            GeneralSetting::where('id', $id)->update([
                'value' => $request->value[$key],
            ]);
        }

        return back()->with('success', 'Success');
    }

    public function indexSettingTicket()
    {
        return $this->loadView('admin.general-setting.indexSettingTicket', [
            'title' => 'General Setting',
            'getSettingTicket' => GeneralSetting::where('name', 'setting_ticket')->first(),
        ]);
    }

    public function updateSettingTicket(Request $request)
    {
        $filename = null;
        if (!empty($request->value)) {
            if ($image = $request->value) {
                $name = $image->getClientOriginalName();
                $img = Image::make($image);
                $img->stream(); // <-- Key point
                Storage::disk('local')->put('public/ticket_background/' . $name, $img);
                $filename = $name;
            }
        } else {
            $filename = null;
        }

        GeneralSetting::where('id', $request->id)->update([
            'value' => $filename,
        ]);

        return back()->with('success', 'Success');
    }

    public function indexSettingWhatsapp()
    {
        $getSettingTicket = GeneralSetting::whereIn('name', ['whatsapp_send', 'whatsapp_replay', 'admin_number_ponsel'])
            ->get();

        return $this->loadView('admin.general-setting.indexSettingWhatsapp', [
            'title' => 'General Setting',
            'getSettingTicket' => $getSettingTicket,
        ]);
    }

    public function updateSettingWhatsapp(Request $request)
    {
        $value = $request->value;
        $name = $request->name;
        $ids = $request->id;

        foreach ($value as $key => $values) {
            $id = $ids[$key];
            GeneralSetting::where('id', $id)->update([
                'value' => $request->value[$key],
            ]);
        }

        return back()->with('success', 'Success');
    }
}
