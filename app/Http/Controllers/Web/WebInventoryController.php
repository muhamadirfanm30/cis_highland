<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use Illuminate\Http\Request;

class WebInventoryController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.inventory.index', [
            'title' => 'Inventory List',
        ]);
    }
}
