<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use App\Models\ReservasiItem;
use App\Models\Reservation;

class WebInvoiceController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.invoice.index', [
            'title' => 'Invoice List',
            'reservasi' => Reservation::with('get_paket')->get(),
        ]);
    }

    public function invoice($id)
    {
        $data = Reservation::with(['user', 'get_paket'])->where('id', $id)->first();
        $item = ReservasiItem::where('id_reservasi', $id)->get();
        return view('admin.invoice.invoice', [
            'data' => $data,
            'item' => $item,
        ]);
    }
}
