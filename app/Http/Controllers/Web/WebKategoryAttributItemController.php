<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use Illuminate\Http\Request;

class WebKategoryAttributItemController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.kategory-attribute-item.index', [
            'title' => 'Kategory Attribute Item',
        ]);
    }
}
