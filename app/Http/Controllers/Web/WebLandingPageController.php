<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use App\Models\Banner;
use App\Models\Campsite;
use App\Models\CampsiteBlockSite;
use App\Models\CampsiteImage;
use App\Models\Package;
use App\Models\Reservation;
use Illuminate\Http\Request;

class WebLandingPageController extends WebController
{
    public function landingPage()
    {
        $showDataCampsite = CampsiteBlockSite::with('block')->orderBy('name')->get();
        $showSlider = Banner::get();
        return $this->loadView('landingpage', [
            'showDataCampsite' => $showDataCampsite,
            'showSlider' => $showSlider,
        ]);
    }

    public function detailPage($id)
    {
        $countImage = CampsiteImage::where('camp_block_site_id', $id)->count();
        $detailCampsite = CampsiteBlockSite::with(['block', 'images'])->where('id', $id)->first();
        return $this->loadView('detailPage', [
            'detailCampsite' => $detailCampsite,
            'countImage' => $countImage,
        ]);
    }

    public function reservation($id)
    {
        $dataReservasi = CampsiteBlockSite::with(['block', 'images'])->where('id', $id)->first();
        $getPaket = Package::get();
        return $this->loadView('reservasi', [
            'dataReservasi' => $dataReservasi,
            'getPaket' => $getPaket,
            'id' => $id,
        ]);
    }

    public function successFrom($id)
    {
        return $this->loadView('success_form', [
            'data' => Reservation::find($id),
        ]);
    }
}
