<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use App\Models\Customer;
use App\Models\Reservation;
use App\Models\VendorItem;
use App\Service\ExportExcelService;
use App\Service\HelperService;
use App\Service\Report\ReportPembiayaanService;
use Illuminate\Http\Request;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class WebReportController extends WebController
{
    public function pembiayaan(Request $request)
    {
        $date_from = empty($request->date_from) ? HelperService::dateMinDays(date('Y-m-d'), 7) : $request->date_from;
        $date_to = empty($request->date_to) ? date('Y-m-d') : $request->date_to;

        $reservations = Reservation::where('checkin_date', '>=', $date_from)
            ->where('checkin_date', '<=', $date_to)
            ->with(['customer', 'package', 'detail_reservasi.vendor_item'])
            ->get();

        $items = VendorItem::where('nama_barang', '!=', 'HTM')->orderBy('nama_barang', 'asc')->get();
        $itemHTM = VendorItem::where('nama_barang', 'HTM')->first();

        return $this->loadView('admin.report.pembiayaan', [
            'title' => 'laporan pembiayaan',
            'reservations' => $reservations,
            'items' => $items,
            'itemHTM' => $itemHTM,
        ]);
    }

    public function pembiayaan_export(Request $request)
    {
        $date_from = empty($request->date_from) ? HelperService::dateMinDays(date('Y-m-d'), 7) : $request->date_from;
        $date_to = empty($request->date_to) ? date('Y-m-d') : $request->date_to;

        $reservations = Reservation::where('checkin_date', '>=', $date_from)
            ->where('checkin_date', '<=', $date_to)
            ->with(['customer', 'package', 'detail_reservasi.vendor_item'])
            ->get();

        $items = VendorItem::where('nama_barang', '!=', 'HTM')->orderBy('nama_barang', 'asc')->get();
        $itemHTM = VendorItem::where('nama_barang', 'HTM')->first();

        ReportPembiayaanService::export($date_from, $date_to, $reservations, $items, $itemHTM);
    }

    public function penjualan()
    {
        $reservations = Reservation::with(['customer', 'package', 'detail_reservasi.vendor_item'])->get();
        $items = VendorItem::orderBy('nama_barang', 'asc')->get();

        return $this->loadView('admin.report.penjualan', [
            'title' => 'laporan penjualan',
            'reservations' => $reservations,
            'items' => $items,
        ]);
    }

    public function penjualan_export()
    {
        header("Content-Type: application/xls");
        header("Content-Disposition: attachment; filename=laporan_penjualan.xls");
        header("Pragma: no-cache");
        header("Expires: 0");

        $reservations = Reservation::with(['customer', 'package'])->get();
        $items = VendorItem::orderBy('nama_barang', 'asc')->get();

        return $this->loadView('admin.report.penjualan_export', [
            'title' => 'laporan penjualan',
            'reservations' => $reservations,
            'items' => $items,
        ]);
    }
}
