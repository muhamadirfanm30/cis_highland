<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use App\Models\VendorItem;
use Illuminate\Http\Request;

class WebReportPembiayaanController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.report.pembiayaan.index', [
            'title' => 'Report Financing',
            'get_item' => VendorItem::get(),
        ]);
    }
}
