<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use App\Models\Campsite;
use App\Models\CampsiteBlockHistoryTransaction;
use App\Models\GeneralSetting;
use App\Models\Package;
use App\Models\Reservation;
use App\Models\User;
use App\Models\WhatsappTemplate;
use App\Service\Reservation\ReservationService;
use SimpleSoftwareIO\QrCode\Facades\QrCode;
use PDF;

class WebReservasiController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.reservasi.index', [
            'title' => 'List Reservations',
        ]);
    }

    public function sendNotification($id)
    {
        $notif_whatsapp = WhatsappTemplate::get();
        return $this->loadView('admin.reservasi.notification-form', [
            'title' => 'List Reservations',
            'notif_whatsapp' => $notif_whatsapp,
            'id' => $id,
        ]);
    }

    public function showTicket($id)
    {
        $get_background = GeneralSetting::where('name', 'setting_ticket')->first();
        $data = Reservation::with(['customer', 'package'])->findOrFail($id);
        $qrcode = QrCode::size(89)->generate($data->id);
        return $this->loadView('admin.reservasi.ticket', [
            'title' => 'Ticket',
            'qrcode' => $qrcode,
            'data' => $data,
            'get_background' => $get_background,
        ]);
    }

    public function printTicket($id)
    {
        $data = Reservation::find($id);
        $qrcode = QrCode::size(89)->generate($data->id);
        $pdf = PDF::loadview('admin.reservasi.ticket', ['data' => $data, 'qrcode' => $qrcode]);
        $pdf->download('laporan-pegawai-pdf');
    }

    public function create()
    {
        return $this->loadView('admin.reservasi.create', [
            'title' => 'Reservation Create',
            'users' => User::get(),
            'packages' => Package::get(),
            'campsite' => Campsite::get(),
        ]);
    }

    public function paymentDetail($id)
    {
        return $this->loadView('admin.payment.detail', [
            'title' => 'Payment Detail',
            'summary' => ReservationService::summary($id),
        ]);
    }

    public function detail($id)
    {
        return $this->loadView('admin.reservasi.detail', [
            'title' => 'Reservation Update',
            'users' => User::get(),
            'package' => Package::get(),
            'reservasi' => Reservation::with(['customer', 'camp_blok_site.block.campsite'])->find($id),
        ]);
    }

    public function edit($id)
    {
        Reservation::find($id)->update([
            'flag_new_additional_item' => 0,
            'read_at' => date('Y-m-d H:i:s')
        ]);

        $reservasion = Reservation::where('id', $id)
            ->with(['tickets.ticket', 'customer', 'capmsite_block_site.block.campsite', 'status', 'details.vendor_item'])
            ->firstOrFail();

        return $this->loadView('admin.reservasi.edit', [
            'title' => 'Reservation Edit',
            'get_campsite' => CampsiteBlockHistoryTransaction::where('reservation_id', $id)->with('get_campsite.block')->get(),
            'users' => User::get(),
            'packages' => Package::get(),
            'campsite' => Campsite::get(),
            'getNotif' => WhatsappTemplate::get(),
            'reservasion' => $reservasion,
        ]);
    }

    public function viewInvoice($id)
    {
        $reservasi = Reservation::with([
            'customer',
            'penanggung_jawab',
            'package',
            'detail_reservasi.vendor_item',
            'detail_reservasi' => function ($query) {
                $query->whereNotNull('vendor_item_id')->where('is_include_paket', '!=', '1');
            },
        ])
            ->find($id);
        return $this->loadView('admin.reservasi.invoice', [
            'reservasi' => $reservasi,
        ]);
    }

    public function cencelReservation()
    {
        return ReservationService::cencelAllMoreThen($jam = 12);
    }

    public function checkoutReservation()
    {
        return ReservationService::checkoutAll();
    }
}
