<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use App\Models\Campsite;
use App\Models\Package;
use App\Models\Reservation;
use App\Models\User;
use Illuminate\Http\Request;

class WebReservasiOnlineController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.reservasi-online.index', [
            'title' => 'List Reservations',
        ]);
    }

    public function edit($id)
    {
        Reservation::find($id)->update([
            'flag_new_additional_item' => 0,
            'read_at' => date('Y-m-d H:i:s')
        ]);

        $reservation = Reservation::where('id', $id)
            ->with([
                'customer',
                'capmsite_block_site.block.campsite',
                'status',
                'details.vendor_item',
            ])
            ->firstOrFail();

        return $this->loadView('admin.reservasi-online.edit', [
            'title' => 'Reservation Edit',
            'users' => User::get(),
            'packages' => Package::get(),
            'campsite' => Campsite::get(),
            'reservasion' => $reservation,
        ]);
    }
}
