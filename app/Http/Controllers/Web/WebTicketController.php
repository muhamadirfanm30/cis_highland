<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\WebController;
use App\Service\Reservation\ReservationService;
use Illuminate\Http\Request;

class WebTicketController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.ticket.index', [
            'title' => 'TICKET',
        ]);
    }

    public function create(Request $request)
    {
        $reservation = null;
        $date = date('Y-m-d');
        if (!empty($request->reservation_id)) {
            $reservation = ReservationService::summary($request->reservation_id);
            $date = date('Y-m-d', strtotime($reservation->checkin_date));
        }
        return $this->loadView('admin.ticket.create', [
            'title' => 'CREATE STAND ALONE',
            'reservation' => $reservation,
            'date' => $date,
        ]);
    }

    public function transactionIndex()
    {
        return $this->loadView('admin.ticket.transaction', [
            'title' => 'TICKET TRANSACTION',
        ]);
    }
}
