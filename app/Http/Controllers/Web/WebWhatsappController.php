<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\WebController;
use App\Models\WhatsappTemplate;
use Illuminate\Http\Request;

class WebWhatsappController extends WebController
{
    public function index()
    {
        return $this->loadView('admin.whatsapp-template.index', [
            'title' => 'List Notification',
        ]);
    }

    public function create()
    {
        return $this->loadView('admin.whatsapp-template.create', [
            'title' => 'Create Notification',
        ]);
    }

    public function update($id)
    {
        return $this->loadView('admin.whatsapp-template.edit', [
            'title' => 'Update Notification',
            'data' => WhatsappTemplate::find($id)
        ]);
    }

    public function edit($id, Request $request)
    {
        $edit = WhatsappTemplate::where('id', $id)->update([
            'category_name' => $request->category_name,
            'desc' => $request->desc,
        ]);

        return redirect()->route('notif.list')->with('success', 'oke');
    }

    public function store(Request $request)
    {
        WhatsappTemplate::create([
            'category_name' => $request->category_name,
            'desc' => $request->desc,
        ]);

        return redirect()->route('notif.list')->with('success', 'oke');
    }
}
