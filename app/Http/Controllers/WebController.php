<?php

namespace App\Http\Controllers;

use App\Models\Reservation;
use App\Models\User;
use App\Models\WebInfo;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class WebController extends Controller
{
    public $viewData = [];

    public function __construct()
    {
        $this->setViewData($this->getCommonViewData());
    }

    public function setViewData(array $data)
    {
        foreach ($data as $key => $value) {
            $this->viewData[$key] = $value;
        }
        return $this;
    }


    public function getViewData()
    {
        return $this->viewData;
    }

    public function getSidebarMenu()
    {
        $countNewAdditionalItemReservasi = Reservation::where('is_tmp', 0)->whereNull('read_at')->count();
        $countReservasiOnline = Reservation::where('is_tmp', '1')->whereNull('read_at')->count();
        if ($countNewAdditionalItemReservasi > 0 || $countReservasiOnline > 0) {
            $countNotif = $countNewAdditionalItemReservasi;
            $countNotifReservasiOnline = $countReservasiOnline;
            $newNotif = "New";
        } else {
            $countNotif = '0';
            $newNotif = '0';
            $countNotifReservasiOnline = '0';
        }

        $route = request()->segment(2);

        // active menu
        $active_tiket = in_array($route, ['tickets']) ? 'active' : '';
        $active_master = in_array($route, ['bussiness', 'customers', 'packages']) ? 'active' : '';
        $active_campsite = in_array($route, ['campsites', 'campsites-block', 'campsites-block-site']) ? 'active' : '';
        $active_vendor = in_array($route, ['vendors', 'vendors-item']) ? 'active' : '';
        $active_checker = in_array($route, ['check-items']) ? 'active' : '';
        $active_setting = in_array($route, ['general-setting-front-end', 'general-setting-ticket', 'general-setting-whatsapp', 'map-config']) ? 'active' : '';
        $active_item_management = in_array($route, ['category-item', 'item', 'kategory-attribute-item', 'attribute-item']) ? 'active' : '';
        $active_TRX = in_array($route, ['reservasi', 'reservasi-online']) ? 'active' : '';
        $active_UTILITY = in_array($route, ['users', 'roles']) ? 'active' : '';
        $active_report = in_array($route, ['finance-vendor', 'finance-a3']) ? 'active' : '';
        $active_landing_page = in_array($route, ['posts', 'banners']) ? 'active' : '';
        $active_finance = in_array($route, ['finance-a3', 'costs', 'cost-categories', 'finance-pembiayaan', 'finance-penjualan', 'finance-vendor']) ? 'active' : '';

        $menus = [
            [
                'name' => 'DASHBOARD',
                'url' => url('admin/dashboard'),
                'icon' => 'fas fa-tachometer-alt',
                'active_class' => $route == 'dashboard' ? 'active' : '',
                'isSubMenu' => false,
                'submenu_open_class' => '',
                'submenu' => [],
                'permission_name' => ['DASHBOARD.READ'],
            ],
            [
                'name' => 'WHATSAPP TEMPLATE',
                'url' => url('admin/whatsapp-template'),
                'icon' => 'fas fa-book',
                'active_class' => $route == 'whatsapp-template' ? 'active' : '',
                'isSubMenu' => false,
                'submenu_open_class' => '',
                'submenu' => [],
                'permission_name' => ['DASHBOARD.READ'],
            ],
            [
                'name' => 'TICKET',
                'url' => '#',
                'icon' => 'fas fa-list',
                'active_class' => $active_tiket,
                'submenu_open_class' => ($active_tiket != '') ? 'menu-open' : '',
                'isSubMenu' => true,
                'permission_name' => ['TICKET.READ'],
                'submenu' => [
                    [
                        'name' => 'TICKET',
                        'url' => url('admin/tickets'),
                        'active_class' => $route == 'tickets' ? 'active' : '',
                        'permission_name' => ['TICKET.READ'],
                    ],
                    [
                        'name' => 'TICKET TRANSACTION',
                        'url' => url('admin/tickets/transaction'),
                        'active_class' => $route == 'tickets-transaction' ? 'active' : '',
                        'permission_name' => ['TICKET.READ'],
                    ],
                ]
            ],

            [
                'name' => 'MASTER',
                'url' => '#',
                'icon' => 'fas fa-list',
                'active_class' => $active_master,
                'submenu_open_class' => ($active_master != '') ? 'menu-open' : '',
                'isSubMenu' => true,
                'permission_name' => ['BUSSINESS.READ', 'CUSTOMER.READ', 'PACKAGE.READ'],
                'submenu' => [
                    [
                        'name' => 'BUSSINESS',
                        'url' => url('admin/bussiness'),
                        'active_class' => $route == 'bussiness' ? 'active' : '',
                        'permission_name' => ['BUSSINESS.READ'],
                    ],
                    [
                        'name' => 'CUSTOMER',
                        'url' => url('admin/customers'),
                        'active_class' => $route == 'customers' ? 'active' : '',
                        'permission_name' => ['CUSTOMER.READ'],
                    ],
                    [
                        'name' => 'PACKAGE',
                        'url' => url('admin/packages'),
                        'active_class' => $route == 'packages' ? 'active' : '',
                        'permission_name' => ['PACKAGE.READ'],
                    ],
                ]
            ],
            [
                'name' => 'CAMPSITE',
                'url' => '#',
                'icon' => 'fas fa-campground',
                'active_class' => $active_campsite,
                'submenu_open_class' => ($active_campsite != '') ? 'menu-open' : '',
                'isSubMenu' => true,
                'permission_name' => ['CAMPSITE.READ'],
                'submenu' => [
                    [
                        'name' => 'CAMPSITE',
                        'url' => url('admin/campsites'),
                        'active_class' => $route == 'campsites' ? 'active' : '',
                        'permission_name' => ['CAMPSITE.READ'],
                    ],
                    [
                        'name' => 'CAMPSITE BLOCK',
                        'url' => url('admin/campsites-block'),
                        'active_class' => $route == 'campsites-block' ? 'active' : '',
                        'permission_name' => ['CAMPSITE.READ'],
                    ],
                    [
                        'name' => 'CAMPSITE BLOCK SITE',
                        'url' => url('admin/campsites-block-site'),
                        'active_class' => $route == 'campsites-block-site' ? 'active' : '',
                        'permission_name' => ['CAMPSITE.READ'],
                    ],
                    [
                        'name' => 'CAMPSITE IMAGE',
                        'url' => url('admin/campsite-image'),
                        'active_class' => $route == 'campsite-image' ? 'active' : '',
                        'permission_name' => ['CAMPSITE.READ'],
                    ],
                ]
            ],
            [
                'name' => 'VENDOR',
                'url' => '#',
                'icon' => 'fas fa-user-tie',
                'active_class' => $active_vendor,
                'submenu_open_class' => ($active_vendor != '') ? 'menu-open' : '',
                'isSubMenu' => true,
                'permission_name' => ['VENDOR.READ', 'VENDOR-ITEM.READ'],
                'submenu' => [
                    [
                        'name' => 'VENDOR',
                        'url' => url('admin/vendors'),
                        'active_class' => $route == 'vendors' ? 'active' : '',
                        'permission_name' => ['VENDOR.READ'],
                    ],
                    [
                        'name' => 'VENDOR ITEM',
                        'url' => url('admin/vendors-item'),
                        'active_class' => $route == 'vendors-item' ? 'active' : '',
                        'permission_name' => ['VENDOR-ITEM.READ'],
                    ],
                ]
            ],
            [
                'name' => 'FINANCE',
                'url' => '#',
                'icon' => 'fas fa-money-bill-wave-alt',
                'active_class' => $active_finance,
                'submenu_open_class' => ($active_finance != '') ? 'menu-open' : '',
                'isSubMenu' => true,
                'permission_name' => ['COST.READ'],
                'submenu' => [
                    [
                        'name' => 'COST CATEGORY',
                        'url' => url('admin/cost-categories'),
                        'active_class' => $route == 'cost-categories' ? 'active' : '',
                        'permission_name' => ['COST.READ'],
                    ],
                    [
                        'name' => 'COST',
                        'url' => url('admin/costs'),
                        'active_class' => $route == 'costs' ? 'active' : '',
                        'permission_name' => ['COST.READ'],
                    ],
                    [
                        'name' => 'PENJUALAN',
                        'url' => url('admin/finance-penjualan'),
                        'active_class' => $route == 'finance-penjualan' ? 'active' : '',
                        'permission_name' => ['COST.READ'],
                    ],
                    [
                        'name' => 'PEMBIAYAAN',
                        'url' => url('admin/finance-pembiayaan'),
                        'active_class' => $route == 'finance-pembiayaan' ? 'active' : '',
                        'permission_name' => ['COST.READ'],
                    ],
                ]
            ],
            [
                'name' => 'TRANSACTION',
                'notif' => $newNotif,
                'url' => '#',
                'icon' => 'fas fa-book',
                'active_class' => $active_TRX,
                'submenu_open_class' => ($active_TRX != '') ? 'menu-open' : '',
                'isSubMenu' => true,
                'permission_name' => ['RESERVASI.READ'],
                'submenu' => [
                    [
                        'name' => 'RESERVASI',
                        'notif' => $countNotif,
                        'url' => url('admin/reservasi'),
                        'active_class' => $route == 'reservasi' ? 'active' : '',
                        'permission_name' => ['RESERVASI.READ'],
                    ],
                    [
                        'name' => 'RESERVASI ONLINE',
                        'notif' => $countNotifReservasiOnline,
                        'url' => url('admin/reservasi-online'),
                        'active_class' => $route == 'reservasi-online' ? 'active' : '',
                        'permission_name' => ['RESERVASI.READ'],
                    ],
                ]
            ],
            [
                'name' => 'LANDING PAGE',
                'url' => '#',
                'icon' => 'fas fa-newspaper',
                'active_class' => $active_landing_page,
                'submenu_open_class' => ($active_landing_page != '') ? 'menu-open' : '',
                'isSubMenu' => true,
                'permission_name' => ['LANDING-PAGE-AND-NEWS-CIS.READ'],
                'submenu' => [
                    [
                        'name' => 'POST',
                        'url' => url('admin/posts'),
                        'active_class' => $route == 'posts' ? 'active' : '',
                        'permission_name' => ['LANDING-PAGE-AND-NEWS-CIS.READ'],
                    ],
                    [
                        'name' => 'BANNER',
                        'url' => url('admin/banners'),
                        'active_class' => $route == 'banners' ? 'active' : '',
                        'permission_name' => ['LANDING-PAGE-AND-NEWS-CIS.READ'],
                    ],
                ]
            ],
            [
                'name' => 'REPORT',
                'url' => '#',
                'icon' => 'fas fa-file-alt',
                'active_class' => $active_report,
                'submenu_open_class' => ($active_report != '') ? 'menu-open' : '',
                'isSubMenu' => true,
                'permission_name' => ['LAPORAN-MINGGUAN.READ'],
                'submenu' => [
                    [
                        'name' => 'VENDOR',
                        'url' => url('admin/finance-vendor'),
                        'active_class' => $route == 'finance-vendor' ? 'active' : '',
                        'permission_name' => ['COST.READ'],
                    ],
                    [
                        'name' => 'A3',
                        'url' => url('admin/finance-a3'),
                        'active_class' => $route == 'finance-a3' ? 'active' : '',
                        'permission_name' => ['COST.READ'],
                    ],
                ]
            ],
            [
                'name' => 'UTILITY SYSTEM',
                'url' => '#',
                'icon' => 'fas fa-wrench',
                'active_class' => $active_UTILITY,
                'submenu_open_class' => ($active_UTILITY != '') ? 'menu-open' : '',
                'isSubMenu' => true,
                'permission_name' => ['USER.READ', 'ROLE.READ'],
                'submenu' => [
                    [
                        'name' => 'USER',
                        'url' => url('admin/users'),
                        'active_class' => $route == 'users' ? 'active' : '',
                        'permission_name' => ['USER.READ'],
                    ],
                    [
                        'name' => 'ROLE',
                        'url' => url('admin/roles'),
                        'active_class' => $route == 'roles' ? 'active' : '',
                        'permission_name' => ['ROLE.READ'],
                    ],
                ]
            ],

            [
                'name' => 'CHECKER & LTS',
                'url' => '#',
                'icon' => 'fas fa-tachometer-alt',
                'active_class' => $active_checker,
                'submenu_open_class' => ($active_checker != '') ? 'menu-open' : '',
                'isSubMenu' => true,
                'permission_name' => ['HISTORY-PELAYANAN.READ'],
                'submenu' => [
                    [
                        'name' => 'HISTORY-PELAYANAN',
                        'url' => url('admin/check-items'),
                        'active_class' => $route == 'check-items' ? 'active' : '',
                        'permission_name' => ['HISTORY-PELAYANAN.READ'],
                    ],
                ]
            ],

            [
                'name' => 'General Setting',
                'url' => '#',
                'icon' => 'fas fa-tachometer-alt',
                'active_class' => $active_setting,
                'submenu_open_class' => ($active_setting != '') ? 'menu-open' : '',
                'isSubMenu' => true,
                'permission_name' => ['HISTORY-PELAYANAN.READ'],
                'submenu' => [
                    [
                        'name' => 'SETTING TICKET',
                        'url' => url('admin/general-setting-ticket'),
                        'active_class' => $route == 'general-setting-ticket' ? 'active' : '',
                        'permission_name' => ['HISTORY-PELAYANAN.READ'],
                    ],
                    [
                        'name' => 'WHATSAPP SETTING',
                        'url' => url('admin/general-setting-whatsapp'),
                        'active_class' => $route == 'general-setting-whatsapp' ? 'active' : '',
                        'permission_name' => ['HISTORY-PELAYANAN.READ'],
                    ],
                    [
                        'name' => 'MAP SETTING',
                        'url' => url('admin/map-config'),
                        'active_class' => $route == 'map-config' ? 'active' : '',
                        'permission_name' => ['HISTORY-PELAYANAN.READ'],
                    ],
                ]
            ],
        ];
        return $menus;
    }

    public function getCommonViewData()
    {
        return [
            'title' => 'myApp',
            'title_header' => '',
            'app_name' => 'myApp',
            'master_template' => 'admin.template.master',
            'sidebar_menu' => $this->getSidebarMenu(),
        ];
    }

    public function loadView($path, $data = [])
    {
        $this->setViewData($data);

        return view($path, $this->getViewData());
    }
}
