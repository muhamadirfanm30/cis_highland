<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BussinessRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = request()->route('id');
        $editBisnis = $id == null ? '' : ',nama_bisnis,' . $id;
        $editLat = $id == null ? '' : ',latitude,' . $id;
        $editLong = $id == null ? '' : ',longitude,' . $id;

        return [
            'nama_bisnis' => 'required|unique:bussinesses' . $editBisnis,
            'latitude' => 'required|unique:bussinesses' . $editLat,
            'longitude' => 'required|unique:bussinesses' . $editLong,
            'alamat' => 'required',
        ];
    }
}
