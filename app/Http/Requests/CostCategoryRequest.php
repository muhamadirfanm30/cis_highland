<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CostCategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = request()->route('id');
        $editRule = $id == null ? '' : ',name,' . $id;

        return [
            'name' => 'required|unique:cost_categories' . $editRule,
        ];
    }
}
