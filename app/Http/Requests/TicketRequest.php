<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class TicketRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = request()->route('id');
        $editname = $id == null ? '' : ',name,' . $id;
        $editlocation = $id == null ? '' : ',location,' . $id;

        return [
            'name' => 'required|unique:tickets' . $editname,
            'location' => 'required|unique:tickets' . $editlocation,
            'opening_hours' => 'required',
            'closing_hours' => 'required',
            'description' => 'required',
            'selling_price' => 'required',
            'purchase_price' => 'required',
        ];
    }
}
