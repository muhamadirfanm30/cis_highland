<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Banner extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'body',
        'image_file',
    ];

    protected $appends = [
        'image_src',
    ];

    public function getImageSrcAttribute()
    {
        return asset('storage/' . $this->image_file);
    }
}
