<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CampsiteBlock extends Model
{
    use HasFactory;

    protected $fillable = [
        'campsite_id',
        'name',
    ];

    public function campsite()
    {
        return $this->belongsTo(Campsite::class, 'campsite_id', 'id');
    }
}
