<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CampsiteBlockHistoryTransaction extends Model
{
    use HasFactory;
    protected $fillable = [
        'reservation_id',
        'capmsite_block_site_id',
    ];

    /**
     * Get the user that owns the CampsiteBlockHistoryTransaction
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function get_campsite()
    {
        return $this->belongsTo(CampsiteBlockSite::class, 'capmsite_block_site_id', 'id');
    }
}
