<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CampsiteBlockSite extends Model
{
    use HasFactory;
    protected $fillable = [
        'campsite_block_id',
        'name',
        'capacity',
        'note',
        'category',
        'type_area',
        'coordinate_x',
        'coordinate_y',
    ];

    /**
     * Get all of the comments for the CampsiteBlockSite
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */

    protected $appends = [
        'images_campsite',
    ];

    public function get_category_camp()
    {
        return $this->hasMany(CategoryCampsiteBlock::class, 'campsite_block_site_id');
    }

    public function block()
    {
        return $this->belongsTo(CampsiteBlock::class, 'campsite_block_id', 'id');
    }

    public function images()
    {
        return $this->hasMany(CampsiteImage::class, 'camp_block_site_id', 'id');
    }

    public function getImagesCampsiteAttribute()
    {
        $count = CampsiteImage::where('camp_block_site_id', $this->id)->count();
        if ($count > 0) {
            $find = CampsiteImage::where('camp_block_site_id', $this->id)->first();
            return $find->image_campsite;
        } else {
            return public_path('picture.png');
        }
    }
}
