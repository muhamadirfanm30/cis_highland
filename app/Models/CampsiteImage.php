<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CampsiteImage extends Model
{
    use HasFactory;
    protected $fillable = [
        'camp_block_site_id',
        'image_campsite',
    ];

    public function campsiteBlockSite()
    {
        return $this->belongsTo(CampsiteBlockSite::class, 'camp_block_site_id', 'id');
    }
}
