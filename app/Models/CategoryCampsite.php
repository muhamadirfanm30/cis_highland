<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryCampsite extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
    ];

    /**
     * Get the user that owns the CategoryCampsite
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function get_categori()
    {
        return $this->belongsTo(CategoryCampsiteBlock::class, 'id', 'category_campsite');
    }
}
