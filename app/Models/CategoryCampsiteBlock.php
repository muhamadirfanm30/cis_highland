<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryCampsiteBlock extends Model
{
    use HasFactory;
    protected $fillable = [
        'campsite_block_site_id',
        'category_campsite',
    ];
}
