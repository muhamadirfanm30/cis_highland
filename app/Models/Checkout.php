<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Checkout extends Model
{
    use HasFactory;
    protected $fillable = [
        'first_name',
        'phone_number',
        'checkin_date',
        'checkout_date',
        'ms_vanue_id',
        'read_at',
        'status',
        'status_payment',
    ];

    
    public function data_vanue()
    {
        return $this->belongsTo(Vanue::class, 'ms_vanue_id', 'id');
    }
}
