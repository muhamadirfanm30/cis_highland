<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cost extends Model
{
    use HasFactory;

    protected $fillable = [
        'date_from',
        'date_to',
        'id',
    ];

    public function details()
    {
        return $this->hasMany(CostDetail::class, 'cost_id', 'id');
    }
}
