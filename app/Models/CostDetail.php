<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CostDetail extends Model
{
    use HasFactory;

    protected $fillable = [
        'cost_total',
        'cost_note',
        'cost_id',
        'cost_category_id',
    ];
}
