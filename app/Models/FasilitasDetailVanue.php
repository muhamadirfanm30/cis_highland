<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class FasilitasDetailVanue extends Model
{
    use HasFactory;
    protected $fillable = [
        'ms_vanue_id',
        'fasilitas_name',
    ];
}
