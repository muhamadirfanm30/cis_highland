<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoryItemService extends Model
{
    use HasFactory;

    protected $fillable = [
        'vendor_item_id',
        'customer_id',
        'reservation_id',
        'history_item_status_id',
        'tgl_penggunaan',
        'tgl_pengembalian',
        'qty',
        'is_include_paket',
        'durasi',
        'item_price',
        'is_new_additional',
    ];

    public function vendor_item()
    {
        return $this->belongsTo(VendorItem::class, 'vendor_item_id', 'id');
    }

    public function status()
    {
        return $this->belongsTo(HistoryItemStatus::class, 'history_item_status_id', 'id');
    }
}
