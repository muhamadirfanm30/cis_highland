<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class HistoryItemStatus extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'note',
    ];

    const BELUM_DIANTAR = 1;
    const DITERIMA = 2;
}
