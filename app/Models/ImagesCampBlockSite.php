<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ImagesCampBlockSite extends Model
{
    use HasFactory;

    protected $fillable = [
        'camp_block_site_id',
        'image_campsite',
    ];
}
