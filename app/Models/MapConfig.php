<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MapConfig extends Model
{
    use HasFactory;

    protected $fillable = [
        'map_src',
        'map_show',
        'map_rate',
        'map_width',
        'map_height',
        'map_max',
        'map_marker_size',
        'map_marker_src',
        'map_marker_draggable',
    ];

    protected $appends = [
        'map_file_src',
        'map_marker_file_src',
    ];

    public function getMapFileSrcAttribute()
    {
        return asset('storage/' . $this->map_src);
    }

    public function getMapMarkerFileSrcAttribute()
    {
        return asset('storage/' . $this->map_marker_src);
    }

    public static function getActiveMap()
    {
        return MapConfig::first();
    }
}
