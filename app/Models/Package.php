<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'bussiness_id',
        'harga',
        'nama_paket',
    ];

    public function bussiness()
    {
        return $this->belongsTo(Bussiness::class, 'bussiness_id', 'id');
    }

    public function hitung_paket()
    {
        return $this->hasMany(Reservation::class, 'package_id', 'id');
    }

    public function items()
    {
        return $this->hasMany(PackageItem::class, 'package_id', 'id');
    }

    public function get_layanan()
    {
        return $this->belongsTo(ItemVendors::class, 'id_item_pelayanan', 'id');
    }

    public function get_item()
    {
        return $this->hasMany(PackageItem::class, 'id_paket');
    }
}
