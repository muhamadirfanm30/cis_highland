<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PackageItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'package_id',
        'vendor_item_id',
    ];

    public function vendor_item()
    {
        return $this->belongsTo(VendorItem::class, 'vendor_item_id', 'id');
    }
}
