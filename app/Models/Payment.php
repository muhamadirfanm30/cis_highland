<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Payment extends Model
{
    use HasFactory;
    protected $fillable = [
        'payment_date',
        'payment_metod',
        'payment_total',
        'payment_description',
        'image',
        'reservation_id',
        'customer_id',
    ];

    public static function getImagePathUpload()
    {
        return 'public/upload-bukti-transfer';
    }
}
