<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentStatus extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'note',
    ];

    const BELUM_LUNAS = 1;
    const LUNAS = 2;
}
