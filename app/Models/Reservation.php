<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    use HasFactory;
    protected $fillable = [
        'id',
        'checkin_date',
        'checkout_date',
        'berapa_malam',
        'note',
        'code_invoice',
        'pax',
        'plat_nomor',
        'customer_id',
        'id_penanggung_jawab',
        'capmsite_block_site_id',
        'reservation_status_id',
        'bussiness_id',
        'package_id',
        'payment_status_id',
        'nilai_project',
        'total_pembiayaan',
        'flag_new_additional_item',
        'read_at',
        'is_tmp',
    ];

    //======================
    // bagian fungsi appends
    //======================
    protected $appends = [
        'count_proggress_item',
        'jumlah_hari',
    ];



    public function getJumlahHariAttribute()
    {
        $dStart = new \DateTime($this->checkin_date);
        $dEnd  = new \DateTime($this->checkout_date);
        $dDiff = $dStart->diff($dEnd);
        return $dDiff->format('%r%a');
    }

    public function getCountProggressItemAttribute()
    {
        $allStatus = HistoryItemService::where('reservation_id', $this->id)->count(); //5
        $statusDiterima = HistoryItemService::where('reservation_id', $this->id)->where('history_item_status_id', 2)->count(); //2
        if ($allStatus > 0) {
            $total = $statusDiterima / $allStatus;
            $hasil = $total * 100;
            return floor($hasil);
        } else {
            return 0;
        }
    }

    //======================
    // bagian fungsi muttator
    //======================
    public function setCheckinDateAttribute($value)
    {
        $this->attributes['checkin_date'] = date('Y-m-d', strtotime($value));
    }

    public function setCheckoutDateAttribute($value)
    {
        $this->attributes['checkout_date'] = date('Y-m-d', strtotime($value));
    }

    //======================
    // bagian fungsi relasi
    //======================
    public function status()
    {
        return $this->belongsTo(ReservationStatus::class, 'reservation_status_id', 'id');
    }

    public function payment_status()
    {
        return $this->belongsTo(PaymentStatus::class, 'payment_status_id', 'id');
    }

    public function customer()
    {
        return $this->belongsTo(Customer::class, 'customer_id', 'id');
    }

    public function capmsite_block_site()
    {
        return $this->belongsTo(CampsiteBlockSite::class, 'capmsite_block_site_id', 'id');
    }

    public function penanggung_jawab()
    {
        return $this->belongsTo(User::class, 'id_penanggung_jawab', 'id');
    }

    public function package()
    {
        return $this->belongsTo(Package::class, 'package_id', 'id');
    }

    public function payments()
    {
        return $this->hasMany(Payment::class, 'id', 'reservation_id');
    }

    public function camp_blok_site()
    {
        return $this->belongsTo(CampsiteBlockSite::class, 'capmsite_block_site_id', 'id');
    }

    public function detail_reservasi()
    {
        return $this->hasMany(HistoryItemService::class, 'reservation_id');
    }

    public function details()
    {
        return $this->hasMany(HistoryItemService::class, 'reservation_id');
    }

    public function tickets()
    {
        return $this->hasMany(TicketTransaction::class, 'reservation_id');
    }
}
