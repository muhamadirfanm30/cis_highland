<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{
    use HasFactory;
    protected $fillable = [
        'vendor_id',
        'name',
        'max_pax',
        'code',
        'location',
        'opening_hours',
        'closing_hours',
        'description',
        'selling_price',
        'purchase_price',
    ];
}
