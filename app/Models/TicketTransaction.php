<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TicketTransaction extends Model
{
    use HasFactory;

    protected $fillable = [
        'transaction_pax',
        'transaction_date',
        'transaction_status',
        'transaction_selling_price',
        'transaction_purchase_price',
        'user_id',
        'customer_id',
        'ticket_id',
        'reservation_id',
    ];

    //======================
    // bagian fungsi muttator
    //======================
    public function setTransactionDateAttribute($value)
    {
        $this->attributes['transaction_date'] = date('Y-m-d', strtotime($value));
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function ticket()
    {
        return $this->belongsTo(Ticket::class, 'ticket_id', 'id');
    }

    public function reservation()
    {
        return $this->belongsTo(Reservation::class, 'reservation_id', 'id');
    }
}
