<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Vanue extends Model
{
    use HasFactory;
    protected $fillable = [
        'vanue_name',
        'location',
        'category',
        'harga',
        'desc',
        'information',
    ];

    protected $appends = [
        'images',
    ];

    /**
     * Get all of the comments for the Vanue
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function get_image()
    {
        return $this->hasMany(VanueImage::class, 'ms_vanue_id', 'id');
    }

    public function get_fasilitas()
    {
        return $this->hasMany(FasilitasDetailVanue::class, 'ms_vanue_id', 'id');
    }

    public function category_vanue()
    {
        return $this->belongsTo(Category::class, 'category', 'id');
    }

    public function getImagesAttribute()
    {
        $find = VanueImage::where('ms_vanue_id', $this->id)->first();
        return $find->image;
    }
}
