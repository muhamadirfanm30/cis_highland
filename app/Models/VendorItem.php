<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VendorItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'vendor_id',
        'nama_barang',
        'stok',
        'harga_jual',
        'pembiayaan',
        'item_type',
        'category_item',
    ];

    public function history_item_services()
    {
        return $this->hasMany(HistoryItemService::class, 'vendor_item_id');
    }

    public function vendor()
    {
        return $this->belongsTo(Vendors::class, 'vendor_id', 'id');
    }
}
