<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WebSetting extends Model
{
    use HasFactory;
    protected $fillable = [
        'image_header',
        'favicon_image',
        'email',
        'phone_number',
        'link_facebook',
        'link_instagram',
        'link_twiter',
        'link_tiktok',
        'address',
    ];
}
