<?php

namespace App\Service;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class ExportExcelService implements \IteratorAggregate
{
    protected static $instancePhpExcel = null;
    protected static $instance         = null;
    public $column                     = array('NO' => 'no');
    public $defaulValue = '';
    public $filename;
    public $rangeLastCol;
    public $pushStart = 4;
    public $style;
    public $callculateData = [];

    public $read_inputFileName;
    public $read_range = '';
    public $read_data_results;
    public function __construct()
    {
        self::$instancePhpExcel = new Spreadsheet();
        self::$instance         = $this;
        $this->setStyle(true);
    }

    /**
     * Set file path
     * Lokasi file excel
     *
     * @param string|path name
     * @return ExportExcel instance
     */
    public function read($inputFileName)
    {
        $this->read_inputFileName = $inputFileName;
        return $this;
    }

    /**
     * Format data jadi seperti fetch from db
     *
     * @param int|index, row di file excel yg akan dijadikan sebagai field
     * @param callable|callback_data get 3 param, (colName, $colVal, $row)
     * @param bool|callback_format
     * @return ExportExcel instance
     */
    public function readDataFormatter($index, $callback_data = null, $callback_format = false)
    {
        $data    = $this->read_data_results;
        $columns = $data[$index - 1];
        $data    = array_slice($data, $index);
        $hasil   = array();
        $cols    = array();

        foreach ($columns as $key => $name) {
            $name = str_replace(' ', '_', strtolower($name));
            if ($name != '') {
                $cols[] = $name;
                foreach ($data as $data_key => $data_value) {
                    $colName = $name;
                    $colVal  = $data_value[$key];
                    $row     = $data_value;
                    if (!is_null($callback_data)) {
                        $colVal = $callback_data($colName, $colVal, $row);
                    }

                    if ($callback_format == true) {
                        $hasil['id'][$data_key]  = ($index + 1) + $data_key;
                        $hasil[$name][$data_key] = $colVal;
                    } else {
                        $hasil[$data_key]['id']  = ($index + 1) + $data_key;
                        $hasil[$data_key][$name] = $colVal;
                    }
                }
            }
        }

        $this->read_data_results = $hasil;

        return $this;
    }

    /**
     * Baca file import berdasar range
     *
     * @param string|read_range, contoh A1:Z10
     * @return ExportExcel instance
     */
    public function readFromRange($read_range)
    {
        $this->read_range = $read_range;
        return $this;
    }

    /**
     * SetDocumentProperties
     *
     * @return ExportExcel instance
     */
    public function setDocumentProperties()
    {
        $objPHPExcel = self::getPHPExcelInstance();

        $objPHPExcel->getProperties()->setCreator("Solog App")
            ->setLastModifiedBy("Solog App")
            ->setTitle("Office 2007 XLSX Test Document")
            ->setSubject("Office 2007 XLSX Test Document")
            ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
            ->setKeywords("office 2007 openxml php")
            ->setCategory("Test result file");

        return $this;
    }

    public function columnWithoutNo()
    {
        $this->column = array();
        return $this;
    }

    /**
     * Excel style
     *
     * @return array
     */
    public function getDataStyleTitle()
    {
        $style = array(
            'alignment' => array(
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ),
            'font'      => array(
                'bold' => true,
                'size' => 15,
            ),
        );

        return $style;
    }

    /**
     * Excel style
     *
     * @return array
     */
    public function getDataStyleHeader()
    {
        $style_header = array(
            'font' => array(
                'bold'  => true,
                'size'  => 12,
            ),
            'fill' => array(
                'type'  => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
            ),
        );

        return $style_header;
    }

    /**
     * Excel style
     *
     * @return array
     */
    public function getDataStyleBorder()
    {
        $style_border = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
            ),
        );

        return $style_border;
    }

    /**
     * Excel style
     *
     * @return array
     */
    public function getDataStyleVerticalAlign()
    {
        $style_vertical_align = array(
            'alignment' => array(
                'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ),
        );

        return $style_vertical_align;
    }

    /**
     * Set title header
     *
     * @param string|name
     * @return ExportExcel instance
     */
    public function setTitleName($name)
    {
        $objPHPExcel = self::getPHPExcelInstance();

        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', str_replace('_', ' ', strtoupper($name)))
            ->setCellValue('A2', 'TANGGAL CETAK : ' . date("d-m-Y"));

        // add style
        $objPHPExcel->setActiveSheetIndex(0)->getStyle("A1")->applyFromArray($this->getDataStyleTitle());

        $this->filename = $this->titleToFileName($name);

        return $this;
    }

    /**
     * Set filename
     *
     * @param string|name
     * @return ExportExcel instance
     */
    public function setFileName($name)
    {
        $this->filename = $name;

        return $this;
    }

    /**
     * Tambahkan datetime untuk filename
     *
     * @param string|name
     * @return string
     */
    public function titleToFileName($name)
    {
        $name = str_replace(' ', '_', strtolower(trim($name)));

        $filename = $name . '_' . date('d_m_Y_His') . '.xls';

        return $filename;
    }

    /**
     * Set column
     *
     * @param array|cols
     * @return ExportExcel instance
     */
    public function setColumn(array $cols, $callback = null)
    {
        $objPHPExcel = self::getPHPExcelInstance();

        $rangeLastCol = $this->getRange(count($cols));
        $rangeStyle   = "A" . $this->pushStart . ':' . $rangeLastCol . $this->pushStart;

        if ($this->getStyle()) {
            // add style
            $objPHPExcel->setActiveSheetIndex(0)
                ->getStyle($rangeStyle)
                ->applyFromArray($this->getDataStyleHeader());

            // add style
            $objPHPExcel->setActiveSheetIndex(0)
                ->getStyle($rangeStyle)
                ->applyFromArray($this->getDataStyleBorder());
        }

        // auto width column
        foreach (range('A', $rangeLastCol) as $columnID) {
            $objPHPExcel->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
        }

        // merge cell
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A1:' . $rangeLastCol . '1');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A2:' . $rangeLastCol . '2');

        foreach ($cols as $key => $value) {
            $this->column[$key] = $value;
        }
        $this->rangeLastCol = $rangeLastCol;

        $this->AddDataToColumn($this->column, $callback);

        return $this;
    }

    /**
     * Get column
     *
     * @return array
     */
    public function getColumnData()
    {
        return $this->column;
    }

    /**
     * Tulis data kolom ke file excel
     *
     * @param array|cols
     * @return ExportExcel instance
     */
    public function AddDataToColumn($column, $callback = null)
    {
        $objPHPExcel = self::getPHPExcelInstance();

        $alphabet = range('A', $this->rangeLastCol);

        // Add some data
        $key_no = 0;
        foreach ($column as $name => $dbName) {
            $val = $name;
            if (!is_null($callback)) {
                $val = $callback($name, $column);
            }
            $objPHPExcel
                ->setActiveSheetIndex(0)
                ->setCellValue($alphabet[$key_no] . $this->pushStart, $val);
            $key_no++;
        }

        return $this;
    }

    /**
     * Tulis data ke file excel
     *
     * @param array|data
     * @return ExportExcel instance
     */
    private function pushData($data, $callback = null)
    {
        $objPHPExcel = self::getPHPExcelInstance();

        $column   = array_values($this->getColumnData());
        $alphabet = range('A', $this->rangeLastCol);

        // add data
        $push_start = $this->pushStart + 1;
        foreach ($column as $key => $name) {
            $chainCol = explode('.', $column[$key]);

            $abc = $alphabet[$key];
            foreach ($data as $no => $row) {
                if ($chainCol[0] == 'no') {
                    $val = $no + 1;
                } else {
                    if (count($chainCol) == 1) {
                        $val = isset($row->{$chainCol[0]}) ? $row->{$chainCol[0]} : '';
                    }
                    if (count($chainCol) == 2) {
                        $val = isset($row->{$chainCol[0]}->{$chainCol[1]}) ? $row->{$chainCol[0]}->{$chainCol[1]} : '';
                    }
                    if (count($chainCol) == 3) {
                        $val = isset($row->{$chainCol[0]}->{$chainCol[1]}->{$chainCol[2]})
                            ? $row->{$chainCol[0]}->{$chainCol[1]}->{$chainCol[2]}
                            : '';
                    }
                }

                if (!is_null($callback)) {
                    $val = $callback($name, $row, $val);
                }

                if ($val === null) {
                    $val = $this->defaulValue;
                }

                $this->callculateData[$name][$no] = $abc . $push_start;

                $objPHPExcel
                    ->setActiveSheetIndex(0)
                    ->setCellValue($abc . $push_start, $val);

                $push_start++;
                $no++;
            }

            $push_start = $this->pushStart + 1;
        }
        // add style
        $end_push   = (count($data) + $this->pushStart);
        $rangeStyle = "A" . $this->pushStart . ':' . $this->rangeLastCol . $end_push;
        if ($this->getStyle()) {
            $objPHPExcel->setActiveSheetIndex(0)
                ->getStyle($rangeStyle)
                ->applyFromArray($this->getDataStyleBorder());
        }

        $this->pushStart = $end_push;
        return $this;
    }

    public function calculateColumn($col, $callback)
    {
        $objPHPExcel = self::getPHPExcelInstance();
        $data = $this->callculateData[$col];

        foreach ($data as $no => $cell) {
            $formula = $callback($this->callculateData, $no);
            $formulaCell = $cell;
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue($formulaCell, $formula);
            $objPHPExcel->setActiveSheetIndex(0)->getCell($formulaCell)->getCalculatedValue();
        }
        return $this;
    }

    public function getRange($num)
    {
        $alphabet = range('A', 'Z');

        return $alphabet[$num];
    }

    /**
     * Sum data
     *
     * @param array|cols kolom yg akan disum
     * @return ExportExcel instance
     */
    public function sumData($cols)
    {
        $objPHPExcel = self::getPHPExcelInstance();

        $dari   = 5;
        $sampai = $this->pushStart += 1;

        $column = $this->getColumnData();
        $column = array_values($column);

        $alphabet = range('A', 'Z');

        foreach ($cols as $key => $value) {
            $search = array_search($value, $column);
            $cell   = $alphabet[$search] . $sampai;
            $sum    = '=SUM(' . $alphabet[$search] . $dari . ':' . $alphabet[$search] . ($sampai - 1) . ')';
            $objPHPExcel->getActiveSheet()->setCellValue($cell, $sum);
            $objPHPExcel->getActiveSheet()->getCell($cell)->getCalculatedValue();
        }

        return $this;
    }

    public function minData($cols)
    {
        $objPHPExcel = self::getPHPExcelInstance();

        $dari   = 5;
        $sampai = $this->pushStart += 1;

        $column = $this->getColumnData();
        $column = array_values($column);

        $alphabet = range('A', 'Z');

        foreach ($cols as $key => $value) {
            $search = array_search($value, $column);
            $cell   = $alphabet[$search] . $sampai;
            $sum    = '=MIN(' . $alphabet[$search] . $dari . ':' . $alphabet[$search] . ($sampai - 1) . ')';
            $objPHPExcel->getActiveSheet()->setCellValue($cell, $sum);
            $objPHPExcel->getActiveSheet()->getCell($cell)->getCalculatedValue();
        }

        return $this;
    }

    /**
     * Save file
     *
     * @param string|name
     */
    public function save($name, $title = '')
    {
        $objPHPExcel = self::getPHPExcelInstance();
        $titleName = $title == '' ? $name : $title;
        // Rename worksheet
        $objPHPExcel
            ->getActiveSheet()
            ->setTitle($titleName);

        $filename = $this->titleToFileName($this->filename) . '.xls';

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($objPHPExcel, 'Xls');
        $writer->save('php://output');
        exit;
    }

    /**
     * Set data untuk di export
     *
     * @param array|data
     * @return ExportExcel instance
     */
    public function exportData($data, $callback = null)
    {
        // add data
        $this->pushData($data, $callback);

        return $this;
    }

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }
        return self::$instance;
    }
    public static function getPHPExcelInstance()
    {
        if (self::$instancePhpExcel === null) {
            self::$instancePhpExcel = new Spreadsheet();
        }
        return self::$instancePhpExcel;
    }

    public function toObject()
    {
        $this->read_data_results = json_decode(json_encode($this->read_data_results), false);

        return $this;
    }

    public function getData()
    {
        return $this->read_data_results;
    }

    public function getStyle()
    {
        return $this->style;
    }

    public function setStyle($style)
    {
        $this->style = $style;

        return $this;
    }

    public function setDefaultValue($val)
    {
        $this->defaulValue = $val;

        return $this;
    }

    public function getIterator()
    {
        return new \ArrayIterator($this->read_data_results);
    }

    public function count()
    {
        return iterator_count($this->read_data_results);
    }
}
