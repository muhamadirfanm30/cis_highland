<?php

namespace App\Service;

use Illuminate\Support\Facades\DB;

class FinanceService
{
    public static function penjualanSummary($date_from = null, $date_to = null)
    {
        $date_from = $date_from == null ? HelperService::dateMinMonth(date('Y-m-d'), 1) : $date_from;
        $date_to = $date_to == null ? date('Y-m-d') : $date_to;

        $summary = DB::table('view_reservation_summary')
            ->select([
                DB::raw('SUM(total_project) AS total_project'),
                DB::raw('SUM(payment_total) AS payment_total'),
                DB::raw('SUM(reservation_total_sisa) AS reservation_total_sisa'),
            ])
            ->where('checkin_date', '>=', $date_from)
            ->where('checkin_date', '<=', $date_to)
            ->first();

        return $summary;
    }

    public static function pembiayaanSummary($date_from = null, $date_to = null)
    {
        $date_from = $date_from == null ? HelperService::dateMinMonth(date('Y-m-d'), 1) : $date_from;
        $date_to = $date_to == null ? date('Y-m-d') : $date_to;

        $summary = DB::table('view_reservation_summary')
            ->select([
                DB::raw('SUM(total_pembiayaan) AS total_pembiayaan'),
            ])
            ->where('checkin_date', '>=', $date_from)
            ->where('checkin_date', '<=', $date_to)
            ->first();

        return $summary;
    }

    public static function vendor($query = false)
    {
        $vendors = DB::table('history_item_services')
            ->select([
                'reservations.checkout_date',
                'reservations.checkin_date',
                'customers.nama as customer_name',
                'vendors.vendor_name',
                DB::raw('vendors.id as vendor_id'),
                DB::raw('reservations.id as reservation_id'),
                DB::raw('COUNT(vendor_items.id) as jumlah_item'),
                DB::raw('
                SUM(IF(
                    vendor_items.item_type = 2, 
                    vendor_items.pembiayaan * history_item_services.qty * reservations.pax * reservations.berapa_malam,
                    vendor_items.pembiayaan * history_item_services.qty
                )) AS total
                '),
            ])
            ->join('reservations', 'reservations.id', '=', 'history_item_services.reservation_id')
            ->join('customers', 'customers.id', '=', 'history_item_services.customer_id')
            ->join('vendor_items', 'vendor_items.id', '=', 'history_item_services.vendor_item_id')
            ->join('vendors', 'vendors.id', '=', 'vendor_items.vendor_id')
            ->groupBy(DB::raw('history_item_services.reservation_id, vendors.id'));

        if ($query == false) {
            return $vendors->get();
        } else {
            return $vendors;
        }
    }

    public static function vendorDetail($query = false)
    {
        $vendors = DB::table('history_item_services')
            ->select([
                'history_item_services.is_include_paket',
                'history_item_services.qty',
                'history_item_services.durasi',
                'vendor_items.nama_barang',
                'vendor_items.pembiayaan',
                DB::raw('
                    IF(
                        vendor_items.item_type = 2, 
                        vendor_items.pembiayaan * history_item_services.qty * reservations.berapa_malam,
                        vendor_items.pembiayaan * history_item_services.qty
                    ) AS total
                '),
                DB::raw('
                    IF(
                        vendor_items.item_type = 2, 
                        "Sewa",
                        "Habis"
                    ) AS item_type_name
                '),
            ])
            ->join('reservations', 'reservations.id', '=', 'history_item_services.reservation_id')
            ->join('vendor_items', 'vendor_items.id', '=', 'history_item_services.vendor_item_id');

        if ($query == false) {
            return $vendors->get();
        } else {
            return $vendors;
        }
    }

    public static function a3($query = false)
    {
        $query = DB::table('view_a3_summary')
            ->select([
                '*',
            ]);

        if ($query == false) {
            return $query->get();
        } else {
            return $query;
        }
    }

    public static function reservationSummary()
    {
        $summary = DB::table('view_reservation_summary')
            ->select(['*'])
            ->get();

        return $summary;
    }
}
