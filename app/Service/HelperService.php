<?php

namespace App\Service;

class HelperService
{
    public static function dateAddDays(string $date, int $days): string
    {
        return date('Y-m-d', strtotime($date . ' +' . $days . ' day'));
    }

    public static function dateMinDays(string $date, int $days): string
    {
        return date('Y-m-d', strtotime($date . ' -' . $days . ' day'));
    }

    public static function dateAddMonth(string $date, int $month): string
    {
        return date('Y-m-d', strtotime($date . ' +' . $month . ' month'));
    }

    public static function dateMinMonth(string $date, int $month): string
    {
        return date('Y-m-d', strtotime($date . ' -' . $month . ' month'));
    }

    public static function humanDate(string $date): string
    {
        return $date == null ? '-' : date('d M Y', strtotime($date));
    }
}
