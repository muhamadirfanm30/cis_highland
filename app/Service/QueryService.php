<?php

namespace App\Service;

use Illuminate\Support\Facades\DB;

class QueryService
{
    public static function getLastId($table_name)
    {
        $row = DB::table($table_name)->orderBy('id', 'desc')->first();
        return $row == null ? 1 : $row->id + 1;
    }
}
