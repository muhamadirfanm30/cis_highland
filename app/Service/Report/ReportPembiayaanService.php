<?php

namespace App\Service\Report;

use App\Models\Reservation;
use App\Models\VendorItem;
use App\Service\HelperService;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;

class ReportPembiayaanService
{
    public static function export($date_from, $date_to, $reservations, $items, $itemHTM)
    {
        // style
        $style_header_title = [
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ],
            'font'      => [
                'bold' => true,
                'size' => 14,
            ],
        ];

        $style_header = [
            'font'      => [
                'bold' => true,
            ],
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
            ),
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                'vertical'   => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
            ],
        ];

        $style_border = array(
            'borders' => array(
                'allBorders' => array(
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ),
            ),
        );

        $objPHPExcel = new Spreadsheet();

        // Set document properties
        $objPHPExcel->getProperties()->setCreator('Deni Alpian')
            ->setLastModifiedBy('Deni Alpian')
            ->setTitle('Office 2007 XLSX Test Document')
            ->setSubject('Office 2007 XLSX Test Document')
            ->setDescription('Test document for Office 2007 XLSX, generated using PHP classes.')
            ->setKeywords('office 2007 openxml php')
            ->setCategory('truck_model result');

        $header1       = 1;
        $header11      = 2;

        $header2 = 3;
        $header3 = 4;
        $no      = 1;

        $start_add_data = 4;

        // HEADER
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A' . $header1, 'LAPORAN PEMBIAYAAN');
        // HEADER
        $headerTitile = 'PERIODE : ' . HelperService::humanDate($date_from) . ' sd ' . HelperService::humanDate($date_to);
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A' . $header11, $headerTitile);

        // Add some data
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A' . $header2, 'NO')
            ->setCellValue('B' . $header2, 'NAMA CLIENT')
            ->setCellValue('C' . $header2, 'JENIS PAKET')
            ->setCellValue('D' . $header2, 'JUMLAH HARI');

        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('A3:A5');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('B3:B5');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('C3:C5');
        $objPHPExcel->setActiveSheetIndex(0)->mergeCells('D3:D5');

        if ($itemHTM != null) {
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' . $header2, $itemHTM->nama_barang);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' . $header2 + 1, $itemHTM->pembiayaan);
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('E' . $header2 + 2, 'PAX');
            $objPHPExcel->setActiveSheetIndex(0)->setCellValue('F' . $header2 + 2, 'JUMLAH');
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E' . $header2 . ':' . 'F' . $header2);
            $objPHPExcel->setActiveSheetIndex(0)->mergeCells('E' . ($header2 + 1) . ':' . 'F' . ($header2 + 1));
        }

        $item_alpabet = ($itemHTM != null) ? 'G' : 'F';
        foreach ($items as $item) {
            // Add some data
            $objPHPExcel
                ->setActiveSheetIndex(0)
                ->setCellValue($item_alpabet . $header2, strtoupper($item->nama_barang));
            $objPHPExcel
                ->setActiveSheetIndex(0)
                ->setCellValue($item_alpabet . ($header2 + 1), $item->pembiayaan);

            $merge1 = $item_alpabet . $header2 . ':';
            $merge2 = $item_alpabet . ($header2 + 1) . ':';

            $objPHPExcel
                ->setActiveSheetIndex(0)
                ->setCellValue($item_alpabet . ($header2 + 2), 'QTY');

            $item_alpabet++;
            $merge1 .= $item_alpabet . $header2;
            $merge2 .= $item_alpabet . ($header2 + 1);

            $objPHPExcel
                ->setActiveSheetIndex(0)
                ->setCellValue($item_alpabet . ($header2 + 2), 'JUMLAH');
            $item_alpabet++;

            $objPHPExcel
                ->setActiveSheetIndex(0)
                ->mergeCells($merge1);

            $objPHPExcel
                ->setActiveSheetIndex(0)
                ->mergeCells($merge2);
        }

        // Add some data
        $objPHPExcel
            ->setActiveSheetIndex(0)
            ->setCellValue($item_alpabet . $header2, 'TOTAL');
        $objPHPExcel
            ->setActiveSheetIndex(0)
            ->mergeCells($item_alpabet . $header2 . ':' . $item_alpabet . '5');

        // add style
        $objPHPExcel
            ->setActiveSheetIndex(0)
            ->getStyle('A' . $header2 . ':' . $item_alpabet . $header2 + 2)
            ->applyFromArray($style_header);

        $objPHPExcel
            ->setActiveSheetIndex(0)
            ->mergeCells('A' . $header1 . ':' . $item_alpabet . $header1);
        $objPHPExcel
            ->setActiveSheetIndex(0)
            ->mergeCells('A' . $header11 . ':' . $item_alpabet . $header11);

        // add style
        $objPHPExcel
            ->setActiveSheetIndex(0)
            ->getStyle('A' . $header1 . ':' . $item_alpabet . $header11)
            ->applyFromArray($style_header_title);

        $counter_row_start = 6;
        foreach ($reservations as $key => $reservation) {
            $total_row = 0;
            // Add some data
            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $counter_row_start, ($key + 1))
                ->setCellValue('B' . $counter_row_start, $reservation->customer->nama)
                ->setCellValue('C' . $counter_row_start, $reservation->package->nama_paket)
                ->setCellValue('D' . $counter_row_start, $reservation->jumlah_hari);

            if ($itemHTM != null) {
                $total_row_biaya = ($reservation->pax * $reservation->jumlah_hari * $itemHTM->pembiayaan);
                $objPHPExcel
                    ->setActiveSheetIndex(0)
                    ->setCellValue('E' . $counter_row_start, $reservation->pax);
                $objPHPExcel
                    ->setActiveSheetIndex(0)
                    ->setCellValue('F' . $counter_row_start, $total_row_biaya);

                $total_row += $total_row_biaya;
            }

            $alpabet_mulai_dari = ($itemHTM != null) ? 'G' : 'F';
            foreach ($items as $item) {
                $detailItem = $reservation->details->where('vendor_item_id', $item->id)->first();
                if ($detailItem == null) {
                    $objPHPExcel
                        ->setActiveSheetIndex(0)
                        ->setCellValue($alpabet_mulai_dari . $counter_row_start, 0);
                    $alpabet_mulai_dari++;
                    $objPHPExcel
                        ->setActiveSheetIndex(0)
                        ->setCellValue($alpabet_mulai_dari . $counter_row_start, 0);
                    $alpabet_mulai_dari++;
                } else {
                    $objPHPExcel
                        ->setActiveSheetIndex(0)
                        ->setCellValue($alpabet_mulai_dari . $counter_row_start, $detailItem->qty);
                    $alpabet_mulai_dari++;
                    $objPHPExcel
                        ->setActiveSheetIndex(0)
                        ->setCellValue($alpabet_mulai_dari . $counter_row_start, ($item->pembiayaan * $detailItem->qty));
                    $alpabet_mulai_dari++;
                    $total_row += ($item->pembiayaan * $detailItem->qty);
                }
            }
            $objPHPExcel
                ->setActiveSheetIndex(0)
                ->setCellValue($alpabet_mulai_dari . $counter_row_start, $total_row);

            $alpabet_mulai_dari++;
            $counter_row_start++;
        }

        // add style
        $objPHPExcel
            ->setActiveSheetIndex(0)
            ->getStyle('A6:' . $item_alpabet . ($counter_row_start - 1))
            ->applyFromArray($style_border);

        $filename = 'LAPORAN_PEMBIAYAAN.xls';

        // Redirect output to a client’s web browser (Xls)
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="' . $filename . '"');
        header('Cache-Control: max-age=0');
        // If you're serving to IE 9, then the following may be needed
        header('Cache-Control: max-age=1');

        // If you're serving to IE over SSL, then the following may be needed
        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        $writer = IOFactory::createWriter($objPHPExcel, 'Xls');
        $writer->save('php://output');
        exit;
    }
}
