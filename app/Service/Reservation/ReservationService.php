<?php

namespace App\Service\Reservation;

use App\Models\MapConfig;
use App\Models\Reservation;
use Illuminate\Support\Facades\DB;

class ReservationService
{
    public static function cencelAllMoreThen(int $jam)
    {
        $current_date = date('Y-m-d H:i:s');

        $reservations = Reservation::query()
            ->with([
                'capmsite_block_site',
                'detail_reservasi.vendor_item',
            ])
            ->where('status', 'Checkin')
            ->where('checkin', '<', $current_date)
            ->where('checkin', '>', DB::raw('DATE_SUB(checkin, INTERVAL ' . $jam . ' hour)'))
            ->get();

        foreach ($reservations as $reservation) {
            foreach ($reservation->detail_reservasi as $detail) {
                if ($detail->vendor_item->item_type == 2) {
                    $reservation->capmsite_block_site->update([
                        'capacity' => $reservation->capmsite_block_site->capacity + $detail->qty,
                    ]);
                }
                $detail->vendor_item->update([
                    'stok' => $detail->vendor_item->stok + $detail->qty,
                ]);
            }
            $reservation->update([
                'status' => 'cencel',
            ]);
        }
    }

    public static function checkoutAll()
    {
        $current_date = date('Y-m-d H:i:s');

        return $reservations = Reservation::query()
            ->with([
                'capmsite_block_site',
                'detail_reservasi.vendor_item',
            ])
            ->where('status', 'Checkin')
            ->where('checkout', '>', $current_date)
            ->update([
                'status' => 'checkout',
            ]);
    }

    public static function summary($id)
    {
        return DB::table('view_reservation_summary')->where('id', $id)->first();
    }

    public static function query()
    {
        return DB::table('view_reservation_summary');
    }

    public static function mapResource()
    {
        $mapConfig = MapConfig::getActiveMap();

        $reservations = DB::table(DB::raw('campsite_block_history_transactions as cbht'))
            ->select([
                'cbht.capmsite_block_site_id',
                'campsite_block_sites.coordinate_x',
                'campsite_block_sites.coordinate_y',
            ])
            ->join('reservations', 'reservations.id', '=', 'cbht.reservation_id')
            ->join('campsite_block_sites', 'campsite_block_sites.id', '=', 'cbht.capmsite_block_site_id')
            ->where(DB::raw('DATE(reservations.checkin_date)'), date('Y-m-d'))
            ->groupBy('cbht.capmsite_block_site_id')
            ->get();

        $markers = [];
        foreach ($reservations as $reservation) {
            $markers[] = [
                'src' => $mapConfig->map_marker_file_src,
                'x' => $reservation->coordinate_x,
                'y' => $reservation->coordinate_y,
                'draggable' => $mapConfig->map_marker_draggable == 1 ? true : false,
                'campsite' => $reservation
            ];
        }
        $maps = [
            'src' => $mapConfig->map_file_src,
            'rate' => $mapConfig->map_rate,
            'width' => $mapConfig->map_width,
            'height' => $mapConfig->map_height,
            'max' => $mapConfig->map_max,
            'markers' => $markers,
            'marker_size' => $mapConfig->map_marker_size,
        ];
        return $maps;
    }
}
