<?php

namespace App\Service\User;

use App\Models\User;
use Illuminate\Http\Request;

class UserUpdateService
{
    public function updateById($id, array $data)
    {
        $user = User::where('id', $id)->firstOrFail();

        $user->update([
            'name' => $data['name'],
            'email' => $data['email'],
        ]);

        return $user;
    }

    public function updateByIdWithRequestObject($id, Request $request)
    {
        $user = User::where('id', $id)->firstOrFail();

        $user->update([
            'name' => $request->name,
            'email' => $request->email,
        ]);

        return $user;
    }
}
