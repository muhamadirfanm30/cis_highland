<?php

if (!function_exists('thousand_separator')) {
    /**
     * Get the database path.
     *
     * @param  string  $path
     * @return string
     */
    function thousand_separator($value = null)
    {
        return $value == null ? 0 : number_format($value);
    }
}
