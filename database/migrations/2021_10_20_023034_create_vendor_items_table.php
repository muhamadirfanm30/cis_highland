<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_items', function (Blueprint $table) {
            $table->id();
            $table->string('nama_barang');
            $table->integer('stok');
            $table->integer('harga_jual');
            $table->integer('pembiayaan');
            $table->integer('item_type');
            $table->integer('category_item')->nullable();
            $table->timestamps();

            $table->bigInteger('vendor_id')->unsigned()->index();
            $table->foreign('vendor_id')
                ->references('id')
                ->on('vendors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_items');
    }
}
