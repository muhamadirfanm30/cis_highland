<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampsiteBlocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campsite_blocks', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->timestamps();

            $table->bigInteger('campsite_id')->unsigned()->index();
            $table->foreign('campsite_id')
                ->references('id')
                ->on('campsites');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campsite_blocks');
    }
}
