<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampsiteBlockSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campsite_block_sites', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->integer('capacity');
            $table->text('note');
            $table->string('category')->nullable();
            $table->string('type_area')->nullable();
            $table->integer('coordinate_x')->nullable();
            $table->integer('coordinate_y')->nullable();
            $table->timestamps();

            $table->bigInteger('campsite_block_id')->unsigned()->index();
            $table->foreign('campsite_block_id')
                ->references('id')
                ->on('campsite_blocks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campsite_block_sites');
    }
}
