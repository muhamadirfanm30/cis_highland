<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateImagesCampBlockSitesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('images_camp_block_sites', function (Blueprint $table) {
            $table->id();
            
            $table->text('image_campsite');
            $table->bigInteger('camp_block_site_id')->unsigned()->index()->nullable();
            $table->foreign('camp_block_site_id')
                ->references('id')
                ->on('campsite_block_sites');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('images_camp_block_sites');
    }
}
