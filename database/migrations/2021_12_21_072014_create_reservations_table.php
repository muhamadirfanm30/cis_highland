<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->id();
            $table->dateTime('checkin_date');
            $table->dateTime('checkout_date');
            $table->tinyInteger('berapa_malam');
            $table->string('pax');
            $table->string('plat_nomor')->nullable();
            $table->string('code_invoice');
            $table->string('nilai_project')->nullable();
            $table->string('total_pembiayaan')->nullable();
            $table->integer('capmsite_block_site_id')->nullable();
            $table->text('note')->nullable();
            $table->integer('voucer_id')->nullable();
            $table->integer('flag_new_additional_item')->nullable();
            $table->date('read_at')->nullable();
            $table->integer('is_tmp')->nullable();
            $table->timestamps();

            $table->bigInteger('reservation_status_id')->unsigned()->index();
            $table->foreign('reservation_status_id')
                ->references('id')
                ->on('reservation_statuses');

            $table->bigInteger('payment_status_id')->unsigned()->index();
            $table->foreign('payment_status_id')
                ->references('id')
                ->on('payment_statuses');

            $table->bigInteger('customer_id')->unsigned()->index();
            $table->foreign('customer_id')
                ->references('id')
                ->on('customers');

            $table->bigInteger('id_penanggung_jawab')->unsigned()->index()->nullable();
            $table->foreign('id_penanggung_jawab')
                ->references('id')
                ->on('users');

            $table->bigInteger('package_id')->unsigned()->index();
            $table->foreign('package_id')
                ->references('id')
                ->on('packages');

            $table->bigInteger('bussiness_id')->unsigned()->index()->nullable();
            $table->foreign('bussiness_id')
                ->references('id')
                ->on('bussinesses');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
