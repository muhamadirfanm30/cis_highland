<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCostDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cost_details', function (Blueprint $table) {
            $table->id();
            $table->integer('cost_total');
            $table->text('cost_note');
            $table->timestamps();

            $table->bigInteger('cost_id')->unsigned()->index();
            $table->foreign('cost_id')
                ->references('id')
                ->on('costs');

            $table->bigInteger('cost_category_id')->unsigned()->index();
            $table->foreign('cost_category_id')
                ->references('id')
                ->on('cost_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cost_details');
    }
}
