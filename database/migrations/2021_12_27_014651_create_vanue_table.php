<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVanueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vanues', function (Blueprint $table) {
            $table->id();
            $table->string('vanue_name');
            $table->string('location');
            $table->string('category');
            $table->string('harga');
            $table->text('desc');
            $table->text('information');
            // $table->string();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vanues');
    }
}
