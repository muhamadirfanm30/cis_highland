<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHistoryItemServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('history_item_services', function (Blueprint $table) {
            $table->id();
            $table->integer('durasi')->nullable();
            $table->dateTime('tgl_penggunaan')->nullable();
            $table->dateTime('tgl_pengembalian')->nullable();
            $table->integer('qty');
            $table->string('item_price')->nullable();
            $table->integer('is_include_paket');
            $table->string('is_new_additional')->nullable();
            $table->timestamps();

            $table->bigInteger('reservation_id')->unsigned()->index();
            $table->foreign('reservation_id')
                ->references('id')
                ->on('reservations');

            $table->bigInteger('vendor_item_id')->unsigned()->index();
            $table->foreign('vendor_item_id')
                ->references('id')
                ->on('vendor_items');

            $table->bigInteger('customer_id')->unsigned()->index();
            $table->foreign('customer_id')
                ->references('id')
                ->on('customers');

            $table->bigInteger('history_item_status_id')->unsigned()->index();
            $table->foreign('history_item_status_id')
                ->references('id')
                ->on('history_item_statuses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('history_item_services');
    }
}
