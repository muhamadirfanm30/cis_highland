<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMapConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('map_configs', function (Blueprint $table) {
            $table->id();
            $table->integer('map_show');
            $table->string('map_src');
            $table->string('map_rate');
            $table->integer('map_width');
            $table->integer('map_height');
            $table->integer('map_max');
            $table->integer('map_marker_size');
            $table->string('map_marker_src');
            $table->integer('map_marker_draggable');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('map_configs');
    }
}
