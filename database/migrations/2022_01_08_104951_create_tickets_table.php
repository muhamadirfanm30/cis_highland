<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('code');
            $table->string('location');
            $table->time('opening_hours');
            $table->time('closing_hours');
            $table->text('description');
            $table->integer('selling_price');
            $table->integer('purchase_price');
            $table->integer('max_pax');
            $table->timestamps();

            $table->bigInteger('vendor_id')->unsigned()->index();
            $table->foreign('vendor_id')
                ->references('id')
                ->on('vendors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
