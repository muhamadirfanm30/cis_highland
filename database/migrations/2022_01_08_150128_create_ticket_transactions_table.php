<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticket_transactions', function (Blueprint $table) {
            $table->id();
            $table->date('transaction_date');
            $table->string('transaction_status');
            $table->integer('transaction_selling_price');
            $table->integer('transaction_purchase_price');
            $table->integer('transaction_pax');
            $table->timestamps();

            $table->bigInteger('user_id')->unsigned()->index();
            $table->foreign('user_id')
                ->references('id')
                ->on('users');

            $table->bigInteger('customer_id')->unsigned()->index();
            $table->foreign('customer_id')
                ->references('id')
                ->on('customers');

            $table->bigInteger('ticket_id')->unsigned()->index();
            $table->foreign('ticket_id')
                ->references('id')
                ->on('tickets');

            $table->bigInteger('reservation_id')->unsigned()->index()->nullable();
            $table->foreign('reservation_id')
                ->references('id')
                ->on('reservations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticket_transactions');
    }
}
