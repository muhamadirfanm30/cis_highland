<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampsiteBlockHistoryTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campsite_block_history_transactions', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->integer('capmsite_block_site_id');
            $table->timestamps();

            $table->bigInteger('reservation_id')->unsigned()->index();
            $table->foreign('reservation_id')
                ->references('id')
                ->on('reservations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campsite_block_history_transactions');
    }
}
