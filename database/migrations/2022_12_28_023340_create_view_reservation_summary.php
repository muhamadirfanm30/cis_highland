<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateViewReservationSummary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE VIEW view_reservation_summary AS
        SELECT 
            reservations.id, 
            reservations.checkin_date, 
            reservations.checkout_date, 
            reservations.berapa_malam, 
            reservations.pax, 
            reservations.plat_nomor, 
            reservations.code_invoice, 
            reservations.note, 
            reservations.reservation_status_id, 
            reservations.payment_status_id, 
            reservations.customer_id, 
            reservations.id_penanggung_jawab, 
            reservations.package_id, 
            reservations.capmsite_block_site_id, 
            reservation_statuses.name AS reservation_status_name, 
            payment_statuses.name AS payment_status_name, 
            customers.nama AS customer_name, 
            customers.telepon AS customer_phone, 
            users.name AS penanggung_jawab, 
            packages.nama_paket AS package_name, 
            packages.harga AS package_price, 
            payment.total AS payment_total, 
            (
                CASE
                    WHEN item_additional.total_penjualan is null THEN packages.harga * pax * reservations.berapa_malam
                    ELSE (packages.harga * pax * reservations.berapa_malam) + item_additional.total_penjualan
                END
                +
                CASE
                    WHEN ticket.selling_price is null THEN 0
                    ELSE ticket.selling_price
                END
            ) AS total_project, 
            (
                CASE
                    WHEN item_additional.total_pembiayaan is null THEN 0
                    ELSE item_additional.total_pembiayaan
                END
                +
                CASE
                    WHEN item_package.total_pembiayaan is null THEN 0
                    ELSE item_package.total_pembiayaan
                END
                +
                CASE
                    WHEN ticket.purchase_price is null THEN 0
                    ELSE ticket.purchase_price
                END
            ) AS total_pembiayaan, 
            item_additional.total_pembiayaan AS item_additional_pembiayaan, 
            item_additional.total_penjualan AS item_additional_penjualan, 
            item_package.total_pembiayaan AS item_package_pembiayaan, 
            item_package.total_penjualan AS item_package_penjualan, 
            (
                packages.harga * pax * reservations.berapa_malam
                +
                CASE
                    WHEN item_additional.total_penjualan is null THEN 0
                    ELSE item_additional.total_penjualan
                END
                +
                CASE
                    WHEN ticket.selling_price is null THEN 0
                    ELSE ticket.selling_price
                END
                -
                CASE
                    WHEN payment.total is null THEN 0
                    ELSE payment.total
                END
            ) AS reservation_total_sisa 
        FROM 
            reservations 
        INNER JOIN reservation_statuses ON reservation_statuses.id = reservations.reservation_status_id 
        INNER JOIN payment_statuses ON payment_statuses.id = reservations.payment_status_id 
        INNER JOIN customers ON customers.id = reservations.customer_id 
        INNER JOIN users ON users.id = reservations.id_penanggung_jawab 
        INNER JOIN packages ON packages.id = reservations.package_id 
        LEFT JOIN (
            SELECT 
                payments.reservation_id, 
                SUM(payments.payment_total) AS total 
            FROM 
                payments 
            GROUP BY 
                payments.reservation_id
        ) AS payment ON payment.reservation_id = reservations.id 
        LEFT JOIN (
            SELECT 
                ticket_transactions.reservation_id, 
                SUM(ticket_transactions.transaction_selling_price) AS selling_price, 
                SUM(ticket_transactions.transaction_purchase_price) AS purchase_price 
            FROM 
                ticket_transactions 
            WHERE ticket_transactions.reservation_id IS NOT NULL
            GROUP BY 
                ticket_transactions.reservation_id
        ) AS ticket ON ticket.reservation_id = reservations.id 
        LEFT JOIN (
            SELECT 
                history_item_services.reservation_id, 
                SUM(
                    CASE
                        WHEN vendor_items.item_type = 2 THEN vendor_items.pembiayaan * history_item_services.qty * reservations.berapa_malam
                        ELSE vendor_items.pembiayaan * history_item_services.qty
                    END
                ) AS total_pembiayaan, 
                SUM(
                    CASE
                        WHEN vendor_items.item_type = 2 THEN vendor_items.harga_jual * history_item_services.qty * reservations.berapa_malam
                        ELSE vendor_items.harga_jual * history_item_services.qty
                    END
                ) AS total_penjualan 
            FROM 
                history_item_services 
            INNER JOIN vendor_items ON vendor_items.id = history_item_services.vendor_item_id 
            INNER JOIN reservations ON reservations.id = history_item_services.reservation_id 
            WHERE 
                history_item_services.is_include_paket = 0 
            GROUP BY 
                history_item_services.reservation_id
        ) AS item_additional ON item_additional.reservation_id = reservations.id 
        LEFT JOIN (
            SELECT 
                history_item_services.reservation_id, 
                SUM(
                    CASE
                        WHEN vendor_items.item_type = 2 THEN vendor_items.pembiayaan * history_item_services.qty * reservations.berapa_malam
                        ELSE vendor_items.pembiayaan * history_item_services.qty
                    END
                ) AS total_pembiayaan, 
                SUM(
                    CASE
                        WHEN vendor_items.item_type = 2 THEN vendor_items.harga_jual * history_item_services.qty * reservations.berapa_malam
                        ELSE vendor_items.harga_jual * history_item_services.qty
                    END
                ) AS total_penjualan 
            FROM 
                history_item_services 
            INNER JOIN vendor_items ON vendor_items.id = history_item_services.vendor_item_id 
            INNER JOIN reservations ON reservations.id = history_item_services.reservation_id 
            WHERE 
                history_item_services.is_include_paket = 1 
            GROUP BY 
                history_item_services.reservation_id
        ) AS item_package ON item_package.reservation_id = reservations.id
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_reservation_summary');
    }
}
