<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

class CreateViewA3summary extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::statement("
        CREATE VIEW view_a3_summary AS
        SELECT 
            reservation_week_period.total_project, 
            DATE(reservation_week_period.first_week) first_week, 
            DATE(reservation_week_period.end_week) end_week, 
            detail_costs.cost_total 
        FROM (
            SELECT 
                SUM(view_reservation_summary.total_project) AS total_project, 
                DATE(view_reservation_summary.checkin_date) - INTERVAL WEEKDAY(DATE(view_reservation_summary.checkin_date)) DAY as first_week, 
                DATE(view_reservation_summary.checkin_date) + INTERVAL ( 6 - WEEKDAY(DATE(view_reservation_summary.checkin_date)) ) DAY as end_week 
            FROM 
                view_reservation_summary 
            GROUP BY 
                first_week
        ) AS reservation_week_period 
        LEFT JOIN costs ON reservation_week_period.first_week = costs.date_from AND reservation_week_period.end_week = costs.date_to 
        LEFT JOIN (
            SELECT 
                cost_details.cost_id, 
                SUM(cost_details.cost_total) AS cost_total 
            FROM 
                cost_details 
            GROUP BY 
                cost_details.cost_id
        ) as detail_costs ON detail_costs.cost_id = costs.id
        ");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('view_reservation_summary');
    }
}
