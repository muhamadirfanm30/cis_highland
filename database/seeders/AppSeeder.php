<?php

namespace Database\Seeders;

use App\Models\Bussiness;
use App\Models\Campsite;
use App\Models\CampsiteBlock;
use App\Models\CampsiteBlockSite;
use App\Models\Cost;
use App\Models\CostCategory;
use App\Models\CostDetail;
use App\Models\Customer;
use App\Models\HistoryItemService;
use App\Models\HistoryItemStatus;
use App\Models\MapConfig;
use App\Models\Package;
use App\Models\PackageItem;
use App\Models\PaymentStatus;
use App\Models\Reservation;
use App\Models\ReservationStatus;
use App\Models\VendorItem;
use App\Models\Vendors;
use App\Service\User\UserCreateService;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AppSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $userCreateService = new UserCreateService;

        $user = $userCreateService->create([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => 'password',
        ]);

        $userManager = $userCreateService->create([
            'name' => 'manager-area',
            'email' => 'manager-area@admin.com',
            'password' => 'password',
        ]);

        $userLanding = $userCreateService->create([
            'name' => 'landing-page',
            'email' => 'landing-page@admin.com',
            'password' => 'password',
        ]);

        $userCS = $userCreateService->create([
            'name' => 'customer-service-finance',
            'email' => 'customer-service-finance@admin.com',
            'password' => 'password',
        ]);

        $userVendor = $userCreateService->create([
            'name' => 'vendor',
            'email' => 'vendor@admin.com',
            'password' => 'password',
        ]);

        $userWR = $userCreateService->create([
            'name' => 'warehouse',
            'email' => 'warehouse@admin.com',
            'password' => 'password',
        ]);

        $userCheck = $userCreateService->create([
            'name' => 'checker',
            'email' => 'checker@admin.com',
            'password' => 'password',
        ]);

        $userVisitor = $userCreateService->create([
            'name' => 'pengunjung',
            'email' => 'pengunjung@admin.com',
            'password' => 'password',
        ]);

        $userPIC = $userCreateService->create([
            'name' => 'pic-event-gathering',
            'email' => 'pic-event-gathering@admin.com',
            'password' => 'password',
        ]);

        $userMarketing = $userCreateService->create([
            'name' => 'marketing',
            'email' => 'marketing@admin.com',
            'password' => 'password',
        ]);

        $this->permissions();
        $this->roles();
        $this->bussinessSeed();
        $this->customerSeed();
        $this->vendorSeed();
        $this->campsiteSeed();
        $this->statusSeed();
        $this->packageSeed();
        // $this->reservasi();
        $this->costCategories();

        $user->assignRole(Role::where('name', 'admin')->firstOrFail());
        $userManager->assignRole(Role::where('name', 'manager-area')->firstOrFail());
        $userLanding->assignRole(Role::where('name', 'landing-page')->firstOrFail());
        $userCS->assignRole(Role::where('name', 'customer-service-finance')->firstOrFail());
        $userVendor->assignRole(Role::where('name', 'vendor')->firstOrFail());
        $userWR->assignRole(Role::where('name', 'warehouse')->firstOrFail());
        $userCheck->assignRole(Role::where('name', 'checker')->firstOrFail());
        $userVisitor->assignRole(Role::where('name', 'pengunjung')->firstOrFail());
        $userPIC->assignRole(Role::where('name', 'pic-event-gathering')->firstOrFail());
        $userMarketing->assignRole(Role::where('name', 'marketing')->firstOrFail());
    }

    public function permissions()
    {
        $moduls = [
            'USER', 'ROLE', 'PERMISSION', 'BUSSINESS',

            'PACKAGE',
            'TICKET',
            'COST',

            'DASHBOARD',

            'RESERVASI', 'INVOICE', 'VENDOR', 'VENDOR-ITEM', 'PRODUK-VENDOR', 'INVENTORY',
            'PAKET', 'LAPORAN-A3', 'LEVEL', 'CUSTOMER',
            'KATEGORI-CAMPSITE', 'CAMPSITE', 'SEUP-FEE', 'LANDING-PAGE-AND-NEWS-CIS',
            'VOUCHER', 'PROFILE-USER',

            'LAPORAN-HARIAN', 'LAPORAN-MINGGUAN', 'LAPORAN-BULANAN',

            'HISTORY-PRODUK', 'LAPORAN-VENDOR',

            'HISTORY-PELAYANAN', 'ADDITIONAL-PAKET-OR-PRODUK',

            'PEMBAYARAN',

            'RESERVASI-GATHERING',

            'CAMPSITE-IMAGE'
        ];
        $results = [];
        foreach ($moduls as $modul) {
            $results[] = ['name' => $modul . '.READ', 'guard_name' => 'web'];
            $results[] = ['name' => $modul . '.CREATE', 'guard_name' => 'web'];
            $results[] = ['name' => $modul . '.UPDATE', 'guard_name' => 'web'];
            $results[] = ['name' => $modul . '.DELETE', 'guard_name' => 'web'];
        }

        Permission::insert($results);
    }

    public function roles()
    {
        $permissions = Permission::all();

        // manager
        $permissionsManagerAreamModuls = [
            'RESERVASI', 'INVOICE', 'VENDOR', 'PRODUK-VENDOR', 'INVENTORY',
            'PAKET', 'LAPORAN-A3', 'user', 'role', 'LEVEL', 'CUSTOMER',
            'KATEGORI-CAMPSITE', 'CAMPSITE', 'SEUP-FEE', 'LANDING-PAGE-AND-NEWS-CIS',
            'VOUCHER', 'PROFILE-USER',
        ];
        $permissionsManagerAreamModulsAkses = [];
        foreach ($permissionsManagerAreamModuls as $modul) {
            $permissionsManagerAreamModulsAkses[] = $modul . '.SHOW';
            $permissionsManagerAreamModulsAkses[] = $modul . '.CREATE';
            $permissionsManagerAreamModulsAkses[] = $modul . '.EDIT';
            $permissionsManagerAreamModulsAkses[] = $modul . '.DELETE';
        }
        $permissionsManagerArea = Permission::whereIn('name', $permissionsManagerAreamModulsAkses)->get();

        // landing page
        $permissionsLandingPage = Permission::whereIn('name', ['LANDING-PAGE-AND-NEWS-CIS'])->get();

        // CS
        $permissionsCustomerServiceModuls = [
            'RESERVASI', 'INVOICE', 'VOUCHER',
            'LAPORAN-HARIAN', 'LAPORAN-MINGGUAN', 'LAPORAN-BULANAN', 'PROFILE-USER'
        ];
        $permissionsCustomerServiceModulsAkses = [];
        foreach ($permissionsCustomerServiceModuls as $modul) {
            switch ($modul) {
                case 'VOUCHER':
                    $permissionsCustomerServiceModulsAkses[] = $modul . '.SHOW';
                    break;

                default:
                    $permissionsCustomerServiceModulsAkses[] = $modul . '.SHOW';
                    $permissionsCustomerServiceModulsAkses[] = $modul . '.CREATE';
                    $permissionsCustomerServiceModulsAkses[] = $modul . '.EDIT';
                    $permissionsCustomerServiceModulsAkses[] = $modul . '.DELETE';
                    break;
            }
        }
        $permissionsCustomerService = Permission::whereIn('name', $permissionsCustomerServiceModulsAkses)->get();

        // vendor
        $permissionsVendorModuls = [
            'PRODUK-VENDOR', 'INVENTORY',
            'HISTORY-PRODUK', 'LAPORAN-VENDOR', 'LAPORAN-BULANAN',
        ];
        $permissionsVendorModulsAkses = [];
        foreach ($permissionsVendorModuls as $modul) {
            switch ($modul) {
                case 'LAPORAN-VENDOR':
                    $permissionsVendorModulsAkses[] = $modul . '.SHOW';
                    break;

                default:
                    $permissionsVendorModulsAkses[] = $modul . '.SHOW';
                    $permissionsVendorModulsAkses[] = $modul . '.CREATE';
                    $permissionsVendorModulsAkses[] = $modul . '.EDIT';
                    $permissionsVendorModulsAkses[] = $modul . '.DELETE';
                    break;
            }
        }
        $permissionsVendor = Permission::whereIn('name', $permissionsVendorModulsAkses)->get();

        // warehouse
        $permissionsWarehouseModuls = [
            'INVENTORY', 'HISTORY-PRODUK', 'PROFILE-USER'
        ];
        $permissionsWarehouseModulsAkses = [];
        foreach ($permissionsWarehouseModuls as $modul) {
            $permissionsWarehouseModulsAkses[] = $modul . '.SHOW';
            $permissionsWarehouseModulsAkses[] = $modul . '.CREATE';
            $permissionsWarehouseModulsAkses[] = $modul . '.EDIT';
            $permissionsWarehouseModulsAkses[] = $modul . '.DELETE';
        }
        $permissionsWarehouse = Permission::whereIn('name', $permissionsWarehouseModulsAkses)->get();

        // cheker
        $permissionsCheckerModuls = [
            'HISTORY-PELAYANAN', 'ADDITIONAL-PAKET-OR-PRODUK', 'PROFILE-USER'
        ];
        $permissionsCheckerModulsAkses = [];
        foreach ($permissionsCheckerModuls as $modul) {
            switch ($modul) {
                case 'HISTORY-PELAYANAN':
                    $permissionsCheckerModulsAkses[] = $modul . '.SHOW';
                    $permissionsCheckerModulsAkses[] = $modul . '.CREATE';
                    $permissionsCheckerModulsAkses[] = $modul . '.EDIT';
                    break;

                case 'ADDITIONAL-PAKET-OR-PRODUK':
                    $permissionsCheckerModulsAkses[] = $modul . '.SHOW';
                    $permissionsCheckerModulsAkses[] = $modul . '.CREATE';
                    $permissionsCheckerModulsAkses[] = $modul . '.EDIT';
                    break;

                default:
                    $permissionsCheckerModulsAkses[] = $modul . '.SHOW';
                    $permissionsCheckerModulsAkses[] = $modul . '.CREATE';
                    $permissionsCheckerModulsAkses[] = $modul . '.EDIT';
                    $permissionsCheckerModulsAkses[] = $modul . '.DELETE';
                    break;
            }
        }
        $permissionsChecker = Permission::whereIn('name', $permissionsCheckerModulsAkses)->get();

        // pengunjung
        $permissionsPengunjungModuls = [
            'HISTORY-PELAYANAN', 'ADDITIONAL-PAKET-OR-PRODUK', 'PROFILE-USER', 'RESERVASI', 'INVOICE', 'PEMBAYARAN'
        ];
        $permissionsPengunjungModulsAkses = [];
        foreach ($permissionsPengunjungModuls as $modul) {
            switch ($modul) {
                case 'HISTORY-PELAYANAN':
                    $permissionsCheckerModulsAkses[] = $modul . '.SHOW';
                    break;

                case 'INVOICE':
                    $permissionsCheckerModulsAkses[] = $modul . '.SHOW';
                    break;

                case 'PEMBAYARAN':
                    $permissionsCheckerModulsAkses[] = $modul . '.SHOW';
                    break;

                case 'ADDITIONAL-PAKET-OR-PRODUK':
                    $permissionsCheckerModulsAkses[] = $modul . '.SHOW';
                    $permissionsCheckerModulsAkses[] = $modul . '.CREATE';
                    $permissionsCheckerModulsAkses[] = $modul . '.EDIT';
                    break;

                default:
                    $permissionsPengunjungModulsAkses[] = $modul . '.SHOW';
                    $permissionsPengunjungModulsAkses[] = $modul . '.CREATE';
                    $permissionsPengunjungModulsAkses[] = $modul . '.EDIT';
                    $permissionsPengunjungModulsAkses[] = $modul . '.DELETE';
                    break;
            }
        }
        $permissionsPengunjung = Permission::whereIn('name', $permissionsPengunjungModuls)->get();

        // pic-event-gathering
        $permissionsPicEventGatheringModuls = [
            'RESERVASI-GATHERING', 'VOUCHER',
        ];
        $permissionsPicEventGatheringModulsAkses = [];
        foreach ($permissionsPicEventGatheringModuls as $modul) {
            $permissionsPicEventGatheringModulsAkses[] = $modul . '.SHOW';
            $permissionsPicEventGatheringModulsAkses[] = $modul . '.CREATE';
            $permissionsPicEventGatheringModulsAkses[] = $modul . '.EDIT';
            $permissionsPicEventGatheringModulsAkses[] = $modul . '.DELETE';
        }
        $permissionsPicEventGathering = Permission::whereIn('name', $permissionsPicEventGatheringModulsAkses)->get();

        // marketing
        $permissionsMarketingModuls = [
            'RESERVASI', 'PROFILE-USER',
        ];
        $permissionsMarketingModulsAkses = [];
        foreach ($permissionsMarketingModuls as $modul) {
            $permissionsMarketingModulsAkses[] = $modul . '.SHOW';
            $permissionsMarketingModulsAkses[] = $modul . '.CREATE';
            $permissionsMarketingModulsAkses[] = $modul . '.EDIT';
            $permissionsMarketingModulsAkses[] = $modul . '.DELETE';
        }
        $permissionsMarketing = Permission::whereIn('name', $permissionsMarketingModulsAkses)->get();

        Role::create([
            'name' => 'admin',
            'guard_name' => 'web',
        ])->givePermissionTo($permissions);

        Role::create([
            'name' => 'manager-area',
            'guard_name' => 'web',
        ])->givePermissionTo($permissionsManagerArea);

        Role::create([
            'name' => 'landing-page',
            'guard_name' => 'web',
        ])->givePermissionTo($permissionsLandingPage);

        Role::create([
            'name' => 'customer-service-finance',
            'guard_name' => 'web',
        ])->givePermissionTo($permissionsCustomerService);

        Role::create([
            'name' => 'vendor',
            'guard_name' => 'web',
        ])->givePermissionTo($permissionsVendor);

        Role::create([
            'name' => 'warehouse',
            'guard_name' => 'web',
        ])->givePermissionTo($permissionsWarehouse);

        Role::create([
            'name' => 'checker',
            'guard_name' => 'web',
        ])->givePermissionTo($permissionsChecker);

        Role::create([
            'name' => 'pengunjung',
            'guard_name' => 'web',
        ])->givePermissionTo($permissionsPengunjung);

        Role::create([
            'name' => 'pic-event-gathering',
            'guard_name' => 'web',
        ])->givePermissionTo($permissionsPicEventGathering);

        Role::create([
            'name' => 'marketing',
            'guard_name' => 'web',
        ])->givePermissionTo($permissionsMarketing);
    }

    public function bussinessSeed()
    {
        $bussinesses = array(
            array('id' => '1', 'nama_bisnis' => 'Highland Camp Curug Panjang', 'latitude' => '12345', 'longitude' => '5432', 'alamat' => 'cilember', 'created_at' => '2021-11-01 18:30:56', 'updated_at' => '2021-11-06 12:09:50'),
            array('id' => '2', 'nama_bisnis' => 'Highland Camp Curug Naga', 'latitude' => '321', 'longitude' => '123', 'alamat' => 'cilember', 'created_at' => '2021-11-01 18:31:44', 'updated_at' => '2021-11-06 12:10:02')
        );

        Bussiness::insert($bussinesses);
    }

    public function customerSeed()
    {
        $customers = array(
            array('id' => '11', 'nama' => 'irfan', 'telepon' => '234567', 'created_at' => '2021-11-26 15:37:33', 'updated_at' => '2021-11-26 15:37:33', 'bussiness_id' => '1'),
            array('id' => '12', 'nama' => 'Achsan Fadil', 'telepon' => '1', 'created_at' => '2021-12-25 14:13:10', 'updated_at' => '2021-12-25 14:13:10', 'bussiness_id' => NULL),
            array('id' => '13', 'nama' => 'Septy', 'telepon' => '2', 'created_at' => '2021-12-25 14:13:55', 'updated_at' => '2021-12-25 14:13:55', 'bussiness_id' => NULL),
            array('id' => '14', 'nama' => 'deni alpian', 'telepon' => '1', 'created_at' => '2021-12-26 02:19:58', 'updated_at' => '2021-12-26 02:19:58', 'bussiness_id' => NULL),
            array('id' => '15', 'nama' => 'irfan', 'telepon' => '1', 'created_at' => '2021-12-26 02:38:35', 'updated_at' => '2021-12-26 02:38:35', 'bussiness_id' => NULL),
            array('id' => '16', 'nama' => 'test', 'telepon' => '1', 'created_at' => '2021-12-26 02:57:30', 'updated_at' => '2021-12-26 02:57:30', 'bussiness_id' => NULL),
            array('id' => '17', 'nama' => 'user2', 'telepon' => '2', 'created_at' => '2021-12-26 03:00:41', 'updated_at' => '2021-12-26 03:00:41', 'bussiness_id' => NULL)
        );

        Customer::insert($customers);
    }

    public function vendorSeed()
    {
        $vendors = array(
            array('id' => '1', 'user_id' => '5', 'bussiness_id' => '1', 'vendor_name' => 'Bunda Catering', 'note' => 'Penuyedia layanan catering', 'created_at' => '2021-11-01 22:54:47', 'updated_at' => '2021-11-06 12:12:04'),
            array('id' => '2', 'user_id' => '5', 'bussiness_id' => '1', 'vendor_name' => 'Tenda Kang Tent', 'note' => 'Layanan Tenda, Matras dan Sleepig Bag', 'created_at' => '2021-11-04 08:56:53', 'updated_at' => '2021-11-06 12:13:32'),
            array('id' => '3', 'user_id' => '5', 'bussiness_id' => '1', 'vendor_name' => 'Highland Camp', 'note' => 'Venue, Listrik, Air dan Lampu', 'created_at' => '2021-11-04 08:57:33', 'updated_at' => '2021-11-06 12:10:58')
        );

        Vendors::insert($vendors);

        $vendor_items = array(
            array('id' => '1', 'nama_barang' => 'Kayu', 'stok' => '41', 'harga_jual' => '100000', 'pembiayaan' => '50000', 'item_type' => '1', 'category_item' => NULL, 'created_at' => '2021-11-01 22:55:48', 'updated_at' => '2021-12-26 03:00:42', 'vendor_id' => '3'),
            array('id' => '4', 'nama_barang' => 'Tenda Besar', 'stok' => '40', 'harga_jual' => '220000', 'pembiayaan' => '100000', 'item_type' => '2', 'category_item' => NULL, 'created_at' => '2021-11-04 08:58:46', 'updated_at' => '2021-12-26 03:00:42', 'vendor_id' => '2'),
            array('id' => '7', 'nama_barang' => 'HTM', 'stok' => '37', 'harga_jual' => '66000', 'pembiayaan' => '40000', 'item_type' => '1', 'category_item' => NULL, 'created_at' => '2021-11-06 12:21:49', 'updated_at' => '2021-12-26 03:00:42', 'vendor_id' => '3'),
            array('id' => '8', 'nama_barang' => 'Parking campervan', 'stok' => '49', 'harga_jual' => '25000', 'pembiayaan' => '20000', 'item_type' => '1', 'category_item' => NULL, 'created_at' => '2021-11-06 12:22:35', 'updated_at' => '2021-12-25 14:13:55', 'vendor_id' => '3'),
            array('id' => '9', 'nama_barang' => 'Sleeping Bag', 'stok' => '45', 'harga_jual' => '20000', 'pembiayaan' => '10000', 'item_type' => '1', 'category_item' => NULL, 'created_at' => '2021-11-06 12:23:10', 'updated_at' => '2021-12-25 14:13:10', 'vendor_id' => '3'),
            array('id' => '10', 'nama_barang' => 'Kettle Listrik', 'stok' => '41', 'harga_jual' => '15000', 'pembiayaan' => '0', 'item_type' => '2', 'category_item' => NULL, 'created_at' => '2021-11-06 12:23:50', 'updated_at' => '2021-12-26 03:00:42', 'vendor_id' => '3'),
            array('id' => '11', 'nama_barang' => 'FLysheet', 'stok' => '45', 'harga_jual' => '55000', 'pembiayaan' => '40000', 'item_type' => '1', 'category_item' => NULL, 'created_at' => '2021-11-06 12:24:17', 'updated_at' => '2021-12-26 03:00:42', 'vendor_id' => '3'),
            array('id' => '12', 'nama_barang' => 'Hammock', 'stok' => '45', 'harga_jual' => '20000', 'pembiayaan' => '10000', 'item_type' => '1', 'category_item' => NULL, 'created_at' => '2021-11-06 12:24:45', 'updated_at' => '2021-12-26 03:00:42', 'vendor_id' => '3'),
            array('id' => '13', 'nama_barang' => 'Matras', 'stok' => '43', 'harga_jual' => '15000', 'pembiayaan' => '7500', 'item_type' => '1', 'category_item' => NULL, 'created_at' => '2021-11-06 12:26:10', 'updated_at' => '2021-12-26 03:00:42', 'vendor_id' => '3'),
            array('id' => '14', 'nama_barang' => 'Nesting', 'stok' => '42', 'harga_jual' => '0', 'pembiayaan' => '0', 'item_type' => '1', 'category_item' => NULL, 'created_at' => '2021-11-06 12:26:59', 'updated_at' => '2021-12-26 03:00:42', 'vendor_id' => '3'),
            array('id' => '15', 'nama_barang' => 'Kompor Set', 'stok' => '45', 'harga_jual' => '65000', 'pembiayaan' => '45000', 'item_type' => '1', 'category_item' => NULL, 'created_at' => '2021-11-06 12:27:27', 'updated_at' => '2021-12-26 03:00:42', 'vendor_id' => '3'),
            array('id' => '16', 'nama_barang' => 'Gas Hi cook', 'stok' => '48', 'harga_jual' => '25000', 'pembiayaan' => '15000', 'item_type' => '1', 'category_item' => NULL, 'created_at' => '2021-11-06 12:27:48', 'updated_at' => '2021-12-26 03:00:42', 'vendor_id' => '3'),
            array('id' => '17', 'nama_barang' => 'Air Mineral Galon', 'stok' => '40', 'harga_jual' => '25000', 'pembiayaan' => '10000', 'item_type' => '1', 'category_item' => NULL, 'created_at' => '2021-11-06 12:28:18', 'updated_at' => '2021-12-26 03:00:42', 'vendor_id' => '3'),
            array('id' => '18', 'nama_barang' => 'Meja', 'stok' => '45', 'harga_jual' => '0', 'pembiayaan' => '0', 'item_type' => '2', 'category_item' => NULL, 'created_at' => '2021-11-06 12:29:15', 'updated_at' => '2021-12-26 03:00:42', 'vendor_id' => '3'),
            array('id' => '19', 'nama_barang' => 'Kasur Set', 'stok' => '37', 'harga_jual' => '60000', 'pembiayaan' => '25000', 'item_type' => '1', 'category_item' => NULL, 'created_at' => '2021-11-06 12:30:24', 'updated_at' => '2021-12-26 03:00:42', 'vendor_id' => '2'),
            array('id' => '20', 'nama_barang' => 'Catering Family', 'stok' => '50', 'harga_jual' => '110000', 'pembiayaan' => '110000', 'item_type' => '1', 'category_item' => NULL, 'created_at' => '2021-11-06 12:31:10', 'updated_at' => '2021-11-06 12:31:10', 'vendor_id' => '1')
        );

        VendorItem::insert($vendor_items);
    }

    public function campsiteSeed()
    {
        $campsites = array(
            array('id' => '1', 'campsite' => 'Ciputri', 'created_at' => '2021-11-24 12:24:44', 'updated_at' => '2021-11-24 12:24:58'),
            array('id' => '2', 'campsite' => 'Puntang', 'created_at' => '2021-11-24 12:25:10', 'updated_at' => '2021-11-24 12:25:10'),
            array('id' => '3', 'campsite' => 'Batu Tapak', 'created_at' => '2021-11-24 12:25:22', 'updated_at' => '2021-11-24 12:25:22')
        );

        Campsite::insert($campsites);

        $campsite_blocks = array(
            array('id' => '1', 'name' => 'kantor', 'created_at' => '2021-11-27 18:54:42', 'updated_at' => '2021-11-27 18:58:05', 'campsite_id' => '1'),
            array('id' => '2', 'name' => 'Wa Idi', 'created_at' => '2021-11-27 18:58:22', 'updated_at' => '2021-11-27 18:58:22', 'campsite_id' => '1'),
            array('id' => '3', 'name' => 'Ex Pleton', 'created_at' => '2021-11-27 18:58:37', 'updated_at' => '2021-11-27 18:58:37', 'campsite_id' => '1'),
            array('id' => '4', 'name' => 'Puntang', 'created_at' => '2021-11-27 18:58:49', 'updated_at' => '2021-11-27 18:58:49', 'campsite_id' => '2'),
            array('id' => '5', 'name' => 'Batu Tapak', 'created_at' => '2021-11-27 18:59:01', 'updated_at' => '2021-11-27 18:59:01', 'campsite_id' => '3')
        );

        CampsiteBlock::insert($campsite_blocks);

        $campsite_block_sites = array(
            array('id' => '1', 'name' => 'A', 'capacity' => '4', 'note' => 'test', 'created_at' => '2021-11-27 19:28:35', 'updated_at' => '2021-11-27 19:28:35', 'campsite_block_id' => '1'),
            array('id' => '2', 'name' => 'B', 'capacity' => '2', 'note' => 'test', 'created_at' => '2021-11-27 19:28:55', 'updated_at' => '2021-11-27 19:28:55', 'campsite_block_id' => '1'),
            array('id' => '3', 'name' => 'A', 'capacity' => '1', 'note' => 'test', 'created_at' => '2021-11-27 19:29:07', 'updated_at' => '2021-11-27 19:29:07', 'campsite_block_id' => '2'),
            array('id' => '4', 'name' => 'A', 'capacity' => '3', 'note' => 'test', 'created_at' => '2021-11-27 19:29:25', 'updated_at' => '2021-11-27 19:29:25', 'campsite_block_id' => '4'),
            array('id' => '5', 'name' => 'B', 'capacity' => '13', 'note' => 'test', 'created_at' => '2021-11-27 19:29:36', 'updated_at' => '2021-11-27 19:29:36', 'campsite_block_id' => '4')
        );

        CampsiteBlockSite::insert($campsite_block_sites);
    }

    public function statusSeed()
    {
        $ReservationStatus = array(
            array('name' => 'checkin'),
            array('name' => 'checkout'),
            array('name' => 'cancel'),
            array('name' => 'reschedule'),
            array('name' => 'reservasi'),
        );
        ReservationStatus::insert($ReservationStatus);

        $HistoryItemStatus = array(
            array('name' => 'belum-diantar'),
            array('name' => 'diterima'),
        );
        HistoryItemStatus::insert($HistoryItemStatus);

        $PaymentStatus = array(
            array('name' => 'belum-lunas'),
            array('name' => 'lunas'),
        );
        PaymentStatus::insert($PaymentStatus);
    }

    public function packageSeed()
    {
        $packages = array(
            array('id' => '2', 'nama_paket' => 'Mandiri Additional', 'harga' => '66000', 'created_at' => '2021-12-03 14:03:46', 'updated_at' => '2021-12-03 14:03:46', 'bussiness_id' => '1'),
            array('id' => '3', 'nama_paket' => 'Mandiri', 'harga' => '70000', 'created_at' => '2021-12-03 14:04:44', 'updated_at' => '2021-12-03 14:04:44', 'bussiness_id' => '1'),
            array('id' => '4', 'nama_paket' => 'Paket 365 K', 'harga' => '100000', 'created_at' => '2021-12-03 14:05:12', 'updated_at' => '2021-12-03 14:05:12', 'bussiness_id' => '1'),
            array('id' => '5', 'nama_paket' => 'akomodasi 265', 'harga' => '265000', 'created_at' => '2021-12-26 02:16:35', 'updated_at' => '2021-12-26 02:16:35', 'bussiness_id' => '1')
        );
        Package::insert($packages);

        $package_items = array(
            array('id' => '7', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '2', 'vendor_item_id' => '1'),
            array('id' => '8', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '2', 'vendor_item_id' => '4'),
            array('id' => '9', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '2', 'vendor_item_id' => '9'),
            array('id' => '10', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '2', 'vendor_item_id' => '10'),
            array('id' => '11', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '2', 'vendor_item_id' => '13'),
            array('id' => '12', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '2', 'vendor_item_id' => '14'),
            array('id' => '13', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '2', 'vendor_item_id' => '17'),
            array('id' => '14', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '2', 'vendor_item_id' => '18'),
            array('id' => '15', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '3', 'vendor_item_id' => '4'),
            array('id' => '16', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '3', 'vendor_item_id' => '7'),
            array('id' => '17', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '3', 'vendor_item_id' => '8'),
            array('id' => '18', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '3', 'vendor_item_id' => '11'),
            array('id' => '19', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '3', 'vendor_item_id' => '12'),
            array('id' => '20', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '3', 'vendor_item_id' => '13'),
            array('id' => '21', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '3', 'vendor_item_id' => '15'),
            array('id' => '22', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '3', 'vendor_item_id' => '16'),
            array('id' => '23', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '3', 'vendor_item_id' => '17'),
            array('id' => '24', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '3', 'vendor_item_id' => '19'),
            array('id' => '25', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '4', 'vendor_item_id' => '1'),
            array('id' => '26', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '4', 'vendor_item_id' => '4'),
            array('id' => '27', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '4', 'vendor_item_id' => '7'),
            array('id' => '28', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '4', 'vendor_item_id' => '8'),
            array('id' => '29', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '4', 'vendor_item_id' => '9'),
            array('id' => '30', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '4', 'vendor_item_id' => '10'),
            array('id' => '31', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '4', 'vendor_item_id' => '11'),
            array('id' => '32', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '4', 'vendor_item_id' => '13'),
            array('id' => '33', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '4', 'vendor_item_id' => '14'),
            array('id' => '34', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '4', 'vendor_item_id' => '15'),
            array('id' => '35', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '4', 'vendor_item_id' => '17'),
            array('id' => '36', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '4', 'vendor_item_id' => '18'),
            array('id' => '37', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '4', 'vendor_item_id' => '19'),
            array('id' => '38', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '5', 'vendor_item_id' => '1'),
            array('id' => '39', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '5', 'vendor_item_id' => '4'),
            array('id' => '40', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '5', 'vendor_item_id' => '7'),
            array('id' => '41', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '5', 'vendor_item_id' => '10'),
            array('id' => '42', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '5', 'vendor_item_id' => '11'),
            array('id' => '43', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '5', 'vendor_item_id' => '12'),
            array('id' => '44', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '5', 'vendor_item_id' => '13'),
            array('id' => '45', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '5', 'vendor_item_id' => '14'),
            array('id' => '46', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '5', 'vendor_item_id' => '15'),
            array('id' => '47', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '5', 'vendor_item_id' => '16'),
            array('id' => '48', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '5', 'vendor_item_id' => '17'),
            array('id' => '49', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '5', 'vendor_item_id' => '18'),
            array('id' => '50', 'created_at' => NULL, 'updated_at' => NULL, 'package_id' => '5', 'vendor_item_id' => '19')
        );
        PackageItem::insert($package_items);
    }

    public function reservasi()
    {
        $reservations = array(
            array('id' => '4', 'checkin_date' => '2021-12-26 00:00:00', 'checkout_date' => '2021-12-27 00:00:00', 'berapa_malam' => '1', 'pax' => '4', 'plat_nomor' => '1', 'code_invoice' => 'INV-004-12-26-2021', 'nilai_project' => '1060000', 'total_pembiayaan' => '1175000', 'note' => '1', 'voucer_id' => NULL, 'created_at' => '2021-12-26 02:19:58', 'updated_at' => '2021-12-26 02:19:58', 'reservation_status_id' => '1', 'payment_status_id' => '1', 'customer_id' => '14', 'id_penanggung_jawab' => '1', 'package_id' => '5', 'bussiness_id' => NULL, 'capmsite_block_site_id' => '1'),
            array('id' => '5', 'checkin_date' => '2021-12-26 00:00:00', 'checkout_date' => '2021-12-28 00:00:00', 'berapa_malam' => '2', 'pax' => '4', 'plat_nomor' => '1', 'code_invoice' => 'INV-005-12-26-2021', 'nilai_project' => '2120000', 'total_pembiayaan' => '1215000', 'note' => NULL, 'voucer_id' => NULL, 'created_at' => '2021-12-26 02:38:35', 'updated_at' => '2021-12-26 02:38:35', 'reservation_status_id' => '5', 'payment_status_id' => '1', 'customer_id' => '15', 'id_penanggung_jawab' => '1', 'package_id' => '5', 'bussiness_id' => NULL, 'capmsite_block_site_id' => '4'),
            array('id' => '6', 'checkin_date' => '2021-12-07 00:00:00', 'checkout_date' => '2021-12-11 00:00:00', 'berapa_malam' => '4', 'pax' => '3', 'plat_nomor' => '1', 'code_invoice' => 'INV-006-12-26-2021', 'nilai_project' => '3180000', 'total_pembiayaan' => '1550000', 'note' => '1', 'voucer_id' => NULL, 'created_at' => '2021-12-26 02:57:30', 'updated_at' => '2021-12-26 02:57:30', 'reservation_status_id' => '5', 'payment_status_id' => '1', 'customer_id' => '16', 'id_penanggung_jawab' => '1', 'package_id' => '5', 'bussiness_id' => NULL, 'capmsite_block_site_id' => '2'),
            array('id' => '7', 'checkin_date' => '2021-12-27 00:00:00', 'checkout_date' => '2021-12-29 00:00:00', 'berapa_malam' => '2', 'pax' => '3', 'plat_nomor' => '2', 'code_invoice' => 'INV-007-12-26-2021', 'nilai_project' => '1590000', 'total_pembiayaan' => '842500', 'note' => '2', 'voucer_id' => NULL, 'created_at' => '2021-12-26 03:00:42', 'updated_at' => '2021-12-26 03:00:42', 'reservation_status_id' => '5', 'payment_status_id' => '1', 'customer_id' => '17', 'id_penanggung_jawab' => '1', 'package_id' => '5', 'bussiness_id' => NULL, 'capmsite_block_site_id' => '5')
        );
        Reservation::insert($reservations);

        $history_item_services = array(
            array('id' => '34', 'durasi' => '1', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '4', 'vendor_item_id' => '1', 'customer_id' => '14', 'history_item_status_id' => '1'),
            array('id' => '35', 'durasi' => '1', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '4', 'vendor_item_id' => '4', 'customer_id' => '14', 'history_item_status_id' => '1'),
            array('id' => '36', 'durasi' => '1', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '4', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '4', 'vendor_item_id' => '7', 'customer_id' => '14', 'history_item_status_id' => '1'),
            array('id' => '37', 'durasi' => '1', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '4', 'vendor_item_id' => '10', 'customer_id' => '14', 'history_item_status_id' => '1'),
            array('id' => '38', 'durasi' => '1', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '4', 'vendor_item_id' => '11', 'customer_id' => '14', 'history_item_status_id' => '1'),
            array('id' => '39', 'durasi' => '1', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '4', 'vendor_item_id' => '12', 'customer_id' => '14', 'history_item_status_id' => '1'),
            array('id' => '40', 'durasi' => '1', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '4', 'vendor_item_id' => '14', 'customer_id' => '14', 'history_item_status_id' => '1'),
            array('id' => '41', 'durasi' => '1', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '4', 'vendor_item_id' => '15', 'customer_id' => '14', 'history_item_status_id' => '1'),
            array('id' => '42', 'durasi' => '1', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '4', 'vendor_item_id' => '17', 'customer_id' => '14', 'history_item_status_id' => '1'),
            array('id' => '43', 'durasi' => '1', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '4', 'vendor_item_id' => '18', 'customer_id' => '14', 'history_item_status_id' => '1'),
            array('id' => '44', 'durasi' => '1', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '4', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '4', 'vendor_item_id' => '19', 'customer_id' => '14', 'history_item_status_id' => '1'),
            array('id' => '45', 'durasi' => '2', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '5', 'vendor_item_id' => '1', 'customer_id' => '15', 'history_item_status_id' => '1'),
            array('id' => '46', 'durasi' => '2', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '5', 'vendor_item_id' => '4', 'customer_id' => '15', 'history_item_status_id' => '1'),
            array('id' => '47', 'durasi' => '2', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '4', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '5', 'vendor_item_id' => '7', 'customer_id' => '15', 'history_item_status_id' => '1'),
            array('id' => '48', 'durasi' => '2', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '5', 'vendor_item_id' => '10', 'customer_id' => '15', 'history_item_status_id' => '1'),
            array('id' => '49', 'durasi' => '2', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '5', 'vendor_item_id' => '11', 'customer_id' => '15', 'history_item_status_id' => '1'),
            array('id' => '50', 'durasi' => '2', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '5', 'vendor_item_id' => '12', 'customer_id' => '15', 'history_item_status_id' => '1'),
            array('id' => '51', 'durasi' => '2', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '5', 'vendor_item_id' => '14', 'customer_id' => '15', 'history_item_status_id' => '1'),
            array('id' => '52', 'durasi' => '2', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '5', 'vendor_item_id' => '15', 'customer_id' => '15', 'history_item_status_id' => '1'),
            array('id' => '53', 'durasi' => '2', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '5', 'vendor_item_id' => '17', 'customer_id' => '15', 'history_item_status_id' => '1'),
            array('id' => '54', 'durasi' => '2', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '5', 'vendor_item_id' => '18', 'customer_id' => '15', 'history_item_status_id' => '1'),
            array('id' => '55', 'durasi' => '2', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '4', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '5', 'vendor_item_id' => '19', 'customer_id' => '15', 'history_item_status_id' => '1'),
            array('id' => '56', 'durasi' => '4', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '6', 'vendor_item_id' => '1', 'customer_id' => '16', 'history_item_status_id' => '1'),
            array('id' => '57', 'durasi' => '4', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '6', 'vendor_item_id' => '4', 'customer_id' => '16', 'history_item_status_id' => '1'),
            array('id' => '58', 'durasi' => '4', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '3', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '6', 'vendor_item_id' => '7', 'customer_id' => '16', 'history_item_status_id' => '1'),
            array('id' => '59', 'durasi' => '4', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '6', 'vendor_item_id' => '10', 'customer_id' => '16', 'history_item_status_id' => '1'),
            array('id' => '60', 'durasi' => '4', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '6', 'vendor_item_id' => '11', 'customer_id' => '16', 'history_item_status_id' => '1'),
            array('id' => '61', 'durasi' => '4', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '6', 'vendor_item_id' => '12', 'customer_id' => '16', 'history_item_status_id' => '1'),
            array('id' => '62', 'durasi' => '4', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '6', 'vendor_item_id' => '15', 'customer_id' => '16', 'history_item_status_id' => '1'),
            array('id' => '63', 'durasi' => '4', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '6', 'vendor_item_id' => '17', 'customer_id' => '16', 'history_item_status_id' => '1'),
            array('id' => '64', 'durasi' => '4', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '6', 'vendor_item_id' => '18', 'customer_id' => '16', 'history_item_status_id' => '1'),
            array('id' => '65', 'durasi' => '4', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '3', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '6', 'vendor_item_id' => '19', 'customer_id' => '16', 'history_item_status_id' => '1'),
            array('id' => '66', 'durasi' => '2', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '7', 'vendor_item_id' => '1', 'customer_id' => '17', 'history_item_status_id' => '1'),
            array('id' => '67', 'durasi' => '2', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '7', 'vendor_item_id' => '4', 'customer_id' => '17', 'history_item_status_id' => '1'),
            array('id' => '68', 'durasi' => '2', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '7', 'vendor_item_id' => '7', 'customer_id' => '17', 'history_item_status_id' => '1'),
            array('id' => '69', 'durasi' => '2', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '7', 'vendor_item_id' => '10', 'customer_id' => '17', 'history_item_status_id' => '1'),
            array('id' => '70', 'durasi' => '2', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '7', 'vendor_item_id' => '11', 'customer_id' => '17', 'history_item_status_id' => '1'),
            array('id' => '71', 'durasi' => '2', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '7', 'vendor_item_id' => '12', 'customer_id' => '17', 'history_item_status_id' => '1'),
            array('id' => '72', 'durasi' => '2', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '7', 'vendor_item_id' => '13', 'customer_id' => '17', 'history_item_status_id' => '1'),
            array('id' => '73', 'durasi' => '2', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '7', 'vendor_item_id' => '14', 'customer_id' => '17', 'history_item_status_id' => '1'),
            array('id' => '74', 'durasi' => '2', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '7', 'vendor_item_id' => '15', 'customer_id' => '17', 'history_item_status_id' => '1'),
            array('id' => '75', 'durasi' => '2', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '7', 'vendor_item_id' => '16', 'customer_id' => '17', 'history_item_status_id' => '1'),
            array('id' => '76', 'durasi' => '2', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '7', 'vendor_item_id' => '17', 'customer_id' => '17', 'history_item_status_id' => '1'),
            array('id' => '77', 'durasi' => '2', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '7', 'vendor_item_id' => '18', 'customer_id' => '17', 'history_item_status_id' => '1'),
            array('id' => '78', 'durasi' => '2', 'tgl_penggunaan' => NULL, 'tgl_pengembalian' => NULL, 'qty' => '1', 'item_price' => '0', 'is_include_paket' => '1', 'created_at' => NULL, 'updated_at' => NULL, 'reservation_id' => '7', 'vendor_item_id' => '19', 'customer_id' => '17', 'history_item_status_id' => '1')
        );
        HistoryItemService::insert($history_item_services);
    }

    public function costCategories()
    {
        $cost_categories = array(
            array('id' => '1', 'name' => 'tenaga kerja', 'note' => 'tenaga kerja', 'created_at' => '2021-12-07 15:45:26', 'updated_at' => '2021-12-07 15:45:26'),
            array('id' => '2', 'name' => 'opearasional crew', 'note' => 'opearasional crew', 'created_at' => '2021-12-07 15:45:39', 'updated_at' => '2021-12-07 15:45:39')
        );
        CostCategory::insert($cost_categories);

        $costs = array(
            array('id' => '1', 'date_from' => '2021-12-20', 'date_to' => '2021-12-26', 'created_at' => '2021-12-25 14:16:09', 'updated_at' => '2021-12-25 14:16:09')
        );

        Cost::insert($costs);

        $cost_details = array(
            array('id' => '1', 'cost_total' => '50000', 'cost_note' => 'makan', 'created_at' => NULL, 'updated_at' => NULL, 'cost_id' => '1', 'cost_category_id' => '1'),
            array('id' => '2', 'cost_total' => '100000', 'cost_note' => 'crew', 'created_at' => NULL, 'updated_at' => NULL, 'cost_id' => '1', 'cost_category_id' => '2')
        );

        CostDetail::insert($cost_details);

        $map_configs = array(
            array('id' => '1', 'map_show' => '1', 'map_src' => 'map/qyZc0jz7kSAM4VMwLD7xXjGqnNXteL6BwuYtFqwI.png', 'map_rate' => '0.2', 'map_width' => '1200', 'map_height' => '800', 'map_max' => '3000', 'map_marker_size' => '40', 'map_marker_src' => 'map/XXHvmTJK4eA35MyFrl9yyISYoKV4ng7HOVixydP2.svg', 'map_marker_draggable' => '0', 'created_at' => NULL, 'updated_at' => '2022-01-02 03:35:32')
        );

        MapConfig::insert($map_configs);
    }
}
