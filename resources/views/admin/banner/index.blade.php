@extends($master_template)
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <a href="{{ url('/admin/banners/create') }}" class="btn btn-primary add-banners"><i class="fa fa-plus"></i> ADD NEW BANNER</a>
        <table id="table"></table>
    </div>
</div>


<!-- <form id="banners-form">
    <div id="dark-header-modal" class="modal fade" tabindex="-1" aria-labelledby="dark-header-modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="dark-header-modalLabel">Modal Heading
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id">
                    <div class="mb-3">
                        <label>Title</label>
                        <input type="text" name="title" class="form-control" value="" placeholder="enter title" required>
                    </div>
                    <div class="mb-3">
                        <label>Body</label>
                        <input type="text" name="body" class="form-control" value="" placeholder="enter body" required>
                    </div>
                    <div class="mb-3">
                        <label>Image File</label>
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" name="image_file" id="exampleInputFile">
                            <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</form> -->

@endsection

@section('js')
<style>

</style>
<script>
    $(function() {
        var $table = $('#table');
        var $modal = $('#dark-header-modal');
        var $form = new FormService($('#banners-form')).withUploadField(['image_file']);
        var $http = new HttpService({
            formService: $form,
            bootrapTable: $table,
            bootrapModal: $modal,
        });

        $table.bootstrapTable({
            sortName: 'created_at',
            sortOrder: 'ASC',
            url: HelperService.base_url + '/api/admin/banners',
            responseHandler: function(res) {
                return res.data;
            },
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'Title',
                    field: 'title',
                    sortable: true,
                },
                {
                    title: 'Image',
                    formatter: function(value, row, index) {
                        return '<img style="border-radius: 50%;display: inline;width: 2.5rem;" class="table-avatar" src="' + row.image_src + '"/>';
                    }
                },
                {
                    title: 'ACTION',
                    sortable: false,
                    formatter: function(value, row, index) {
                        var link = '';
                        link += '<button type="button" class="btn btn-sm waves-effect btn-xs btn-danger delete">';
                        link += 'Delete';
                        link += '</button>';
                        return link;
                    },
                    events: {
                        'click .edit': function(e, value, row) {
                            $form.emptyFormData();
                            $modal.modal('show')
                            $modal.find('.modal-title').text('UPDATE BANNER');
                            $form.setFormData(row);
                        },
                        'click .delete': function(e, value, row) {
                            HelperService.confirm(function() {
                                $http.delete(`/api/admin/banners/${row.id}`, row)
                            })
                        }
                    }
                },
            ]
        });

        // $('.add-banners').click(function() {
        //     $form.emptyFormData();
        //     $modal.modal('show')
        //     $modal.find('.modal-title').text('ADD BANNER');
        // });

        // $form.onSubmit(function(data, form_data_upload) {
        //     // insert
        //     if (data.id == '') {
        //         $http.post('/api/admin/banners', form_data_upload)
        //     }

        //     // update
        //     if (data.id != '') {
        //         $http.post(`/api/admin/banners/${data.id}`, form_data_upload)
        //     }
        // })
    })
</script>
@endsection