@extends($master_template)
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <!-- <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button> -->
            <a href="{{ url('admin/blog') }}" class="btn btn-primary add-category"> BACK</a>
        </div>
    </div>
    <form action="{{ url('admin/blog/store') }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="">Title</label>
                <input type="text" class="form-control" name="title" placeholder="Title" required>
            </div>
            <div class="col-md-12" style="margin-bottom: 20px;">
                <label for="">Publish Date</label>
                <input type="text" class="form-control checkin" id="checkin" name="date" >
            </div>
            <div class="form-group">
                <label for="">Blog Image</label>
                <input type="file" class="form-control" name="image_blog" placeholder="Title" required>
            </div>
            <div class="form-group">
                <label for="">Writed By</label>
                <input type="text" class="form-control" name="writed_by" placeholder="Writed By" required> 
            </div>
            <div class="form-group">
                <label for="">Authors Photo</label>
                <input type="file" class="form-control" name="authors_photo" placeholder="Title" required>
            </div>
            <div class="form-group">
                <label for="">Description</label>
                <textarea id="body-input" name="desc"></textarea>
            </div>
        </div>
        <div class="card-footer">
            <a href="{{ url('admin/blog') }}" class="btn btn-success">Back</a>
            <button type="submit" class="btn btn-primary">Save Blog</button>
        </div>
    </form>
</div>

@endsection

@section('js')
<!-- include summernote css/js -->
<script src="https://cdn.ckeditor.com/ckeditor5/31.0.0/classic/ckeditor.js"></script>
<script>
    HelperService.datepicker('.checkin')
    HelperService.WYSIWYG('#body-input');
</script>
@endsection