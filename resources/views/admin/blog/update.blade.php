@extends($master_template)
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <!-- <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button> -->
            <a href="{{ url('admin/blog') }}" class="btn btn-primary add-category"> BACK</a>
        </div>
    </div>
    <form action="{{ url('admin/blog/edit/'.$datas->id) }}" method="post" enctype="multipart/form-data">
        @csrf
        <div class="card-body">
            <div class="form-group">
                <label for="">Title</label>
                <input type="text" class="form-control" name="title" value="{{ $datas->title }}" placeholder="Title">
            </div>
            <div class="col-md-12" style="margin-bottom: 20px;">
                <label for="">Publish Date</label>
                <input type="text" class="form-control checkin" value="{{ $datas->date }}" id="checkin" name="date" >
            </div>
            <div class="form-group">
                <label for="">Blog Image</label>
                <input type="file" class="form-control" name="image_blog" placeholder="Title">
                @if(!empty($datas->image_blog))
                    <a href="{{ url('storage/img-blog/'.$datas->image_blog) }}" target="_blank">{{ $datas->image_blog }}</a>
                @endif
            </div>
            <div class="form-group">
                <label for="">Writed By</label>
                <input type="text" class="form-control" name="writed_by" value="{{ $datas->writed_by }}" placeholder="Writed By">
            </div>
            <div class="form-group">
                <label for="">Authors Photo</label>
                <input type="file" class="form-control" name="authors_photo" placeholder="Title">
                @if(!empty($datas->authors_photo))
                    <a href="{{ url('storage/author-image/'.$datas->authors_photo) }}" target="_blank">{{ $datas->authors_photo }}</a>
                @endif
            </div>
            <div class="form-group">
                <label for="">Description</label>
                <textarea id="body-input" name="desc">{{ $datas->desc }}</textarea>
            </div>
        </div>
        <div class="card-footer">
            <a href="{{ url('admin/blog') }}" class="btn btn-success">Back</a>
            <button type="submit" class="btn btn-primary">Save Blog</button>
        </div>
    </form>
</div>

@endsection

@section('js')
<!-- include summernote css/js -->
<script src="https://cdn.ckeditor.com/ckeditor5/31.0.0/classic/ckeditor.js"></script>
<script>
    HelperService.datepicker('.checkin')
    HelperService.WYSIWYG('#body-input');
</script>
@endsection