@extends($master_template)
@section('content')
<!-- Default box -->
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <button type="button" class="btn btn-primary add-campsite-blocks"><i class="fa fa-plus"></i> ADD NEW CAMPSITE BLOCK</button>
        <table id="table"></table>
    </div>
</div>


<form id="campsite-blocks-form">
    <div id="dark-header-modal" class="modal fade" tabindex="-1" aria-labelledby="dark-header-modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="dark-header-modalLabel">Modal Heading
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" />
                    <div class="mb-3">
                        <label>Campsite</label>
                        <select class="form-control col-12" name="campsite_id" required>
                            <option selected value="">Choose...</option>
                            @foreach($getCampsite as $campsites)
                            <option value="{{ $campsites->id }}">{{ $campsites->campsite }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label>Block Name</label>
                        <input type="text" name="name" class="form-control" value="" placeholder="Block">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('js')
<style>

</style>
<script>
    $(function() {
        var $table = $('#table');
        var $modal = $('#dark-header-modal');
        var $form = new FormService($('#campsite-blocks-form'));
        var $http = new HttpService({
            formService: $form,
            bootrapTable: $table,
            bootrapModal: $modal,
        });

        HelperService.loadBussinesSelect2();

        $table.bootstrapTable({
            sortName: 'created_at',
            sortOrder: 'ASC',
            url: HelperService.base_url + '/api/admin/campsite-blocks',
            responseHandler: function(res) {
                return res.data;
            },
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'Campsite',
                    field: 'campsite_id',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return row.campsite.campsite;
                    }
                },
                {
                    title: 'Block name',
                    field: 'name',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return row.name;
                    }
                },
                {
                    title: 'ACTION',
                    formatter: function(value, row, index) {
                        var link = '';
                        link += '<button type="button" class="btn btn-sm btn-xs btn-success edit">';
                        link += 'Edit';
                        link += '</button>';
                        link += '<button type="button" class="btn btn-sm btn-xs btn-danger delete">';
                        link += 'Delete';
                        link += '</button>';
                        return link;
                    },
                    events: {
                        'click .edit': function(e, value, row) {
                            $form.emptyFormData();
                            $modal.modal('show')
                            $modal.find('.modal-title').text('UPDATE CAMPSITE BLOCK');
                            $form.setFormData(row);
                        },
                        'click .delete': function(e, value, row) {
                            $http.delete(`/api/admin/block-camp/${row.id}/delete`)
                                .then(function(resp) {
                                    $table.bootstrapTable('refresh');
                                })
                        }
                    }
                },
            ]
        });

        $('.add-campsite-blocks').click(function() {
            $form.emptyFormData();
            $modal.modal('show')
            $modal.find('.modal-title').text('ADD CAMPSITE BLOCK');
        });

        $form.onSubmit(function(data) {
            // insert
            if (data.id == '') {
                $http.post('/api/admin/campsite-blocks', data)
            }

            // update
            if (data.id != '') {
                $http.put(`/api/admin/campsite-blocks/${data.id}`, data)
            }
        })
    })
</script>
@endsection