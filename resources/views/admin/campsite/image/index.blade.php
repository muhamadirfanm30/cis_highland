@extends($master_template)
@section('content')
<!-- Default box -->
<div class="card">
    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <a href="{{ url('admin/campsite-image/create') }}" class="btn btn-primary add-bussiness text-right"><i class="fa fa-plus"></i> ADD NEW IMAGE CAMPSITE</a>
        <hr>
        <table id="image_campsite"></table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        Footer
    </div>
    <!-- /.card-footer-->
</div>
<!-- /.card -->


@endsection

@section('js')
<style>

</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<script>
    $(function() {
        var $table = $('#image_campsite');

        $table.bootstrapTable({
            sortName: 'created_at',
            sortOrder: 'ASC',
            theadClasses: 'table-dark',
            url: HelperService.base_url + '/api/admin/campsite-image/datatables',
            responseHandler: function(res) {
                return res.data;
            },
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: "Campsite",
                    field: "id",
                    formatter: function(value, row, index) {
                        if(row.campsite_block_site !== null){
                            if(row.campsite_block_site.block !== null){
                                if(row.campsite_block_site.block.campsite !== null){
                                    param = 'Campsite : ' + row.campsite_block_site.block.campsite.campsite + ' | Block : ' + row.campsite_block_site.block.name
                                }
                            }
                        }
                        return param;
                    }
                },
                {
                    title: "image campsite",
                    field: "image_campsite",
                    formatter: function(value, row, index) {
                        view_img = "<a target='_blank' href='" + HelperService.url('/storage/campsite-image/' + row.image_campsite) + "'>View Image</a>";
                        return view_img;
                    }
                },
            ]
        });
    })

    
</script>
@endsection