@extends($master_template)
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <a href="{{ url('/admin/campsites-block-site/create') }}" class="btn btn-primary"><i class="fa fa-plus"></i> ADD NEW CAMPSITE BLOCK</a>
        <table id="table"></table>
    </div>
</div>


<form id="campsite-block-sites-form">
    <div id="dark-header-modal" class="modal fade" tabindex="-1" aria-labelledby="dark-header-modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="dark-header-modalLabel">Modal Heading
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" />
                    <div class="mb-3">
                        <label>Campsite Block</label>
                        <select class="form-control col-12" name="campsite_block_id" required>
                            <option selected value="">Choose...</option>
                            @foreach($blocks as $block)
                            <option value="{{ $block->id }}">{{ $block->campsite->campsite }} - {{ $block->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label>Site Name</label>
                        <input type="text" name="name" class="form-control" value="" placeholder="Block">
                    </div>
                    <div class="mb-3">
                        <label>Capacity</label>
                        <input type="number" name="capacity" class="form-control" value="" placeholder="Block">
                    </div>
                    <div class="mb-3">
                        <label>Note</label>
                        <input type="text" name="note" class="form-control" value="" placeholder="Block">
                    </div>
                    <div class="mb-3">
                        <label>Category</label>
                        <select class="form-control col-12" name="category" required>
                            <option selected value="">Choose...</option>
                            <option selected value="1">Family</option>
                            <option selected value="2">Gathering</option>
                            <option selected value="3">Mandiri</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label>Type Area</label>
                        <select class="form-control col-12" name="type_area" required>
                            <option selected value="">Choose...</option>
                            <option selected value="1">Exclusive</option>
                            <option selected value="2">Non Exclusive</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</form>

<div id="dark-header-modal-modal-map" class="modal fade" tabindex="-1" aria-labelledby="dark-header-modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="dark-header-modalLabel">Modal Heading
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="mb-3 zoom-marker-div" id="zoom-marker-div">
                    <img class="zoom-marker-img" id="zoom-marker-img" name="viewArea" draggable="false" />
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<link rel="stylesheet" href="{{ asset('plugins/zoomMarker/css/easy-loading.css') }}">
<script src="{{ asset('plugins/zoomMarker/js/easy-loading.js') }}"></script>

<script src="{{ asset('plugins/zoomMarker/js/jquery.mousewheel.min.js') }}"></script>
<script src="{{ asset('plugins/zoomMarker/js/hammer.min.js') }}"></script>

<link rel="stylesheet" href="{{ asset('plugins/zoomMarker/css/zoom-marker.css') }}">
<script src="{{ asset('plugins/zoomMarker/js/zoom-marker.js') }}"></script>
<style>
    .zoom-marker-img {
        box-shadow: none;
        height: 400px;
    }
</style>
<script>
    $(function() {
        var isLoadMap = false;
        var $table = $('#table');
        var $modal = $('#dark-header-modal');
        var $form = new FormService($('#campsite-block-sites-form'));
        var $http = new HttpService({
            formService: $form,
            bootrapTable: $table,
            bootrapModal: $modal,
        });

        var $modalMap = $('#dark-header-modal-modal-map');
        var $httpMap = new HttpService({
            bootrapTable: $table,
            bootrapModal: $modalMap,
        });

        HelperService.loadBussinesSelect2();

        $table.bootstrapTable({
            sortName: 'created_at',
            sortOrder: 'ASC',
            url: HelperService.base_url + '/api/admin/campsite-block-sites',
            responseHandler: function(res) {
                return res.data;
            },
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'Campsite',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return row.block.campsite.campsite;
                    }
                },
                {
                    title: 'Block',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return row.block.name;
                    }
                },
                {
                    title: 'Site',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return row.name;
                    }
                },
                {
                    title: 'Capacity',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return row.capacity;
                    }
                },
                {
                    title: 'Note',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return row.note;
                    }
                },
                {
                    title: 'ACTION',
                    sortable: false,
                    formatter: function(value, row, index) {
                        var mapBTN = '';
                        mapBTN += '<button type="button" class="btn btn-sm btn-xs btn-primary map-coordinate">';
                        mapBTN += 'MAP';
                        mapBTN += '</button>';
                        btn_edit = "<a class='btn btn-success btn-xs' href='" + HelperService.url('/admin/campsites-block-site/update/' + row.id) + "'>Edit</a>";
                        btn_delete = '<button type="button" class="btn btn-sm btn-xs btn-danger delete">Delete</button>';
                        // btn_delete_item = '<button type="button" data-id="' + row.request_code + '" class="btn btn-danger btn-sm delete-btb"><i class="fa fa-trash"></i></button>';
                        return mapBTN + ' ' +btn_edit + ' ' + btn_delete;
                    },
                    events: {
                        'click .map-coordinate': function(e, value, row) {
                            $modalMap.modal('show');
                            console.log(row);
                            $("#zoom-marker-img").zoomMarker_CleanMarker();
                            $("#zoom-marker-img").zoomMarker_AddMarker({
                                src: "{{ $map->map_marker_file_src }}",
                                x: row.coordinate_x,
                                y: row.coordinate_y,
                                id: row.id
                            });
                        },
                        'click .edit': function(e, value, row) {
                            $form.emptyFormData();
                            $modal.modal('show')
                            $modal.find('.modal-title').text('UPDATE CAMPSITE BLOCK SITE');
                            $form.setFormData(row);
                        },
                        'click .delete': function(e, value, row) {
                            $http.delete(`/api/admin/block-camp/${row.id}/delete`)
                                .then(function(resp) {
                                    $table.bootstrapTable('refresh');
                                })
                        }
                    }
                },
            ]
        });

        $('.add-campsite-block-sites').click(function() {
            $form.emptyFormData();
            $modal.modal('show')
            $modal.find('.modal-title').text('ADD CAMPSITE BLOCK SITE');
        });

        $form.onSubmit(function(data) {
            // insert
            if (data.id == '') {
                $http.post('/api/admin/campsite-block-sites', data)
            }

            // update
            if (data.id != '') {
                $http.put(`/api/admin/campsite-block-sites/${data.id}`, data)
            }
        })

        $('#zoom-marker-img').zoomMarker({
            src: "{{ $map->map_file_src }}",
            rate: 0.2,
            width: 400,
            max: 3000,
            markers: []
        });

        $('#zoom-marker-img').on("zoom_marker_click", function(event, size) {
            $httpMap.post('/api/admin/campsite-block-sites/update-map-coordinate', {
                coordinate_x: size.param.x,
                coordinate_y: size.param.y,
                id: size.param.id
            })

            console.log("image has been loaded with size: ", size);
        });
    })
</script>
@endsection