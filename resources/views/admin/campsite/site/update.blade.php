@extends($master_template)
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <form id="save_campsite">
        <div class="card-body">
            <div class="modal-body">
                <input type="hidden" name="id" id="ids" class="ids" value="{{ $id }}" />
                <div class="mb-3">
                    <label>Campsite Block</label>
                    <select class="form-control col-12" name="campsite_block_id" required>
                        <option selected value="">Choose...</option>
                        @foreach($blocks as $block)
                            <option value="{{ $block->id }}" {{ $data->campsite_block_id == $block->id ? 'selected' : ''}}>{{ $block->campsite->campsite }} - {{ $block->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="mb-3">
                    <label>Site Name</label>
                    <input type="text" name="name" class="form-control" value="{{ $data->name }}" placeholder="Block">
                </div>
                <div class="mb-3">
                    <label>Capacity</label>
                    <input type="number" name="capacity" class="form-control" value="{{$data->capacity}}" placeholder="Block">
                </div>
                <div class="mb-3">
                    <label>Note</label>
                    <input type="text" name="note" class="form-control" value="{{ $data->note }}" placeholder="Block">
                </div>
                <div class="mb-3">
                    <label>Category</label>
                    <div class="row">
                        @foreach($category as $val)
                            <div class="col-md-3">
                                <div class="custom-control custom-checkbox">
                                    <input class="custom-control-input" name="category_campsite" type="checkbox" id="customCheckbox{{ $val->id }}" value="{{ $val->id }}">
                                    <label for="customCheckbox{{ $val->id }}" class="custom-control-label">{{ $val->name }}</label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
                <div class="mb-3">
                    <label>Type Area</label>
                    <select class="form-control col-12" name="type_area" required>
                        <option selected value="">Choose...</option>
                        <option selected value="1" {{ $data->type_area == 1 ? 'selected' : '' }}>Exclusive</option>
                        <option selected value="2" {{ $data->type_area == 1 ? 'selected' : '' }}>Non Exclusive</option>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button> -->
                <button type="submit" class="btn btn-dark">Save changes</button>
            </div>
        </div>
    </form>
</div>


<!-- <form id="campsite-block-sites-form">
    <div id="dark-header-modal" class="modal fade" tabindex="-1" aria-labelledby="dark-header-modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="dark-header-modalLabel">Modal Heading
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" />
                    <div class="mb-3">
                        <label>Campsite Block</label>
                        <select class="form-control col-12" name="campsite_block_id" required>
                            <option selected value="">Choose...</option>
                            @foreach($blocks as $block)
                            <option value="{{ $block->id }}">{{ $block->campsite->campsite }} - {{ $block->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label>Site Name</label>
                        <input type="text" name="name" class="form-control" value="" placeholder="Block">
                    </div>
                    <div class="mb-3">
                        <label>Capacity</label>
                        <input type="number" name="capacity" class="form-control" value="" placeholder="Block">
                    </div>
                    <div class="mb-3">
                        <label>Note</label>
                        <input type="text" name="note" class="form-control" value="" placeholder="Block">
                    </div>
                    <div class="mb-3">
                        <label>Category</label>
                        <select class="form-control col-12" name="category" required>
                            <option selected value="">Choose...</option>
                            <option selected value="1">Family</option>
                            <option selected value="2">Gathering</option>
                            <option selected value="3">Mandiri</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label>Type Area</label>
                        <select class="form-control col-12" name="type_area" required>
                            <option selected value="">Choose...</option>
                            <option selected value="1">Exclusive</option>
                            <option selected value="2">Non Exclusive</option>
                        </select>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</form> -->
@endsection

@section('js')
<style>

</style>
<script>
    $(function() {
        var $http = new HttpService({
            formService: $form,
        });
        var $form = new FormService($('#save_campsite')).withArrayField(['category_campsite']);

        $http.get('/api/admin/campsite-block-sites/' + $('.ids').val())
            .then(function(resp) {
                var package = resp.data;
                $form.setFormDataManual({
                    category_campsite: package.get_category_camp.map(function(row) {
                        return row.category_campsite;
                    }),
                });
            })

        $form.onSubmit(function(data) {
        
            $http.put('/api/admin/campsite-block-sites/'+$('.ids').val(), data).then(function(resp) {
                // location.reload();
                HelperService.redirect('/admin/campsites-block-site');
            })
        })

        HelperService.loadBussinesSelect2();

        // $('.add-campsite-block-sites').click(function() {
        //     $form.emptyFormData();
        //     $modal.modal('show')
        //     $modal.find('.modal-title').text('ADD CAMPSITE BLOCK SITE');
        // });

        // $form.onSubmit(function(data) {
        //     // insert
        //     if (data.id == '') {
        //         $http.post('/api/admin/campsite-block-sites', data)
        //         HelperService.url('/admin/campsites-block-site')
        //     }
        // })
    })
</script>
@endsection