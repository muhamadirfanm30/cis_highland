@extends($master_template)
@section('content')
<!-- Default box -->
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <form action="{{ url('admin/check-items/status-update/'.$id) }}" method="post">
            @csrf 
            <div class="col-md-12">
                @if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                @if(session()->has('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>
                @endif
            </div>
            <table class="table table-striped">
                <thead>
                    <th>Item</th>
                    <th>Status</th>
                </thead>
                <tbody>
                    @foreach($getItem as $item)
                        <tr>
                            <td>{{ $item->vendor_item->nama_barang }}</td>
                            <td>
                                <input type="hidden" name="ids[]" value="{{ $item->id }}">
                                @if($item->history_item_status_id == 1)
                                    <select name="status_barang[]" class="form-control" id="">
                                        <option value="1" selected>Belum Diantar</option>
                                        <option value="2">Diterima</option>
                                    </select>
                                @else
                                    <select name="status_barang[]" class="form-control" id="">
                                        <option value="2" selected>Diterima</option>
                                        <option value="1">Belum Diantar</option>
                                    </select>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <button type="submit" class="btn btn-success btn-block">Save Data</button>
        </form>
    </div>
    
    <form id="simpan_tambahan_item">
        <!-- /.card-body -->
        <input type="hidden" name="get_id_order" value="{{ $id }}">
        <div class="card-body">
            <div class="card card-primary">
                <div class="card-header">
                    <h4 class="card-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseAdditionalItem">
                            #ADD DATA ADDITIONAL ITEM
                        </a>
                    </h4>
                </div>
                <div id="collapseAdditionalItem" class="panel-collapse in collapse show">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-12" style="margin-top: 10px;" id="outlet-content"> </div>
                            <div class="col-md-12" style="margin-top: 10px;">
                                <button type="button" class="btn btn-sm btn-primary waves-effect" id="btn-add-outlet"><i class="fa fa-plus"></i> Add Item</button>
                                <button type="submit" class="btn btn-info btn-sm">Save Additional Item</button>
                                <!-- <button type="submit" class="btn btn-sm btn-success waves-effect"><i class="fa fa-plus"></i> Save Item</button> -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!-- /.card-footer-->
</div>
<!-- /.card -->


@endsection

@section('js')
<style>

</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<script>
    var DATA_UNIQ = 1;
    var $http = new HttpService();
    var $form = new FormService($('#simpan_tambahan_item')).withArrayField(['include_item', 'include_qty', 'include_id', 'unit_id', 'qty', 'durasi', 'item_price']);
    $form.onSubmit(function(data) {
        
        $http.post('/api/admin/reservasi/store-new-item-reservation', data).then(function(resp) {
            alert('Success')
            location.reload();
        })
    })
    show();

    $('#btn-add-outlet').click(function() {
        user_id = $('.user-select').find(':selected').val();
        $('#outlet-content').append(outletBoxContentTemplate(DATA_UNIQ));
        loadUnitSelect2(DATA_UNIQ);
        DATA_UNIQ++;
    })

    function show() {
        $('#outlet-content').append(outletBoxContentTemplate(DATA_UNIQ, show_delete_btn = false));
        loadUnitSelect2(DATA_UNIQ);
        DATA_UNIQ++;
    }

    function outletBoxContentTemplate(uniq, show_delete_btn = true) {
        btn_delete_style = 'display:none';
        if (show_delete_btn) {
            btn_delete_style = '';
        }
        var template = `
            <div class="card container-detail-card-${uniq}" style="margin-bottom: 20px;">
                <input type="hidden" id="get_id_uniq" value="${uniq}">
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Item</label>
                                    <select class="form-control unit-select unit-select-${uniq}" id="unit_id" name="unit_id" style="width:100%" data-uniq="${uniq}">
                                        <option value="">Select Item</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Item Price</label>
                                    <input type="number" class="form-control item_price item_price-${uniq}" id="item_price" name="item_price" placeholder="item price" data-uniq="${uniq}" readonly>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Qty</label>
                                    <input type="number" class="form-control qty" id="qty" name="qty" placeholder="QTY">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Durasi</label>
                                    <input type="number" class="form-control durasi" id="durasi" name="durasi" placeholder="Durasi">
                                </div>
                            </div>
                        </div>
                    </div>
                    <button style="${btn_delete_style}" type="button" class="btn btn-sm btn-danger waves-effect btn-delete-outlet" data-uniq="${uniq}"><i
                            class="fa fa-trash"></i> Delete Item</button>
                </div>
            </div>
        `;
        return template;
    }

    $(document).on('change', '.unit-select', function() {
        id = $(this).val();
        uniq = $(this).attr('data-uniq');
        
        $http.get('/api/admin/reservasi/select2-get-price/' + id)
            .then(function(resp) {
                
                // var arr = data.item_price,
                // n   = arr.length,
                // sum = 0;
                // while(n--)
                // sum += parseFloat(arr[n]) || 0;
                // $('#nila-project').val(sum)
                // console.log(sum)
                $('.item_price-'+uniq).val(resp.data.harga_jual);
                var array = JSON.parse("[" + resp.data.harga_jual + "]");
                console.log(array)
                
            })
            .catch(function(request, status, error) {
                $('#additional-item').html('');
            })
    })


    function loadUnitSelect2(uniq) {
        HelperService
            .select2Static(".unit-select-" + uniq, '/api/admin/reservasi/select2-item', function(item) {
                return {
                    id: item.id,
                    text: item.nama_barang,
                    data: item,
                }
            })
    }
</script>
@endsection