@extends($master_template)
@section('content')
<!-- Default box -->
<div class="col-md-12">
@if(session()->has('success'))
                    <div class="alert alert-success">
                        {{ session()->get('success') }}
                    </div>
                @endif
                @if(session()->has('error'))
                    <div class="alert alert-danger">
                        {{ session()->get('error') }}
                    </div>
                @endif
</div>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <!-- <a href="{{ url('admin/reservasi/create') }}" class="btn btn-primary add-bussiness text-right"><i class="fa fa-plus"></i> ADD NEW RESERVATION</a> -->
        <hr>
        <table id="table"></table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        Footer
    </div>
    <!-- /.card-footer-->
</div>
<!-- /.card -->


@endsection

@section('js')
<style>

</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<script>
    $(function() {
        var $table = $('#table');

        $table.bootstrapTable({
            sortName: 'created_at',
            sortOrder: 'ASC',
            theadClasses: 'table-dark',
            url: HelperService.base_url + '/api/admin/reservasi',
            responseHandler: function(res) {
                return res.data;
            },
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'ACTION',
                    sortable: false,
                    formatter: function(value, row, index) {
                        view_link = "<a class='btn btn-primary btn-sm' href='" + HelperService.url('/admin/check-items/detail-item/' + row.id) + "'><i class='fa fa-eye'></i></a>";
                        // view_inv = "<a class='btn btn-success btn-sm' target='_blank' href='" + HelperService.url('/admin/reservasi/view-invoice/' + row.id) + "'>Invoice</a>";
                        // view_pay = "<a class='btn btn-danger btn-sm' href='" + HelperService.url('/admin/reservasi/payment-detail/' + row.id) + "'>Payment</a>";
                        // btn_delete_item = '<button type="button" data-id="' + row.request_code + '" class="btn btn-danger btn-sm delete-btb"><i class="fa fa-trash"></i></button>';
                        return view_link;
                    },
                },
                {
                    title: 'RESERVATION STATUS',
                    formatter: function(value, row, index) {
                        if (row.status.id == 1) {
                            return '<span class="badge badge-primary badge-phil">Checkin</span>';
                        } else if (row.status.id == 2) {
                            return '<span class="badge badge-warning badge-phil " style="color:white">Checkout</span>';
                        } else if (row.status.id == 3) {
                            return '<span class="badge badge-danger badge-phil">Cancel</span>';
                        } else if (row.status.id == 4) {
                            return '<span class="badge badge-success badge-phil" id="res">Reservation</span>';
                        }
                    }
                },
                {
                    title: 'Progress',
                    formatter: function(value, row, index) {
                        return row.count_proggress_item + '%';
                    }
                    
                },
                {
                    title: 'PAYMENT STATUS',
                    formatter: function(value, row, index) {
                        if (row.payment_status.id == 1) {
                            return '<span class="badge badge-danger badge-pill">Belum Lunas</span>';
                        } else if (row.payment_status.id == 2) {
                            return '<span class="badge badge-success badge-pill">Lunas</span>';
                        }
                    }
                },
                {
                    title: 'CUSTOMER',
                    field: 'customer.nama',
                },
                {
                    title: 'PLAT NOMOR',
                    field: 'plat_nomor',
                },
                {
                    title: "CHECKIN",
                    field: "checkin_date",
                    formatter: function(value, row, index) {
                        return moment(row.checkin_date).format("DD MMMM YYYY");
                    }
                },
                {
                    title: "CHECKOUT",
                    field: "checkout_date",
                    formatter: function(value, row, index) {
                        return moment(row.checkout_date).format("DD MMMM YYYY");
                    }
                },
                {
                    title: 'PACKAGE',
                    field: 'package.nama_paket',
                },
                {
                    title: 'BERAPA MALAM',
                    field: 'berapa_malam',
                },
            ]
        });

        // $table.bootstrapTable({
        //     sortName: 'created_at',
        //     sortOrder: 'ASC',
        //     theadClasses: 'table-dark',
        //     url: HelperService.base_url + '/api/admin/reservasi',
        //     responseHandler: function(res) {
        //         return res.data;
        //     },
        //     columns: [{
        //             title: 'NO',
        //             sortable: true,
        //             formatter: function(value, row, index) {
        //                 return index + 1;
        //             }
        //         },
        //         {
        //             title: 'customer',
        //             field: 'customer.nama',
        //         },
        //         {
        //             title: "checkin",
        //             field: "checkin",
        //             formatter: function(value, row, index) {
        //                 return moment(row.checkin).format("DD MMMM YYYY");
        //             }
        //         },
        //         {
        //             title: "checkout",
        //             field: "checkout",
        //             formatter: function(value, row, index) {
        //                 return moment(row.checkout).format("DD MMMM YYYY");
        //             }
        //         },
        //         {
        //             title: 'package',
        //             field: 'package.nama_paket',
        //         },
        //         {
        //             title: 'Percentage',
        //             formatter: function(value, row, index) {
        //                 return row.count_proggress_item + ' %';
        //             }
        //         },
        //         {
        //             title: 'berapa_malam',
        //             field: 'berapa_malam',
        //         },
        //         {
        //             title: 'status',
        //             formatter: function(value, row, index) {
        //                 if (row.payment !== null) {
        //                     if (row.payment.status_payment == 'Belum Lunas') {
        //                         return '<span class="badge badge-danger badge-pill">Belum Lunas</span>';
        //                     } else if (row.payment.status_payment == 'Lunas') {
        //                         return '<span class="badge badge-success badge-pill">Lunas</span>';
        //                     } else {
        //                         return '-';
        //                     }
        //                 } else {
        //                     return '-';
        //                 }
        //             }
        //         },
        //         {
        //             title: 'ACTION',
        //             sortable: false,
        //             formatter: function(value, row, index) {
        //                 view_link = "<a class='btn btn-primary btn-sm' href='" + HelperService.url('/admin/check-items/detail-item/' + row.id) + "'><i class='fa fa-eye'></i></a>";
        //                 // view_inv = "<a class='btn btn-success btn-sm' target='_blank' href='" + HelperService.url('/admin/reservasi/view-invoice/' + row.id) + "'><i class='fa fa-file-alt'></i></a>";
        //                 // view_pay = "<a class='btn btn-danger btn-sm' href='" + HelperService.url('/admin/reservasi/payment-detail/' + row.id) + "'><i class='fa fa-hand-holding-usd'></i></a>";
        //                 // btn_delete_item = '<button type="button" data-id="' + row.request_code + '" class="btn btn-danger btn-sm delete-btb"><i class="fa fa-trash"></i></button>';
        //                 return view_link;
        //             },
        //         },
        //     ]
        // });
    })
</script>
@endsection