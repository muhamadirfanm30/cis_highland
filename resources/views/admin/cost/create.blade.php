@extends($master_template)

@section('content')
<form id="form-cost">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">{{ $title }}</h3>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                    <i class="fas fa-times"></i>
                </button>
            </div>
        </div>
        <div class="card-body">
            <div class="mb-3">
                <label>Date From</label>
                <input type="date" name="date_from" class="form-control" value="{{ $request->date_from }}" required>
            </div>
            <div class="mb-3">
                <label>Date To</label>
                <input type="date" name="date_to" class="form-control" value="{{ $request->date_to }}" required>
            </div>
        </div>
    </div>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">{{ $title }}</h3>

            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                    <i class="fas fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                    <i class="fas fa-times"></i>
                </button>
            </div>
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                    <tr>
                        <th>CATEGORY</th>
                        <th>NOTE</th>
                        <th>TOTAL</th>
                    </tr>
                </thead>
                <tbody id="cost-input-area">
                </tbody>
            </table>
            <div style="margin-top: 10px;">
                <button type="button" class="btn btn-default add-field"> <i class="fa fa-plus"></i> Add Field</button>
                <button type="submit" class="btn btn-primary"> <i class="fa fa-plus"></i> Save</button>
            </div>
        </div>
    </div>
</form>
@endsection

@section('js')
<script>
    $(function() {
        var $area = $('#cost-input-area');
        var $form = new FormService($('#form-cost')).withArrayField(['cost_category_id', 'cost_note', 'cost_total']);
        var $http = new HttpService({
            formService: $form,
        });
        var categories = [];
        var categories_options = '';

        new HttpService().get('/api/admin/cost-categories/list').then(function(resp) {
            categories = resp.data;
            categories_options = generateCategoryOption(categories);
            $area.append(costInputTemplate(categories_options));
        })

        $form.onSubmit(function(data) {
            $http.post('/api/admin/costs', data).then(function(resp) {
                HelperService.redirect('/admin/costs');
            })
        })

        $('.add-field').click(function() {
            $area.append(costInputTemplate(categories_options));
        })
    })


    function generateCategoryOption(categories) {
        var option = '';
        $.each(categories, function(key, category) {
            option += `<option value="${category.id}">${category.name}</option>`;
        });
        return option;
    }

    function costInputTemplate(categories_options) {
        var template = `
        <tr>
            <td>
                <select class="form-control col-12" name="cost_category_id">
                    <option selected value="">Choose...</option>
                    ${categories_options}
                </select>
            </td>
            <td><input type="text" class="form-control" name="cost_note"></td>
            <td> <input type="number" class="form-control" name="cost_total"></td>
        </tr>
        `;

        return template;
    }
</script>
@endsection