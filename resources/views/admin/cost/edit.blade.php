@extends($master_template)

@section('content')
<div class="card">
    <div class="card-body">
        <form id="form-cost-filter">
            <div class="mb-3">
                <label>Date From</label>
                <input type="date" name="date_from" class="form-control" value="{{ $cost->date_from }}" required>
            </div>
            <div class="mb-3">
                <label>Date To</label>
                <input type="date" name="date_to" class="form-control" value="{{ $cost->date_to }}" required>
            </div>
            <button type="submit" class="btn btn-primary"> <i class="fa fa-search"></i> Filter</button>
        </form>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{ $title }}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <form id="form-cost">
            <input type="hidden" name="cost_id" value="{{ $cost->id }}">
            <table class="table">
                <thead>
                    <tr>
                        <th>CATEGORY</th>
                        <th>NOTE</th>
                        <th>TOTAL</th>
                    </tr>
                </thead>
                <tbody id="cost-input-area">
                </tbody>
            </table>
            <div style="margin-top: 10px;">
                <button type="button" class="btn btn-default add-field"> <i class="fa fa-plus"></i> Add Field</button>
                <button type="submit" class="btn btn-primary"> <i class="fa fa-plus"></i> Save</button>
            </div>
        </form>
    </div>
</div>
@endsection

@section('js')
<script>
    $(function() {
        var $area = $('#cost-input-area');
        var $formFilter = new FormService($('#form-cost-filter'));
        var $form = new FormService($('#form-cost')).withArrayField(['id', 'cost_category_id', 'cost_note', 'cost_total']);
        var $http = new HttpService();
        var CATEGORIES = [];

        $http.get('/api/admin/cost-categories/list').then(function(resp) {
            CATEGORIES = resp.data;
            $('#form-cost-filter').trigger('submit');
        })

        $form.onSubmit(function(data) {
            $http.put('/api/admin/costs', data).then(function(resp) {
                HelperService.redirect('/admin/costs');
            })
        })

        $formFilter.onSubmit(function(data) {
            $http.post('/api/admin/costs/filter', data).then(function(resp) {
                $.each(resp.data, function(key, cost) {
                    categories_options = generateCategoryOption(CATEGORIES, cost.cost_category_id);
                    $area.append(costInputTemplate(categories_options, cost));
                })
            })
        })

        $('.add-field').click(function() {
            categories_options = generateCategoryOption(CATEGORIES);
            $area.append(costInputTemplate(categories_options));
        })
    })


    function generateCategoryOption(categories, id = 0) {
        var option = '';
        $.each(categories, function(key, category) {
            selected = category.id == id ? 'selected' : '';
            option += `<option value="${category.id}" ${selected}>${category.name}</option>`;
        });
        return option;
    }

    function costInputTemplate(categories_options, row = null) {
        if (row == null) {
            row = {
                id: 0,
                cost_date: '',
                cost_note: '',
                cost_total: '',
            }
        }
        var template = `
        <tr>
            <td>
            <input type="hidden" class="form-control" name="id" value="${row.id}">
                <select class="form-control col-12" name="cost_category_id">
                    <option selected value="">Choose...</option>
                    ${categories_options}
                </select>
            </td>
            <td><input type="text" class="form-control" name="cost_note" value="${row.cost_note}"></td>
            <td> <input type="number" class="form-control" name="cost_total" value="${row.cost_total}"></td>
        </tr>
        `;

        return template;
    }
</script>
@endsection