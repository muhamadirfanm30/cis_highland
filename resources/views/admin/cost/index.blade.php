@extends($master_template)

@section('content')
<!-- Default box -->
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{ $title }}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <a href="{{ url('/admin/costs/create') }}" class="btn btn-primary add-user-btn"><i class="fa fa-plus"></i> ADD NEW COST</a>
        <table id="role-table"></table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        Footer
    </div>
    <!-- /.card-footer-->
</div>
<!-- /.card -->
@endsection

@section('js')
<script>
    $(function() {
        var $table = $('#role-table');
        var $http = new HttpService({
            bootrapTable: $table,
        });

        $table.bootstrapTable({
            sortName: 'date_from',
            sortOrder: 'ASC',
            url: HelperService.base_url + '/api/admin/costs',
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'DATE FROM',
                    field: 'date_from',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return moment(value).format("DD MMMM YYYY");
                    }
                },
                {
                    title: 'DATE TO',
                    field: 'date_to',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return moment(value).format("DD MMMM YYYY");
                    }
                },
                {
                    title: 'TOTAL DETAIL',
                    field: 'details_count',
                    sortable: false,
                    formatter: function(value, row, index) {
                        return HelperService.thousandsSeparators(value);
                    }
                },
                {
                    title: 'GRAND TOTAL',
                    field: 'details_sum_cost_total',
                    sortable: false,
                    formatter: function(value, row, index) {
                        return HelperService.thousandsSeparators(value);
                    }
                },
                {
                    title: 'ACTION',
                    formatter: function(value, row, index) {
                        var link = '';
                        link += '<button type="button" class="btn btn-sm btn-xs btn-success edit">';
                        link += 'Edit';
                        link += '</button>';
                        link += '<button type="button" class="btn btn-sm btn-xs btn-danger delete">';
                        link += 'Delete';
                        link += '</button>';
                        return link;
                    },
                    events: {
                        'click .edit': function(e, value, row) {
                            HelperService.redirect(`/admin/costs/${row.id}/edit`)
                        },
                        'click .delete': function(e, value, row) {
                            $http.delete(`/api/admin/costs/${row.id}`)
                        }
                    }
                },
            ]
        });
    })
</script>
@endsection