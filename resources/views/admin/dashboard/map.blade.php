<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $title }} | {{ $app_name }}</title>

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400&display=swap" rel="stylesheet">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
</head>

<body>
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{$title}}</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                            <i class="fas fa-minus"></i>
                        </button>
                        <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                            <i class="fas fa-times"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body zoom-marker-div" id="zoom-marker-div" class="">
                    <img class="zoom-marker-img" id="zoom-marker-img" name="viewArea" draggable="false" />
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">DATA CAMPSITE HARI INI</h3>
                </div>
                <div class="card-body">
                    <table id="table-today-reservation"></table>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">DATA CAMPSITE AVAILABLE</h3>
                </div>
                <div class="card-body">
                    <table id="table-available-reservation"></table>
                </div>
            </div>
        </div>
    </div>
    @include('admin.template.js')
    <link rel="stylesheet" href="{{ asset('plugins/zoomMarker/css/easy-loading.css') }}">
    <script src="{{ asset('plugins/zoomMarker/js/easy-loading.js') }}"></script>

    <script src="{{ asset('plugins/zoomMarker/js/jquery.mousewheel.min.js') }}"></script>
    <script src="{{ asset('plugins/zoomMarker/js/hammer.min.js') }}"></script>

    <link rel="stylesheet" href="{{ asset('plugins/zoomMarker/css/zoom-marker.css') }}">
    <script src="{{ asset('plugins/zoomMarker/js/zoom-marker.js') }}"></script>
    <style>
        .zoom-marker-img {
            box-shadow: none;
            height: {{ $map->map_height . 'px' }};
        }
    </style>
    <script>
        var $http = new HttpService();
        var $table_today_reservation = $('#table-today-reservation');
        var $table_available_reservation = $('#table-available-reservation');

        $table_today_reservation.bootstrapTable({
            sortName: 'checkin_date',
            sortOrder: 'ASC',
            search: false,
            showRefresh: false,
            showColumns: false,
            url: HelperService.base_url + '/api/public/dashboard/today-reservation',
            responseHandler: function(res) {
                return res.data;
            },
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'NAME',
                    field: 'customer_name',
                },
                {
                    title: 'AREA',
                    field: 'campsite_block_sites_name',
                    formatter: function(value, row, index) {
                        return row.campsites_name + ' ' + row.campsite_block_name + ' ' + row.campsite_block_sites_name;
                    }
                },
                {
                    title: 'STATUS',
                    field: 'reservation_status_name',
                },
            ]
        });

        $table_available_reservation.bootstrapTable({
            sortName: 'id',
            sortOrder: 'ASC',
            search: false,
            showRefresh: false,
            showColumns: false,
            url: HelperService.base_url + '/api/public/dashboard/available-reservation',
            responseHandler: function(res) {
                return res.data;
            },
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'NAME',
                    field: 'name',
                },
                {
                    title: 'KAPASITAS',
                    field: 'capacity',
                },
            ]
        });

        $(document).ready(function() {
            $http.get('/api/public/dashboard/map-reservation')
                .then(function(resp) {
                    map_config = resp.data;
                    console.log(resp);
                    $('#zoom-marker-img').zoomMarker(map_config);

                    // $('#zoom-marker-img').zoomMarker({
                    //     src: "{{ asset('dist/img/hcp map luas.png') }}",
                    //     rate: 0.2,
                    //     width: 900,
                    //     max: 3000,
                    //     markers: [{
                    //             src: "{{ asset('plugins/zoomMarker/img/marker.svg') }}",
                    //             x: 1800,
                    //             y: 367,
                    //             uniq_id: 0
                    //         },
                    //         {
                    //             src: "{{ asset('plugins/zoomMarker/img/marker.svg') }}",
                    //             x: 1820,
                    //             y: 367,
                    //             uniq_id: 0
                    //         }
                    //     ]
                    // });
                });
        })

        $('#zoom-marker-img').on("zoom_marker_click", function(event, size) {
            console.log("image has been loaded with size: ", size);
        });
    </script>
</body>

</html>