@extends($master_template)

@section('content')
<form id="map-form">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">{{ $title }}</h3>
        </div>
        <div class="card-body">
            <div class="mb-3">
                <label>map_show</label>
                <select name="map_show" class="form-control">
                    <option value="0" {{ $map->map_show == 0 ? 'selected' : '' }}>HIDDEN</option>
                    <option value="1" {{ $map->map_show == 1 ? 'selected' : '' }}>SHOW</option>
                </select>
            </div>
            <div class="mb-3">
                <label>map_src</label>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" name="map_src" id="map_src_input">
                    <label class="custom-file-label" for="map_src_input">Choose file</label>
                </div>
                <img src="{{ $map->map_file_src }}" class="img-responsive" width="100" height="100">
            </div>
            <div class="mb-3">
                <label>map_rate</label>
                <input type="text" name="map_rate" class="form-control" placeholder="enter map_rate" value="{{ $map->map_rate }}" required>
            </div>
            <div class="mb-3">
                <label>map_width</label>
                <input type="number" name="map_width" class="form-control" placeholder="enter map_width" value="{{ $map->map_width }}" required>
            </div>
            <div class="mb-3">
                <label>map_height</label>
                <input type="number" name="map_height" class="form-control" placeholder="enter map_height" value="{{ $map->map_height }}" required>
            </div>
            <div class="mb-3">
                <label>map_max</label>
                <input type="number" name="map_max" class="form-control" placeholder="enter map_max" value="{{ $map->map_max }}" required>
            </div>
            <div class="mb-3">
                <label>map_marker_size</label>
                <input type="number" name="map_marker_size" class="form-control" placeholder="enter map_marker_size" value="{{ $map->map_marker_size }}" required>
            </div>
            <div class="mb-3">
                <label>map_marker_src</label>
                <div class="custom-file">
                    <input type="file" class="custom-file-input" name="map_marker_src" id="map_marker_src_input">
                    <label class="custom-file-label" for="map_marker_src_input">Choose file</label>
                </div>
                <img src="{{ $map->map_marker_file_src }}" class="img-responsive" width="100" height="100">
            </div>
            <div class="mb-3">
                <label>map_marker_draggable</label>
                <select name="map_marker_draggable" class="form-control">
                    <option value="0" {{ $map->map_marker_draggable == 0 ? 'selected' : '' }}>NO</option>
                    <option value="1" {{ $map->map_marker_draggable == 1 ? 'selected' : '' }}>YES</option>
                </select>
            </div>
            <div class="mb-3">
                <button type="submit" class="btn btn-dark">Save changes</button>
            </div>
        </div>
    </div>
</form>
@endsection

@section('js')
<script>
    $(function() {
        var $form = new FormService($('#map-form')).withUploadField(['map_src', 'map_marker_src']);
        var $http = new HttpService({
            formService: $form,
        });

        $form.onSubmit(function(data, form_data_upload) {
            $http.post(`/api/admin/map/update`, form_data_upload).then(function(resp) {
                HelperService.redirect('/admin/map-config');
            })
        })
    })
</script>
@endsection