@extends($master_template)
@section('content')
<!-- Ionicons -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<div class="card">
    <div class="card-body">
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-info">
                        <div class="inner">
                            <h3>
                                @if($countReservasi > 0)
                                    {{ $countReservasi }}
                                @else 
                                    0
                                @endif
                            </h3>
                            <p>Rotal Reservation</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-success">
                        <div class="inner">
                            <h3>
                                @if($totalPengunjung > 0)
                                    {{ $totalPengunjung }}
                                @else 
                                    0
                                @endif
                            </h3>

                            <p>Total Visitors</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-danger">
                        <div class="inner">
                            <h3>0</h3>

                            <p>Total Customer</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-info">
                        <div class="inner" >
                            <h3>{{ $countnewAdditional }}</h3>

                            <p>Unique Visitors</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                            <a href="{{ url('admin/reservasi') }}" class="small-box-footer" >More Info <i class="fas fa-arrow-circle-right" ></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div id="accordion">
                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="card-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseCustomer">
                                DATA RESERVASI MINGGU INI
                            </a>
                        </h4>
                    </div>
                    <div id="collapseCustomer" class="panel-collapse in collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-9">
                                    <table id="example" class="table table-striped table-bordered">
                                        <thead>
                                            <th>Reservasi</th>
                                            <th>Checkin</th>
                                            <th>Status</th>
                                            <!-- <th>Checkout</th> -->
                                            <th>Status Pelayanan</th>
                                            <th>Status Pembayaran</th>
                                            
                                        </thead>
                                        <tbody>
                                            @foreach($listWeeklyReport as $list)
                                                <tr>
                                                    <td>{{ !empty($list->customer) ? $list->customer->nama : '-' }}</td>
                                                    <td>{{ date('Y-M-d', strtotime($list->checkin_date)) }} - {{ date('Y-M-d', strtotime($list->checkout_date)) }}</td>
                                                    <td>
                                                        @if($list->reservation_status_id == 1)
                                                            <span class="badge badge-success">Checkin</span>
                                                        @elseif($list->reservation_status_id == 2)
                                                            <span class="badge badge-info">Checkout</span>
                                                        @elseif($list->reservation_status_id == 3)
                                                            <span class="badge badge-primary">Reservasi</span>
                                                        @else 
                                                            <span>-</span>
                                                        @endif
                                                    </td>
                                                    <td>{{ $list->count_proggress_item }}%</td>
                                                    <td>
                                                        @if(!empty($list->payment_status))
                                                            @if($list->payment_status->name == "belum-lunas")
                                                                <span class="badge badge-danger">Belum Lunas</span>
                                                            @else 
                                                                <span class="badge badge-success">Lunas</span>
                                                            @endif
                                                        @endif
                                                    </td>
                                                    
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <div class="col-md-3">
                                    <div class="alert alert-primary" role="alert">
                                        Total Reservasi : <strong>{{ $weeklyAllReservasi > 0 ? $weeklyAllReservasi : 0 }}</strong>
                                    </div>
                                    <div class="alert alert-success" role="alert">
                                        Total Checkin : <strong>{{ $weeklyCheckin > 0 ? $weeklyCheckin : 0 }}</strong>
                                    </div>
                                    <!-- <div class="alert alert-warning" role="alert" style="color: white;">
                                        Sudah Terlayani : <strong>1</strong>
                                    </div>
                                    <div class="alert alert-danger" role="alert">
                                        On Progress : <strong>1</strong>
                                    </div> -->
                                    <div class="alert alert-info" role="alert">
                                        Total Checkout : <strong>{{ $weeklyCheckout > 0 ? $weeklyCheckout : 0 }}</strong>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="accordion1">
                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="card-title">
                            <a data-toggle="collapse" data-parent="#accordion1" href="#collapseCustomer1">
                            Sales Chart
                            </a>
                        </h4>
                    </div>
                    <div id="collapseCustomer1" class="panel-collapse in collapse show">
                        <div class="card-body">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-8">
                                        <p class="text-center">
                                            <!-- <strong>Sales: 1 Jan, 2014 - 30 Jul, 2014</strong> -->
                                        </p>

                                        <div class="chart">
                                            <!-- Sales Chart Canvas -->
                                            <!-- <canvas id="salesChart" height="180" style="height: 180px;"></canvas> -->
                                            <!-- <canvas id="canvas" height="280" width="600"></canvas> -->
                                            <canvas id="mataChart" class="chartjs" width="undefined" height="undefined"></canvas>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <p class="text-center">
                                            <strong>Package Sold</strong>
                                        </p>
                                        @foreach($getPackage as $paket)
                                            <div class="progress-group">
                                                {{ $paket->nama_paket }}
                                                <span class="float-right">Terjual : <b>{{ $paket->hitung_paket_count }}</b></span>
                                                <!-- <div class="progress progress-sm">
                                                    <div class="progress-bar bg-primary" style="width: 80%"></div>
                                                </div> -->
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                                <div class="card-footer">
                                    <div class="row">
                                    <div class="col-sm-4 col-4">
                                        <div class="description-block border-right">
                                        <!-- <span class="description-percentage text-success"><i class="fas fa-caret-up"></i> 17%</span> -->
                                        <h5 class="description-header">Rp. {{ number_format($totalPendapatan->jumlah) }}</h5>
                                        <span class="description-text">TOTAL INCOME</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-4 col-4">
                                        <div class="description-block border-right">
                                        <!-- <span class="description-percentage text-warning"><i class="fas fa-caret-left"></i> 0%</span> -->
                                        <h5 class="description-header">Rp. {{ number_format($totalPembiayaan->jumlah) }}</h5>
                                        <span class="description-text">TOTAL FINANCING</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <div class="col-sm-4 col-4">
                                        <div class="description-block border-right">
                                        <!-- <span class="description-percentage text-success"><i class="fas fa-caret-up"></i> 20%</span> -->
                                        <h5 class="description-header">Rp. {{ number_format($averagePendapatan->jumlah) }}</h5>
                                        <span class="description-text">TOTAL AVERAGE INCOME</span>
                                        </div>
                                        <!-- /.description-block -->
                                    </div>
                                    <!-- /.col -->
                                    <!-- <div class="col-sm-4 col-6">
                                        <div class="description-block">
                                        <span class="description-percentage text-danger"><i class="fas fa-caret-down"></i> 18%</span>
                                        <h5 class="description-header">1200</h5>
                                        <span class="description-text">GOAL COMPLETIONS</span>
                                        </div>
                                    </div> -->
                                    </div>
                                    <!-- /.row -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="accordion2">
                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="card-title">
                            <a data-toggle="collapse" data-parent="#accordion2" href="#collapseCustomer">
                                Stock Inventory
                            </a>
                        </h4>
                    </div>
                    <div id="collapseCustomer" class="panel-collapse in collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="inventory_stock" class="table table-striped table-bordered">
                                        <thead>
                                            <th>Item Name</th>
                                            <th>Stock Available</th>
                                            <th>Price</th>
                                            <!-- <th>Created At</th> -->
                                        </thead>
                                        <tbody>
                                            @foreach($getDataItem as $list)
                                                @if($list->stok <= 5)
                                                    <tr style="background-color: red; color:white">
                                                        <td>{{ $list->nama_barang }} <a href="{{ url('/admin/vendors-item') }}" class="badge badge-warning" style="color: white;"> Update Stock</a></td>
                                                        <td>{{ $list->stok }}</td>
                                                        <td>Rp. {{ number_format($list->harga_jual) }}</td>
                                                    </tr>
                                                @else 
                                                    <tr>
                                                        <td>{{ $list->nama_barang }}</td>
                                                        <td>{{ $list->stok }}</td>
                                                        <td>Rp. {{ number_format($list->harga_jual) }}</td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection

@section('js')
<style>

</style>
<!-- ChartJS -->
<script src="{{ asset('adminAssets/chart.js/Chart.min.js') }}"></script>
<!-- jQuery Mapael -->
<script src="{{ asset('plugins/jquery-mousewheel/jquery.mousewheel.js') }}"></script>
<script src="{{ asset('plugins/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-mapael/jquery.mapael.min.js') }}"></script>
<script src="{{ asset('plugins/jquery-mapael/maps/usa_states.min.js') }}"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js"></script>

<script type="text/javascript">
    var ctx = document.getElementById("mataChart").getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: <?php echo json_encode($label); ?>,
            datasets: [
                {
                    label: 'Total Income',
                    backgroundColor: '#6c5ce7',
                    borderColor: '#6c5ce7',
                    data: <?php echo json_encode($pendapatanBulanan); ?>
                },
                {
                    label: 'Total Financing',
                    backgroundColor: '#a29bfe',
                    borderColor: '#a29bfe',
                    data: <?php echo json_encode($pembiayaanBulanan); ?>
                },
                {
                    label: 'Total Average Income',
                    backgroundColor: '#74b9ff',
                    borderColor: '#74b9ff',
                    data: <?php echo json_encode($averageBulanan); ?>
                },
            ],
            options: {
                animation: {
                    onProgress: function(animation) {
                        progress.value = animation.animationObject.currentStep / animation.animationObject.numSteps;
                    }
                }
            }
        },
    });
</script>

<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );

    $(document).ready(function() {
        $('#inventory_stock').DataTable( {
            "order": [[ 1, "asc" ]]
        } );
    } );
</script>
@endsection