@extends($master_template)
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <a href="{{ url('admin/fasilitas/create') }}" class="btn btn-primary add-category"><i class="fa fa-plus"></i> ADD NEW FASILITAS</a>
        <table id="table-fasilitas"></table>
    </div>
</div>



<form id="fasilitas-form">
    <div id="dark-header-modal" class="modal fade" tabindex="-1" aria-labelledby="dark-header-modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="dark-header-modalLabel">Modal Heading
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="text" name="id">
                    <div class="mb-3">
                        <label>Fasilitas Name</label>
                        <input type="text" name="name" class="form-control" value="" placeholder="Fasilitas Name" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('js')
<script>
    $(function() {
        var $table = $('#table-fasilitas');
        var $modal = $('#dark-header-modal');
        var $form = new FormService($('#fasilitas-form'));
        var $http = new HttpService({
            formService: $form,
            bootrapTable: $table,
            bootrapModal: $modal,
        });

        $table.bootstrapTable({
            sortName: 'created_at',
            sortOrder: 'ASC',
            url: HelperService.base_url + '/api/admin/fasilitas',
            responseHandler: function(res) {
                return res.data;
            },
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'Fasilitas Name',
                    field: 'name',
                    sortable: true,
                },
                {
                    title: 'Icon',
                    field: 'icon',
                    sortable: true,
                },
                {
                    title: 'ACTION',
                    sortable: false,
                    formatter: function(value, row, index) {
                        view_link = '<button type="button" class="btn btn-sm waves-effect btn-xs btn-success edit">Edit</button>';
                        btn_delete_item = '<button type="button" class="btn btn-sm waves-effect btn-xs btn-danger delete">Delete</button>';
                        return view_link + ' ' + btn_delete_item;
                    },
                    events: {
                        'click .edit': function(e, value, row) {
                            $form.emptyFormData();
                            $modal.modal('show')
                            $modal.find('.modal-title').text('UPDATE FASILITAS');
                            $form.setFormData(row);
                        },
                        'click .delete': function(e, value, row) {
                            HelperService.confirm(function() {
                                $http.delete(`/api/admin/fasilitas/${row.id}`, row)
                            })
                        }
                    }
                },
            ]
        });

        $form.onSubmit(function(data) {
            // update
            if (data.id != '') {
                $http.put(`/api/admin/fasilitas/${data.id}`, data)
            }
        })
    })
</script>
@endsection