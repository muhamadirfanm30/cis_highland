@extends($master_template)
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
        <!-- /.card-header -->
    <form action="{{ url('admin/fasilitas/edit/'.$data->id) }}" method="post" enctype="multipart/form-data">
        @csrf
        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Sorry !</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div> 
        @endif
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <div data-role="dynamic-fields">
                        <div class="form-inline">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Fasilitas Name</label>
                                    <input type="text" name="name" value="{{ $data->name }}" class="form-control" id="field-name" placeholder="Judul Vidio" style="width:100%" required>
                                    <small style="color: white;">ab</small>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Icon Fasilitas</label>
                                    <input type="file" name="icon" class="form-control" id="field-value" placeholder="Url Vidio" style="width:100%" required>
                                    @if(!empty($data->icon))
                                        <a href="{{ url('storage/icon-fasilitas/'.$data->icon) }}" target="_blank">View Image</a>
                                    @endif
                                </div>
                            </div>
                            <!-- <div class="col-md-1">
                                <label for="" style="color:white">add btn</label>
                                <button class="btn btn-danger btn-sm" data-role="remove">
                                    <span class="fa fa-minus"></span>
                                </button>
                                <button class="btn btn-primary btn-sm" data-role="add">
                                    <span class="fa fa-plus"></span>
                                </button>
                            </div><br><br><br> -->
                        </div>  <!-- /div.form-inline -->
                    </div>  <!-- /div[data-role="dynamic-fields"] -->
                </div>  <!-- /div.col-md-12 -->
            </div> 
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-md-4"></div>
                <div class="col-md-4"></div>
                <div class="col-md-2"><a href="{{ url('admin/fasilitas') }}" class="btn btn-danger btn-block" style="float: right">Back</a></div>
                <div class="col-md-2"><button type="submit" class="btn btn-success btn-block" style="float: right">Submit</button></div>
            </div>
            
            
        </div>
    </form>
</div>
@endsection

@section('js')
<style>
    .entry:not(:first-of-type)
        [data-role="dynamic-fields"] > .form-inline + .form-inline {
        margin-top: 0.5em;
    }

    [data-role="dynamic-fields"] > .form-inline [data-role="add"] {
        display: none;
    }

    [data-role="dynamic-fields"] > .form-inline:last-child [data-role="add"] {
        display: inline-block;
    }

    [data-role="dynamic-fields"] > .form-inline:last-child [data-role="remove"] {
        display: none;
    }
</style>


<script type="text/javascript" src="https://code.jquery.com/jquery-1.7.1.min.js"></script>
<script>
    $(function() {
        // Remove button click
        $(document).on(
            'click',
            '[data-role="dynamic-fields"] > .form-inline [data-role="remove"]',
            function(e) {
                e.preventDefault();
                $(this).closest('.form-inline').remove();
            }
        );
        // Add button click
        $(document).on(
            'click',
            '[data-role="dynamic-fields"] > .form-inline [data-role="add"]',
            function(e) {
                e.preventDefault();
                var container = $(this).closest('[data-role="dynamic-fields"]');
                new_field_group = container.children().filter('.form-inline:first-child').clone();
                new_field_group.find('input').each(function(){
                    $(this).val('');
                });
                container.append(new_field_group);
            }
        );
    });
</script>
@endsection