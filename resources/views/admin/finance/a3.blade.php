@extends($master_template)

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">SEARCH</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <form id="search-form">
            <div class="row">
                <div class="col-12 col-md-6">
                    <label>Date From</label>
                    <input type="date" name="date_from" class="form-control">
                </div>
                <div class="col-12 col-md-6">
                    <label>Date To</label>
                    <input type="date" name="date_to" class="form-control">
                </div>
                <div class="col-12" style="margin-top: 10px;">
                    <button type="submit" class="btn btn-dark">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{ $title }}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <table id="a3-table"></table>
    </div>
</div>
@endsection

@section('js')
<script>
    $(function() {
        var $formSearch = new FormService($('#search-form'));
        var $table = $('#a3-table');
        var $http = new HttpService({
            bootrapTable: $table,
        });

        $formSearch.onSubmit(function(data) {
            var playload = '?date_from=' + data.date_from + '&date_to=' + data.date_to;
            $table.bootstrapTable('refresh', {
                url: HelperService.base_url + '/api/admin/finance-a3' + playload
            });
            event.preventDefault();
        });

        $table.bootstrapTable({
            sortName: 'first_week',
            sortOrder: 'ASC',
            url: HelperService.base_url + '/api/admin/finance-a3',
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'DATE FROM',
                    field: 'first_week',
                    formatter: function(value, row, index) {
                        return moment(value).format("DD MMMM YYYY");
                    }
                },
                {
                    title: 'DATE TO',
                    field: 'end_week',
                    formatter: function(value, row, index) {
                        return moment(value).format("DD MMMM YYYY");
                    }
                },
                {
                    title: 'BIAYA OPERASIONAL',
                    field: 'cost_total',
                    formatter: function(value, row, index) {
                        create_bo = "<a target='__blank' class='btn btn-primary btn-sm' href='" + HelperService.url(`/admin/costs/create?date_from=${moment(row.first_week).format("YYYY-MM-DD")}&date_to=${moment(row.end_week).format("YYYY-MM-DD")}`) + "'>ADD BIAYA OPERASIONAL</a>";
                        return value == null ? create_bo : HelperService.thousandsSeparators(value);
                    }
                },
                {
                    title: 'TOTAL PROJECT',
                    field: 'total_project',
                    formatter: function(value, row, index) {
                        return HelperService.thousandsSeparators(value);
                    }
                },
                {
                    title: 'ACTION',
                    formatter: function(value, row, index) {
                        print_link = "<a target='__blank' class='btn btn-success btn-sm' href='" + HelperService.url(`/admin/finance-a3-print?date_from=${moment(row.first_week).format("YYYY-MM-DD")}&date_to=${moment(row.end_week).format("YYYY-MM-DD")}`) + "'>PRINT</a>";
                        return print_link;
                    }
                },
            ]
        });
    })
</script>
@endsection