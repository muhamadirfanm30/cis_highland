<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ $title }}</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="../../plugins/fontawesome-free/css/all.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="../../dist/css/adminlte.min.css">
</head>

<body>
    <div class="wrapper">
        <section class="invoice">
            <div class="row">
                <div class="col-12">
                    <img src="{{ asset('dist/img/logo.png') }}" class="logo-inv img-responsive">
                </div>
            </div>
            <div class="row invoice-info">
                <div class="col-sm-6 invoice-col">
                    <address>
                        <strong>Laporan Kegiatan Camping Priode April 2021</strong><br>
                        <strong>PENDAPATAN, PEMBIAYAAN DAN SHARE</strong><br>
                    </address>
                </div>
                <div class="col-sm-6 invoice-col">
                    <table class="table">
                        <thead>
                            <tr>
                                <th>RUMUS BO</th>
                            </tr>
                            <tr>
                                <th>Pendapatan</th>
                                <th>{{ $summary->total_project }}</th>
                            </tr>
                            <tr>
                                <th>Biaya Oprasional</th>
                                <th>{{ $summary->cost_total }}</th>
                            </tr>
                            <tr>
                                <th>Presentasi</th>
                                <th>{{ $bo_persentase }}%</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-12 table-responsive">
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <th>NAMA CLIENT</th>
                                <th>PENDAPATAN</th>
                                <th>PEMBIAYAAN</th>
                                <th>LABA KOTOR</th>
                                <th>BO(%)</th>
                                <th>LABA BERSIH</th>
                                <th>SHARE 20%</th>
                                <th>SHARE 2,5%</th>
                                <th>SHARE 5 %</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $total_pendapatan = 0;
                            $total_pembiayaan = 0;
                            $total_bo = 0;
                            $bo;
                            $total_laba_kotor = 0;
                            $total_laba_bersih = 0;
                            $total_share_20 = 0;
                            $total_share_2dot5 = 0;
                            $total_share_5 = 0;
                            ?>
                            @foreach($A3 as $row)
                            <?php
                            $laba_kotor = $row->total_project - $row->total_pembiayaan;
                            $bo =  $row->total_project * $bo_persentase / 100;
                            $laba_bersih = $laba_kotor - $bo;
                            $share_20 =   $laba_bersih * 20 / 100;
                            $share_2dot5 =   $laba_bersih * 2.5 / 100;
                            $share_5 =   $laba_bersih * 5 / 100;

                            $total_pendapatan += $row->total_project;
                            $total_pembiayaan += $row->total_pembiayaan;
                            $total_bo += $bo;
                            $total_laba_kotor += $laba_kotor;
                            $total_laba_bersih += $laba_bersih;
                            $total_share_20 += $share_20;
                            $total_share_2dot5 += $share_2dot5;
                            $total_share_5 += $share_5;
                            ?>
                            <tr>
                                <td>{{ $row->customer_name }}</td>
                                <td>{{ thousand_separator($row->total_project) }}</td>
                                <td>{{ thousand_separator($row->total_pembiayaan) }}</td>
                                <td>{{ thousand_separator($laba_kotor) }}</td>
                                <td>{{ thousand_separator($bo) }}</td>
                                <td>{{ thousand_separator($laba_bersih) }}</td>
                                <td>{{ thousand_separator($share_20) }}</td>
                                <td>{{ thousand_separator($share_2dot5) }}</td>
                                <td>{{ thousand_separator($share_5) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                        <tfoot>
                            <tr>
                                <th></th>
                                <th>{{ $total_pendapatan }}</th>
                                <th>{{ $total_pembiayaan }}</th>
                                <th>{{ $total_laba_kotor }}</th>
                                <th>{{ $total_bo }}</th>
                                <th>{{ $total_laba_bersih }}</th>
                                <th>{{ $total_share_20 }}</th>
                                <th>{{ $total_share_2dot5 }}</th>
                                <th>{{ $total_share_5 }}</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.col -->
            </div>
        </section>
    </div>
    <script>
        window.addEventListener("load", window.print());
    </script>
</body>

</html>