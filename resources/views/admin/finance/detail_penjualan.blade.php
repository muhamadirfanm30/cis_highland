@extends($master_template)
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <caption>INCLUDE PACKAGE</caption>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Qty</th>
                            <th>Durasi</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($items as $detail)
                            @if($detail->is_include_paket == 1)
                                <?php
                                $total_paket += $detail->total;
                                ?>
                                <tr>
                                    <td>{{ $detail->nama_barang }}</td>
                                    <td>Rp. {{ thousand_separator($detail->pembiayaan) }}</td>
                                    <td>{{ $detail->qty }}</td>
                                    <td>{{ $detail->item_type_name == "Sewa" ? $detail->durasi . ' / day' : '-' }}</td>
                                    <td>Rp. {{ thousand_separator($detail->total) }}</td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>TOTAL</th>
                            <td colspan="4">Rp. {{ thousand_separator($total_paket) }}</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card">
            <div class="card-body">
                <caption>INCLUDE ADDITIONAL</caption>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>Price</th>
                            <th>Qty</th>
                            <th>Durasi</th>
                            <th>Subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($items as $detail)
                            @if($detail->is_include_paket == 0)
                            <?php
                            $total_additional += $detail->total;
                            ?>
                                <tr>
                                    <td>{{ $detail->nama_barang }}</td>
                                    <td>Rp. {{ thousand_separator($detail->pembiayaan) }}</td>
                                    <td>{{ $detail->qty }}</td>
                                    <td>{{ $detail->item_type_name == "Sewa" ? $detail->durasi : '-' }}</td>
                                    <td>Rp. {{ thousand_separator($detail->total) }}</td>
                                </tr>
                            @endif
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>TOTAL</th>
                            <td>{{ thousand_separator($total_additional) }}</td>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">GRAND TOTAL : {{ thousand_separator($total_paket + $total_additional) }}</h3>
    </div>
</div>
@endsection

@section('js')
@endsection