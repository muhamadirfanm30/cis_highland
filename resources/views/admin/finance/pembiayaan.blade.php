@extends($master_template)

@section('content')
<div class="row">
    <div class="col-12 col-sm-4">
        <div class="info-box bg-success">
            <span class="info-box-icon"><i class="fas fa-dollar-sign"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">TOTAL PROJECT</span>
                <span class="info-box-number">{{ thousand_separator($summary->total_pembiayaan) }}</span>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-4">
        <div class="info-box bg-primary">
            <span class="info-box-icon"><i class="fas fa-dollar-sign"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">TOTAL PAYMENT</span>
                <span class="info-box-number"></span>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-4">
        <div class="info-box bg-danger">
            <span class="info-box-icon"><i class="fas fa-dollar-sign"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">TOTAL SISA</span>
                <span class="info-box-number"></span>
            </div>
        </div>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">SEARCH</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <form id="search-form">
            <div class="row">
                <div class="col-12 col-md-6">
                    <label>Date From</label>
                    <input type="date" name="date_from" class="form-control">
                </div>
                <div class="col-12 col-md-6">
                    <label>Date To</label>
                    <input type="date" name="date_to" class="form-control">
                </div>
                <div class="col-12" style="margin-top: 10px;">
                    <button type="submit" class="btn btn-dark">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{ $title }}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <table id="pembiayaan-table"></table>
    </div>
</div>
@endsection

@section('js')
<script>
    $(function() {
        var $formSearch = new FormService($('#search-form'));
        var $table = $('#pembiayaan-table');
        var $http = new HttpService({
            bootrapTable: $table,
        });

        $table.bootstrapTable({
            sortName: 'id',
            sortOrder: 'ASC',
            url: HelperService.base_url + '/api/admin/finance-pembiayaan',
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'CUSTOMER',
                    field: 'customer_name',
                },
                {
                    title: 'CHECK-IN',
                    field: 'checkin_date',
                    formatter: function(value, row, index) {
                        return moment(value).format("DD MMMM YYYY");
                    }
                },
                {
                    title: 'CHECK-OUT',
                    field: 'checkout_date',
                    formatter: function(value, row, index) {
                        return moment(value).format("DD MMMM YYYY");
                    }
                },
                {
                    title: 'STATUS RESERVASI',
                    field: 'reservation_status_name',
                },
                {
                    title: 'STATUS PAYMENT',
                    field: 'payment_status_name',
                },
                {
                    title: 'TOTAL',
                    field: 'total_pembiayaan',
                    formatter: function(value, row, index) {
                        return HelperService.thousandsSeparators(value);
                    }
                },
                {
                    title: 'ACTION',
                    formatter: function(value, row, index) {
                        print_link = "<a target='__blank' class='btn btn-success btn-sm' href='" + HelperService.url(`/admin/finance-pembiayaan/detail/${row.id}`) + "'>DETAIL</a>";
                        return print_link;
                    }
                },
            ]
        });

        $formSearch.onSubmit(function(data) {
            var playload = '?date_from=' + data.date_from + '&date_to=' + data.date_to;
            $table.bootstrapTable('refresh', {
                url: HelperService.base_url + '/api/admin/finance-pembiayaan' + playload
            });
        });
    })
</script>
@endsection