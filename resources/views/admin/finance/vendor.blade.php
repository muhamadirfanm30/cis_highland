@extends($master_template)

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">SEARCH</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <form id="search-finance-vendor-form">
            <div class="row">
                <div class="col-12 col-md-4">
                    <label>Date From</label>
                    <input type="date" name="date_from" class="form-control">
                </div>
                <div class="col-12 col-md-4">
                    <label>Date To</label>
                    <input type="date" name="date_to" class="form-control">
                </div>
                <div class="col-md-4">
                    <label for="">Vendor</label>
                    <select name="vendor_id" class="form-control vendor-select" style="width: 100%;">
                        <option value="">Choose Vendor</option>
                    </select>
                </div>
                <div class="col-12" style="margin-top: 10px;">
                    <button type="submit" class="btn btn-dark">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{ $title }}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <table id="vendor-table"></table>
    </div>
</div>

<div id="dark-header-modal" class="modal fade" tabindex="-1" aria-labelledby="dark-header-modalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="dark-header-modalLabel">Vendor Item
                </h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table id="vendor-item-table"></table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    $(function() {
        HelperService.loadVendorSelect2();

        var $modal = $('#dark-header-modal');
        var $tableItem = $('#vendor-item-table');

        var $formSearch = new FormService($('#search-finance-vendor-form'));
        var $table = $('#vendor-table');
        var $http = new HttpService({
            bootrapTable: $table,
        });

        $formSearch.onSubmit(function(data) {
            var playload = `?date_from=${data.date_from}&date_to=${data.date_to}&vendor_id=${data.vendor_id}`;
            $table.bootstrapTable('refresh', {
                url: HelperService.base_url + '/api/admin/finance-vendor' + playload
            });
        });

        $table.bootstrapTable({
            sortName: 'checkin_date',
            sortOrder: 'ASC',
            url: HelperService.base_url + '/api/admin/finance-vendor',
            showFooter: true,
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'CUSTOMER',
                    field: 'customer_name',
                },
                {
                    title: 'CHECK-IN',
                    field: 'checkin_date',
                    formatter: function(value, row, index) {
                        return moment(value).format("DD MMMM YYYY");
                    }
                },
                {
                    title: 'CHECK-OUT',
                    field: 'checkout_date',
                    formatter: function(value, row, index) {
                        return moment(value).format("DD MMMM YYYY");
                    }
                },
                {
                    title: 'VENDOR NAME',
                    field: 'vendor_name',
                },
                {
                    title: 'JUMLAH ITEM',
                    field: 'jumlah_item',
                    formatter: function(value, row, index) {
                        return HelperService.thousandsSeparators(value);
                    },
                    footerFormatter: function(data) {
                        return HelperService.thousandsSeparators(HelperService.footerSum(data, this.field));
                    }
                },
                {
                    title: 'TOTAL',
                    field: 'total',
                    formatter: function(value, row, index) {
                        return HelperService.thousandsSeparators(value);
                    },
                    footerFormatter: function(data) {
                        return HelperService.thousandsSeparators(HelperService.footerSum(data, this.field));
                    }
                },
                {
                    title: 'ACTION',
                    formatter: function(value, row, index) {
                        var link = '';
                        link += '<button type="button" class="btn btn-sm btn-xs btn-success detail">';
                        link += 'DETAIL';
                        link += '</button>';
                        return link;
                    },
                    events: {
                        'click .detail': function(e, value, row) {
                            $modal.modal('show');
                            var playload = `?reservation_id=${row.reservation_id}&vendor_id=${row.vendor_id}`;
                            $tableItem.bootstrapTable('refresh', {
                                url: HelperService.base_url + '/api/admin/finance-vendor-detail' + playload
                            });
                        },
                    }
                },
            ]
        });

        $tableItem.bootstrapTable({
            sortName: 'nama_barang',
            sortOrder: 'ASC',
            url: HelperService.base_url + '/api/admin/finance-vendor-detail',
            showFooter: true,
            columns: [
                {
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                }, 
                {
                    title: 'VENDOR NAME',
                    field: 'nama_barang',
                },
                {
                    title: 'PEMBIAYAAN',
                    field: 'total',
                    formatter: function(value, row, index) {
                        return HelperService.thousandsSeparators(value);
                    },
                    footerFormatter: function(data) {
                        return HelperService.thousandsSeparators(HelperService.footerSum(data, this.field));
                    }
                },
            ]
        });
    })
</script>
@endsection