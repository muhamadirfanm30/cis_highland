@extends($master_template)

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">SEARCH</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <form id="search-finance-vendor-form">
            <div class="row">
                <div class="col-12 col-md-4">
                    <label>Date From</label>
                    <input type="date" name="date_from" class="form-control">
                </div>
                <div class="col-12 col-md-4">
                    <label>Date To</label>
                    <input type="date" name="date_to" class="form-control">
                </div>
                <div class="col-md-4">
                    <label for="">Vendor</label>
                    <select name="vendor_id" class="form-control vendor-select" style="width: 100%;">
                        <option value="">Choose Vendor</option>
                    </select>
                </div>
                <div class="col-12" style="margin-top: 10px;">
                    <button type="submit" class="btn btn-dark">Search</button>
                </div>
            </div>
        </form>
    </div>
</div>
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{ $title }}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <table class="table table-responsive" style="white-space: nowrap;">
            <thead>
                <tr>
                    @foreach($reportVendorHeader as $column)
                    <th>{{ $column }}</th>
                    @endforeach
                </tr>
            </thead>
            <tbody>                
                    @foreach($reportVendors as $rows)
                    <tr>
                        @foreach($rows as $key => $val)
                            <th>{{ $val }}</th>
                        @endforeach
                    </tr>
                    @endforeach
            </tbody>
        </table>
    </div>
</div>

@endsection

@section('js')
<script>
    $(function() {
        HelperService.loadVendorSelect2();

        var $formSearch = new FormService($('#search-finance-vendor-form'));

        $formSearch.onSubmit(function(data) {
            var playload = `?date_from=${data.date_from}&date_to=${data.date_to}&vendor_id=${data.vendor_id}`;
            HelperService.redirect('/admin/finance-vendor' + playload);
        });
    })
</script>
@endsection