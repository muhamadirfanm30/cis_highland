@extends($master_template)
@section('content')
<!-- Default box -->
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Sorry !</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div> 
@endif
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <form action="{{ url('admin/general-setting/update') }}" method="post">
            @csrf
            @foreach($getSetting as $setting)
                <div class="form-group">
                    <label for="">{{ $setting->desc }}</label>
                    <input type="hidden" value="{{ $setting->id }}" name="id[]">
                    <input type="hidden" name="name[]" class="form-control" value="{{ $setting->name }}" placeholder="{{ $setting->name  }}">
                    <input type="text" name="value[]" class="form-control" value="{{ $setting->value }}" placeholder="{{ $setting->desc  }}">
                </div>
            @endforeach
            <button type="submit" class="btn btn-primary btn-block">Submit</button>
        </form>
    </div>
</div>
@endsection

@section('js')

@endsection