@extends($master_template)
@section('content')
<!-- Default box -->
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Sorry !</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div> 
@endif
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <form action="{{ url('admin/general-setting-ticket/update-setting-ticket') }}" method="post" enctype="multipart/form-data">
            @csrf
                <div class="form-group">
                    <label for="">{{ !empty($getSettingTicket) ? $getSettingTicket->desc : '' }}</label>
                    <input type="text" value="{{ !empty($getSettingTicket) ? $getSettingTicket->id : '' }}" name="id">
                    <input type="hidden" name="name" class="form-control" value="{{ !empty($getSettingTicket) ? $getSettingTicket->name : '' }}" placeholder="{{ !empty($getSettingTicket) ? $getSettingTicket->name : ''  }}">
                    <input type="file" name="value" class="form-control" value="{{ !empty($getSettingTicket) ? $getSettingTicket->value : '' }}" placeholder="{{ !empty($getSettingTicket) ? $getSettingTicket->desc : ''  }}">
                </div>
                @if(!empty($getSettingTicket->value))
                    <a href="{{ url('storage/background-ticket/'.$getSettingTicket->value) }}" target="_blank">View Image</a>
                @endif
            <button type="submit" class="btn btn-primary btn-block">Submit</button>
        </form>
    </div>
</div>
@endsection

@section('js')

@endsection