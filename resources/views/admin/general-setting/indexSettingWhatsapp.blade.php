@extends($master_template)
@section('content')
<!-- Default box -->
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <strong>Sorry !</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif

@if(session('success'))
    <div class="alert alert-success">
        {{ session('success') }}
    </div> 
@endif
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <form action="{{ url('admin/general-setting-whatsapp/update-setting-whatsapp') }}" method="post">
            @csrf
            @foreach($getSettingTicket as $setting)
                @if($setting->name == "admin_number_ponsel")
                    <div class="form-group">
                        <label for="">{{ $setting->desc }}</label>
                        <input type="hidden" value="{{ $setting->id }}" name="id[]">
                        <input type="hidden" name="name[]" class="form-control" value="{{ $setting->name }}" placeholder="{{ $setting->name  }}">
                        <input type="text" name="value[]" class="form-control" value="{{ $setting->value }}" placeholder="{{ $setting->desc  }}">
                    </div>
                @elseif($setting->name == "whatsapp_send")
                    <div class="form-group">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-10">
                                    <label for="">{{ $setting->desc }}</label>
                                    <input type="hidden" value="{{ $setting->id }}" name="id[]">
                                    <input type="hidden" name="name[]" class="form-control" value="{{ $setting->name }}" placeholder="{{ $setting->name  }}">
                                    <textarea class="form-control" name="value[]" id="body-input-1" placeholder="Enter body">{{ $setting->value }}</textarea>
                                </div>
                                <div class="col-md-2"><br>
                                    [{nama_user}] <br>
                                    [{nomor_whatsapp}] <br>
                                    [{paket}] <br>
                                    [{get_checkin}] <br>
                                    [{get_checkout}] <br>
                                    [{get_pax}] <br>
                                    [{get_durasi}] <br>
                                </div>
                            </div>
                        </div>
                        
                        <!-- <input type="text" name="" class="form-control" value="" placeholder="{{ $setting->desc  }}"> -->
                        
                    </div>
                @elseif($setting->name == "whatsapp_replay")
                    <div class="form-group">
                        <label for="">{{ $setting->desc }}</label>
                        <input type="hidden" value="{{ $setting->id }}" name="id[]">
                        <input type="hidden" name="name[]" class="form-control" value="{{ $setting->name }}" placeholder="{{ $setting->name  }}">
                        <!-- <input type="text" name="value[]" class="form-control" value="{{ $setting->value }}" placeholder="{{ $setting->desc  }}"> -->
                        <textarea class="form-control" name="value[]" id="body-input-2" placeholder="Enter body">{{ $setting->value }}</textarea>
                    </div>
                @endif
            @endforeach
            <button type="submit" class="btn btn-primary btn-block">Submit</button>
        </form>
    </div>
</div>
@endsection

@section('js')
<script src="https://cdn.ckeditor.com/ckeditor5/31.0.0/classic/ckeditor.js"></script>
<script>
    HelperService.WYSIWYG('#body-input-1');
    HelperService.WYSIWYG('#body-input-2');
</script>
@endsection