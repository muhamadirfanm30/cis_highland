@extends($master_template)
@section('content')
<!-- Default box -->
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        {{-- <button type="button" class="btn btn-primary add-bussiness"><i class="fa fa-plus"></i> ADD NEW BUSSINESS</button> --}}
        <table id="example" class="display" style="width:100%">
            <thead>
                <tr>
                    <th>Name</th>
                    <th>Checkin</th>
                    <th>Checkout</th>
                    <th>Pax</th>
                    <th>Paket</th>
                    <th>Actions</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($reservasi as $res)
                    <tr>
                        <td>{{ $res->name }}</td>
                        <td>{{ date('M-d-Y', strtotime($res->checkin)) }}</td>
                        <td>{{ date('M-d-Y', strtotime($res->checkout)) }}</td>
                        <td>{{ $res->pax }}</td>
                        <td>{{ $res->get_paket->nama }}</td>
                        <td>
                            <a href="{{ url('admin/invoice/show/'.$res->id) }}" target="_blank" class="btn btn-primary btn-sm"><i class="fas fa-book"></i></a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        Footer
    </div>
    <!-- /.card-footer-->
</div>
<!-- /.card -->
@endsection

@section('js')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>

<script>
    $(document).ready(function() {
        $('#example').DataTable();
    } );
</script>
@endsection