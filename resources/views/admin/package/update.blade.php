@extends($master_template)

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{ $title }}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <form id="update_package">
            <input type="hidden" value="{{ $id }}" id="id_package">
            @include('admin.template._bussines_select')
            <div class="mb-3" style="margin-bottom: 20px;">
                <label>Nama Paket</label>
                <input type="text" name="nama_paket" class="form-control" placeholder="enter Nama Paket">
            </div>
            <div class="mb-3" style="margin-bottom: 20px;">
                <label>Harga</label>
                <input type="number" name="harga" id="harga" class="form-control" placeholder="Enter Price">
            </div>
            <hr>
            <div class="mb-3" style="margin-bottom: 20px;">
                <label>Pelayanan</label>
                <div class="row">
                    @foreach($vendor_item as $vendor)
                    <div class="col-md-3">
                        <div class="custom-control custom-checkbox">
                            <input class="custom-control-input id_item_pelayanan" name="vendor_item_id" type="checkbox" id="customCheckbox{{ $vendor->id }}" value="{{ $vendor->id }}">
                            <label for="customCheckbox{{ $vendor->id }}" class="custom-control-label">{{ $vendor->nama_barang }}</label>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>

            <!-- <div class="col-md-12"> -->
            <button class="btn btn-primary btn-block">Save Data</button>
            <!-- </div> -->
        </form>
    </div>
</div>

@endsection

@section('js')
<script>
    $(function() {
        HelperService.loadBussinesSelect2();

        var $form = new FormService($('#update_package')).withArrayField(['vendor_item_id']);
        var $http = new HttpService({
            formService: $form,
        });
        var id_package = $('#id_package').val();

        $http.get('/api/admin/packages/' + id_package)
            .then(function(resp) {
                var package = resp.data;
                $form.setFormDataManual({
                    bussiness_id: package.bussiness_id,
                    nama_paket: package.nama_paket,
                    harga: package.harga,
                    vendor_item_id: package.items.map(function(row) {
                        return row.vendor_item_id;
                    }),
                });
            })

        $form.onSubmit(function(data) {
            $http.put('/api/admin/packages/' + id_package, data).then(function(resp) {
                HelperService.redirect('/admin/packages');
            })
        })
    })
</script>
@endsection