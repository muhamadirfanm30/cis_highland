@extends($master_template)
@section('content')
<div class="row">
    <div class="col-12 col-sm-4">
        <div class="info-box bg-success">
            <span class="info-box-icon"><i class="fas fa-dollar-sign"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">TOTAL PROJECT</span>
                <span class="info-box-number">{{ thousand_separator($summary->total_project) }}</span>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-4">
        <div class="info-box bg-primary">
            <span class="info-box-icon"><i class="fas fa-dollar-sign"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">TOTAL PAYMENT</span>
                <span class="info-box-number">{{ thousand_separator($summary->payment_total) }}</span>
            </div>
        </div>
    </div>
    <div class="col-12 col-sm-4">
        <div class="info-box bg-danger">
            <span class="info-box-icon"><i class="fas fa-dollar-sign"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">TOTAL SISA</span>
                <span class="info-box-number">{{ thousand_separator($summary->reservation_total_sisa) }}</span>
            </div>
        </div>
    </div>
</div>

<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>

    <div class="card-body">
        <div class="col-md-12">
            <!-- <form id="payment_form"> -->
            <form action="{{ url('/admin/reservasi/reservasi-payment/'.$summary->id) }}" method="post" enctype="multipart/form-data">
                @csrf
                <div class="row">
                    <div class="col-md-12">
                        @if(session()->has('success'))
                        <div class="alert alert-success">
                            {{ session()->get('success') }}
                        </div>
                        @endif
                        @if(session()->has('error'))
                        <div class="alert alert-danger">
                            {{ session()->get('error') }}
                        </div>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12" style="margin-bottom: 15px;">
                        <label for="">Deskirpsi Pembayaran</label>
                        <input type="text" class="form-control" name="payment_description" id="desc_pembayaran" placeholder="Deskripsi Pembayaran" required>
                    </div>
                    <div class="col-md-12" style="margin-bottom: 20px;">
                        <label for="">Jumlah Pembayaran</label>
                        <input type="number" class="form-control" name="payment_total" id="dp" placeholder="Rp.xxx" required>
                    </div>
                    <div class="col-md-12">
                        <!-- <input type="file" name="image" class="form-control image-upload" required> -->
                        <!-- Upload image input-->
                        <div class="input-group mb-3 px-2 py-2 rounded-pill bg-white shadow-sm">
                            <input id="upload" type="file" name="image" onchange="readURL(this);" class="form-control border-0 image-upload" required>
                            <label id="upload-label" for="upload" class="font-weight-light text-muted">Choose file</label>
                            <div class="input-group-append">
                                <label for="upload" class="btn btn-light m-0 rounded-pill px-4"> <i class="fa fa-cloud-upload mr-2 text-muted"></i><small class="text-uppercase font-weight-bold text-muted">Choose file</small></label>
                            </div>
                        </div>

                        <!-- Uploaded image area-->
                        <p class="font-italic text-center">The image uploaded will be rendered inside the box below.</p>
                        <div class="image-area"><img id="imageResult" src="#" alt="" class="img-fluid rounded shadow-sm mx-auto d-block" style="width: 50%;"></div>
                    </div>
                    <div class="col-md-6">
                        <input type="hidden" id="get_id" value="{{ $summary->id }}">
                        <button type="submit" class="btn btn-primary btn-block" {{ ($summary->payment_status_id == 2) ? 'disabled' : ''  }}> Simpan Pembayaran</button>
                    </div>
                    <div class="col-md-6">
                        <input type="hidden" id="get_id" value="{{ $summary->id }}">
                        @if(Request::segment(2) == 'reservasi-online')
                            <a href="{{ url('admin/reservasi-online/edit/'.$summary->id) }}" class="btn btn-info btn-block"> Lihat Detail Reservasi</a>
                        @else 
                            <a href="{{ url('admin/reservasi/edit/'.$summary->id) }}" class="btn btn-info btn-block"> Lihat Detail Reservasi</a>
                        @endif
                    </div>
                </div>
            </form>
        </div>
        <hr>
        <table id="history_payment" class="display" style="width:100%">
            <thead>
                <th style="display: none;">id</th>
                <th>Tanggal</th>
                <th>Deskripsi Pembayaran</th>
                <th>Jumlah Dibayar</th>
                <th>Bukti Transfer</th>
            </thead>
        </table>
    </div>
</div>

<style>
    #upload {
        opacity: 0;
    }

    #upload-label {
        position: absolute;
        top: 20%;
        left: 1rem;
        transform: translateY(-20%);
    }

    .image-area {
        border: 2px dashed rgba(255, 255, 255, 0.7);
        padding: 1rem;
        position: relative;
    }

    .image-area::before {
        color: black;
        font-weight: bold;
        text-transform: uppercase;
        position: absolute;
        top: 20%;
        left: 20%;
        transform: translate(-20%, -20%);
        font-size: 0.8rem;
        z-index: 1;
    }

    .image-area img {
        z-index: 2;
        position: relative;
    }
</style>
@endsection

@section('js')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js"></script>
<script>
    /*  ==========================================
        SHOW UPLOADED IMAGE
    * ========================================== */
    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function(e) {
                $('#imageResult')
                    .attr('src', e.target.result);
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $(function() {
        $('#upload').on('change', function() {
            readURL(input);
        });
    });

    /*  ==========================================
        SHOW UPLOADED IMAGE NAME
    * ========================================== */
    var input = document.getElementById('upload');
    var infoArea = document.getElementById('upload-label');

    input.addEventListener('change', showFileName);

    function showFileName(event) {
        var input = event.srcElement;
        var fileName = input.files[0].name;
        infoArea.textContent = 'File name: ' + fileName;
    }
</script>
<script>
    $(document).ready(function() {
        $('#history_payment').DataTable({
            "order": [
                [0, "asc"]
            ],
            language: {
                buttons: {
                    colvis: '<i class="fa fa-list-ul"></i>'
                },
                search: '',
                searchPlaceholder: "Search...",
                processing: '<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span> '
            },
            oLanguage: {
                sLengthMenu: "_MENU_",
            },
            buttons: [{
                    extend: 'colvis'
                },
                {
                    text: '<i class="fa fa-refresh"></i>',
                    action: function(e, dt, node, config) {
                        dt.ajax.reload();
                    }
                }
            ],

            ajax: {
                url: HelperService.apiUrl('/admin/reservasi/datatables-list-payment/' + $('#get_id').val()),
                "type": "get",
            },
            columnDefs: [{
                "targets": [0],
                "visible": false,
                "searchable": false
            }, ],
            columns: [
                // {
                //     data: "DT_RowIndex",
                //     name: "DT_RowIndex",
                //     sortable: false,
                //     searchable: false,
                //     width: "10%"
                // },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, row) {
                        return row.id;
                    }
                },
                {
                    data: "payment_date",
                    name: "payment_date",
                    render: function(data, type, row) {
                        return row.payment_date;
                    }
                },
                {
                    data: "payment_description",
                    name: "payment_description",
                    render: function(data, type, row) {
                        return row.payment_description;
                    }
                },
                {
                    data: "payment_total",
                    name: "payment_total",
                    render: function(data, type, row) {
                        return 'Rp.' + HelperService.thousandsSeparators(row.payment_total);
                    }
                },
                {
                    data: "id",
                    name: "id",
                    render: function(data, type, row) {
                        // <a class='btn btn-primary btn-sm' href='" + HelperService.url('/admin/reservasi/detail/' + row.id) + "'><i class='fa fa-eye'></i></a>
                        if (row.image !== null) {
                            return "<a class='btn btn-primary btn-sm' target='_blank' href='" + HelperService.url('/storage/media-bukti-transfer/' + row.image) + "'>Lihat Bukti</a>"
                        } else {
                            return "-";
                        }
                    }
                },
            ],
        });
    });

    $('#payment_form').submit(function(e) {
        console.log('submited')
        HelperService.loadingStart();
        var data = new FormData($('#media-form')[0]);
        var bodyFormData = new FormData();

        $('#ids').each(function() {
            bodyFormData.append('ids', $(this).val());
        });
        $('#get_id').each(function() {
            bodyFormData.append('get_id', $(this).val());
        });
        $('#total_pembayaran').each(function() {
            bodyFormData.append('total_pembayaran', $(this).val());
        });
        $('#sisa_pembayaran').each(function() {
            bodyFormData.append('sisa_pembayaran', $(this).val());
        });
        $('#desc_pembayaran').each(function() {
            bodyFormData.append('desc_pembayaran', $(this).val());
        });
        $('#dp').each(function() {
            bodyFormData.append('dp', $(this).val());
        });

        var file = $('.image-upload').prop('files');
        fileName = null;
        if (file.length) {
            fileName = file[0];
            bodyFormData.append('image2', fileName);
        }

        Axios.post('/admin/reservasi/reservasi-payment/' + $('#get_id').val(), bodyFormData, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                }
            })
            .then(function(response) {
                console.log(response.data.id)
                alert('data reservasi berhasil di buat');
                HelperService.loadingStop();
                // window.location.href = HelperService.url('/admin/reservasi/payment-detail/' + );
                // location.reload();
                // Helper.successNotif('Success Updated');
            })
            .catch(function(error) {
                HelperService.loadingStop();
                alert('terjadi kesalahan');
                HelperService.showNotification(error)
            });

        e.preventDefault();
    })
</script>
@endsection