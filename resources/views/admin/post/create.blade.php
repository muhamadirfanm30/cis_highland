@extends($master_template)

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{ $title }}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <form id="form-post">
            <div class="mb-3">
                <label>TITLE</label>
                <input type="text" class="form-control" name="title" required>
            </div>
            <div class="mb-3">
                <label>BODY</label>
                <textarea class="form-control" name="body" id="body-input" placeholder="Enter body"></textarea>
            </div>
            <button type="submit" class="btn btn-primary"> <i class="fa fa-plus"></i> Save</button>
        </form>
    </div>
</div>
@endsection

@section('js')
<script src="https://cdn.ckeditor.com/ckeditor5/31.0.0/classic/ckeditor.js"></script>
<script>
    HelperService.WYSIWYG('#body-input');

    $(function() {
        var $form = new FormService($('#form-post'));
        var $http = new HttpService({
            formService: $form,
        });

        $form.onSubmit(function(data) {
            $http.post('/api/admin/posts', data).then(function(resp) {
                HelperService.redirect('/admin/posts');
            })
        })
    })
</script>
@endsection