@extends($master_template)

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{ $title }}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <a href="{{ url('/admin/posts/create') }}" class="btn btn-primary add-user-btn"><i class="fa fa-plus"></i> ADD NEW POST</a>
        <table id="post-table"></table>
    </div>
</div>
@endsection

@section('js')
<script>
    $(function() {
        var $table = $('#post-table');
        var $http = new HttpService({
            bootrapTable: $table,
        });

        $table.bootstrapTable({
            sortName: 'title',
            sortOrder: 'ASC',
            url: HelperService.base_url + '/api/admin/posts',
            responseHandler: function(res) {
                return res.data;
            },
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'TITLE',
                    field: 'title',
                },
                {
                    title: 'ACTION',
                    formatter: function(value, row, index) {
                        var link = '';
                        link += '<button type="button" class="btn btn-sm btn-xs btn-success edit">';
                        link += 'Edit';
                        link += '</button>';
                        link += '<button type="button" class="btn btn-sm btn-xs btn-danger delete">';
                        link += 'Delete';
                        link += '</button>';
                        return link;
                    },
                    events: {
                        'click .edit': function(e, value, row) {
                            HelperService.redirect(`/admin/posts/${row.id}/edit`)
                        },
                        'click .delete': function(e, value, row) {
                            $http.delete(`/api/admin/posts/${row.id}`)
                        }
                    }
                },
            ]
        });
    })
</script>
@endsection