@extends($master_template)
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <form id="bussiness-form">
            <div class="mb-3">
                <label>Date From</label>
                <input type="date" name="date_from" class="form-control date_from_input">
            </div>
            <div class="mb-3">
                <label>Date To</label>
                <input type="date" name="date_to" class="form-control date_to_input">
            </div>
            <button type="button" class="btn btn-dark btn-preview-report">Preview</button>
            <button type="button" class="btn btn-success btn-export-report">Export</button>
        </form>
    </div>
</div>

<!-- Default box -->
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body table-responsive">
        <table class="table table-custom table-bordered" style="white-space: nowrap;">
            <thead>
                <tr>
                    <th rowspan="3">NO</th>
                    <th rowspan="3">NAMA CLIENT</th>
                    <th rowspan="3">JENIS PAKET</th>
                    <th rowspan="3">JUMLAH HARI</th>

                    @if($itemHTM != null)
                    <th colspan="2">{{ $itemHTM->nama_barang }}</th>
                    @endif

                    @foreach($items as $item)
                    <th colspan="2">{{ strtoupper($item->nama_barang) }}</th>
                    @endforeach
                    <th rowspan="3">TOTAL</th>
                </tr>
                <tr>
                    @if($itemHTM != null)
                    <th colspan="2">Rp.{{ number_format($itemHTM->pembiayaan) }}</th>
                    @endif
                    @foreach($items as $item)
                    <th colspan="2">Rp.{{ number_format($item->pembiayaan) }}</th>
                    @endforeach
                </tr>
                <tr>
                    @foreach($items as $item)
                    <th>PCS</th>
                    <th>JUMLAH</th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @foreach($reservations as $key => $reservation)
                <?php
                $total_row = 0;
                ?>
                <tr>
                    <td>{{ $key + 1}}</td>
                    <td>{{ $reservation->customer->nama}}</td>
                    <td>{{ $reservation->package->nama_paket}}</td>
                    <td>{{ $reservation->jumlah_hari}}</td>

                    @if($itemHTM != null)
                    <td>{{ $reservation->pax}}</td>
                    <td>Rp.{{ number_format($reservation->pax * $reservation->jumlah_hari * $itemHTM->pembiayaan) }}</td>
                    <?php
                    $total_row += $reservation->pax * $reservation->jumlah_hari * $itemHTM->pembiayaan;
                    ?>
                    @endif

                    @foreach($items as $item)
                    <?php
                    $detailItem = $reservation->details->where('vendor_item_id', $item->id)->first();
                    if ($detailItem != null) {
                        $total_row += $item->pembiayaan * $detailItem->qty;
                    }
                    ?>
                    @if($detailItem != null)
                    <td>{{ $detailItem->qty }}</td>
                    <td>Rp. {{ number_format($item->pembiayaan * $detailItem->qty) }}</td>
                    @else
                    <td>0</td>
                    <td>Rp.0</td>
                    @endif
                    @endforeach
                    <td>Rp.{{ number_format($total_row) }}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<style>
    .table-custom thead tr th {
        text-align: center;
        vertical-align: middle;
    }
</style>
@endsection

@section('js')
<script>
    $('.btn-preview-report').click(function(e) {
        HelperService.redirect('/admin/report-pembiayaan' + getUrlParam());
    })

    $('.btn-export-report').click(function(e) {
        HelperService.redirect('/admin/report-pembiayaan-export' + getUrlParam());
    })

    function getUrlParam() {
        var date_from = $('.date_from_input').val();
        var date_to = $('.date_to_input').val();
        return '?date_from=' + date_from + '&date_to=' + date_to
    }
</script>
@endsection