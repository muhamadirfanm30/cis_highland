@extends($master_template)
@section('content')
<!-- Default box -->
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body table-responsive">
    <a href="{{ url('admin/report-penjualan-export') }}" class="btn btn-success text-right">EXPORT</a>
        <table class="table table-custom table-bordered" style="white-space: nowrap;">
            <thead>
                <tr>
                    <th rowspan="3">NO</th>
                    <th rowspan="3">NAMA CLIENT</th>
                    <th rowspan="3">JENIS PAKET</th>
                    <th rowspan="3">JUMLAH HARI</th>
                    @foreach($items as $item)
                        <th colspan="2">{{ strtoupper($item->nama_barang) }}</th>
                    @endforeach
                    <th rowspan="3">TOTAL</th>
                </tr>
                <tr>
                    @foreach($items as $item)
                        <th colspan="2">Rp.{{ number_format($item->harga_jual) }}</th>
                    @endforeach
                </tr>
                <tr>
                    @foreach($items as $item)
                        <th>PCS</th>
                        <th>JUMLAH</th>
                    @endforeach
                </tr>
            </thead>
            <tbody>
                @foreach($reservations as $key => $reservation)
                <tr>
                    <td>{{ $key + 1}}</td>
                    <td>{{ $reservation->customer->nama}}</td>
                    <td>{{ $reservation->package->nama_paket}}</td>
                    <td>{{ $reservation->jumlah_hari}}</td>
                    
                    @foreach($items as $item)
                        <?php
                            $detailItem = $reservation->details->where('vendor_item_id', $item->id)->first();
                        ?>

                        @if($detailItem != null)
                            <?php
                                $jml = $item->harga_jual * $detailItem->qty;
                                $total = 0;
                                foreach($reservation->detail_reservasi as $key => $details){
                                    $total += $details->vendor_item->harga_jual * $details->qty;
                                }
                                
                            ?>
                            <td>{{ $detailItem->qty }}</td>
                            <td>Rp.{{ number_format($jml) }}</td>
                            
                        @else
                            <td>0</td>
                            <td>Rp.0</td>
                        @endif
                    
                    @endforeach
                    <td>Rp. {{ number_format($total) }}</td>
                </tr>
                <tr>
                <!-- <td>Rp.0</td> -->
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<style>
    .table-custom thead tr th {
        text-align: center;
        vertical-align: middle;
    }
</style>
@endsection

@section('js')
@endsection