<table class="table" style="white-space: nowrap;">
    <thead>
        <tr>
            <th style="border: 1px solid black;" rowspan="3">NO</th>
            <th style="border: 1px solid black;" rowspan="3">NAMA CLIENT</th>
            <th style="border: 1px solid black;" rowspan="3">JENIS PAKET</th>
            <th style="border: 1px solid black;" rowspan="3">JUMLAH HARI</th>

            @foreach($items as $item)
            <th style="border: 1px solid black;" colspan="2">{{ strtoupper($item->nama_barang) }}</th>
            @endforeach
            <th style="border: 1px solid black;" rowspan="3">TOTAL</th>
        </tr>
        <tr>
            @foreach($items as $item)
            <th style="border: 1px solid black;" colspan="2">Rp.{{ $item->pembiayaan }}</th>
            @endforeach
        </tr>
        <tr>
            @foreach($items as $item)
            <th style="border: 1px solid black;">PCS</th>
            <th style="border: 1px solid black;">JUMLAH</th>
            @endforeach
        </tr>
    </thead>
    <tbody>
        @foreach($reservations as $key => $reservation)
        <tr>
            <td style="border: 1px solid black;">{{ $key + 1}}</td>
            <td style="border: 1px solid black;">{{ $reservation->customer->nama}}</td>
            <td style="border: 1px solid black;">{{ $reservation->package->nama_paket}}</td>
            <td style="border: 1px solid black;">{{ $reservation->jumlah_hari}}</td>
            @foreach($items as $item)
            <?php
            $detailItem = $reservation->details->where('vendor_item_id', $item->id)->first();
            ?>
            @if($detailItem != null)
            <td style="border: 1px solid black;">{{ $detailItem->qty }}</td>
            <td style="border: 1px solid black;">{{ $item->pembiayaan * $detailItem->qty }}</td>
            @else
            <td style="border: 1px solid black;">0</td>
            <td style="border: 1px solid black;">Rp</td>
            @endif
            @endforeach
            <?php
                $total = 0;
                foreach($reservation->detail_reservasi as $key => $details){
                    $total += $details->vendor_item->harga_jual * $details->qty;
                }
                
            ?>
            <td style="border: 1px solid black;">{{ $total }}</td>
        </tr>
        @endforeach
    </tbody>
</table>