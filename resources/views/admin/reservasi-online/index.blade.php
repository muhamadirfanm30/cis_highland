@extends($master_template)
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <!-- <a href="{{ url('admin/reservasi/create') }}" class="btn btn-primary add-bussiness text-right"><i class="fa fa-plus"></i> ADD NEW RESERVATION</a> -->
        <hr>
        <table id="table"></table>
    </div>
</div>


@endsection

@section('js')
<style>

</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.29.1/moment.min.js"></script>
<script>
    $(function() {
        var $table = $('#table');

        $table.bootstrapTable({
            sortName: 'created_at',
            sortOrder: 'ASC',
            url: HelperService.base_url + '/api/admin/reservasi-online',
            responseHandler: function(res) {
                return res.data;
            },
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'CUSTOMER',
                    // field: 'customer.nama',
                    formatter: function(value, row, index) {
                        if(row.read_at == null){
                            return row.customer.nama + ' <span class="badge badge-primary badge-phil">New Reservation</span';
                        }else{
                            return row.customer.nama;
                        }
                        
                    }
                },
                {
                    title: "CHECKIN",
                    field: "checkin_date",
                    formatter: function(value, row, index) {
                        return moment(row.checkin_date).format("DD MMMM YYYY");
                    }
                },
                {
                    title: "CHECKOUT",
                    field: "checkout_date",
                    formatter: function(value, row, index) {
                        return moment(row.checkout_date).format("DD MMMM YYYY");
                    }
                },
                {
                    title: 'RESERVATION STATUS',
                    formatter: function(value, row, index) {
                        if (row.status.id == 1) {
                            return '<button type="button" data-id ="'+ row.id +'" class="btn btn-primary btn-xs btn_checkin">Checkin</button>';
                            // return '<span class="badge badge-primary btn-sm btn_checkin">Checkin</span>';
                        } else if (row.status.id == 2) {
                            // return '<button type="button" data-id ="'+ row.id +'" class="btn btn-warning btn-xs " style="color:white">Checkout</button>';
                            return '<span class="badge badge-warning btn-sm " style="color:white">Checkout</span>';
                        } else if (row.status.id == 3) {
                            // return '<button type="button" data-id ="'+ row.id +'" class="btn btn-danger btn-xs">Cancel</button>';
                            return '<span class="badge badge-danger btn-sm">Cancel</span>';
                        } else if (row.status.id == 4) {
                            // return '<button type="button" data-id ="'+ row.id +'" class="btn btn-success btn-xs reschedule" id="res">Reservation</button>';
                            return '<span class="badge badge-info btn-sm reschedule" id="res">Reschedule</span>';
                        } else if (row.status.id == 5) {
                            return '<button type="button" data-id ="'+ row.id +'" class="btn btn-success btn-xs btn_reserv" id="res">Reservation</button>';
                            // return '<span class="badge badge-success btn-sm btn_reserv" id="res">Reservation</span>';
                        }
                    }
                },
                {
                    title: 'PACKAGE',
                    field: 'package.nama_paket',
                },
                {
                    title: 'BERAPA MALAM',
                    field: 'berapa_malam',
                },
                {
                    title: 'PLAT NOMOR',
                    field: 'plat_nomor',
                },
                {
                    title: 'PAYMENT STATUS',
                    formatter: function(value, row, index) {
                        if (row.payment_status.id == 1) {
                            return '<span class="badge badge-danger badge-pill">Belum Lunas</span>';
                        } else if (row.payment_status.id == 2) {
                            return '<span class="badge badge-success badge-pill">Lunas</span>';
                        }
                    }
                },
                
                {
                    title: 'ACTION',
                    sortable: false,
                    formatter: function(value, row, index) {
                        view_link = "<a class='btn btn-primary btn-sm' href='" + HelperService.url('/admin/reservasi-online/edit/' + row.id) + "'>Edit</a>";
                        // view_inv = "<a class='btn btn-success btn-sm' target='_blank' href='" + HelperService.url('/admin/reservasi-online/view-invoice/' + row.id) + "'>Invoice</a>";
                        view_pay = "<a class='btn btn-danger btn-sm' href='" + HelperService.url('/admin/reservasi-online/payment-detail/' + row.id) + "'>Payment</a>";
                        // btn_delete_item = '<button type="button" data-id="' + row.request_code + '" class="btn btn-danger btn-sm delete-btb"><i class="fa fa-trash"></i></button>';
                        return view_link + ' ' + view_pay;
                    },
                },
                
                
            ]
        });

        $(document).on('click', '.btn_reserv', function(e) {
            id = $(this).attr('data-id');
            HelperService.confirm(function() {
                HelperService.loadingStart();
                $.ajax({
                    url: HelperService.apiUrl('/admin/reservasi/change-status-checkin/' + id),
                    type: 'post',

                    success: function(res) {
                        HelperService.loadingStop();
                        HelperService.showNotification('success', 'Berhasil Checkin.');
                        location.reload();
                    },
                    error: function(res) {
                        console.log(res);
                        HelperService.showNotification('error', res.responseJSON.message);
                        HelperService.loadingStop();
                    }
                })
            })
            e.preventDefault()
        })

        $(document).on('click', '.btn_checkin', function(e) {
            id = $(this).attr('data-id');
            HelperService.confirm(function() {
                HelperService.loadingStart();
                $.ajax({
                    url: HelperService.apiUrl('/admin/reservasi/change-status-checkout/' + id),
                    type: 'post',

                    success: function(res) {
                        HelperService.loadingStop();
                        HelperService.showNotification('success', 'Berhasil Checkout.');
                        location.reload();
                    },
                    error: function(res) {
                        console.log(res);
                        HelperService.showNotification('error', res.responseJSON.message);
                        HelperService.loadingStop();
                    }
                })
            })
            e.preventDefault()
        })
    })

    
</script>
@endsection