@extends($master_template)
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <form id="save_data_reservasi">
        <div class="card-body">
            <div id="accordion">
                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="card-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseCustomer">
                                #1 DATA CUSTOMER
                            </a>
                        </h4>
                    </div>
                    <div id="collapseCustomer" class="panel-collapse in collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="">Nama User</label>
                                    <input type="text" class="form-control nama" id="nama" name="nama">
                                </div>
                                <div class="col-md-6">
                                    <label for="">Nomor Telepon</label>
                                    <input type="number" class="form-control telepon" id="telepon" name="telepon">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="card-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseReservasi">
                                #2 DATA RESERVASI
                            </a>
                        </h4>
                    </div>
                    <div id="collapseReservasi" class="panel-collapse in collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6" style="margin-bottom: 20px;">
                                    <label for="">Paket</label>
                                    <select name="id_paket" id="id_paket" class="form-control id_paket">
                                        <option value="" selected>Pilih Paket</option>
                                        @foreach($packages as $package)
                                        <option value="{{ $package->id }}">{{ $package->nama_paket }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6" style="margin-bottom: 20px;">
                                    <label for="">Berapa Malam</label>
                                    <input type="number" class="form-control berapa_malam" id="berapa_malam" name="berapa_malam">
                                </div>
                                <div class="col-md-6" style="margin-bottom: 20px;">
                                    <label for="">Checkin</label>
                                    <input type="text" class="form-control checkin" id="checkin" name="checkin">
                                </div>
                                <div class="col-md-6" style="margin-bottom: 20px;">
                                    <label for="">Berapa Orang</label>
                                    <input type="number" class="form-control pax" id="pax" name="pax">
                                </div>
                                <div class="col-md-12" style="margin-bottom: 20px;">
                                    <label for="">Marketing</label>
                                    <select name="id_penanggung_jawab" id="id_penanggung_jawab" class="form-control id_penanggung_jawab">
                                        @foreach($users as $usr)
                                        <option value="{{ $usr->id }}">{{ $usr->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <!-- <div class="col-md-12" style="margin-bottom: 20px;">
                                    <label for="">Campsite</label>
                                    <select name="campsite_id" id="campsite_id" class="form-control campsite_id">
                                        <option value="">Choose Campsite</option>
                                        @foreach($campsite as $camp)
                                        <option value="{{ $camp->id }}">{{ $camp->campsite }}</option>
                                        @endforeach
                                    </select>
                                </div> -->
                                <!-- <div class="col-md-12" style="margin-bottom: 20px;">
                                    <label for="">Block</label>
                                    <select name="capmsite_block_site_id" id="blok_camp_id" class="form-control blok_camp_id">

                                    </select>
                                </div> -->
                                <div class="col-md-12" style="margin-bottom: 20px;">
                                    <label for="">Status</label>
                                    <select name="reservation_status_id" class="form-control reservation_status-select">
                                        <!-- <option value="">Choose Status</option> -->
                                    </select>
                                </div>
                                <div class="col-md-12" style="margin-bottom: 20px;">
                                    <label for="">Plat Nomor Pengunjung</label>
                                    <input type="text" name="plat_nomor" class="form-control" placeholder="F 1234 ABC">
                                </div>
                                <div class="col-md-12">
                                    <label for="">Note</label>
                                    <textarea name="note" id="note" class="form-control note" cols="30" rows="5"></textarea>
                                </div>
                                <!-- <input type="text" name="nilai_project" id="nila-project" class="form-control" readonly> -->
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="card-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseBlockSite">
                                #3 DATA CAMPSITE
                            </a>
                        </h4>
                    </div>
                    <div id="collapseBlockSite" class="panel-collapse in collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12" style="margin-top: 10px;" id="block-site-content"> </div>
                                <div class="col-md-12" style="margin-top: 10px;">
                                    <button type="button" class="btn btn-sm btn-primary waves-effect" id="btn-add-campsite"><i class="fa fa-plus"></i> Add Item</button>
                                    <!-- <button type="submit" class="btn btn-sm btn-success waves-effect"><i class="fa fa-plus"></i> Save Item</button> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="card-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseAdditionalItem">
                                #4 DATA ADDITIONAL ITEM (Optional)
                            </a>
                        </h4>
                    </div>
                    <div id="collapseAdditionalItem" class="panel-collapse in collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12" style="margin-top: 10px;" id="outlet-content"> </div>
                                <div class="col-md-12" style="margin-top: 10px;">
                                    <button type="button" class="btn btn-sm btn-primary waves-effect" id="btn-add-outlet"><i class="fa fa-plus"></i> Add Item</button>
                                    <!-- <button type="submit" class="btn btn-sm btn-success waves-effect"><i class="fa fa-plus"></i> Save Item</button> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="card-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseItemIncludePackage">
                                #5 ITEM INCLUDE IN PACKAGE
                            </a>
                        </h4>
                    </div>
                    <div id="collapseItemIncludePackage" class="panel-collapse in collapse show">
                        <div class="card-body" id="additional-item">
                        </div>
                    </div>
                </div>

                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="card-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTicket">
                                #6 TICKET
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTicket" class="panel-collapse in collapse show">
                        <div class="card-body" id="ticket-area">
                        </div>
                        <div class="card-footer">
                            <button type="button" class="btn btn-sm btn-primary waves-effect" id="btn-add-ticket"><i class="fa fa-plus"></i> Add Ticket</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-header">
            <button type="submit" class="btn btn-primary">SAVE RESERVASI</button>
        </div>
    </form>
</div>
@endsection

@section('js')
<script>
    var DATA_UNIQ = 1;
    var $http = new HttpService();
    var $form = new FormService($('#save_data_reservasi'))
        .withArrayField([
            'ticket_id',
            'include_item',
            'include_qty',
            'include_id',
            'unit_id',
            'qty',
            'durasi',
            'item_price',
            'ticket_selling_price',
            'capmsite_block_site_id'
        ]);

    HelperService.datepicker('.checkin')
    HelperService.loadStatusReservasiSelect2();
    ticketInputTemplateAppend(DATA_UNIQ);
    show();
    showBlockSite();

    $form.onSubmit(function(data) {

        $http.post('/api/admin/reservasi/store', data).then(function(resp) {
            HelperService.redirect('/admin/reservasi');
        })
    })

    // var total_part = 0;
    // $.each(item_price, function(key, category) {

    // });
    // return option;

    $('#id_paket').change(function() {
        id = $(this).val();
        $http.get('/api/admin/reservasi/select2-get-include-item/' + id)
            .then(function(resp) {
                template = '';
                $.each(resp.data.items, function(k, row) {
                    template += additionalItemTemplate(DATA_UNIQ, row);
                    DATA_UNIQ++;
                })

                $('#additional-item').html(template);
            })
            .catch(function(request, status, error) {
                $('#additional-item').html('');
            })
    })

    // $(document).on('change', '.campsite_id', function() {
    //     $('.blok_camp_id').html('<option value="">Choose Block</option>');
    //     campsite_id = $(this).find(':selected').val();
    //     loaSelectBlock(campsite_id);
    // })

    $('#btn-add-outlet').click(function() {
        user_id = $('.user-select').find(':selected').val();
        $('#outlet-content').append(outletBoxContentTemplate(DATA_UNIQ));
        loadUnitSelect2(DATA_UNIQ);
        DATA_UNIQ++;
    })

    $('#btn-add-campsite').click(function() {
        user_id = $('.user-select').find(':selected').val();
        $('#block-site-content').append(campSiteBoxContentTemplate(DATA_UNIQ));
        loaSelectBlock(DATA_UNIQ);
        DATA_UNIQ++;
    })

    $('#btn-add-ticket').click(function() {
        ticketInputTemplateAppend(DATA_UNIQ);
    })

    $(document).on('click', '.btn-delete-outlet', function() {
        console.log('klik')
        uniq = $(this).attr('data-uniq');
        $('.container-detail-card-' + uniq).remove();
    })

    $(document).on('click', '.h', function() {
        console.log('klik')
        uniq = $(this).attr('data-uniq');
        $('.container-detail-card-' + uniq).remove();
    })

    $(document).on('click', '.hapus-tiket', function() {
        uniq = $(this).attr('data-uniq');
        $('.container-input-ticket-' + uniq).remove();
    })

    function show() {
        $('#outlet-content').append(outletBoxContentTemplate(DATA_UNIQ, show_delete_btn = false));
        loadUnitSelect2(DATA_UNIQ);
        DATA_UNIQ++;
    }

    function showBlockSite() {
        $('#block-site-content').append(campSiteBoxContentTemplate(DATA_UNIQ, show_delete_btn = false));
        loaSelectBlock(DATA_UNIQ);
        DATA_UNIQ++;
    }

    function loadUnitSelect2(uniq) {
        HelperService
            .select2Static(".unit-select-" + uniq, '/api/admin/reservasi/select2-item', function(item) {
                return {
                    id: item.id,
                    text: item.nama_barang,
                    data: item,
                }
            })
    }

    function loaSelectBlock(uniq) {
        HelperService
            .select2Static(".blok_camp_id-" + uniq, '/api/admin/campsites/list-block', function(item) {
                return {
                    id: item.id,
                    text: item.block.name + ' - ' + item.name + ' : Capacity ' + item.capacity,
                    data: item,
                }
            })
    }

    $(document).on('change', '.unit-select', function() {
        id = $(this).val();
        uniq = $(this).attr('data-uniq');

        $http.get('/api/admin/reservasi/select2-get-price/' + id)
            .then(function(resp) {

                // var arr = data.item_price,
                // n   = arr.length,
                // sum = 0;
                // while(n--)
                // sum += parseFloat(arr[n]) || 0;
                // $('#nila-project').val(sum)
                // console.log(sum)
                $('.item_price-' + uniq).val(resp.data.harga_jual);
                var array = JSON.parse("[" + resp.data.harga_jual + "]");
                console.log(array)

            })
            .catch(function(request, status, error) {
                $('#additional-item').html('');
            })
    })

    $(document).on('change', '.ticket-select', function() {
        ticket = null;
        uniq = $(this).attr('data-uniq');
        ticketSelectData = $(this).select2('data');        
        if(ticketSelectData.length){
            ticket = ticketSelectData[0].data;
            $(`.ticket_selling_price-${uniq}`).val(ticket.selling_price);
        }
    })

    function outletBoxContentTemplate(uniq, show_delete_btn = true) {
        btn_delete_style = 'display:none';
        if (show_delete_btn) {
            btn_delete_style = '';
        }
        var template = `
            <div class="card container-detail-card-${uniq}" style="margin-bottom: 20px;">
                <input type="hidden" id="get_id_uniq" value="${uniq}">
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Item</label>
                                    <select class="form-control unit-select unit-select-${uniq}" id="unit_id" name="unit_id" style="width:100%" data-uniq="${uniq}">
                                        <option value="">Select Item</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Item Price</label>
                                    <input type="number" class="form-control item_price item_price-${uniq}" id="item_price" name="item_price" placeholder="item price" data-uniq="${uniq}" readonly>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Qty</label>
                                    <input type="number" class="form-control qty" id="qty" name="qty" placeholder="QTY">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Durasi</label>
                                    <input type="number" class="form-control durasi" id="durasi" name="durasi" placeholder="Durasi">
                                </div>
                            </div>
                        </div>
                    </div>
                    <button style="${btn_delete_style}" type="button" class="btn btn-sm btn-danger waves-effect btn-delete-outlet" data-uniq="${uniq}"><i
                            class="fa fa-trash"></i> Delete Item</button>
                </div>
            </div>
        `;
        return template;
    }

    function campSiteBoxContentTemplate(uniq, show_delete_btn = true) {
        btn_delete_style = 'display:none';
        if (show_delete_btn) {
            btn_delete_style = '';
        }
        var template = `
            <div class="card container-detail-card-${uniq}" style="margin-bottom: 20px;">
                <input type="hidden" id="get_id_uniq" value="${uniq}">
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Campsite Block</label>
                                    <select name="capmsite_block_site_id" id="blok_camp_id" class="form-control blok_camp_id-${uniq}" style="width:100%" data-uniq="${uniq}">
                                        <option value="">Select Campsite Block</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button style="${btn_delete_style}" type="button" class="btn btn-sm btn-danger waves-effect btn-delete-outlet" data-uniq="${uniq}"><i
                            class="fa fa-trash"></i> Delete Item</button>
                </div>
            </div>
        `;
        return template;
    }

    function additionalItemTemplate(uniq, row) {
        console.log(row);
        var template = `
            <div class="card container-detail-card-${uniq}" style="margin-bottom: 20px;">
                <input type="hidden" id="get_id_uniq" value="${uniq}">
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Item</label>
                                    <input type="text" class="form-control include_item" name="include_item" placeholder="item" value="${row.vendor_item.nama_barang}" disabled>
                                    <input type="hidden" class="form-control include_id" name="include_id" placeholder="ids" value="${row.vendor_item_id}" disabled>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Qty</label>
                                    <input type="number" class="form-control include_qty" name="include_qty" placeholder="QTY">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `;
        return template;
    }

    function ticketInputTemplate(uniq) {
        var template = `        
            <div class="card container-input-ticket-${uniq}" style="margin-bottom: 20px;">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8">                            
                            <div class="form-group">
                                <label>Ticket</label>
                                <select class="form-control ticket-select ticket-select-${uniq}" data-uniq="${uniq}" name="ticket_id">
                                    <option selected value="">Choose...</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Harga Jual</label>                            
                            <div class="input-group">        
                                <input type="text" class="form-control ticket_selling_price-${uniq}" name="ticket_selling_price">
                                <span class="input-group-append">
                                    <button type="button" class="btn btn-sm btn-danger hapus-tiket" data-uniq="${uniq}">Delete</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `;
        return template;
    }

    function ticketInputTemplateAppend(uniq) {
        $('#ticket-area').append(ticketInputTemplate(uniq));
        HelperService.loadTicketSelect2(`.ticket-select-${uniq}`);
        DATA_UNIQ++;
    }

    
</script>
@endsection
