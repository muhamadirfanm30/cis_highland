@extends($master_template)
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{$title}}</h3>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="card">
            <div class="card-body">
                <div class="p-5 bg-white rounded mb-5">
                    <!-- Lined tabs-->
                    <ul id="myTab2" role="tablist" class="nav nav-tabs nav-pills with-arrow lined flex-column flex-sm-row text-center">
                        <li class="nav-item flex-sm-fill">
                            <a id="tab-customer-tab" data-toggle="tab" href="#tab-customer" role="tab" aria-controls="tab-customer" aria-selected="true" class="nav-link text-uppercase font-weight-bold mr-sm-3 rounded-0 active">#1 DATA CUSTOMER</a>
                        </li>
                        <li class="nav-item flex-sm-fill">
                            <a id="profile2-tab" data-toggle="tab" href="#profile2" role="tab" aria-controls="profile2" aria-selected="false" class="nav-link text-uppercase font-weight-bold mr-sm-3 rounded-0">Profile</a>
                        </li>
                    </ul>
                    <div id="myTab2Content" class="tab-content">
                        <div id="tab-custome" role="tabpanel" aria-labelledby="home-tab" class="tab-pane fade px-4 py-5 show active">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="">Nama User</label>
                                    <input type="text" class="form-control nama" id="nama" name="nama">
                                </div>
                                <div class="col-md-6">
                                    <label for="">Nomor Telepon</label>
                                    <input type="number" class="form-control telepon" id="telepon" name="telepon">
                                </div>
                            </div>
                        </div>
                        <div id="profile2" role="tabpanel" aria-labelledby="profile-tab" class="tab-pane fade px-4 py-5">
                            <p class="leade font-italic">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                                irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                            <p class="leade font-italic mb-0">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                                irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                    </div>
                    <!-- End lined tabs -->
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{$title}}</h3>
            </div>
        </div>
    </div>
</div>


@endsection

@section('js')
<style>

</style>
<script>
</script>
@endsection