@extends($master_template)
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <form id="save_data_reservasi">
        <input type="hidden" name="id" value="{{ $reservasion->id }}">
        <div class="card-body">
            <div id="accordion">
                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="card-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseCustomer">
                                #1 DATA CUSTOMER
                            </a>
                        </h4>
                    </div>
                    <div id="collapseCustomer" class="panel-collapse in collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="">Nama User</label>
                                    <input type="text" class="form-control nama" name="nama" value="{{ $reservasion->customer->nama }}">
                                </div>
                                <div class="col-md-6">
                                    <label for="">Nomor Telepon</label>
                                    <input type="number" class="form-control telepon" name="telepon" value="{{ $reservasion->customer->telepon }}">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="card-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseReservasi">
                                #2 DATA RESERVASI
                            </a>
                        </h4>
                    </div>
                    <div id="collapseReservasi" class="panel-collapse in collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6" style="margin-bottom: 20px;">
                                    <label for="">Paket</label>
                                    <select name="id_paket" class="form-control select_package">
                                        <option value="" selected>Pilih Paket</option>
                                        @foreach($packages as $package)
                                        <option value="{{ $package->id }}" {{ $package->id == $reservasion->package_id ? 'selected' : '' }}>{{ $package->nama_paket }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-6" style="margin-bottom: 20px;">
                                    <label for="">Berapa Malam</label>
                                    <input type="number" class="form-control berapa_malam" name="berapa_malam" value="{{ $reservasion->berapa_malam }}">
                                </div>
                                <div class="col-md-6" style="margin-bottom: 20px;">
                                    <label for="">Checkin</label>
                                    <input type="text" class="form-control checkin" name="checkin" value="{{ $reservasion->checkin_date }}">
                                </div>
                                <div class="col-md-6" style="margin-bottom: 20px;">
                                    <label for="">Berapa Orang</label>
                                    <input type="number" class="form-control pax" name="pax" value="{{ $reservasion->pax }}">
                                </div>
                                <div class="col-md-12" style="margin-bottom: 20px;">
                                    <label for="">Penanggung jawab</label>
                                    <select name="id_penanggung_jawab" class="form-control id_penanggung_jawab">
                                        @foreach($users as $usr)
                                        <option value="{{ $usr->id }}" {{ $usr->id == $reservasion->user_id ? 'selected' : '' }}>{{ $usr->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                
                                <div class="col-md-12" style="margin-bottom: 20px;">
                                    <label for="">Status</label>
                                    <select name="reservation_status_id" class="form-control reservation_status-select">
                                        <option value="{{ $reservasion->reservation_status_id }}" selected>{{ $reservasion->status->name }}</option>
                                    </select>
                                </div>
                                <div class="col-md-12" style="margin-bottom: 20px;">
                                    <label for="">Plat Nomor Pengunjung</label>
                                    <input type="text" name="plat_nomor" class="form-control" placeholder="F 1234 ABC" value="{{ $reservasion->plat_nomor }}">
                                </div>
                                <div class="col-md-12">
                                    <label for="">Note</label>
                                    <textarea name="note" class="form-control note" cols="30" rows="5">{{ $reservasion->note }}</textarea>
                                </div>
                                <input type="text" name="nilai_project" id="nila-project" class="form-control" value="{{ $reservasion->nilai_project }}" readonly>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="card-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseBlockSite">
                                #3 DATA CAMPSITE
                            </a>
                        </h4>
                    </div>
                    <div id="collapseBlockSite" class="panel-collapse in collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12" style="margin-top: 10px;" id="block-site-content">
                                    @foreach($get_campsite as $camp)
                                        <div class="card container-detail-card" style="margin-bottom: 20px;">
                                            <input type="hidden" id="get_id_uniq" value="{{ $camp->id }}">
                                            <div class="card-body">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label>Campsite Block</label>
                                                                <select name="capmsite_block_site_id" id="blok_camp_id" class="form-control blok_camp_id" style="width:100%" data-uniq="{{ $camp->id }}">
                                                                    <option value="{{ $camp->capmsite_block_site_id }}">Block : {{ $camp->get_campsite->block->name }} | Site : {{ $camp->get_campsite->name }}</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                                <div class="col-md-12" style="margin-top: 10px;">
                                    <button type="button" class="btn btn-sm btn-primary waves-effect" id="btn-add-campsite"><i class="fa fa-plus"></i> Add Item</button>
                                    <!-- <button type="submit" class="btn btn-sm btn-success waves-effect"><i class="fa fa-plus"></i> Save Item</button> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="card-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseAdditionalItem">
                                #4 DATA ADDITIONAL ITEM (Optional)
                            </a>
                        </h4>
                    </div>
                    <div id="collapseAdditionalItem" class="panel-collapse in collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12" style="margin-top: 10px;" id="outlet-content">
                                    @foreach($reservasion->details as $detail)
                                    @if($detail->is_include_paket == 0)
                                    <div class="card container-detail-card-{{$detail->id}}" style="margin-bottom: 20px;">
                                        <input type="hidden" name="additional_reservation_detail_id" value="{{ $detail->id }}">
                                        <div class="card-body">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            @if($detail->is_new_additional == 1)
                                                            <label>Item <span class="badge badge-primary badge-phil">New Items</span></label>
                                                            @else
                                                            <label>Item</label>
                                                            @endif
                                                            <select class="form-control unit-select unit-select-{{$detail->vendor_item_id}}" name="additional_item_id" style="width:100%" data-uniq="{{$detail->id}}">
                                                                <option value="{{$detail->vendor_item_id}}" selected>{{$detail->vendor_item->nama_barang}}</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Item Price</label>
                                                            <input type="number" class="form-control item_price item_price-{{$detail->id}}" name="item_price" placeholder="item price" data-uniq="{{$detail->id}}" value="{{ $detail->item_price }}" readonly>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Qty</label>
                                                            <input type="number" class="form-control additional_qty" name="additional_qty" placeholder="QTY" value="{{ $detail->qty }}">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="form-group">
                                                            <label>Durasi</label>
                                                            <input type="number" class="form-control additional_durasi" name="additional_durasi" placeholder="Durasi" value="{{ $detail->durasi }}">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <button type="button" class="btn btn-sm btn-danger waves-effect btn-delete-outlet" data-uniq="{{$detail->id}}">
                                                <iclass="fa fa-trash"></i> Delete Item
                                            </button>
                                        </div>
                                    </div>
                                    @endif
                                    @endforeach
                                </div>
                                <div class="col-md-12" style="margin-top: 10px;">
                                    <button type="button" class="btn btn-sm btn-primary waves-effect" id="btn-add-outlet"><i class="fa fa-plus"></i> Add Item</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="card-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseItemIncludePackage">
                                #5 ITEM INCLUDE IN PACKAGE
                            </a>
                        </h4>
                    </div>
                    <div id="collapseItemIncludePackage" class="panel-collapse in collapse show">
                        <div class="card-body" id="item-package-content">
                            @foreach($reservasion->details as $detail)
                            @if($detail->is_include_paket == 1)
                            <div class="card container-detail-card-{{ $detail->id }}" style="margin-bottom: 20px;">
                                <input type="hidden" name="package_item_reservation_detail_id" value="{{ $detail->id }}">
                                <div class="card-body">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Item</label>
                                                    <input type="text" class="form-control include_item" name="include_item" placeholder="item" value="{{ $detail->vendor_item->nama_barang }}" disabled>
                                                    <input type="hidden" class="form-control package_item_id" name="package_item_id" placeholder="ids" value="{{ $detail->vendor_item_id }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Qty</label>
                                                    <input type="number" class="form-control package_item_qty" name="package_item_qty" placeholder="QTY" value="{{ $detail->qty }}">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="card-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseTicket">
                                #6 TICKET
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTicket" class="panel-collapse in collapse show">
                        <div class="card-body" id="ticket-area">
                            @foreach($reservasion->tickets as $ticket)
                            <input name="ticket_transaction_id" type="hidden" value="{{ $ticket->id }}">
                            <div class="card container-input-ticket-{{ $ticket->id }}" style="margin-bottom: 20px;">
                                <div class="card-body">
                                    <div class="row">
                                    <div class="col-md-8">                            
                                        <div class="form-group">
                                            <label>Ticket</label>
                                            <select class="form-control ticket-select-{{ $ticket->id }}" data-uniq="{{ $ticket->id }}" name="ticket_id">
                                                <option value="{{$ticket->ticket_id}}" selected>{{$ticket->ticket->name}}</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label>Harga Jual</label>                            
                                        <div class="input-group">        
                                            <input type="text" class="form-control ticket_selling_price-${uniq}" value="{{ $ticket->transaction_selling_price }}" name="ticket_selling_price">
                                            <span class="input-group-append">
                                                <button style="margin-top: 10px;" type="button" trx-id="{{ $ticket->id }}" class="btn btn-sm btn-danger hapus-tiket" data-uniq="{{ $ticket->id }}">Delete</button>
                                            </span>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="card-footer">
                            <button type="button" class="btn btn-sm btn-primary waves-effect" id="btn-add-ticket"><i class="fa fa-plus"></i> Add Ticket</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-header">
            <button type="submit" class="btn btn-primary">SAVE RESERVASI</button>
        </div>
    </form>
</div>
@endsection

@section('js')
<script>
    var DATA_UNIQ = 1;
    var $http = new HttpService();
    var $form = new FormService($('#save_data_reservasi'))
        .withArrayField([
            'package_item_reservation_detail_id',
            'package_item_qty',
            'package_item_id',
            'additional_item_id',
            'additional_qty',
            'additional_durasi',
            'additional_reservation_detail_id',
            'ticket_id',
            'ticket_transaction_id',
            'ticket_selling_price'
        ]);

    HelperService.datepicker('.checkin')
    HelperService.loadStatusReservasiSelect2();
    HelperService.select2Static(".unit-select", '/api/admin/reservasi/select2-item', function(item) {
        return {
            id: item.id,
            text: item.nama_barang,
            data: item,
        }
    });
    show();
    showBlockSite();

    $form.onSubmit(function(data) {

        $http.put('/api/admin/reservasi/' + data.id, data).then(function(resp) {
            HelperService.redirect('/admin/reservasi');
        })
    })

    $('.select_package').change(function() {
        id = $(this).val();
        $http.get('/api/admin/reservasi/select2-get-include-item/' + id)
            .then(function(resp) {
                template = '';
                $.each(resp.data.items, function(k, row) {
                    template += packageItemTemplate(DATA_UNIQ, row);
                    DATA_UNIQ++;
                })

                $('#item-package-content').html(template);
            })
            .catch(function(request, status, error) {
                $('#item-package-content').html('');
            })
    })

    // $(document).on('change', '.campsite_id', function() {
    //     $('.blok_camp_id').html('<option value="">Choose Block</option>');
    //     campsite_id = $(this).find(':selected').val();
    //     loaSelectBlock(campsite_id);
    // })

    $('#btn-add-outlet').click(function() {
        user_id = $('.user-select').find(':selected').val();
        $('#outlet-content').append(additionalItemTemplate(DATA_UNIQ));
        loadUnitSelect2(DATA_UNIQ);
        DATA_UNIQ++;
    })

    $('#btn-add-campsite').click(function() {
        user_id = $('.user-select').find(':selected').val();
        $('#block-site-content').append(campSiteBoxContentTemplate(DATA_UNIQ));
        loaSelectBlock(DATA_UNIQ);
        DATA_UNIQ++;
    })

    $(document).on('click', '.btn-delete-outlet', function() {
        console.log('klik')
        uniq = $(this).attr('data-uniq');
        $('.container-detail-card-' + uniq).remove();
    })

    $(document).on('click', '.hapus-tiket', function() {
        uniq = $(this).attr('data-uniq');
        id = $(this).attr('trx-id');
        if (id != 0) {
            $http.delete(`/api/admin/tickets/transaction/${id}`).then(function() {
                $('.container-input-ticket-' + uniq).remove();
            })
        } else {
            $('.container-input-ticket-' + uniq).remove();
        }
    })

    $('#btn-add-ticket').click(function() {
        ticketInputTemplateAppend(DATA_UNIQ);
    })

    function show() {
        $('#outlet-content').append(additionalItemTemplate(DATA_UNIQ, show_delete_btn = false));
        loadUnitSelect2(DATA_UNIQ);
        DATA_UNIQ++;
    }

    function showBlockSite() {
        $('#block-site-content').append(campSiteBoxContentTemplate(DATA_UNIQ, show_delete_btn = false));
        loaSelectBlock(DATA_UNIQ);
        DATA_UNIQ++;
    }

    function loadUnitSelect2(uniq) {
        HelperService
            .select2Static(".unit-select-" + uniq, '/api/admin/reservasi/select2-item', function(item) {
                return {
                    id: item.id,
                    text: item.nama_barang,
                    data: item,
                }
            })
    }

    function loaSelectBlock(uniq) {
        HelperService
            .select2Static(".blok_camp_id-" + uniq, '/api/admin/campsites/list-block', function(item) {
                return {
                    id: item.id,
                    text: item.block.name + ' - ' + item.name + ' : Capacity ' + item.capacity,
                    data: item,
                }
            })
    }

    $(document).on('change', '.unit-select', function() {
        id = $(this).val();
        uniq = $(this).attr('data-uniq');

        $http.get('/api/admin/reservasi/select2-get-price/' + id)
            .then(function(resp) {
                $('.item_price-' + uniq).val(resp.data.harga_jual);
                var array = JSON.parse("[" + resp.data.harga_jual + "]");
            })
            .catch(function(request, status, error) {
                $('#item-package-content').html('');
            })
    })

    $(document).on('change', '.ticket-select', function() {
        ticket = null;
        uniq = $(this).attr('data-uniq');
        ticketSelectData = $(this).select2('data');        
        if(ticketSelectData.length){
            ticket = ticketSelectData[0].data;
            $(`.ticket_selling_price-${uniq}`).val(ticket.selling_price);
        }
    })

    function additionalItemTemplate(uniq, show_delete_btn = true) {
        btn_delete_style = 'display:none';
        if (show_delete_btn) {
            btn_delete_style = '';
        }
        var template = `
            <div class="card container-detail-card-${uniq}" style="margin-bottom: 20px;">
                <input type="hidden" name="additional_reservation_detail_id" value="0">
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Item</label>
                                    <select class="form-control unit-select unit-select-${uniq}" name="additional_item_id" style="width:100%" data-uniq="${uniq}">
                                        <option value="">Select Item</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Item Price</label>
                                    <input type="number" class="form-control item_price item_price-${uniq}"  name="item_price" placeholder="item price" data-uniq="${uniq}" readonly>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Qty</label>
                                    <input type="number" class="form-control additional_qty" name="additional_qty" placeholder="QTY">
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label>Durasi</label>
                                    <input type="number" class="form-control additional_durasi" name="additional_durasi" placeholder="Durasi">
                                </div>
                            </div>
                        </div>
                    </div>
                    <button style="${btn_delete_style}" type="button" class="btn btn-sm btn-danger waves-effect btn-delete-outlet" data-uniq="${uniq}"><i
                            class="fa fa-trash"></i> Delete Item</button>
                </div>
            </div>
        `;
        return template;
    }

    function campSiteBoxContentTemplate(uniq, show_delete_btn = true) {
        btn_delete_style = 'display:none';
        if (show_delete_btn) {
            btn_delete_style = '';
        }
        var template = `
            <div class="card container-detail-card-${uniq}" style="margin-bottom: 20px;">
                <input type="hidden" id="get_id_uniq" value="${uniq}">
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label>Campsite Block</label>
                                    <select name="capmsite_block_site_id" id="blok_camp_id" class="form-control blok_camp_id-${uniq}" style="width:100%" data-uniq="${uniq}">
                                        <option value="">Select Campsite Block</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button style="${btn_delete_style}" type="button" class="btn btn-sm btn-danger waves-effect btn-delete-outlet" data-uniq="${uniq}"><i
                            class="fa fa-trash"></i> Delete Item</button>
                </div>
            </div>
        `;
        return template;
    }

    function packageItemTemplate(uniq, row) {
        console.log(row);
        var template = `
            <div class="card container-detail-card-${uniq}" style="margin-bottom: 20px;">
                <input type="hidden" name="package_item_reservation_detail_id" value="0">
                <div class="card-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Item</label>
                                    <input type="text" class="form-control" placeholder="item" value="${row.vendor_item.nama_barang}" disabled>
                                    <input type="hidden" class="form-control package_item_id" name="package_item_id" placeholder="ids" value="${row.vendor_item_id}" disabled>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Qty</label>
                                    <input type="number" class="form-control package_item_qty" name="package_item_qty" placeholder="QTY">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `;
        return template;
    }

    function ticketInputTemplate(uniq) {
        var template = `        
            <div class="card container-input-ticket-${uniq}" style="margin-bottom: 20px;">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-8">                            
                            <div class="form-group">
                                <label>Ticket</label>
                                <select class="form-control ticket-select ticket-select-${uniq}" data-uniq="${uniq}" name="ticket_id">
                                    <option selected value="">Choose...</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <label>Harga Jual</label>                            
                            <div class="input-group">        
                                <input type="text" class="form-control ticket_selling_price-${uniq}" name="ticket_selling_price">
                                <span class="input-group-append">
                                    <button type="button" class="btn btn-sm btn-danger hapus-tiket" data-uniq="${uniq}">Delete</button>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        `;
        return template;
    }

    function ticketInputTemplateAppend(uniq) {
        $('#ticket-area').append(ticketInputTemplate(uniq));
        HelperService.loadTicketSelect2(`.ticket-select-${uniq}`);
        DATA_UNIQ++;
    }
</script>
@endsection
