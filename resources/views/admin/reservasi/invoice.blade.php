<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
   
    <title>{{ $reservasi->code_invoice }} | Invoice</title>

    <!-- Google Font: Source Sans Pro -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
</head>
    <style type="text/css">
        /* style sheet for "A4" printing */
        @media print and (width: 21cm) and (height: 29.7cm) {
                @page {
                    margin: 3cm;
                }
        }

        /* style sheet for "letter" printing */
        @media print and (width: 3in) and (height: 3in) {
            @page {
                margin: 1in;
            }
        }

        /* A4 Landscape*/
        @page {
            size: A4 potret;
            /* margin: 10%; */
        }
    </style> 
<body class="hold-transition sidebar-mini">
    <div id="invoice">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-4">
                        <table  style="width: 80%">
                            <tr>
                                <td><strong>person responsible : {{ !empty($reservasi->penanggung_jawab) ? $reservasi->penanggung_jawab->name : '-' }}</strong></td>
                            </tr>
                            <tr>
                                <td>Jl. Situhiang, Megamendung,<br> Kec. Megamendung, Bogor, Jawa Barat 16770</td>
                            </tr>
                        </table><br>
                    </div>
                    <div class="col-md-4 text-center">
                        <table  style="width: 80%">
                            <tr>
                                <td class="text-right"><strong>To : </strong></td>
                                <td class="text-left">{{ $reservasi->customer->nama }}</td>
                            </tr>
                            <tr>
                                <td class="text-right"><strong>Phone : </strong></td>
                                {{-- <td>:</td> --}}
                                <td class="text-left">{{ $reservasi->customer->telepon }}</td>
                            </tr>
                            
                        </table><br>
                    </div>
                    <div class="col-md-4" class="text-right">
                        <table  class="text-right" style="width:100%">
                            <tr>
                                <td>No Invoice</td>
                                <td>{{ $reservasi->code_invoice }}</td>
                            </tr>
                            <tr>
                                <td>Invoice Date</td>
                                <td>{{ date('M-d-Y', strtotime($reservasi->created_at)) }}</td>
                            </tr>
                        </table>
                    </div>
                    
                </div>
            </div>
        </div>
        <table border="1" style="width:100%">
        <tr>
                <td>Item</td>
                <td>Durasi</td>
                <td>Harga</td>
                <td>PAX</td>
                <td>Total</td>
            </tr>
            <tbody>
                @php 
                    $totalPax = $reservasi->package->harga * $reservasi->pax * $reservasi->berapa_malam;
                @endphp
                <tr>
                    <td>{{ $reservasi->package->nama_paket }}</td>
                    <td>{{ $reservasi->berapa_malam }} Hari</td>
                    <td>Rp.{{ number_format($reservasi->package->harga) }}</td>
                    <td>{{ $reservasi->pax }}</td>
                    <td>Rp. {{ number_format($totalPax) }}</td>
                </tr>
            </tbody>
            <tbody>
                @foreach($reservasi->detail_reservasi as $detail)
                    @php 
                        $total = $detail->vendor_item->harga_jual * $detail->qty;
                    @endphp
                    <tr>
                        @if($detail->is_include_paket == 1)
                            <td>{{ $detail->vendor_item->nama_barang }} (Include dalam Paket)</td>
                            <td>{{ $reservasi->berapa_malam }} Hari</td>
                            <td>Rp.0</td>
                            <td>{{ $detail->qty }}</td>
                            <td>0</td>
                        @else
                           @php 
                                $jumlahAdditionalPart = $detail->vendor_item->harga_jual * $detail->qty;
                           @endphp
                            <td>{{ $detail->vendor_item->nama_barang }}</td>
                            <td>{{  $reservasi->berapa_malam }} Hari</td>
                            <td>Rp.{{ number_format($detail->vendor_item->harga_jual) }}</td>
                            <td>{{ $detail->qty }}</td>
                            <td>Rp. {{  number_format($jumlahAdditionalPart) }}</td>
                        @endif
                    </tr>
                @endforeach
            </tbody>
            <tbody>
                @php
                    $arr = 0;
                    foreach ($reservasi->detail_reservasi as $value) {
                        if($value->is_include_paket == 0){
                            $arr += $value->qty * $value->vendor_item->harga_jual;
                        }else{
                            $arr = 0;
                        }
                    }
                    $subtotal = $arr + $totalPax;
                @endphp
                <tr>
                    <td colspan="4" class="text-right">Sub Total</td>
                    <td>Rp. {{ number_format($subtotal) }}</td>
                </tr>
                <tr>
                    <td colspan="4" class="text-right">Grand Total</td>
                    <td>Rp. {{ number_format($subtotal) }}</td>
                </tr>
                <tr>
                    <td colspan="4" class="text-right">DP</td>
                    <td>Rp. 0</td>
                </tr>
                <tr>
                    <td colspan="4" class="text-right">Sisa Pembayaran</td>
                    <td>Rp. 0</td>
                </tr>
            </tbody>
        </table><br><br>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-7">
                    <!--<table>-->
                    <!--    <tr>-->
                    <!--        <td style="text-decoration: underline"><center>Customer</center></td>-->
                    <!--    </tr>-->
                    <!--    <tr>-->
                    <!--        <td><br></td>-->
                    <!--    </tr>-->
                    <!--    <tr>-->
                    <!--        <td><br></td>-->
                    <!--    </tr>-->
                    <!--    <tr>-->
                    <!--        <td><br></td>-->
                    <!--    </tr>-->
                    <!--    <tr>-->
                    <!--        <td><br></td>-->
                    <!--    </tr>-->
                    <!--    <tr>-->
                    <!--        <td><br></td>-->
                    <!--    </tr>-->
                    <!--    <tr>-->
                    <!--        <td>(........................................................)</td>-->
                    <!--    </tr>-->
                    </table>
                </div>
                <div class="col-md-3">
                    <table class="text-right">
                        <tr>
                            <td style="text-decoration: underline"><center>Finance / Accounting</center></td>
                        </tr>
                        <tr>
                            <td><br></td>
                        </tr>
                        <tr>
                            <td><br></td>
                        </tr>
                        <tr>
                            <td>(........................................................)</td>
                        </tr>
                        <tr>
                            <td><center>Signed</center></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="row no-print">
        <div class="col-12">
            <button class="btn btn-primary btn-xl .no-print"  onclick="window.print();return false;"><i class="fas fa-print"></i>&nbsp; Print Quotation</button>
        </div>
    </div>
    <style>
        #invoice{
        padding: 30px;
    }
    
    .invoice {
        position: relative;
        background-color: #FFF;
        min-height: 680px;
        padding: 15px
    }
    
    .invoice header {
        padding: 10px 0;
        margin-bottom: 20px;
        border-bottom: 1px solid #3989c6
    }
    
    .invoice .company-details {
        text-align: right
    }
    
    .invoice .company-details .name {
        margin-top: 0;
        margin-bottom: 0
    }
    
    .invoice .contacts {
        margin-bottom: 20px
    }
    
    .invoice .invoice-to {
        text-align: left
    }
    
    .invoice .invoice-to .to {
        margin-top: 0;
        margin-bottom: 0
    }
    
    .invoice .invoice-details {
        text-align: right
    }
    
    .invoice .invoice-details .invoice-id {
        margin-top: 0;
        color: #3989c6
    }
    
    .invoice main {
        padding-bottom: 50px
    }
    
    .invoice main .thanks {
        margin-top: -100px;
        font-size: 2em;
        margin-bottom: 50px
    }
    
    .invoice main .notices {
        padding-left: 6px;
        border-left: 6px solid #3989c6
    }
    
    .invoice main .notices .notice {
        font-size: 1.2em
    }
    
    .invoice table {
        width: 100%;
        border-collapse: collapse;
        border-spacing: 0;
        margin-bottom: 20px
    }
    
    .invoice table td,.invoice table th {
        padding: 15px;
        background: #eee;
        border-bottom: 1px solid #fff
    }
    
    .invoice table th {
        white-space: nowrap;
        font-weight: 400;
        font-size: 16px
    }
    
    .invoice table td h3 {
        margin: 0;
        font-weight: 400;
        color: #3989c6;
        font-size: 1.2em
    }
    
    .invoice table .qty,.invoice table .total,.invoice table .unit {
        text-align: right;
        font-size: 1.2em
    }
    
    .invoice table .no {
        color: #fff;
        font-size: 1.6em;
        background: #3989c6
    }
    
    .invoice table .unit {
        background: #ddd
    }
    
    .invoice table .total {
        background: #3989c6;
        color: #fff
    }
    
    .invoice table tbody tr:last-child td {
        border: none
    }
    
    .invoice table tfoot td {
        background: 0 0;
        border-bottom: none;
        white-space: nowrap;
        text-align: right;
        padding: 10px 20px;
        font-size: 1.2em;
        border-top: 1px solid #aaa
    }
    
    .invoice table tfoot tr:first-child td {
        border-top: none
    }
    
    .invoice table tfoot tr:last-child td {
        color: #3989c6;
        font-size: 1.4em;
        border-top: 1px solid #3989c6
    }
    
    .invoice table tfoot tr td:first-child {
        border: none
    }
    
    .invoice footer {
        width: 100%;
        text-align: center;
        color: #777;
        border-top: 1px solid #aaa;
        padding: 8px 0
    }
    
    @media print {
        .invoice {
            font-size: 11px!important;
            overflow: hidden!important
        }
    
        .invoice footer {
            position: absolute;
            bottom: 10px;
            page-break-after: always
        }
    
        .invoice>div:last-child {
            page-break-before: always
        }
    }
    </style>
</body>
</html>