@extends($master_template)
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <div id="accordion">
            <div class="card card-primary">
                <div class="card-header">
                    <h4 class="card-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseCustomer">
                            Send Notification
                        </a>
                    </h4>
                </div>
                <form id="send_notification">
                    <input type="hidden" id="get_id_order" name="get_id_order" value="{{ $id }}">
                    <div id="collapseCustomer" class="panel-collapse in collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12" style="margin-bottom: 15px;">
                                    <label for="">Notification Category</label>
                                    <select name="notification_send" class="form-control" id="">
                                        <option value="">Choose...</option>
                                        @foreach($notif_whatsapp as $notif)
                                            <option value="{{ $notif->id }}">{{ $notif->category_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12">
                                    <button class="btn btn-success" id="btn_send_notif">Send Notification</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<script>
    
    var DATA_UNIQ = 1;
    var $http = new HttpService();
    var $form_send_notif = new FormService($('#send_notification'))

    $form_send_notif.onSubmit(function(data) {
        
        $http.post('/api/admin/reservasi/send-notification/' + $('#get_id_order').val(), data).then(function(resp) {
            console.log(resp.data.get_link)
            window.open(resp.data.full_link,'_blank');
            // HelperService.redirect('/admin/reservasi');
        })
    })
</script>
@endsection
