<!DOCTYPE html>
<html>
<head>
    <title>Ticket Reservations</title>
	<style type="text/css">
        @media print {
            #printPageButton {
                display: none;
            }
        }
    
        @page :header {
            display: none
        }

        body{
            background-color: white;
        }
		.container{
			width: 800px;
			margin: auto;
		}
		.pembungkus{
			position: relative;
		}
		img {
            border: 5px dotted ;
		}
        div.h2{
			position: absolute;
			left: 250px;
			top: 140px;
            font-weight: bold;
            font-size: 20px;
		}
		div.h4{
			position: absolute;
			left: 250px;
			top: 170px;
            font-weight: bold;
            font-size: 13px;
		}
		
        div.h2_nama{
			position: absolute;
			left: 250px;
			top: 190px;
            font-weight: bold;
            font-size: 20px;
		}
        div.h4_nama{
			position: absolute;
			left: 250px;
			top: 215px;
            font-weight: bold;
            font-size: 13px;
		}
        div.h2_date{
			position: absolute;
			left: 250px;
			top: 230px;
            font-weight: bold;
            font-size: 20px;
		}
        div.h4_date{
			position: absolute;
			left: 250px;
			top: 255px;
            font-weight: bold;
            font-size: 13px;
		}
        div.h2_pax{
			position: absolute;
			left: 350px;
			top: 230px;
            font-weight: bold;
            font-size: 20px;
		}
        div.h4_pax{
			position: absolute;
			left: 350px;
			top: 255px;
            font-weight: bold;
            font-size: 13px;
		}
        div.barcode{
			position: absolute;
			left: 509px;
			top: 160px;
            font-weight: bold;
            font-size: 20px;
		}
        div.h2_scan_here{
			position: absolute;
			left: 509px;
			top: 125px;
            font-weight: bold;
            font-size: 20px;
		}
	</style>
</head>
<body>
<div class="container">
	<div class="pembungkus">
		<img src="{{ url('storage/background-ticket/'.$get_background->value) }}">
		<div class="h2">Package</div>
        <div class="h4">{{ $data->package->nama_paket }}</div>
        <div class="h2_nama">Name</div>
        <div class="h4_nama">{{ $data->customer->nama }}</div>
        <div class="h2_date">Date</div>
        <div class="h4_date">{{ date('d-M-Y', strtotime($data->checkin_date)) }}</div>
        <div class="h2_pax">Pax</div>
        <div class="h4_pax">{{ $data->pax }} Orang</div>
        <div class="h2_scan_here">Scan Here</div>
        <div class="barcode">
            {!! $qrcode !!}
        </div>
	</div>
</div>
</body>
</html>
<button onclick="window.print();return false;" id="printPageButton" class="btn btn-primary" target="_blank">CETAK PDF</button>

<!-- <style>
    img.tengah {
        display: block;
        margin-left: auto;
        margin-right: auto;
    }
    .center{
        margin-left: 62.7%;
        margin-top: -9%;
    }
</style>
<img class="tengah" src="{{ asset('tiket.png') }}" alt="">

<div style="margin-left: 62.7%;margin-top: -9%;">
    {!! $qrcode !!}
</div>

<div style="margin-left: 46%;margin-top: -9%; ">
    <table>
        <tr>
            <td style="font-weight: bold;font-size:20px">Package</td>
        </tr>
        <tr>
            <td>Akomodasi</td>
        </tr>
        <tr>
            <td style="font-weight: bold;font-size:20px">Name</td>
        </tr>
        <tr>
            <td>{{ $data->name }}</td>
        </tr>
        <tr>
            <td style="font-weight: bold;font-size:20px">Duration</td>
        </tr>
        <tr>
            <td>{{ $data->berapa_malam }} Day</td>
        </tr>
        <tr>
            <td style="font-weight: bold;font-size:20px">Date</td>
        </tr>
        <tr>
            <td>{{ date('d-M-Y', strtotime($data->checkin_date)) }}</td>
        </tr>
    </table>
</div>

<button onclick="window.print();" class="btn btn-primary" target="_blank">CETAK PDF</button> -->