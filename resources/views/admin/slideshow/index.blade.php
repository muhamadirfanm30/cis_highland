@extends($master_template)
@section('content')
<div class="card">
    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div> 
    @endif
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <a href="{{ url('admin/slider/create') }}" class="btn btn-primary add-category"><i class="fa fa-plus"></i> ADD NEW SLIDER</a>
        <table id="table-slider"></table>
    </div>
</div>



<!-- <form id="category-form">
    <div id="dark-header-modal" class="modal fade" tabindex="-1" aria-labelledby="dark-header-modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="dark-header-modalLabel">Modal Heading
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id">
                    <div class="mb-3">
                        <label>Category Name</label>
                        <input type="text" name="name" class="form-control" value="" placeholder="Category Name" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</form> -->
@endsection

@section('js')
<script>
    $(function() {
        var $table = $('#table-slider');
        var $modal = $('#dark-header-modal');
        var $form = new FormService($('#category-form'));
        var $http = new HttpService({
            formService: $form,
            bootrapTable: $table,
            bootrapModal: $modal,
        });

        $table.bootstrapTable({
            sortName: 'created_at',
            sortOrder: 'ASC',
            url: HelperService.base_url + '/api/admin/slider',
            responseHandler: function(res) {
                return res.data;
            },
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'Header',
                    field: 'header',
                    sortable: true,
                },
                {
                    title: 'Description',
                    field: 'desc',
                    sortable: true,
                },
                {
                    title: 'Image',
                    field: 'images_slider',
                    sortable: true,
                },
                {
                    title: 'ACTION',
                    sortable: false,
                    formatter: function(value, row, index) {
                        // view_link = "<a class='btn btn-primary btn-xs' href='" + HelperService.url('/admin/slider/update/' + row.id) + "'>Edit</a>";
                        btn_delete_item = '<button type="button" class="btn btn-sm waves-effect btn-xs btn-danger delete">Delete</button>';
                        return btn_delete_item;
                    },
                    events: {
                        'click .delete': function(e, value, row) {
                            HelperService.confirm(function() {
                                $http.delete(`/api/admin/slider/${row.id}`, row)
                            })
                        }
                    }
                },
            ]
        });
    })
</script>
@endsection