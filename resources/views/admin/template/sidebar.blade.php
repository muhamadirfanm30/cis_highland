<div class="sidebar">
    <!-- User Info -->
    <div class="user-info"
        style="background: url('https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR0k_cQD4r1p_7z1lZd_kbO0g6kJj5n_bOcmQ&usqp=CAU') no-repeat no-repeat; background-size: cover;">
        <div class="image" style="margin-bottom: 20px;">
            <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRkiIFjCOZ-mMeqxd2ryrneiHedE8G9S0AboA&usqp=CAU" width="48" height="48"
                alt="User" style="border-radius: 50%;"/>
        </div>
        <div class="info-container">
            <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                ADMIN</div>
            <div class="email">{{ auth()->user()->email }}</div>
            <div class="btn-group user-helper-dropdown">
            </div>
        </div>
    </div>
    <!-- #User Info -->

    <!-- SidebarSearch Form -->
    <div class="form-inline" style="display: none;">
        <div class="input-group" data-widget="sidebar-search">
            <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
                <button class="btn btn-sidebar">
                    <i class="fas fa-search fa-fw"></i>
                </button>
            </div>
        </div>
    </div>
    <?php
        $permissions = auth()->user()->getAllPermissions();
    ?>
    <!-- Sidebar Menu -->
    <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
            <li class="nav-header">MAIN NAVIGATION</li>
            <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
            @foreach($sidebar_menu as $menu)
            @if($permissions->whereIn('name', $menu['permission_name'])->first() != null)
            <li class="nav-item {{ $menu['active_class'] }} {{ $menu['submenu_open_class'] }}">
                <a href="{{ $menu['url'] }}" class="nav-link">
                    <i class="nav-icon {{ $menu['icon'] }}"></i>
                    <p>{{ $menu['name'] }}  @if($menu['isSubMenu'])<i class="right fas fa-angle-left"></i>@endif</p>
                    @if(isset($menu['notif']))
                        <span class='badge badge-pill badge-warning' style='border-radius: 0px;padding: 5px;'>{{ $menu['notif'] }}</span>
                    @endif
                </a>
                @if($menu['isSubMenu'])
                <ul class="nav nav-treeview">
                    @foreach($menu['submenu'] as $submenu)
                    @if($permissions->whereIn('name', $submenu['permission_name'])->first() != null)
                    <li class="nav-item">
                        <a href="{{ $submenu['url'] }}" class="nav-link {{ $submenu['active_class'] }}">
                            <i class="far fa-circle nav-icon"></i>
                            <p>{{ $submenu['name'] }}</p>
                            @if(isset($submenu['notif']))
                                <span class='badge badge-pill badge-warning' style='border-radius: 0px;padding: 5px;'>{{ $submenu['notif'] }}</span>
                            @endif
                        </a>
                    </li>
                    @endif
                    @endforeach
                </ul>
                @endif
            </li>
            @endif
            @endforeach
            <li class="nav-item">
                <a href="#" class="nav-link" onclick="logout()">
                    <i class="nav-icon fas fa-signout"></i>
                    <p>Logout</p>
                </a>
            </li>
        </ul>
    </nav>
    <!-- /.sidebar-menu -->
</div>