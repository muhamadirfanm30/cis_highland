@extends($master_template)
@section('content')
<form id="form-post">
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">{{ $title }}</h3>
                </div>
            </div>
        </div>
        <div class="col-md-8">
            <div class="card">
                <div class="card-body">
                    <div class="mb-3">
                        <label>Ticket</label>
                        <select class="form-control ticket-select" name="ticket_id" required>
                            <option selected value="">Choose...</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label>DATE</label>
                        <input type="date" value="{{ $date }}" class="form-control" name="transaction_date" required>
                    </div>
                    <div class="mb-3">
                        <label>PAX</label>
                        <input type="number" class="form-control" name="transaction_pax" required>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-body">
                    @if($reservation == null)
                    <div class="mb-3">
                        <label for="">Nama Customer</label>
                        <input type="text" class="form-control nama" name="customer_name">
                    </div>
                    <div class="mb-3">
                        <label for="">Nomor Telepon</label>
                        <input type="number" class="form-control telepon" name="customer_phone">
                    </div>
                    @else
                    <input type="hidden" name="reservation_id" value="{{ $reservation->id }}">
                    <div class="widget-user-2">
                        <div class="widget-user-header">
                            <!-- /.widget-user-image -->
                            <h3 class="widget-user-username">{{ $reservation->customer_name }}</h3>
                            <h5 class="widget-user-desc">{{ $reservation->customer_phone }}</h5>
                        </div>
                        <div class="card-footer p-0">
                            <ul class="nav flex-column">
                            <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        Package <span class="float-right">{{ $reservation->package_name }}</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        Check in <span class="float-right">{{ human_date($reservation->checkin_date) }}</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        Check Out <span class="float-right">{{ human_date($reservation->checkout_date) }}</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="#" class="nav-link">
                                        Total Projects <span class="float-right">{{ thousand_separator($reservation->total_project) }}</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <button type="submit" class="btn btn-primary"> <i class="fa fa-plus"></i> Save</button>
                </div>
            </div>
        </div>
    </div>

</form>
@endsection

@section('js')
<script>
    $(function() {
        HelperService.loadTicketSelect2();

        var $form = new FormService($('#form-post'));
        var $http = new HttpService({
            formService: $form,
        });

        $form.onSubmit(function(data) {
            $http.post('/api/admin/tickets/transaction', data).then(function(resp) {
                HelperService.redirect('/admin/tickets/transaction');
            })
        })
    })
</script>
@endsection