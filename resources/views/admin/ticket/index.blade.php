@extends($master_template)
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{$title}}</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-primary btn-sm add-tickets"><i class="fa fa-plus"></i> ADD NEW TICKET</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <table id="table"></table>
            </div>
        </div>
    </div>
</div>

<form id="tickets-form">
    <div id="dark-header-modal" class="modal fade" tabindex="-1" aria-labelledby="dark-header-modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="dark-header-modalLabel">Modal Heading
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id">
                    <div class="mb-3">
                        <label for="">Vendor</label>
                        <select name="vendor_id" class="form-control vendor-select" style="width: 100%;">
                            <option value="">Choose Vendor</option>
                        </select>
                    </div>
                    <div class="mb-3">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control" value="" placeholder="enter Name" required>
                    </div>
                    <div class="mb-3">
                        <label>Max Pax</label>
                        <input type="text" name="max_pax" class="form-control" value="" placeholder="enter pax" required>
                    </div>
                    <div class="mb-3">
                        <label>Location</label>
                        <input type="text" name="location" class="form-control" value="" placeholder="enter location" required>
                    </div>
                    <div class="mb-3">
                        <label>Opening hours</label>
                        <input type="time" name="opening_hours" class="form-control" value="" placeholder="enter opening_hours" required>
                    </div>
                    <div class="mb-3">
                        <label>Closing hours</label>
                        <input type="time" name="closing_hours" class="form-control" value="" placeholder="enter closing_hours" required>
                    </div>
                    <div class="mb-3">
                        <label>Description</label>
                        <textarea name="description" class="form-control" id="" cols="30" rows="3"></textarea>
                    </div>
                    <div class="mb-3">
                        <label>Purchase price</label>
                        <input type="number" name="purchase_price" class="form-control" value="" placeholder="enter purchase_price" required>
                    </div>
                    <div class="mb-3">
                        <label>Selling price</label>
                        <input type="number" name="selling_price" class="form-control" value="" placeholder="enter selling_price" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('js')
<style>

</style>
<script>
    $(function() {
        HelperService.loadVendorSelect2();

        var $table = $('#table');
        var $modal = $('#dark-header-modal');
        var $form = new FormService($('#tickets-form'));
        var $http = new HttpService({
            formService: $form,
            bootrapTable: $table,
            bootrapModal: $modal,
        });

        $table.bootstrapTable({
            sortName: 'created_at',
            sortOrder: 'ASC',
            url: HelperService.base_url + '/api/admin/tickets',
            responseHandler: function(res) {
                return res.data;
            },
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'Name',
                    field: 'name',
                },
                {
                    title: 'Location',
                    field: 'location',
                },
                {
                    title: 'Opening hours',
                    field: 'opening_hours',
                },
                {
                    title: 'Closing hours',
                    field: 'closing_hours',
                },
                {
                    title: 'Selling price',
                    field: 'selling_price',
                },
                {
                    title: 'Purchase price',
                    field: 'purchase_price',
                },
                {
                    title: 'Action',
                    sortable: false,
                    formatter: function(value, row, index) {
                        var link = '';
                        link += '<button type="button" class="btn btn-sm waves-effect btn-xs btn-success edit">';
                        link += 'Edit';
                        link += '</button>';
                        link += '<button type="button" class="btn btn-sm waves-effect btn-xs btn-danger delete">';
                        link += 'Delete';
                        link += '</button>';
                        return link;
                    },
                    events: {
                        'click .edit': function(e, value, row) {
                            $form.emptyFormData();
                            $modal.modal('show')
                            $modal.find('.modal-title').text('UPDATE TICKET');
                            $form.setFormData(row);
                        },
                        'click .delete': function(e, value, row) {
                            HelperService.confirm(function() {
                                $http.delete(`/api/admin/tickets/${row.id}`, row)
                            })
                        }
                    }
                },
            ]
        });

        $('.add-tickets').click(function() {
            $form.emptyFormData();
            $modal.modal('show')
            $modal.find('.modal-title').text('ADD TICKET');
        });

        $form.onSubmit(function(data) {
            // insert
            if (data.id == '') {
                $http.post('/api/admin/tickets', data)
            }

            // update
            if (data.id != '') {
                $http.put(`/api/admin/tickets/${data.id}`, data)
            }
        })
    })
</script>
@endsection