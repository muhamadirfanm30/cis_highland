@extends($master_template)
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">{{$title}}</h3>
                <div class="card-tools">
                    <a href="{{ url('admin/tickets/create-stand-alone') }}" class="btn btn-primary text-right"><i class="fa fa-plus"></i> CREATE NEW STAND ALONE TICKET</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <table id="table"></table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
<style>

</style>
<script>
    $(function() {
        var $table = $('#table');
        var $http = new HttpService({
            bootrapTable: $table,
        });

        $table.bootstrapTable({
            sortName: 'created_at',
            sortOrder: 'ASC',
            url: HelperService.base_url + '/api/admin/tickets/transaction',
            responseHandler: function(res) {
                return res.data;
            },
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'TICKET',
                    field: 'ticket.name',
                },
                {
                    title: 'DATE',
                    field: 'transaction_date',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return moment(value).format("DD MMMM YYYY");
                    }
                },
                {
                    title: 'PAX',
                    field: 'transaction_pax',
                },
                {
                    title: 'PURCHASE PRICE',
                    field: 'transaction_purchase_price',
                    sortable: false,
                    formatter: function(value, row, index) {
                        return HelperService.thousandsSeparators(value);
                    }
                },
                {
                    title: 'SELLING PRICE',
                    field: 'transaction_selling_price',
                    sortable: false,
                    formatter: function(value, row, index) {
                        return HelperService.thousandsSeparators(value);
                    }
                },
                {
                    title: 'RESERVATION',
                    sortable: false,
                    formatter: function(value, row, index) {
                        var link = '';
                        if (row.reservation != null) {
                            view_link = "<a class='btn bg-navy btn-xs' href='" + HelperService.url('/admin/reservasi/edit/' + row.reservation.id) + "'>View</a>";
                            return view_link;
                        }                        
                        return link;
                    },
                },
                {
                    title: 'ACTION',
                    sortable: false,
                    formatter: function(value, row, index) {
                        var link = '';
                        link += '<button type="button" class="btn waves-effect btn-xs btn-danger delete">';
                        link += 'Delete';
                        link += '</button>';
                        return link;
                    },
                    events: {
                        'click .delete': function(e, value, row) {
                            HelperService.confirm(function() {
                                $http.delete(`/api/admin/tickets/transaction/${row.id}`, row)
                            })
                        }
                    }
                },
            ]
        });
    })
</script>
@endsection