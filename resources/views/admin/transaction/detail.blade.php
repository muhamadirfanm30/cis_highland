@extends($master_template)
@section('content')
<div class="card">
    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div> 
    @endif
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <div id="accordion">
            <div class="card card-primary">
                <div class="card-header">
                    <h4 class="card-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapsePersonalData">
                            #1 Persolan Data
                        </a>
                    </h4>
                </div>
                <div id="collapsePersonalData" class="panel-collapse in collapse show">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6" style="margin-bottom: 15px;">
                                <label for="">Name</label>
                                <input type="text" name="" value="{{ $data->first_name }}" class="form-control" readonly>
                            </div>
                            <div class="col-md-6" style="margin-bottom: 15px;">
                                <label for="">Phone</label>
                                <input type="text" name="" value="{{ $data->phone_number }}" class="form-control" readonly>
                            </div>
                            <div class="col-md-6" style="margin-bottom: 15px;">
                                <label for="">Checkin Data</label>
                                <input type="text" name="" value="{{ date('M-d-Y', strtotime($data->checkin_date)) }}" class="form-control" readonly>
                            </div>
                            <div class="col-md-6" style="margin-bottom: 15px;">
                                <label for="">Checkout Date</label>
                                <input type="text" name="" value="{{ date('M-d-Y', strtotime($data->checkout_date)) }}" class="form-control" readonly>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card card-primary">
                <div class="card-header">
                    <h4 class="card-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#collapseImages">
                            #2 Detail Vanue
                        </a>
                    </h4>
                </div>
                <div id="collapseImages" class="panel-collapse in collapse show">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6" style="margin-bottom: 15px;">
                                <label for="">Vanue Name</label>
                                <input type="text" name="" value="{{ !empty($data->data_vanue) ? $data->data_vanue->vanue_name : 'Deleted Data Vanue'  }}" class="form-control" readonly>
                            </div>
                            <div class="col-md-6" style="margin-bottom: 15px;">
                                <label for="">Price</label>
                                <input type="text" name="" value="Rp. {{ !empty($data->data_vanue) ? number_format($data->data_vanue->harga) : 'Deleted Data Vanue'  }}" class="form-control" readonly>
                            </div>
                            <div class="col-md-5" style="margin-bottom: 20px;"><hr></div>
                            <div class="col-md-2 text-center" style="margin-bottom: 20px;"><strong>Fasilitas Vanue</strong></div>
                            <div class="col-md-5" style="margin-bottom: 20px;"><hr></div>
                            @if(!empty($data->data_vanue))
                                @if(!empty($data->data_vanue->get_fasilitas))
                                    @foreach($data->data_vanue->get_fasilitas as $val)
                                        <div class="col-md-4"><i class="fa fa-check"></i>&nbsp; {{ $val->fasilitas_name }}</div>
                                    @endforeach
                                @else 
                                    <p>Fasilitas Has been Deleted</p>
                                @endif
                            @else 
                                <p>Deleted Data Vanue</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <a href="https://wa.me/{{ $data->phone_number }}" target="_blank" class="btn btn-success btn-block">Chat Via Whatsapp</a>
            </div>
            <div class="col-md-6">
                <a href="http://" class="btn btn-info btn-block">Back</a>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')

@endsection