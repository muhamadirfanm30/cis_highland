@extends($master_template)
@section('content')
<div class="card">
    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div> 
    @endif
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <!-- <a href="{{ url('admin/list-transaction/create') }}" class="btn btn-primary add-category"><i class="fa fa-plus"></i> ADD NEW SLIDER</a> -->
        <table id="table-transaction"></table>
    </div>
</div>



<!-- <form id="transaction-form">
    <div id="dark-header-modal" class="modal fade" tabindex="-1" aria-labelledby="dark-header-modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="dark-header-modalLabel">Modal Heading
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id">
                    <div class="mb-3">
                        <label>Category Name</label>
                        <input type="text" name="name" class="form-control" value="" placeholder="Category Name" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</form> -->
@endsection

@section('js')
<script>
    $(function() {
        var $table = $('#table-transaction');
        var $modal = $('#dark-header-modal');
        var $form = new FormService($('#transaction-form'));
        var $http = new HttpService({
            formService: $form,
            bootrapTable: $table,
            bootrapModal: $modal,
        });

        $table.bootstrapTable({
            sortName: 'created_at',
            sortOrder: 'ASC',
            url: HelperService.base_url + '/api/admin/list-transaction',
            responseHandler: function(res) {
                return res.data;
            },
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'Name',
                    sortable: true,
                    formatter: function(value, row, index) {
                        if(row.read_at !== null){
                            return row.first_name;
                        }else{
                            return row.first_name + ' ' + '<span class="badge badge-success badge-phil">New</span>';
                        }
                    }
                },
                {
                    title: 'Phone Number',
                    field: 'phone_number',
                    sortable: true,
                },
                {
                    title: 'Status Order',
                    formatter: function(value, row, index) {
                        if (row.status == 'Reservasi') {
                            return '<span class="badge badge-success badge-pill">Reservasi</span>';
                        } else if (row.status == 2) {
                            return '<span class="badge badge-success badge-pill">Lunas</span>';
                        }
                    }
                },
                {
                    title: 'Payment Status',
                    formatter: function(value, row, index) {
                        if (row.status_payment == 'Belum Lunas') {
                            return '<span class="badge badge-danger badge-pill">Belum Lunas</span>';
                        } else if (row.status_payment == 2) {
                            return '<span class="badge badge-success badge-pill">Lunas</span>';
                        }
                    }
                },
                {
                    title: 'ACTION',
                    sortable: false,
                    formatter: function(value, row, index) {
                        // btn_whatsapp = "<a class='btn btn-success btn-xs' href='" + HelperService.url('/admin/list-transaction/update/' + row.id) + "'><i class='fab fa-whatsapp' aria-hidden='true'></i></a>";
                        btn_detail = "<a class='btn btn-primary btn-sm' href='" + HelperService.url('/admin/list-transaction/detail/' + row.id) + "'><i class='fa fa-eye'></i></a>";
                        return btn_detail;
                    },
                },
            ]
        });
    })
</script>
@endsection