@extends($master_template)

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{ $title }}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <button type="button" class="btn btn-primary add-user-btn"><i class="fa fa-plus"></i> ADD NEW USER</button>
        <table id="user-table"></table>
    </div>
</div>


<form id="user-create-form">
    <div id="user-create-modal" class="modal fade" tabindex="-1" aria-labelledby="user-create-modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="user-create-modalLabel">ADD NEW USER
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="mb-3">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control" value="" placeholder="enter name">
                    </div>
                    <div class="mb-3">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control" value="" placeholder="enter email">
                    </div>
                    <div class="mb-3">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control" value="" placeholder="enter password">
                    </div>
                    <div class="mb-3">
                        <label>Role</label>
                        <select class="form-control col-12" name="role_id" required>
                            <option selected value="">Choose...</option>
                            @foreach($roles as $role)
                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label>Bussiness Access</label>
                        <br>
                        @foreach($bussinesses as $bussines)
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="bussiness_id" value="{{ $bussines->id }}" id="bussines-id2-{{ $bussines->id }}">
                            <label class="form-check-label" for="bussines-id-{{ $bussines->id }}">{{ $bussines->nama_bisnis }}</label>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</form>

<form id="user-edit-form">
    <div id="user-edit-modal" class="modal fade" tabindex="-1" aria-labelledby="user-edit-modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="user-edit-modalLabel">EDIT USER
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" />
                    <div class="mb-3">
                        <label>Name</label>
                        <input type="text" name="name" class="form-control" value="" placeholder="enter name" required>
                    </div>
                    <div class="mb-3">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control" value="" placeholder="enter email" required>
                    </div>
                    <div class="mb-3">
                        <label>Role</label>
                        <select class="form-control col-12" name="role_id" required>
                            <option selected value="">Choose...</option>
                            @foreach($roles as $role)
                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label>Bussiness Access</label>
                        <br>
                        @foreach($bussinesses as $bussines)
                        <div class="form-check">
                            <input type="checkbox" class="form-check-input" name="bussiness_id" value="{{ $bussines->id }}" id="bussines-id1-{{ $bussines->id }}">
                            <label class="form-check-label" for="bussines-id-{{ $bussines->id }}">{{ $bussines->nama_bisnis }}</label>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('js')
<script>
    $(function() {
        var $table = $('#user-table');
        var $modalCreate = $('#user-create-modal');
        var $modalEdit = $('#user-edit-modal');

        var $formCreate = new FormService($('#user-create-form')).withArrayField(['bussiness_id']);
        var $formEdit = new FormService($('#user-edit-form')).withArrayField(['bussiness_id']);

        var $http = new HttpService();

        var $httpCreate = new HttpService({
            formService: $formCreate,
            bootrapTable: $table,
            bootrapModal: $modalCreate,
        });

        var $httpEdit = new HttpService({
            formService: $formEdit,
            bootrapTable: $table,
            bootrapModal: $modalEdit,
        });

        $table.bootstrapTable({
            sortName: 'name',
            sortOrder: 'ASC',
            url: HelperService.base_url + '/api/admin/users',
            responseHandler: function(res) {
                return res.data;
            },
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'NAME',
                    field: 'name',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return value;
                    }
                },
                {
                    title: 'EMAIL',
                    field: 'email',
                    sortable: true,
                },
                {
                    title: 'ACTION',
                    formatter: function(value, row, index) {
                        var link = '';
                        link += '<button type="button" class="btn btn-sm btn-xs btn-success edit">';
                        link += 'Edit';
                        link += '</button>';
                        link += '<button type="button" class="btn btn-sm btn-xs btn-danger delete">';
                        link += 'Delete';
                        link += '</button>';
                        return link;
                    },
                    events: {
                        'click .edit': function(e, value, row) {
                            $formEdit.emptyFormData();
                            $modalEdit.modal('show')
                            $formEdit.setFormDataManual({
                                id: row.id,
                                name: row.name,
                                email: row.email,
                                role_id: row.roles[0].id,
                                bussiness_id: row.bussiness_users.map(function(row){
                                    return row.bussiness_id;
                                }),
                            });
                        },
                        'click .delete': function(e, value, row) {
                            $http.delete(`/api/admin/users/${row.id}`)
                                .then(function(resp) {
                                    $table.bootstrapTable('refresh');
                                })
                        }
                    }
                },
            ]
        });

        $('.add-user-btn').click(function() {
            $formCreate.emptyFormData();
            $modalCreate.modal('show')
        });

        $formCreate.onSubmit(function(data) {
            $httpCreate.post('/api/admin/users', data);
        });

        $formEdit.onSubmit(function(data) {
            $httpEdit.put(`/api/admin/users/${data.id}`, data)
        });
    })
</script>
@endsection