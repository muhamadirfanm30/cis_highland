@extends($master_template)
@section('content')
<!-- Default box -->
<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-info">
                        <div class="inner">
                            <h3>
                               {{ $hitungTotalVendor > 0 ? $hitungTotalVendor : 0 }}
                            </h3>
                            <p>Total Vendor</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-success">
                        <div class="inner">
                            <h3>
                            {{ $hitungTotalProdukHampirHabis > 0 ? $hitungTotalProdukHampirHabis : 0 }}
                            </h3>

                            <p>Produk Hampir Habis</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-danger">
                        <div class="inner">
                            <h3>
                            {{ $hitungTotalProdukHabis > 0 ? $hitungTotalProdukHabis : 0 }}
                            </h3>

                            <p>Produk Habis</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                        </div>
                    </div>
                    <div class="col-lg-3 col-6">
                        <!-- small box -->
                        <div class="small-box bg-warning">
                        <div class="inner" style="color: white;">
                            <h3>
                            {{ $hitungTotalProduk > 0 ? $hitungTotalProduk : 0 }}
                            </h3>

                            <p>Semua Produk</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                            <a href="#" class="small-box-footer" style="color: white;"> <i class="fas fa-arrow-circle-right" style="color: white;"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="card-body">
        <button type="button" class="btn btn-primary add-vendor-items"><i class="fa fa-plus"></i> ADD NEW VENDOR ITEM</button>
        <table id="table"></table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        Footer
    </div>
    <!-- /.card-footer-->
</div>
<!-- /.card -->


<form id="vendor-items-form">
    <div id="dark-header-modal" class="modal fade" tabindex="-1" aria-labelledby="dark-header-modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="dark-header-modalLabel">Modal Heading
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id">
                    <div class="mb-3">
                        <label>Vendor Name</label>
                        <select class="form-control col-12" name="vendor_id" required>
                            <option selected value="">Choose...</option>
                            @foreach($vendor as $item)
                            <option value="{{ $item->id }}">{{ $item->vendor_name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label>Produk Name</label>
                        <input type="text" name="nama_barang" class="form-control" value="" placeholder="Product Name">
                    </div>
                    <div class="mb-3">
                        <label>Stock</label>
                        <input type="number" name="stok" class="form-control" value="" placeholder="enter Stok">
                    </div>
                    <div class="mb-3">
                        <label>Selling Price</label>
                        <input type="number" name="harga_jual" class="form-control" value="" placeholder="enter Selling Price">
                    </div>
                    <div class="mb-3">
                        <label>Finance Price</label>
                        <input type="number" name="pembiayaan" class="form-control" value="" placeholder="enter Finance Price">
                    </div>
                    <div class="mb-3">
                        <label>Item Type</label>
                        <select name="item_type" class="form-control">
                            <option value="">Choose...</option>
                            <option value="1">Habis</option>
                            <option value="2">Sewa</option>
                            <!-- <option value="3">Pack</option> -->
                        </select>
                    </div>
                    <!-- <div class="mb-3">
                        <label>Category Item</label>
                        <select name="category_item" class="form-control">
                            <option value="">Choose...</option>
                            <option value="1">Habis</option>
                            <option value="2">Kembali</option>
                        </select>
                    </div> -->
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('js')
<style>

</style>
<script>
    $(function() {
        var $table = $('#table');
        var $modal = $('#dark-header-modal');
        var $form = new FormService($('#vendor-items-form'));
        var $http = new HttpService({
            formService: $form,
            bootrapTable: $table,
            bootrapModal: $modal,
        });

        HelperService.loadBussinesSelect2();

        $table.bootstrapTable({
            sortName: 'nama_barang',
            sortOrder: 'ASC',
            theadClasses: 'table-dark',
            url: HelperService.base_url + '/api/admin/vendor-items',
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'NAME',
                    field: 'nama_barang',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return row.nama_barang;
                    }
                },
                {
                    title: 'STOCK',
                    field: 'stok',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return row.stok;
                    }
                },
                {
                    title: 'SELLING PRICE',
                    field: 'harga_jual',
                    sortable: true,
                },
                {
                    title: 'FINANCE PRICE',
                    field: 'pembiayaan',
                    sortable: true,
                },
                {
                    title: 'ACTION',
                    sortable: false,
                    formatter: function(value, row, index) {
                        var link = '';
                        link += '<button type="button" class="btn btn-sm waves-effect btn-xs btn-success edit">';
                        link += 'Edit';
                        link += '</button>';
                        link += '<button type="button" class="btn btn-sm waves-effect btn-xs btn-danger delete">';
                        link += 'Delete';
                        link += '</button>';
                        return link;
                    },
                    events: {
                        'click .edit': function(e, value, row) {
                            $form.emptyFormData();
                            $modal.modal('show')
                            $modal.find('.modal-title').text('UPDATE VENDOR ITEM');
                            $form.setFormData(row);
                        },
                        'click .delete': function(e, value, row) {
                            HelperService.confirm(function() {
                                $http.delete(`/api/admin/vendor-items/${row.id}`, row)
                            })
                        }
                    }
                },
            ]
        });

        $('.add-vendor-items').click(function() {
            $form.emptyFormData();
            $modal.modal('show')
            $modal.find('.modal-title').text('ADD VENDOR ITEM');
        });

        $form.onSubmit(function(data) {
            // insert
            if (data.id == '') {
                $http.post('/api/admin/vendor-items', data)
            }

            // update
            if (data.id != '') {
                $http.put(`/api/admin/vendor-items/${data.id}`, data)
            }
        })
    })
</script>
@endsection