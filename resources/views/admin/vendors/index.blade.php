@extends($master_template)
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <button type="button" class="btn btn-primary add-vendors"><i class="fa fa-plus"></i> ADD NEW VENDOR</button>
        <table id="table"></table>
    </div>
</div>


<form id="vendors-form">
    <div id="dark-header-modal" class="modal fade" tabindex="-1" aria-labelledby="dark-header-modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="dark-header-modalLabel">Modal Heading
                    </h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="id" />
                    @include('admin.template._bussines_select')
                    <div class="mb-3">
                        <label>Users</label>
                        <select class="form-control col-12" name="user_id" required>
                            <option selected value="">Choose...</option>
                            @foreach($user as $usr)
                            <option value="{{ $usr->id }}">{{ $usr->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="mb-3">
                        <label>Vendor Name</label>
                        <input type="text" name="vendor_name" class="form-control" value="" placeholder="enter name">
                    </div>
                    <div class="mb-3">
                        <label>Note</label>
                        <textarea name="note" class="form-control" id="" cols="30" rows="3"></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-dark">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</form>
@endsection

@section('js')
<style>

</style>
<script>
    $(function() {
        var $table = $('#table');
        var $modal = $('#dark-header-modal');
        var $form = new FormService($('#vendors-form'));
        var $http = new HttpService({
            formService: $form,
            bootrapTable: $table,
            bootrapModal: $modal,
        });

        HelperService.loadBussinesSelect2();

        $table.bootstrapTable({
            sortName: 'created_at',
            sortOrder: 'ASC',
            url: HelperService.base_url + '/api/admin/vendors',
            responseHandler: function(res) {
                return res.data;
            },
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'BUSSINES',
                    field: 'bussiness_id',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return row.get_bisnis.nama_bisnis;
                    }
                },
                {
                    title: 'USER NAME',
                    field: 'user_id',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return row.get_user.name;
                    }
                },
                {
                    title: 'VENDOR NAME',
                    field: 'vendor_name',
                    sortable: true,
                },
                {
                    title: 'ACTION',
                    sortable: false,
                    formatter: function(value, row, index) {
                        var link = '';
                        link += '<button type="button" class="btn btn-sm waves-effect btn-xs btn-success edit">';
                        link += 'Edit';
                        link += '</button>';
                        link += '<button type="button" class="btn btn-sm waves-effect btn-xs btn-danger delete">';
                        link += 'Delete';
                        link += '</button>';
                        return link;
                    },
                    events: {
                        'click .edit': function(e, value, row) {
                            $form.emptyFormData();
                            $modal.modal('show')
                            $modal.find('.modal-title').text('UPDATE VENDOR');
                            $form.setFormData(row);
                        },
                        'click .delete': function(e, value, row) {
                            HelperService.confirm(function() {
                                $http.delete(`/api/admin/vendors/${row.id}`, row)
                            })
                        }
                    }
                },
            ]
        });

        $('.add-vendors').click(function() {
            $form.emptyFormData();
            $modal.modal('show')
            $modal.find('.modal-title').text('ADD VENDOR');
        });

        $form.onSubmit(function(data) {
            // insert
            if (data.id == '') {
                $http.post('/api/admin/vendors', data)
            }

            // update
            if (data.id != '') {
                $http.put(`/api/admin/vendors/${data.id}`, data)
            }
        })
    })
</script>
@endsection