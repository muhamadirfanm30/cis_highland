@extends($master_template)
@section('content')
<div class="card">
    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div> 
    @endif
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <!-- <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button> -->
            <a href="{{ url('admin/villa-or-hotel') }}" class="btn btn-primary add-category">Back</a>
        </div>
    </div>
    <form action="{{ url('admin/vanue/store') }}" method="post" enctype="multipart/form-data">
        @csrf 
        <div class="card-body">
            <div id="accordion">
                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="card-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseCustomer">
                                #1 DATA VANUE
                            </a>
                        </h4>
                    </div>
                    <div id="collapseCustomer" class="panel-collapse in collapse show">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4" style="margin-bottom: 15px;">
                                    <label for="">Villa Name</label>
                                    <input type="text" class="form-control vanue_name" id="vanue_name" name="vanue_name" >
                                </div>
                                <div class="col-md-4" style="margin-bottom: 15px;">
                                    <label for="">Price</label>
                                    <input type="number" class="form-control harga" id="harga" name="harga" >
                                </div>
                                <div class="col-md-4" style="margin-bottom: 15px;">
                                    <label for="">Category</label>
                                    <select name="category" class="form-control" id="category">
                                        <option value="">Choose...</option>
                                        @foreach($getCategory as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-12" style="margin-bottom: 15px;">
                                    <label for="">Location</label>
                                    <textarea name="location" class="form-control" id="location" cols="30" rows="3"></textarea>
                                </div>
                                <div class="col-md-12" style="margin-bottom: 15px;">
                                    <label for="">Description</label>
                                    <textarea id="summernote1" name="desc"></textarea>
                                </div>
                                <div class="col-md-12" style="margin-bottom: 15px;">
                                    <label for="">Information</label>
                                    <textarea id="summernote2" name="information"></textarea>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="card-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseImages">
                                #2 DATA IMAGE VANUE
                            </a>
                        </h4>
                    </div>
                    <div id="collapseImages" class="panel-collapse in collapse show">
                        <div class="card-body">
                            <div class="form-group multiple-form-group" data-max=10>
                                <label>Upload Image</label>
                                <div class="form-group input-group">
                                    <input type="file" name="image[]" class="form-control" required>
                                    <span class="input-group-btn"><button type="button" class="btn btn-default btn-add">+
                                    </button></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card card-primary">
                    <div class="card-header">
                        <h4 class="card-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseFasilitas">
                                #2 DATA FASILITAS VANUE
                            </a>
                        </h4>
                    </div>
                    <div id="collapseFasilitas" class="panel-collapse in collapse show">
                        <div class="card-body">
                            <div class="form-group">
                                <label for="">Fasilitas</label><br>
                                <div class="col-md-12">
                                    <div class="row">
                                        @foreach($getFasilitas as $fasilitas)
                                            <div class="col-md-3">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" name="fasilitas_name[]" id="fasilitas_name" value="{{ $fasilitas->name }}">
                                                    <label class="form-check-label" for="inlineCheckbox{{ $fasilitas->id }}">{{ $fasilitas->name }}</label>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-success btn-block">Save</button>
        </div>
    </form>
</div>
@endsection

@section('js')
<!-- include summernote css/js -->
<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.css" rel="stylesheet">
<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>
<script>
    $(document).ready(function() {
        $('#summernote1').summernote();
        $('#summernote2').summernote();
    });
</script>

<!-- add image  -->
<script>
    (function ($) {
        $(function () {

            var addFormGroup = function (event) {
                event.preventDefault();

                var $formGroup = $(this).closest('.form-group');
                var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
                var $formGroupClone = $formGroup.clone();

                $(this)
                    .toggleClass('btn-default btn-add btn-danger btn-remove')
                    .html('–');

                $formGroupClone.find('input').val('');
                $formGroupClone.insertAfter($formGroup);

                var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
                if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
                    $lastFormGroupLast.find('.btn-add').attr('disabled', true);
                }
            };

            var removeFormGroup = function (event) {
                event.preventDefault();

                var $formGroup = $(this).closest('.form-group');
                var $multipleFormGroup = $formGroup.closest('.multiple-form-group');

                var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
                if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
                    $lastFormGroupLast.find('.btn-add').attr('disabled', false);
                }

                $formGroup.remove();
            };

            var countFormGroup = function ($form) {
                return $form.find('.form-group').length;
            };

            $(document).on('click', '.btn-add', addFormGroup);
            $(document).on('click', '.btn-remove', removeFormGroup);

        });
    })(jQuery);
</script>
@endsection