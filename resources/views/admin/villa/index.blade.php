@extends($master_template)
@section('content')
<div class="card">
    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div> 
    @endif
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <a href="{{ url('admin/vanue/create') }}" class="btn btn-primary add-category"><i class="fa fa-plus"></i> ADD NEW VANUE</a>
        <table id="table-vanue"></table>
    </div>
</div>
@endsection

@section('js')
<script>
    $(function() {
        var $table = $('#table-vanue');
        var $modal = $('#dark-header-modal');
        var $form = new FormService($('#category-form'));
        var $http = new HttpService({
            formService: $form,
            bootrapTable: $table,
            bootrapModal: $modal,
        });

        $table.bootstrapTable({
            sortName: 'created_at',
            sortOrder: 'ASC',
            url: HelperService.base_url + '/api/admin/vanue',
            responseHandler: function(res) {
                return res.data;
            },
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'Name',
                    field: 'vanue_name',
                    sortable: true,
                },
                {
                    title: 'Price',
                    field: 'harga',
                    sortable: true,
                },
                {
                    title: 'Category',
                    field: 'category',
                    sortable: true,
                },
                {
                    title: 'ACTION',
                    sortable: false,
                    formatter: function(value, row, index) {
                        view_link = "<a class='btn btn-primary btn-xs' href='" + HelperService.url('/admin/vanue/update/' + row.id) + "'>Edit</a>";
                        btn_delete_item = '<button type="button" class="btn btn-sm waves-effect btn-xs btn-danger delete">Delete</button>';
                        return view_link + ' ' + btn_delete_item;
                    },
                    events: {
                        'click .delete': function(e, value, row) {
                            HelperService.confirm(function() {
                                $http.delete(`/api/admin/vanue/delete/${row.id}`, row)
                            })
                        }
                    }
                },
            ]
        });
    })
</script>
@endsection