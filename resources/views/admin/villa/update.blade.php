@extends($master_template)
@section('content')
<div class="card">
    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div> 
    @endif
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <!-- <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button> -->
            <a href="{{ url('admin/villa-or-hotel') }}" class="btn btn-primary add-category">Back</a>
        </div>
    </div>
    
        <div class="card-body">
            <div id="accordion">
                <div class="card card-primary">
                    <form action="{{ url('admin/vanue/image-store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card-header">
                            <h4 class="card-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseImages">
                                    #1 UPDATE DATA IMAGE VANUE
                                </a>
                            </h4>
                        </div>
                        <div id="collapseImages" class="panel-collapse in collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-7">
                                        <table class="table table-primary" id="table_image">
                                            <thead>
                                                <th>No</th>
                                                <th>Image</th>
                                                <th>Action</th>
                                            </thead>
                                            <tbody>
                                                @foreach($get_image as $img)
                                                    <tr>
                                                        <td>{{ $no ++ }}</td>
                                                        <td><img src="{{ url('storage/img-vanue/'.$img->image) }}" alt="image_vanue" width="100px"></td>
                                                        <td><button type="button" class="btn btn-danger btn-sm id_btn_delete_image" data-id="{{ $img->id }}"><i class="fa fa-trash"></i></button></td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group multiple-form-group" data-max=10>
                                            <label>Upload Image</label>
                                            <div class="form-group input-group">
                                                <input type="file" name="image[]" class="form-control images_vanue">
                                                <span class="input-group-btn"><button type="button" class="btn btn-default btn-add">+
                                                </button></span>
                                            </div>
                                        </div>
                                        <input type="hidden" name="id" value="{{ $vanue->id }}">
                                        <button type="submit" class="btn btn-info btn-success btn-sm btn-block">Save Change Image</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <form id="get_data_fasilitas">
                    <div class="card card-primary">
                        <div class="card-header">
                            <h4 class="card-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseCustomer">
                                    #2 UPDATE DATA VANUE
                                </a>
                            </h4>
                        </div>
                        <div id="collapseCustomer" class="panel-collapse in collapse show">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4" style="margin-bottom: 15px;">
                                        <label for="">Villa Name</label>
                                        <input type="text" class="form-control vanue_name" id="vanue_name" name="vanue_name" value="{{ $vanue->vanue_name }}">
                                    </div>
                                    <div class="col-md-4" style="margin-bottom: 15px;">
                                        <label for="">Price</label>
                                        <input type="number" class="form-control harga" id="harga" name="harga" value="{{ $vanue->harga }}">>
                                    </div>
                                    <div class="col-md-4" style="margin-bottom: 15px;">
                                        <label for="">Category</label>
                                        <select name="category" class="form-control" id="category">
                                            <option value="">Choose...</option>
                                            @foreach($getCategory as $category)
                                                <option value="{{ $category->id }}" {{ $vanue->category == $category->id ? 'selected' : '' }}>{{ $category->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-12" style="margin-bottom: 15px;">
                                        <label for="">Location</label>
                                        <textarea name="location" class="form-control" id="location" cols="30" rows="3">{{ $vanue->location }}</textarea>
                                    </div>
                                    <div class="col-md-12" style="margin-bottom: 15px;">
                                        <label for="">Description</label>
                                        <textarea id="konten_2" class="form-control" name="desc" rows="10" cols="5">{{ $vanue->desc }}</textarea>
                                    </div>
                                    <div class="col-md-12" style="margin-bottom: 15px;">
                                        <label for="">Information</label>
                                        <textarea id="konten_1" class="form-control" name="information" rows="10" cols="5">{{ $vanue->information }}</textarea>
                                        <!-- <textarea id="summernote2" name="information"></textarea> -->
                                    </div>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card-primary">
                        <div class="card-header">
                            <h4 class="card-title">
                                <a data-toggle="collapse" data-parent="#accordion" href="#collapseFasilitas">
                                    #3 UPDATE DATA FASILITAS VANUE
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFasilitas" class="panel-collapse in collapse show">
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="">Fasilitas</label><br>
                                    <div class="col-md-12">
                                        <div class="row">
                                            @foreach($getFasilitas as $fasilitas)
                                                <div class="col-md-3">
                                                    <div class="custom-control custom-checkbox">
                                                        <input class="custom-control-input" name="fasilitas_name" type="checkbox" id="customCheckbox{{ $fasilitas->id }}" value="{{ $fasilitas->name }}">
                                                        <label for="customCheckbox{{ $fasilitas->id }}" class="custom-control-label">{{ $fasilitas->name }}</label>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div><br>
                    <button type="submit" class="btn btn-success btn-block">Save</button>
                    <input type="hidden" name="ids" class="ids" value="{{ $vanue->id }}">
                    <input type="hidden" name="id" value="{{ $vanue->id }}">
                </form>
            </div>
        </div>
        <!-- <div class="card-footer">
            
        </div> -->
        
</div>
@endsection

@section('js')
<link rel="stylesheet" href="https://cdn.datatables.net/1.11.3/css/jquery.dataTables.min.css">
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.11.3/js/jquery.dataTables.min.js"></script>

<script>
    $(function() {
        
        var $http = new HttpService({
            formService: $form,
        });

        var $form = new FormService($('#get_data_fasilitas')).withArrayField(['fasilitas_name']);

        $http.get('/api/admin/list-transaction/get-data-fasilitas/' + $('.ids').val())
            .then(function(resp) {
                var package = resp.data;
                $form.setFormDataManual({
                    fasilitas_name: package.get_fasilitas.map(function(row) {
                        return row.fasilitas_name;
                    }),
                });
            })

        $form.onSubmit(function(data) {
            $http.post(`/api/admin/vanue/${data.id}`, data).then(function(resp) {
                // HelperService.redirect('/admin/vanue');
            })
        })
    })

    $(".id_btn_delete_image").click(function() {
        var id = $(this).attr('data-id');
        $.ajax({
            url: HelperService.apiUrl('/admin/vanue/' + id),
            type: 'DELETE',
            success: function(result) {
                // Do something with the result
                // HelperService.onSuccess(result.data.msg);
                alert('data berhasil dihapus');
                location.reload();
                // HelperService.onSuccess(result.msg);
            }
        });
    })
</script>

<!-- ckeditor dan datatables -->
<script>
    var konten = document.getElementById("konten_1");
        CKEDITOR.replace(konten,{
            language:'en-gb'
        });
    CKEDITOR.config.allowedContent = true;

    var konten = document.getElementById("konten_2");
        CKEDITOR.replace(konten,{
            language:'en-gb'
        });
    CKEDITOR.config.allowedContent = true;

    $(document).ready(function() {
        $('#table_image').DataTable();
    } );
</script>

<!-- add image  -->
<script>
    (function ($) {
        $(function () {

            var addFormGroup = function (event) {
                event.preventDefault();

                var $formGroup = $(this).closest('.form-group');
                var $multipleFormGroup = $formGroup.closest('.multiple-form-group');
                var $formGroupClone = $formGroup.clone();

                $(this)
                    .toggleClass('btn-default btn-add btn-danger btn-remove')
                    .html('–');

                $formGroupClone.find('input').val('');
                $formGroupClone.insertAfter($formGroup);

                var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
                if ($multipleFormGroup.data('max') <= countFormGroup($multipleFormGroup)) {
                    $lastFormGroupLast.find('.btn-add').attr('disabled', true);
                }
            };

            var removeFormGroup = function (event) {
                event.preventDefault();

                var $formGroup = $(this).closest('.form-group');
                var $multipleFormGroup = $formGroup.closest('.multiple-form-group');

                var $lastFormGroupLast = $multipleFormGroup.find('.form-group:last');
                if ($multipleFormGroup.data('max') >= countFormGroup($multipleFormGroup)) {
                    $lastFormGroupLast.find('.btn-add').attr('disabled', false);
                }

                $formGroup.remove();
            };

            var countFormGroup = function ($form) {
                return $form.find('.form-group').length;
            };

            $(document).on('click', '.btn-add', addFormGroup);
            $(document).on('click', '.btn-remove', removeFormGroup);

        });
    })(jQuery);
</script>
@endsection