@extends($master_template)
@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <form action="{{ url('admin/web-setting/store') }}" method="post">
        @csrf
        <div class="card-body">
            <div class="row">
                @foreach($get_data_setting as $setting)
                    <div class="col-md-12" style="margin-bottom: 20px;">
                        <label for="">{{ $setting->desc }}</label>
                        <input type="hidden" name="id[]" value="{{ $setting->id }}">
                        <input type="text" name="value[]" class="form-control" id="" value="{{ $setting->value }}" placeholder="{{ $setting->desc }}">
                    </div>
                @endforeach
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-success">Save</button>
        </div>
    </form>
</div>
@endsection

@section('js')

@endsection