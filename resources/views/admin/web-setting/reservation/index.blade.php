@extends($master_template)

@section('content')
<!-- Default box -->
<div class="card">
    <div class="card-header">
        <h3 class="card-title">{{ $title }}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <a href="url()" class="btn btn-primary add-item-vendor-btn">Add Nwe Reservation</a>
        <button type="button" class="btn btn-primary add-item-vendor-btn"><i class="fa fa-plus"></i> ADD NEW VENDORS ITEM</button>
        <table id="vendor-table"></table>
    </div>
    <!-- /.card-body -->
    <div class="card-footer">
        Footer
    </div>
    <!-- /.card-footer-->
</div>
<!-- /.card -->
@endsection

@section('js')
<script>
    $(function() {
        var $table = $('#vendor-table');
        var $modalCreate = $('#item-vendor-create-modal');
        var $modalEdit = $('#item-vendor-edit-modal');

        var $formCreate = new FormService($('#item-vendor-create-form'));
        var $formEdit = new FormService($('#item-vendor-edit-form'));

        var $http = new HttpService();

        var $httpCreate = new HttpService({
            formService: $formCreate,
            bootrapTable: $table,
            bootrapModal: $modalCreate,
        });

        var $httpEdit = new HttpService({
            formService: $formEdit,
            bootrapTable: $table,
            bootrapModal: $modalEdit,
        });

        $table.bootstrapTable({
            sortName: 'nama_barang',
            sortOrder: 'ASC',
            theadClasses: 'table-dark',
            url: HelperService.base_url + '/api/admin/vendor-items',
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'Product Name',
                    field: 'id',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return row.nama_barang;
                    }
                },
                {
                    title: 'Stock',
                    field: 'id',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return row.stok;
                    }
                },
                {
                    title: 'Selling Price',
                    field: 'harga_jual',
                    sortable: true,
                },
                {
                    title: 'Finance Price',
                    field: 'pembiayaan',
                    sortable: true,
                },
                {
                    title: 'ACTION',
                    formatter: function(value, row, index) {
                        var link = '';
                        link += '<button type="button" class="btn btn-sm btn-xs btn-success edit">';
                        link += 'Edit';
                        link += '</button>';
                        link += '<button type="button" class="btn btn-sm btn-xs btn-danger delete">';
                        link += 'Delete';
                        link += '</button>';
                        return link;
                    },
                    events: {
                        'click .edit': function(e, value, row) {
                            $formEdit.emptyFormData();
                            $modalEdit.modal('show')
                            $formEdit.setFormData({
                                id: row.id,
                                nama_barang: row.nama_barang,
                                stok: row.stok,
                                harga_jual: row.harga_jual,
                                pembiayaan: row.pembiayaan,
                                vendor_id: row.vendor_id,
                                // vendor_id: row.vendor_id,
                            });
                        },
                        'click .delete': function(e, value, row) {
                            $http.delete(`/api/admin/vendor-items/${row.id}/delete`)
                                .then(function(resp) {
                                    $table.bootstrapTable('refresh');
                                })
                        }
                    }
                },
            ]
        });

        $('.add-item-vendor-btn').click(function() {
            $formCreate.emptyFormData();
            $modalCreate.modal('show')
        });

        $formCreate.onSubmit(function(data) {
            $httpCreate.post('/api/admin/vendor-items', data);
        });

        $formEdit.onSubmit(function(data) {
            $httpEdit.put(`/api/admin/vendor-items/${data.id}/update`, data)
        });
    })
</script>
@endsection