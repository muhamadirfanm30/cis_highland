@extends($master_template)
@section('content')
<div class="card">
    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div> 
    @endif
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <form action="{{ url('admin/whatsapp-template/store') }}" method="post">
        @csrf
        <div class="card-body">
            <!-- <a href="{{ url('admin/vanue/create') }}" class="btn btn-primary add-category"><i class="fa fa-plus"></i> ADD NEW VANUE</a> -->
            <div class="form-group" style="margin-bottom: 15px;">
                <label for="">Category Name</label>
                <input type="text" name="category_name" class="form-control" value="">
            </div>

            <div class="form-group" style="margin-bottom: 15px;">
                <label for="">Description</label>
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-10">
                            <div class="form-group">
                                <textarea class="form-control" name="desc" id="body-input-2" placeholder="Enter body"></textarea>
                            </div>
                            </div>
                            <div class="col-md-2">
                                [{nama_user}] <br>
                                [{nomor_whatsapp}] <br>
                                [{paket}] <br>
                                [{get_checkin}] <br>
                                [{get_checkout}] <br>
                                [{get_pax}] <br>
                                [{get_durasi}] <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <button type="submit" class="btn btn-success btn-block">Save Notification</button>
            </div>

        </div>
    </form>
</div>
@endsection

@section('js')
<script src="https://cdn.ckeditor.com/ckeditor5/31.0.0/classic/ckeditor.js"></script>
<script>
    HelperService.WYSIWYG('#body-input-2');
</script>
@endsection