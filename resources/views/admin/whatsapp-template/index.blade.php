@extends($master_template)
@section('content')
<div class="card">
    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div> 
    @endif
    <br>
    <div class="card-header">
        <h3 class="card-title">{{$title}}</h3>

        <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
                <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
                <i class="fas fa-times"></i>
            </button>
        </div>
    </div>
    <div class="card-body">
        <a href="{{ url('admin/whatsapp-template/create') }}" class="btn btn-primary add-category"><i class="fa fa-plus"></i> ADD NEW NOTIFICATION</a>
        <table id="table-whatsapp"></table>
    </div>
</div>
@endsection

@section('js')
<script>
    $(function() {
        var $table = $('#table-whatsapp');
        var $modal = $('#dark-header-modal');
        var $form = new FormService($('#category-form'));
        var $http = new HttpService({
            formService: $form,
            bootrapTable: $table,
            bootrapModal: $modal,
        });

        $table.bootstrapTable({
            sortName: 'created_at',
            sortOrder: 'ASC',
            url: HelperService.base_url + '/api/admin/notification',
            responseHandler: function(res) {
                return res.data;
            },
            columns: [{
                    title: 'NO',
                    sortable: true,
                    formatter: function(value, row, index) {
                        return index + 1;
                    }
                },
                {
                    title: 'Notification Type',
                    field: 'category_name',
                    sortable: true,
                },
                {
                    title: 'ACTION',
                    sortable: false,
                    formatter: function(value, row, index) {
                        view_link = "<a class='btn btn-primary btn-xs' href='" + HelperService.url('/admin/whatsapp-template/update/' + row.id) + "'>Edit</a>";
                        btn_delete_item = '<button type="button" class="btn btn-sm waves-effect btn-xs btn-danger delete">Delete</button>';
                        return view_link + ' ' + btn_delete_item;
                    },
                    events: {
                        'click .delete': function(e, value, row) {
                            HelperService.confirm(function() {
                                $http.delete(`/api/admin/whatsapp-template/delete/${row.id}`, row)
                            })
                        }
                    }
                },
            ]
        });
    })
</script>
@endsection