<!DOCTYPE html>
<html lang="zxx">
@include('customer.customer_template.header')

<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>
    <!-- Header Section Begin -->
    <!-- <header class="header">
        <div class="header__top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="header__top__left">
                            <ul>
                                <li><i class="fa fa-envelope"></i> {{ !empty($get_email) ? $get_email->value : '' }}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="header__logo">
                        <a href="./index.html"><img src="{{ asset('geo_logo_2.png') }}" alt="Logo Highland" width="150px"></a>
                    </div>
                </div>
                <div class="col-lg-6"></div>
                <div class="col-lg-3">
                    <div class="header__cart">
                        <ul>
                            <li><a href="#"><i class="fa fa-heart"></i> </a></li>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i> </a></li>
                            <li><a href="#" style="color: black;"><i class="fa fa-user"></i>&nbsp; Login</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="humberger__open">
                <i class="fa fa-bars"></i>
            </div>
        </div>
    </header> -->
    <div class="card">
        <div class="col-md-12">
            <div class="row">
                <div class="col-lg-3">
                    <div class="header__logo">
                        <a href="./index.html"><img src="{{ asset('geo_logo_2.png') }}" alt="" width="100px"></a>
                    </div>
                </div>
                <div class="col-lg-9 text-right">
                    <nav class="header__menu">
                        <ul>
                            <li class="active"><a href="{{ url('/') }}">Home</a></li>
                            <li><a href="{{ url('/grid-product') }}">Products</a></li>
                            <!-- <li><a href="#">Pages</a>
                                <ul class="header__menu__dropdown">
                                    <li><a href="./shop-details.html">Shop Details</a></li>
                                    <li><a href="./shoping-cart.html">Shoping Cart</a></li>
                                    <li><a href="./checkout.html">Check Out</a></li>
                                    <li><a href="./blog-details.html">Blog Details</a></li>
                                </ul>
                            </li> -->
                            <li><a href="{{ url('grid-blogs') }}">Blog</a></li>
                            <li><a href="{{ url('contact-us') }}">Contact</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    <!-- Header Section End -->
        @if(session('success'))
            <div class="alert alert-success">
                <strong>{{ session('success') }}</strong>
            </div> 
        @endif
        <div class="row">
            <div class="col-md-12">
                <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        @foreach($showing_slider as $index => $slider)
                            @if($index == 0)
                                <div class="carousel-item active">
                                    <img class="d-block w-100" src="{{ url('storage/img-slider/'.$slider->images_slider) }}" alt="First slide">
                                    <div class="carousel-caption d-none d-md-block">
                                        <h5 style="color: white;">{{ $slider->header }}</h5>
                                        <p style="color: white;">{{ $slider->desc }}</p>
                                    </div>
                                </div>
                            @else 
                                <div class="carousel-item">
                                    <img class="d-block w-100" src="{{ url('storage/img-slider/'.$slider->images_slider) }}" alt="Second slide">
                                    <div class="carousel-caption d-none d-md-block">
                                    <h5 style="color: white;">{{ $slider->header }}</h5>
                                        <p style="color: white;">{{ $slider->desc }}</p>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>
            </div>
            <!-- <div class="col-lg-3">
                <div class="hero__categories">
                    <div class="hero__categories__all">
                        <i class="fa fa-bars"></i>
                        <span>All Categories</span>
                    </div>
                    <ul>
                        @foreach($show_category as $categori)
                            <li><a href="#">{{ $categori->name }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="hero__item set-bg">
                    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            @foreach($showing_slider as $index => $slider)
                                @if($index == 0)
                                    <div class="carousel-item active">
                                        <img class="d-block w-100" src="{{ url('storage/img-slider/'.$slider->images_slider) }}" alt="First slide">
                                        <div class="carousel-caption d-none d-md-block">
                                            <h5 style="color: white;">{{ $slider->header }}</h5>
                                            <p style="color: white;">{{ $slider->desc }}</p>
                                        </div>
                                    </div>
                                @else 
                                    <div class="carousel-item">
                                        <img class="d-block w-100" src="{{ url('storage/img-slider/'.$slider->images_slider) }}" alt="Second slide">
                                        <div class="carousel-caption d-none d-md-block">
                                        <h5 style="color: white;">{{ $slider->header }}</h5>
                                            <p style="color: white;">{{ $slider->desc }}</p>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                            <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                            <span class="carousel-control-next-icon" aria-hidden="true"></span>
                            <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
    <!-- Hero Section End -->
    
    <!-- Featured Section Begin -->
    <section class="featured spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title">
                        <h2>Featured Product</h2>
                    </div>
                    <div class="featured__controls">
                        <ul>
                            <li class="active" data-filter="*">All</li>
                            @foreach($show_category as $categori)
                                <li data-filter=".{{ $categori->name }}">{{ $categori->name }}</li>
                                <!-- <li data-filter=".fresh-meat">Hotel</li>
                                <li data-filter=".vegetables">Outbond</li>
                                <li data-filter=".fastfood">Tracking</li> -->
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            <div class="row featured__filter">
                @foreach($get_product as $prod)
                    <div class="col-lg-3 col-md-4 col-sm-6 mix {{ !empty($prod->category_vanue) ? $prod->category_vanue->name : '' }}">
                        <div class="featured__item">
                            <!-- <div class="featured__item__pic set-bg" data-setbg="{{ asset('assets/img/featured/feature-1.jpg') }}"> -->
                            <div>
                                <img src="{{ url('storage/img-vanue/'.$prod->images) }}" alt="">
                                
                            </div>
                            <div class="featured__item__text">
                                <h6><a href="{{ url('single-product/'.encrypt($prod->id)) }}">{{ $prod->vanue_name }}</a></h6>
                                <i class="fa fa-map-marker"></i>&nbsp;<small>{{ $prod->location }}</small>
                                <h6>Rp.{{ number_format($prod->harga) }}</h6>
                                <a href="{{ url('single-product/'.encrypt($prod->id)) }}" class="btn btn-success btn-xs btn-block"> View Detail</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <!-- Featured Section End -->
    <div class="row">
        <div class="col-md-5"></div>
        <div class="col-md-2">
            <a href="{{ url('grid-product') }}" class="btn btn-info btn-block">View All Product </a>
        </div>
        <div class="col-md-5"></div>
    </div><br><br>

    <!-- Breadcrumb Section Begin -->
    <style>
        .centered {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            font-size: 40px;
        }
    </style>
    <div class="col-md-12">
        <img src="{{ asset('breadcrumb_2.png') }}" alt="">
        <div class="centered"><strong></strong></div>
    </div>

    <!-- Blog Section Begin -->
    <section class="from-blog spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title from-blog__title">
                        <h2>From The Blog</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach($blog as $val)
                    <div class="col-lg-4 col-md-4 col-sm-6">
                        <div class="blog__item">
                            <div class="blog__item__pic">
                                <img src="{{ url('storage/img-blog/'.$val->image_blog) }}" alt="">
                            </div>
                            <div class="blog__item__text">
                                <ul>
                                    <li><i class="fa fa-calendar-o"></i> {{ date('M-d-Y', strtotime($val->date)) }}</li>
                                    <!-- <li><i class="fa fa-comment-o"></i> 5</li> -->
                                </ul>
                                <h5><a href="{{ url('single-blogspot/'.encrypt($val->id)) }}">{{ \Illuminate\Support\Str::limit($val->title, 50, $end='...')   }}</a></h5>
                                <a href="{{ url('single-blogspot/'.encrypt($val->id)) }}" class="btn btn-success btn-xs"> View Detail</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
    <div class="row">
        <div class="col-md-5"></div>
        <div class="col-md-2">
            <a href="{{ url('grid-blogs') }}" class="btn btn-info btn-block">View All Blogs </a>
        </div>
        <div class="col-md-5"></div>
    </div>
    <!-- Blog Section End -->
<a href="" class="mb-2 mr-2 btn-icon btn-icon-only btn btn-link btn-sm link-dropdown-noty" style="color: white;">
    <i class="fa fa-envelope btn-icon-wrapper font-size-xlg"> </i>
</a>
    <!-- Footer Section Begin -->
    <footer class="footer spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="footer__about">
                        <div class="footer__about__logo">
                            <a href="./index.html"><img src="{{ asset('geo_logo_1.png') }}" alt=""></a>
                        </div>
                        <ul>
                            <li>Address: {{ !empty($get_address) ? $get_address->value : '-' }}</li>
                            <li>Phone: {{ !empty($get_phone) ? $get_phone->value : '-' }}</li>
                            <li>Email: {{ !empty($get_email) ? $get_email->value : '-' }}</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 offset-lg-1">
                    <div class="footer__widget">
                        <h6>Useful Links</h6>
                        <ul>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">About Our Shop</a></li>
                            <li><a href="#">Secure Shopping</a></li>
                            <li><a href="#">Delivery infomation</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Our Sitemap</a></li>
                        </ul>
                        <ul>
                            <li><a href="#">Who We Are</a></li>
                            <li><a href="#">Our Services</a></li>
                            <li><a href="#">Projects</a></li>
                            <li><a href="#">Contact</a></li>
                            <li><a href="#">Innovation</a></li>
                            <li><a href="#">Testimonials</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="footer__widget">
                        <h6>Join Our Newsletter Now</h6>
                        <p>Get E-mail updates about our latest shop and special offers.</p>
                        <form action="#">
                            <input type="text" placeholder="Enter your mail">
                            <button type="submit" class="site-btn">Subscribe</button>
                        </form>
                        <div class="footer__widget__social">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-pinterest"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer__copyright">
                        <div class="footer__copyright__text"><p>
                            <!-- Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a> -->
                        </div>
                        <!-- <div class="footer__copyright__payment"><img src="{{ asset('assets/img/payment-item.png') }}" alt=""></div> -->
                    </div>
                </div>
            </div>
        </div>
    </footer>
    
    <!-- Footer Section End -->
    @include('customer.customer_template.script')
</body>
</html>