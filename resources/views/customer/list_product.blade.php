<!DOCTYPE html>
<html lang="zxx">
@include('customer.customer_template.header')

<body>
    
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>
    <!-- Header Section Begin -->
    <!-- <header class="header">
        <div class="header__top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6">
                        <div class="header__top__left">
                            <ul>
                                <li><i class="fa fa-envelope"></i> {{ $get_email->value }}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="header__logo">
                        <a href="./index.html"><img src="{{ asset('geo_logo_2.png') }}" alt="Logo Highland" width="150px"></a>
                    </div>
                </div>
                <div class="col-lg-6"></div>
                <div class="col-lg-3">
                    <div class="header__cart">
                        <ul>
                            <li><a href="#"><i class="fa fa-heart"></i> </a></li>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i> </a></li>
                            <li><a href="#" style="color: black;"><i class="fa fa-user"></i>&nbsp; Login</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="humberger__open">
                <i class="fa fa-bars"></i>
            </div>
        </div>
    </header> -->
    <div class="col-md-12">
            <div class="row">
                <div class="col-lg-3">
                    <div class="header__logo">
                        <a href="./index.html"><img src="{{ asset('geo_logo_2.png') }}" alt="" width="100px"></a>
                    </div>
                </div>
                <div class="col-lg-9 text-right">
                    <nav class="header__menu">
                        <ul>
                            <li><a href="{{ url('/') }}">Home</a></li>
                            <li class="active"><a href="{{ url('/grid-product') }}">Products</a></li>
                            <!-- <li><a href="#">Pages</a>
                                <ul class="header__menu__dropdown">
                                    <li><a href="./shop-details.html">Shop Details</a></li>
                                    <li><a href="./shoping-cart.html">Shoping Cart</a></li>
                                    <li><a href="./checkout.html">Check Out</a></li>
                                    <li><a href="./blog-details.html">Blog Details</a></li>
                                </ul>
                            </li> -->
                            <li><a href="{{ url('grid-blogs') }}">Blog</a></li>
                            <li><a href="{{ url('contact-us') }}">Contact</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div><br>
    <!-- Header Section End -->

    <!-- Breadcrumb Section Begin -->
    <style>
        .centered {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%);
            font-size: 40px;
        }
    </style>
    <div class="col-md-12">
        <img src="{{ asset('breadcrumb_2.png') }}" alt="">
        <div class="centered"><strong>Product</strong></div>
    </div>

    <!-- Product Section Begin -->
    <section class="product spad">
        <div class="container">
            <div class="row">
                <!-- <div class="col-lg-3 col-md-5">
                    <div class="sidebar">
                        <div class="sidebar__item">
                            <h4>Department</h4>
                            <ul>
                                <li><a href="#">Fresh Meat</a></li>
                                <li><a href="#">Vegetables</a></li>
                                <li><a href="#">Fruit & Nut Gifts</a></li>
                                <li><a href="#">Fresh Berries</a></li>
                                <li><a href="#">Ocean Foods</a></li>
                                <li><a href="#">Butter & Eggs</a></li>
                                <li><a href="#">Fastfood</a></li>
                                <li><a href="#">Fresh Onion</a></li>
                                <li><a href="#">Papayaya & Crisps</a></li>
                                <li><a href="#">Oatmeal</a></li>
                            </ul>
                        </div>
                    </div>
                </div> -->
                <div class="col-lg-12 col-md-12">
                    <div class="filter__item">
                        <div class="row">
                            <div class="col-lg-4 col-md-5">
                                <div class="filter__sort">
                                    <form id="kategori-form" action="{{ route('cs-order-produk.show') }}" method="POST">
                                        @csrf
                                        <span>Sort By</span>
                                        <select name="kategori" id="kategoriProduk" onchange="event.preventDefault();document.getElementById('kategori-form').submit();">
                                            <option value="">All</option>
                                            @foreach($get_category as $cat)
                                                <option value="{{ $cat->id }}" {{ $kategori == $cat->id ? "selected":"" }}>{{ $cat->name }}</option>
                                            @endforeach
                                        </select>
                                    </form>
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="filter__found">
                                    <!-- <h6><span>16</span> Products found</h6> -->
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-3">
                                <div class="filter__option">
                                    <span class="icon_grid-2x2"></span>
                                    <span class="icon_ul"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        @foreach($showProduct as $k => $product_list)
                            <div class="col-lg-3 col-md-4 col-sm-6 mix {{ !empty($product_list->category_vanue) ? $product_list->category_vanue->name : '' }}">
                                <div class="featured__item">
                                    <!-- <div class="featured__item__pic set-bg" data-setbg="{{ asset('assets/img/featured/feature-1.jpg') }}"> -->
                                    <div>
                                        <img src="{{ url('storage/img-vanue/'.$product_list->images) }}" alt="">
                                        
                                    </div>
                                    <div class="featured__item__text">
                                        <h6><a href="{{ url('single-product/'.encrypt($product_list->id)) }}">{{ $product_list->vanue_name }}</a></h6>
                                        <i class="fa fa-map-marker"></i>&nbsp;<small>{{ $product_list->location }}</small>
                                        <h6>Rp.{{ number_format($product_list->harga) }}</h6>
                                        <a href="{{ url('single-product/'.encrypt($product_list->id)) }}" class="btn btn-success btn-xs btn-block"> View Detail</a>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="product__pagination">
                        {!! $showProduct->render() !!} 
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Footer Section Begin -->
    <footer class="footer spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="footer__about">
                        <div class="footer__about__logo">
                            <a href="./index.html"><img src="{{ asset('geo_logo_1.png') }}" alt=""></a>
                        </div>
                        <ul>
                            <li>Address: {{ $get_address->value }}</li>
                            <li>Phone: {{ $get_phone->value }}</li>
                            <li>Email: {{ $get_email->value }}</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 offset-lg-1">
                    <div class="footer__widget">
                        <h6>Useful Links</h6>
                        <ul>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">About Our Shop</a></li>
                            <li><a href="#">Secure Shopping</a></li>
                            <li><a href="#">Delivery infomation</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Our Sitemap</a></li>
                        </ul>
                        <ul>
                            <li><a href="#">Who We Are</a></li>
                            <li><a href="#">Our Services</a></li>
                            <li><a href="#">Projects</a></li>
                            <li><a href="#">Contact</a></li>
                            <li><a href="#">Innovation</a></li>
                            <li><a href="#">Testimonials</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="footer__widget">
                        <h6>Join Our Newsletter Now</h6>
                        <p>Get E-mail updates about our latest shop and special offers.</p>
                        <form action="#">
                            <input type="text" placeholder="Enter your mail">
                            <button type="submit" class="site-btn">Subscribe</button>
                        </form>
                        <div class="footer__widget__social">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-pinterest"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer__copyright">
                        <div class="footer__copyright__text"><p>
                            <!-- Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a> -->
                        </div>
                        <!-- <div class="footer__copyright__payment"><img src="{{ asset('assets/img/payment-item.png') }}" alt=""></div> -->
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Product Section End -->
    @include('customer.customer_template.script')
</body>
</html>