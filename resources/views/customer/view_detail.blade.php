<!DOCTYPE html>
<html lang="zxx">
    @include('customer.customer_template.header')
<body>
    <!-- Page Preloder -->
    <div id="preloder">
        <div class="loader"></div>
    </div>
    <!-- Header Section Begin -->
    <!-- <header class="header">
        <div class="header__top">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6">
                        <div class="header__top__left">
                            <ul>
                                <li><i class="fa fa-envelope"></i> {{ $get_email->value }}</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3">
                    <div class="header__logo">
                        <a href="./index.html"><img src="{{ asset('geo_logo_2.png') }}" alt="Logo Highland" width="150px"></a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <nav class="header__menu">
                        <ul></ul>
                    </nav>
                </div>
                <div class="col-lg-3">
                    <div class="header__cart">
                        <ul>
                            <li><a href="#"><i class="fa fa-heart"></i> <span>1</span></a></li>
                            <li><a href="#"><i class="fa fa-shopping-bag"></i> <span>3</span></a></li>
                            <li><a href="#" style="color: black;"><i class="fa fa-user"></i>&nbsp; Login</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="humberger__open">
                <i class="fa fa-bars"></i>
            </div>
        </div>
    </header> -->
    <div class="col-md-12">
        <div class="row">
            <div class="col-lg-3">
                <div class="header__logo">
                    <a href="./index.html"><img src="{{ asset('geo_logo_2.png') }}" alt="" width="100px"></a>
                </div>
            </div>
            <div class="col-lg-9 text-right">
                <nav class="header__menu">
                    <ul>
                        <li><a href="{{ url('/') }}">Home</a></li>
                        <li  class="active"><a href="{{ url('/grid-product') }}">Products</a></li>
                        <li><a href="{{ url('grid-blogs') }}">Blog</a></li>
                        <li><a href="{{ url('contact-us') }}">Contact</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>
    <!-- Header Section End -->


    <!-- Breadcrumb Section Begin -->
    <section class="breadcrumb-section set-bg" data-setbg="{{ asset('breadcrumb_2.png') }}">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <div class="breadcrumb__text">
                        <h2 style="color:black">{{ $get_product->vanue_name }}</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Breadcrumb Section End -->

    <!-- Product Details Section Begin -->
    <section class="product-details spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6">
                    <div class="product__details__pic">
                        <div class="product__details__pic__item">
                            <img class="product__details__pic__item--large"
                                src="{{ url('storage/img-vanue/'.$get_product->images) }}" alt="">
                        </div>
                        <div class="product__details__pic__slider owl-carousel">
                            @foreach($get_product->get_image as $img)
                                <img data-imgbigurl="{{ url('storage/img-vanue/'.$img->image) }}"
                                    src="{{ url('storage/img-vanue/'.$img->image) }}" alt="">
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-md-6">
                    <div class="product__details__text">
                        <h3>{{ $get_product->vanue_name }}</h3>
                        <h6>Category : {{ !empty($get_product->category_vanue) ? $get_product->category_vanue->name : '' }}</h6>
                        <div style="margin-bottom: 20px;">
                            <i class="fa fa-map-marker"></i>&nbsp;<small>{{ $get_product->location }}</small>
                        </div>
                        <h5><b>Fasilitas</b></h5>
                        <div class="row">
                            @foreach($get_product->get_fasilitas as $fasilitas)
                                <div class="col-md-4"><i class="fa fa-check"></i>&nbsp; {{ $fasilitas->fasilitas_name }}</div>
                            @endforeach
                        </div><br>
                        <!-- <div class="product__details__rating">
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star"></i>
                            <i class="fa fa-star-half-o"></i>
                            <span>(18 reviews)</span>
                        </div> -->
                        <div class="product__details__price">Rp. {{ number_format($get_product->harga) }}</div>
                        <div class="row">
                            <div class="col-md-3">
                                <a href="#" class="btn btn-primary btn-block"><span class="icon_heart_alt"></span>&nbsp; Save</a>
                            </div>
                            <div class="col-md-5">
                                <a href="" class="btn btn-success btn-block" style="color: white;"><i class="fa fa-whatsapp"></i>&nbsp; Cek Available Date</a>
                            </div>
                            <div class="col-md-4">
                                <a href="{{ url('checkout/'.encrypt($get_product->id)) }}" class="btn btn-info btn-block"><i class="fa fa-check"></i>&nbsp; Booking Now</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="product__details__tab">
                        <ul class="nav nav-tabs" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" data-toggle="tab" href="#tabs-1" role="tab"
                                    aria-selected="true">Description</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs-2" role="tab"
                                    aria-selected="false">Information</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="tab" href="#tabs-3" role="tab"
                                    aria-selected="false">Reviews <span>(1)</span></a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="tabs-1" role="tabpanel">
                                <div class="product__details__tab__desc">
                                    {!! $get_product->desc !!}
                                </div>
                            </div>
                            <div class="tab-pane" id="tabs-2" role="tabpanel">
                                <div class="product__details__tab__desc">
                                    {!! $get_product->information !!}
                                </div>
                            </div>
                            <div class="tab-pane" id="tabs-3" role="tabpanel">
                                <div class="product__details__tab__desc">
                                    <h6>Products Infomation</h6>
                                    <p>Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.
                                        Pellentesque in ipsum id orci porta dapibus. Proin eget tortor risus.
                                        Vivamus suscipit tortor eget felis porttitor volutpat. Vestibulum ac diam
                                        sit amet quam vehicula elementum sed sit amet dui. Donec rutrum congue leo
                                        eget malesuada. Vivamus suscipit tortor eget felis porttitor volutpat.
                                        Curabitur arcu erat, accumsan id imperdiet et, porttitor at sem. Praesent
                                        sapien massa, convallis a pellentesque nec, egestas non nisi. Vestibulum ac
                                        diam sit amet quam vehicula elementum sed sit amet dui. Vestibulum ante
                                        ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae;
                                        Donec velit neque, auctor sit amet aliquam vel, ullamcorper sit amet ligula.
                                        Proin eget tortor risus.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Product Details Section End -->

    <!-- Related Product Section Begin -->
    <section class="related-product">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title related__product__title">
                        <h2>Related Villa</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                @if(!empty($get_related))
                    @foreach($get_related as $related)
                        <div class="col-lg-3 col-md-4 col-sm-6 mix {{ !empty($related->category_vanue) ? $related->category_vanue->name : '' }}">
                            <div class="featured__item">
                                <!-- <div class="featured__item__pic set-bg" data-setbg="{{ asset('assets/img/featured/feature-1.jpg') }}"> -->
                                <div>
                                    <img src="{{ url('storage/img-vanue/'.$related->images) }}" alt="">
                                    
                                </div>
                                <div class="featured__item__text">
                                    <h6><a href="{{ url('single-product/'.encrypt($related->id)) }}">{{ $related->vanue_name }}</a></h6>
                                    <i class="fa fa-map-marker"></i>&nbsp;<small>{{ $related->location }}</small>
                                    <h6>Rp.{{ number_format($related->harga) }}</h6>
                                    <a href="{{ url('single-product/'.encrypt($related->id)) }}" class="btn btn-success btn-xs btn-block"> View Detail</a>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @endif
            </div>
        </div>
    </section>
    <!-- Related Product Section End -->

    <!-- Footer Section Begin -->
    <footer class="footer spad">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6">
                    <div class="footer__about">
                        <div class="footer__about__logo">
                            <a href="./index.html"><img src="{{ asset('geo_logo_1.png') }}" alt=""></a>
                        </div>
                        <ul>
                            <li>Address: {{ $get_address->value }}</li>
                            <li>Phone: {{ $get_phone->value }}</li>
                            <li>Email: {{ $get_email->value }}</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6 col-sm-6 offset-lg-1">
                    <div class="footer__widget">
                        <h6>Useful Links</h6>
                        <ul>
                            <li><a href="#">About Us</a></li>
                            <li><a href="#">About Our Shop</a></li>
                            <li><a href="#">Secure Shopping</a></li>
                            <li><a href="#">Delivery infomation</a></li>
                            <li><a href="#">Privacy Policy</a></li>
                            <li><a href="#">Our Sitemap</a></li>
                        </ul>
                        <ul>
                            <li><a href="#">Who We Are</a></li>
                            <li><a href="#">Our Services</a></li>
                            <li><a href="#">Projects</a></li>
                            <li><a href="#">Contact</a></li>
                            <li><a href="#">Innovation</a></li>
                            <li><a href="#">Testimonials</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-4 col-md-12">
                    <div class="footer__widget">
                        <h6>Join Our Newsletter Now</h6>
                        <p>Get E-mail updates about our latest shop and special offers.</p>
                        <form action="#">
                            <input type="text" placeholder="Enter your mail">
                            <button type="submit" class="site-btn">Subscribe</button>
                        </form>
                        <div class="footer__widget__social">
                            <a href="#"><i class="fa fa-facebook"></i></a>
                            <a href="#"><i class="fa fa-instagram"></i></a>
                            <a href="#"><i class="fa fa-twitter"></i></a>
                            <a href="#"><i class="fa fa-pinterest"></i></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="footer__copyright">
                        <div class="footer__copyright__text"><p>
                            <!-- Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a> -->
                        </div>
                        <!-- <div class="footer__copyright__payment"><img src="{{ asset('assets/img/payment-item.png') }}" alt=""></div> -->
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer Section End -->
    @include('customer.customer_template.script')
</body>
</html>