<!doctype html>
<html lang="en">

    <head>

		<!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        
        <title>Highland Camp</title>

        <!-- CSS -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,600">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
        <link rel="stylesheet" href="{{ asset('assets/css/animate.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
        <link rel="stylesheet" href="{{ asset('assets/css/media-queries.css') }}">
		<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.6.0/font/bootstrap-icons.css">

        <!-- Favicon and touch icons -->
        <link rel="shortcut icon" href="{{ asset('assets/img/logo-highland.png') }}">
        <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{ asset('assets/ico/apple-touch-icon-144-precomposed.png') }}">
        <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{ asset('assets/ico/apple-touch-icon-114-precomposed.png') }}">
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{ asset('assets/ico/apple-touch-icon-72-precomposed.png') }}">
        <link rel="apple-touch-icon-precomposed" href="{{ asset('assets/ico/apple-touch-icon-57-precomposed.png') }}">

    </head>

    <body>

		<!-- Top menu -->
		<nav class="navbar navbar-dark fixed-top navbar-expand-md navbar-no-bg">
			<div class="container">
				<a class="navbar-brand" href="index.html"><img src="{{ asset('assets/img/logo-highland.png') }}" alt="Highland" width="50" class="pr-2"> Highland Camp</a>
			    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
			        <span class="navbar-toggler-icon"></span>
			    </button>
			    <div class="collapse navbar-collapse" id="navbarNav">
			        <ul class="navbar-nav ml-auto">
			            <li class="nav-item">
			                <a class="nav-link scroll-link" href="#top-content">beranda</a>
			            </li>
			            <li class="nav-item">
			                <a class="nav-link scroll-link" href="#section-1">explore</a>
			            </li>
			            <li class="nav-item">
			                <a class="nav-link scroll-link" href="#section-2">pricing</a>
			            </li>
			            <li class="nav-item">
			                <a class="nav-link scroll-link" href="#section-3">location</a>
						<li class="nav-item">
							<div class="collapse navbar-collapse" id="navbarNavDarkDropdown">
								<ul class="navbar-nav">
								  <li class="nav-item dropdown">
									<a class="nav-link dropdown-toggle" href="#" id="navbarDarkDropdownMenuLink" role="button" data-bs-toggle="dropdown" aria-expanded="false">
									  akstivitas 
									</a>
									<ul class="dropdown-menu dropdown-menu-dark" aria-labelledby="navbarDarkDropdownMenuLink">
									  <li><a class="dropdown-item" href="{{ url('/login-form') }}">Login</a></li>
									  <li><a class="dropdown-item" href="#">Another action</a></li>
									  <li><a class="dropdown-item" href="#">Something else here</a></li>
									</ul>
								  </li>
								</ul>
							  </div>
						</li>
			        </ul>
			    </div>
		    </div>
		</nav>

        <!-- Top content -->
        <div class="top-content">
       		<div class="row no-gutters">
       			<div class="col">
       				<div id="carousel-example" class="carousel slide" data-ride="carousel">
       					<div class="carousel-inner">
							@foreach($showSlider as $index => $slider)
								@if($index == 0)
									<div class="carousel-item active">
										<img src="{{ url('storage/slider/'.$slider->image_file) }}" class="d-block w-100" alt="img1" max-height="700">
									</div>
								@else 
									<div class="carousel-item">
										<img src="{{ url('storage/slider/'.$slider->image_file) }}" class="d-block w-100" alt="img2" max-height="700">
										<!-- <div class="carousel-caption">
											<h1 class="wow fadeInLeftBig">Don't Leave Anything But Foot Prints</h1>
											<div class="description wow fadeInUp">
												<p>Me, Nature And Our Memories.</p>
												<center>
													<a href="#" class="button mt-4 text-capitalize">reservation now</a>
													<div class="search">
														<div class="input-group shadow">
															<input type="text" class="form-control opacity-75" placeholder="Search...">
															<div class="input-group-append">
															<button class="btn btn-secondary" type="button">
																<i class="fa fa-search"></i>
															</button>
															</div>
														</div>
													</div>
												</center>
											</div>
										</div> -->
									</div>
								@endif
							@endforeach
							<div class="carousel-item">
								<img src="{{ asset('assets/img/map.png') }}" class="d-block w-100" alt="img3" max-height="700">
							</div>
       					</div>
						<a class="carousel-control-prev" href="#carousel-example" role="button" data-slide="prev">
							<span class="carousel-control-prev-icon bg-secondary text-white rounded-circle" aria-hidden="true"></span>
							<span class="sr-only">Previous</span>
						</a>
						<a class="carousel-control-next" href="#carousel-example" role="button" data-slide="next">
							<span class="carousel-control-next-icon bg-secondary text-white rounded-circle" aria-hidden="true"></span>
							<span class="sr-only">Next</span>
						</a>
       				</div>
       			</div>
       		</div>
        </div>

		<!-- Section 3 -->
        <div class="section-3-container section-container pt-3">
	        <div class="container">
	            <div class="row">
	                <div class="col section-3 section-description wow fadeIn">
	                    <h2>Here We Are!</h2>
	                    <div class="divider-1 wow fadeInUp"><span></span></div>
	                </div>
	            </div>
	            <div class="row">
					<div class="col-md-12">
						<div class="zoom-marker-div" id="zoom-marker-div" class="">
							<img class="zoom-marker-img" id="zoom-marker-img" name="viewArea" draggable="false" />
						</div>
					</div>				
				</div>
	        </div>
        </div>

        <!-- EXPLORE -->
        <!-- <div class="section-1-container section-container">
	        <div class="container mt-3">
	            <div class="row">
	                <div class="col section-1 section-description wow fadeIn">
	                    <h2>Explore</h2>
	                    <div class="divider-1 wow fadeInUp"><span></span></div>
	                </div>
	            </div>
	            <div class="row">
					<div class="col-md-4 section-1-box wow fadeInUp">
						<div class="card card1 card--15 shadow">
							<h3 class="card__title">Wonderful Highland</h3>
							<img class="card__img" src="assets/img/wonderful-highland.jpg" alt="">
							<p class="card__text text-justify">Dengan nuansa tingginya puncak dan hijaunya hutan, Anda akan dimanjakan dengan megahnya gunung Gede Pangrango dan gunung Salak. Pada malam harinya, keindahan lampu kota yang gemerlap akan meramaikan suasana berkemah bersama orang terkasih.</p>
							<a class="card__btn" href="" class="rounded-3 pb-3">Go Explore >></a>
						</div>
					</div>
					<div class="col-md-4 section-1-box wow fadeInUp">
						<div class="card card2 card--15 shadow">
							<h3 class="card__title">highland curug</h3>
							<img class="card__img" src="assets/img/curug-cibulao.jpg" alt="">
							<p class="card__text text-justify">Kepopuleran birunya warna air yang mirip warna bulao berpadu padan dengan pesona keindahan alam yang terbentuk dari lanskap pebukitan dengan dominasi rimbunnya hutan pegunungan telah menjadi daya tarik wisata air terjun dan telah menghipnotis banyak wisatawan.</p>
							<a class="card__btn" href="" class="rounded-3 pb-3">Go Explore >></a>
						</div>
					</div>
					<div class="col-md-4 section-1-box wow fadeInUp">
						<div class="card card3 card--15 shadow">
							<h3 class="card__title">highland camp</h3>
							<img class="card__img" src="assets/img/camp.jpg" alt="">
							<p class="card__text text-justify">Highland menghadirkan keindahan lanskap buatan diantara pesona lanskap alam untuk kegiatan rekreasi, edukasi, pelatihan dan pengembangan SDM. Highland sangat cocok untuk Anda yang ingin menghirup udara segar pegunungan dan rehat sejenak dari hiruk pikuk perkotaan.</p>
							<a class="card__btn" href="" class="rounded-3 pb-3">Go Explore >></a>
						</div>
					</div>
	            </div>
				
	        </div>
        </div> -->

        <!-- PRICING -->
        <div class="section-2-container section-container section-container-gray-bg">
	        <div class="container pt-3">
	            <div class="row">
	                <div class="col section-2 section-description wow fadeIn">
						<h2>Go Reservation</h2>
						<div class="divider-1 wow fadeIn"><span></span></div>
						<h5 class="text-muted">*note: jumlah kapasitas tergantung tenda yang digunakan</h5>
	                </div>
	            </div>
	            <div class="row">
	            	<div class="col section-2-box wow fadeInLeft">
                    	<div class="contain">
							<div class="row">
								@foreach($showDataCampsite as $index => $slider_camps)
									<div class="col-md-3">
										<div class="card-box shadow-lg">
											<div class="card-thumbnail">
												<img src="{{ asset('assets/img/exclusive.jpg') }}" class="img-fluid" alt="" >
											</div>
											<h3><a href="#" class="mt-2 text-danger">{{ $slider_camps->block->name }}</a></h3>
											<p class="text-secondary text-justify">
												<b>Campsite: </b> <i class="text-danger">{{ $slider_camps->name }}</i> <br>
												<b>IDR: </b> <i class="text-danger">Rp. 66.000</i> / Malam <br>
												{{ $slider_camps->note }}
											</p>
											<a href="{{ url('detail/' .$slider_camps->id) }}" class="btn btn-sm btn-danger float-left">Book Now</a>
										</div>
									</div>
								@endforeach
								<!-- <div class="col-md-12">
									<div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
										<ol class="carousel-indicators">
											<li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
											<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
										</ol>
										<div class="carousel-inner">
											@foreach($showDataCampsite as $index => $slider_camps)
												@if(in_array($index, [0,1,2]))
													<div class="carousel-item active">
														<div class="row">
															<div class="col-md-3">
																<div class="card-box shadow-lg">
																	<div class="card-thumbnail">
																		<div class="card-mark">{{ $slider_camps->name }}</div>
																		<img src="{{ asset('assets/img/exclusive.jpg') }}" class="img-fluid" alt="" >
																	</div>
																	<h3><a href="#" class="mt-2 text-danger">Kijabud</a></h3>
																	<p class="text-secondary text-justify">
																		<b>IDR: </b> <i class="text-danger">XXX</i> / Malam <br>
																		Privat Camp yang terpisah dari site lain. Cocok untuk Anda yang ingin menikmati alam tanpa berbagi space dengan orang lain.
																	</p>
																	<a href="#" class="btn btn-sm btn-danger float-left">Book Now</a>
																</div>
															</div>
														</div>
													</div>
												@else 
													<div class="carousel-item">
														<div class="row">
															<div class="col-md-3">
																<div class="card-box">
																	<div class="card-thumbnail">
																		<img src="{{ asset('assets/img/camp.jpg') }}" class="img-fluid" alt="">
																	</div>
																	<h3><a href="#" class="mt-2 text-danger">Nirmala</a></h3>
																	<p class="text-secondary text-justify">
																		<b>IDR: </b> <i class="text-danger">66K</i> / Malam <br>
																		<b>Jumlah Site:</b> X <br>
																		<b>Jumlah Blok: </b> X <br>
																	</p>
																	<a href="#" class="btn btn-sm btn-danger float-right">Book Now</a>
																</div>
															</div>
														</div>
													</div>
												@endif
											@endforeach
										</div>
									</div>
									<a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev" style="margin-left: -100px;">
										<span class="carousel-control-prev-icon bg-secondary text-white rounded-circle" aria-hidden="true"></span>
										<span class="sr-only">Previous</span>
									</a>
									<a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next" style="margin-right: -100px;">
										<span class="carousel-control-next-icon bg-secondary text-white rounded-circle" aria-hidden="true"></span>
										<span class="sr-only">Next</span>
									</a>
								</div> -->
							</div>
						</div>
	                </div>
	            </div>
	        </div>
        </div>

		<!-- Section 3 -->
        <div class="section-3-container section-container pt-3">
	        <div class="container">
	            <div class="row">
	                <div class="col section-3 section-description wow fadeIn">
	                    <h2>Here We Are!</h2>
	                    <div class="divider-1 wow fadeInUp"><span></span></div>
	                </div>
	            </div>
	            <div class="row">
					<div class="maps-highland">
						<div id="map_container">
						<div id="map" class="rounded shadow-sm"></div>
					</div>
				</div>
	        </div>
        </div>

        <!-- Footer -->
        <footer class="footer-container shadow d-flex flex-wrap justify-content-between align-items-center p-3 border-top" style="margin-bottom: -100px;">
			
					<div class="col-md-4 d-flex align-items-center">
					  
						<img src="{{ asset('assets/img/logo-highland.png') }}" height="35" alt="">
					  
					  <span class="text-muted">&copy; 2021 Highland Camp</span>
					</div>
					<ul class="nav col-md-4 justify-content-end list-unstyled d-flex">
					  <li class="ms-3"><a class="text-muted" href="#"><i class="bi bi-whatsapp width="30" height="30""></i></a></li>
					  <li class="ms-3"><a class="text-muted" href="#"><i class="bi bi-instagram width="30" height="30""></i></li>
					  <li class="ms-3"><a class="text-muted" href="#"><i class="bi bi-facebook width="30" height="30"""></i></a></li>
					</ul>
			
        </footer>

        <!-- Javascript -->
		<script src="{{ asset('assets/js/jquery-3.3.1.min.js') }}"></script>
		<script src="{{ asset('assets/js/jquery-migrate-3.0.0.min.js') }}"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js" integrity="sha384-wHAiFfRlMFy6i5SRaxvfOCifBUQy1xHdJ/yoi7FRNXMRBu5WHdZYu1hA6ZOblgut" crossorigin="anonymous"></script>
		<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js" integrity="sha384-B0UglyR+jN6CkvvICOB2joaf5I4l3gm9GU6Hc1og6Ls7i6U/mkkaduKaBhlAXv9k" crossorigin="anonymous"></script>
		<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
        <script src="{{ asset('assets/js/jquery.backstretch.min.js') }}"></script>
        <script src="{{ asset('assets/js/wow.min.js') }}"></script>
        <script src="{{ asset('assets/js/waypoints.min.js') }}"></script>
        <script src="{{ asset('assets/js/scripts.js') }}"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB41DRUbKWJHPxaFjMAwdrzWzbVKartNGg&callback=initMap&libraries=&v=weekly&channel=2" async></script>



		<link rel="stylesheet" href="{{ asset('plugins/zoomMarker/css/easy-loading.css') }}">
		<script src="{{ asset('plugins/zoomMarker/js/easy-loading.js') }}"></script>

		<script src="{{ asset('plugins/zoomMarker/js/jquery.mousewheel.min.js') }}"></script>
		<script src="{{ asset('plugins/zoomMarker/js/hammer.min.js') }}"></script>

		<link rel="stylesheet" href="{{ asset('plugins/zoomMarker/css/zoom-marker.css') }}">
		<script src="{{ asset('plugins/zoomMarker/js/zoom-marker.js') }}"></script>
		<style>
			body{
				background-color: var(--bs-body-bg);
			}
			.zoom-marker-img {
				box-shadow: none;
			}
		</style>
		<script>
			$(document).ready(function() {
				$.ajax({
                    type: 'get',
                    url: "{{ url('') }}" + '/api/public/dashboard/map-reservation',
                    success: function(resp) {
						map_config = resp.data;
						console.log(resp);
						// $('#zoom-marker-img').zoomMarker(map_config);

						$('#zoom-marker-img').zoomMarker({
						    src: map_config.src,
						    rate: 0.2,
						    width: map_config.width,
						    max: 3000,
						    zoom_lock: true,
							move_limit: true,
						    markers: map_config.markers
						});
                    },
                    error: function(request, status, error) {
                    }
                });
        	})

			$('#zoom-marker-img').on("zoom_marker_click", function(event, size) {
				console.log("image has been loaded with size: ", size);
			});
		</script>
    </body>

</html>