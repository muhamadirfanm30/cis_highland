<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Reservation | Highland Camp</title>
</head>
<body>
<meta name="csrf-token" content="{{ csrf_token() }}" />
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
<div class="container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto" >
    <div class="card card0 border-0">
        <div class="row d-flex">
            <div class="col-lg-5">
                <div class="card1 pb-5"><BR><BR>
                    {{-- <div class="row"> <img src="https://i.imgur.com/CXQmsmF.png" class="logo"> </div> --}}
                    <div class="row px-3 justify-content-center mt-4 mb-5 border-line">
                         <img src="{{ url('storage/campsite-image/'. $dataReservasi->images_campsite) }}" class="image" style="width: 93%;height:auto">
                         <div class="col-md-6">
                             <h5><strong>Nama Campsite</strong></h5>
                             <small><i class="fa fa-home"></i> Capacity : 1 - 4 Tenda</small>
                         </div>
                         <div class="col-md-6 text-right">
                             <small><strong>Individual Price</strong>  <p style="color:#00cc39">Rp.66.000</p></small>
                         </div>
                    </div>
                </div>
            </div>
            
            <div class="col-lg-7"><br>
                <div class="container">
                    <center><h6>Book Ticket Camp</h6></center><hr>
                    <!-- <form> -->
                        
                        <div class="row">
                            <div class="col-md-12" style="margin-bottom: 10px;">
                                <label for="">Name</label>
                                <input type="text" id="name" name="name" class="form-control name" placeholder="Name">
                            </div>
                            <div class="col-md-6" style="margin-bottom: 10px;">
                                <label for="">Phone Number</label>
                                <input type="number" name="telepon" class="form-control telepon" id="telepon" placeholder="telepon Number">
                            </div>
                            <div class="col-md-6" style="margin-bottom: 10px;">
                                <label for="">Package</label>
                                <select class="form-control id_paket" id="id_paket" name="id_paket" aria-label="Default select example">
                                    <option selected>Choose ...</option>
                                    @foreach($getPaket as $paket)
                                        <option value="{{ $paket->id }}">{{ $paket->nama_paket }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-6" style="margin-bottom: 10px;">
                                <label for="">Check in</label>
                                <input type="date" name="checkin" class="form-control checkin" id="checkin">
                                
                            </div>
                            <div class="col-md-6" style="margin-bottom: 10px;">
                                <label for="">Durations</label>
                                <input type="number" name="berapa_malam" class="form-control berapa_malam" id="berapa_malam" placeholder="Nights">
                            </div>
                            <div class="col-md-12" style="margin-bottom: 10px;">
                                <label for="">Number of people</label>
                                <input type="number" name="pax" id="pax" class="form-control pax" placeholder="Number of people">
                            </div>
                            <input type="hidden" name="id_campsite" class="form-control id_campsite" id="id_campsite" value="{{ $id }}">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-block btn-submit-reservasi-online">Search Reservation</button>
                            </div>
                        </div><br>
                    <!-- </form> -->
                </div>
            </div>
        </div>
        {{-- <div class="bg-blue py-4">
            <div class="row px-3"> <small class="ml-4 ml-sm-5 mb-2">Copyright &copy; 2019. All rights reserved.</small>
                <div class="social-contact ml-4 ml-sm-auto"> <span class="fa fa-facebook mr-4 text-sm"></span> <span class="fa fa-google-plus mr-4 text-sm"></span> <span class="fa fa-linkedin mr-4 text-sm"></span> <span class="fa fa-twitter mr-4 mr-sm-5 text-sm"></span> </div>
            </div>
        </div> --}}
    </div>
</div>

<style>
    body {
        color: #000;
        overflow-x: hidden;
        height: 100%;
        background-color: #B0BEC5;
        background-repeat: no-repeat
    }

    .card0 {
        box-shadow: 0px 4px 8px 0px #757575;
        border-radius: 0px
    }

    .card2 {
        margin: 0px 40px
    }

    .logo {
        width: 150px;
        height: 70px;
        margin-top: 20px;
        margin-left: 35px
    }

    .image {
        width: 460px;
        height: 360px
    }

    .border-line {
        border-right: 1px solid #EEEEEE
    }

    .facebook {
        background-color: #3b5998;
        color: #fff;
        font-size: 18px;
        padding-top: 5px;
        border-radius: 50%;
        width: 35px;
        height: 35px;
        cursor: pointer
    }

    .twitter {
        background-color: #1DA1F2;
        color: #fff;
        font-size: 18px;
        padding-top: 5px;
        border-radius: 50%;
        width: 35px;
        height: 35px;
        cursor: pointer
    }

    .linkedin {
        background-color: #2867B2;
        color: #fff;
        font-size: 18px;
        padding-top: 5px;
        border-radius: 50%;
        width: 35px;
        height: 35px;
        cursor: pointer
    }

    .line {
        height: 1px;
        width: 45%;
        background-color: #E0E0E0;
        margin-top: 10px
    }

    .or {
        width: 10%;
        font-weight: bold
    }

    .text-sm {
        font-size: 14px !important
    }

    ::placeholder {
        color: #BDBDBD;
        opacity: 1;
        font-weight: 300
    }

    :-ms-input-placeholder {
        color: #BDBDBD;
        font-weight: 300
    }

    ::-ms-input-placeholder {
        color: #BDBDBD;
        font-weight: 300
    }

    input,
    textarea {
        padding: 10px 12px 10px 12px;
        border: 1px solid lightgrey;
        border-radius: 2px;
        margin-bottom: 5px;
        margin-top: 2px;
        width: 100%;
        box-sizing: border-box;
        color: #2C3E50;
        font-size: 14px;
        letter-spacing: 1px
    }

    input:focus,
    textarea:focus {
        -moz-box-shadow: none !important;
        -webkit-box-shadow: none !important;
        box-shadow: none !important;
        border: 1px solid #304FFE;
        outline-width: 0
    }

    button:focus {
        -moz-box-shadow: none !important;
        -webkit-box-shadow: none !important;
        box-shadow: none !important;
        outline-width: 0
    }

    a {
        color: inherit;
        cursor: pointer
    }

    .btn-blue {
        background-color: #1A237E;
        width: 150px;
        color: #fff;
        border-radius: 2px
    }

    .btn-blue:hover {
        background-color: #000;
        cursor: pointer
    }

    .bg-blue {
        color: #fff;
        background-color: #1A237E
    }

    @media screen and (max-width: 991px) {
        .logo {
            margin-left: 0px
        }

        .image {
            width: 300px;
            height: 220px
        }

        .border-line {
            border-right: none
        }

        .card2 {
            border-top: 1px solid #EEEEEE !important;
            margin: 0px 15px
        }
    }
</style>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.1/jquery.min.js"></script>
<script type="text/javascript">
    // $.ajaxSetup({
    //     headers: {
    //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    //     }
    // });

    $(".btn-submit-reservasi-online").click(function(e){
        e.preventDefault();
        var name = $("input[name=name]").val();
        var id_campsite = $("input[name=id_campsite]").val();
        var telepon = $("input[name=telepon]").val();
        var checkin = $("input[name=checkin]").val();
        var berapa_malam = $("input[name=berapa_malam]").val();
        var pax = $("input[name=pax]").val();
        var id_paket = $('select[name="id_paket"]').val();

        $.ajax({
            type:'POST',
            url: "{{ url('api/admin/reservasi/store-reservasi-online') }}",
            data:{
                "_token": "{{ csrf_token() }}", 
                name:name, 
                telepon:telepon, 
                checkin:checkin, 
                berapa_malam:berapa_malam, 
                pax:pax, 
                id_paket:id_paket, 
                id_campsite:id_campsite
            },
            success:function(data){
                console.log(data.data)
                window.open(data.data.generateWaLink,'_blank');
                window.location.href = '/reservasi-success/' + data.data.data_reservasi.id;
                // document.getElementById("text-info").innerHTML = '<div class="alert alert-success">'+text_yes+'</div>';                
            }
        });

    });
</script>
</body>
</html>