<?php

use App\Http\Controllers\Api\ApiAuthController;
use App\Http\Controllers\Api\ApiPackageController;
use App\Http\Controllers\Api\ApiRoleController;
use App\Http\Controllers\Api\ApiUserController;
use App\Http\Controllers\Api\ApiBussinessController;
use App\Http\Controllers\Api\ApiCampsiteBlockController;
use App\Http\Controllers\Api\ApiCampsiteBlockSiteController;
use App\Http\Controllers\Api\ApiVendorsController;
use App\Http\Controllers\Api\ApiKategoriItemController;
use App\Http\Controllers\Api\ApiReservasiController;
use App\Http\Controllers\Api\ApiCampsiteController;
use App\Http\Controllers\Api\ApiCostController;
use App\Http\Controllers\Api\ApiDashboardController;
use App\Http\Controllers\Api\ApiImageCampsiteController;
use App\Http\Controllers\Api\ApiFinanceController;
use App\Http\Controllers\Api\ApiMapConfigController;
use App\Http\Controllers\Api\ApiReservasiOnlineController;
use App\Http\Controllers\Api\ApiTicketController;
use App\Http\Controllers\Api\ApiVendorItemController;
use App\Http\Controllers\api\ApiWhatsappController;
use App\Http\Controllers\Api\Master\ApiBannerController;
use App\Http\Controllers\Api\Master\ApiCostCategoryController;
use App\Http\Controllers\Api\Master\ApiCustomerController;
use App\Http\Controllers\Api\Master\ApiPostController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', [ApiAuthController::class, 'login']);
Route::post('admin/reservasi/store-reservasi-online', [ApiReservasiController::class, 'storeReservasiOnline']);
Route::prefix('public')->name('public')->group(function () {
    Route::prefix('dashboard')->name('dashboard.')->group(function () {
        Route::get('/today-reservation', [ApiDashboardController::class, 'todayReservation'])->name('today-reservation');
        Route::get('/available-reservation', [ApiDashboardController::class, 'availableReservation'])->name('available-reservation');
        Route::get('/map-reservation', [ApiDashboardController::class, 'todayReservationMap'])->name('map-reservation');
    });
});

Route::middleware('auth:sanctum')->group(function () {

    Route::prefix('admin')->group(function () {

        Route::prefix('dashboard')->name('dashboard.')->group(function () {
            Route::get('/today-reservation', [ApiDashboardController::class, 'todayReservation'])->name('today-reservation');
            Route::get('/available-reservation', [ApiDashboardController::class, 'availableReservation'])->name('available-reservation');
            Route::get('/map-reservation', [ApiDashboardController::class, 'todayReservationMap'])->name('map-reservation');
        });

        Route::name('api.admin.master.')->group(function () {
            Route::prefix('tickets')->name('tickets.')->group(function () {
                Route::get('/', [ApiTicketController::class, 'index'])->middleware('permission:TICKET.READ')->name('read');
                Route::get('/all', [ApiTicketController::class, 'all'])->middleware('permission:TICKET.READ')->name('read');
                Route::post('/', [ApiTicketController::class, 'store'])->middleware('permission:TICKET.CREATE')->name('create');
                Route::put('{id}', [ApiTicketController::class, 'update'])->middleware('permission:TICKET.UPDATE')->name('update');
                Route::delete('{id}', [ApiTicketController::class, 'destroy'])->middleware('permission:TICKET.DELETE')->name('delete');
            });

            Route::prefix('customers')->name('customers.')->group(function () {
                Route::get('/', [ApiCustomerController::class, 'index'])->middleware('permission:CUSTOMER.READ')->name('read');
                Route::post('/', [ApiCustomerController::class, 'store'])->middleware('permission:CUSTOMER.CREATE')->name('create');
                Route::put('{id}', [ApiCustomerController::class, 'update'])->middleware('permission:CUSTOMER.UPDATE')->name('update');
                Route::delete('{id}', [ApiCustomerController::class, 'destroy'])->middleware('permission:CUSTOMER.DELETE')->name('delete');
            });

            Route::prefix('bussiness')->name('bussiness.')->group(function () {
                Route::get('/', [ApiBussinessController::class, 'index'])->middleware('permission:BUSSINESS.READ')->name('read');
                Route::get('/user-access', [ApiBussinessController::class, 'userAccessList'])->middleware('permission:BUSSINESS.READ')->name('user-access');
                Route::post('/', [ApiBussinessController::class, 'store'])->middleware('permission:BUSSINESS.CREATE')->name('create');
                Route::put('{id}', [ApiBussinessController::class, 'update'])->middleware('permission:BUSSINESS.UPDATE')->name('update');
                Route::delete('{id}', [ApiBussinessController::class, 'destroy'])->middleware('permission:BUSSINESS.DELETE')->name('delete');
            });

            Route::prefix('vendors')->name('vendors.')->group(function () {
                Route::get('/', [ApiVendorsController::class, 'index'])->middleware('permission:VENDOR.READ')->name('read');
                Route::get('/all', [ApiVendorsController::class, 'all'])->middleware('permission:VENDOR.READ')->name('all');
                Route::post('/', [ApiVendorsController::class, 'store'])->middleware('permission:VENDOR.CREATE')->name('create');
                Route::put('{id}', [ApiVendorsController::class, 'update'])->middleware('permission:VENDOR.UPDATE')->name('update');
                Route::delete('{id}', [ApiVendorsController::class, 'destroy'])->middleware('permission:VENDOR.DELETE')->name('delete');
            });

            Route::prefix('vendor-items')->name('vendor-items.')->group(function () {
                Route::get('/', [ApiVendorItemController::class, 'index'])->middleware('permission:VENDOR-ITEM.READ')->name('read');
                Route::post('/', [ApiVendorItemController::class, 'store'])->middleware('permission:VENDOR-ITEM.CREATE')->name('create');
                Route::put('{id}', [ApiVendorItemController::class, 'update'])->middleware('permission:VENDOR-ITEM.UPDATE')->name('update');
                Route::delete('{id}', [ApiVendorItemController::class, 'destroy'])->middleware('permission:VENDOR-ITEM.DELETE')->name('delete');
            });

            Route::prefix('packages')->group(function () {
                Route::get('/', [ApiPackageController::class, 'index'])->middleware('permission:PACKAGE.READ')->name('read');
                Route::get('/{id}', [ApiPackageController::class, 'show'])->middleware('permission:PACKAGE.READ')->name('read');
                Route::post('/', [ApiPackageController::class, 'store'])->middleware('permission:PACKAGE.READ')->name('read');
                Route::put('/{id}', [ApiPackageController::class, 'update'])->middleware('permission:PACKAGE.READ')->name('read');
                Route::delete('{id}', [ApiPackageController::class, 'destroy'])->middleware('permission:PACKAGE.READ')->name('read');
            });

            Route::prefix('campsites')->name('campsites.')->group(function () {
                Route::get('/', [ApiCampsiteController::class, 'index'])->middleware('permission:CAMPSITE.READ')->name('read');
                Route::get('/list-block', [ApiCampsiteController::class, 'listBlock'])->middleware('permission:CAMPSITE.READ')->name('list-block');
                // Route::get('/list-block/{campsite_id}', [ApiCampsiteController::class, 'listBlock'])->middleware('permission:CAMPSITE.READ')->name('list-block');
                Route::post('/', [ApiCampsiteController::class, 'store'])->middleware('permission:CAMPSITE.CREATE')->name('create');
                Route::put('{id}', [ApiCampsiteController::class, 'update'])->middleware('permission:CAMPSITE.UPDATE')->name('update');
                Route::delete('{id}', [ApiCampsiteController::class, 'destroy'])->middleware('permission:CAMPSITE.DELETE')->name('delete');
            });

            Route::prefix('campsite-blocks')->name('campsite-blocks.')->group(function () {
                Route::get('/', [ApiCampsiteBlockController::class, 'index'])->middleware('permission:CAMPSITE.READ')->name('read');
                Route::post('/', [ApiCampsiteBlockController::class, 'store'])->middleware('permission:CAMPSITE.CREATE')->name('create');
                Route::put('{id}', [ApiCampsiteBlockController::class, 'update'])->middleware('permission:CAMPSITE.UPDATE')->name('update');
                Route::delete('{id}', [ApiCampsiteBlockController::class, 'destroy'])->middleware('permission:CAMPSITE.DELETE')->name('delete');
            });

            Route::prefix('campsite-block-sites')->name('campsite-block-sites.')->group(function () {
                Route::get('/', [ApiCampsiteBlockSiteController::class, 'index'])->middleware('permission:CAMPSITE.READ')->name('read');
                Route::post('/', [ApiCampsiteBlockSiteController::class, 'store'])->middleware('permission:CAMPSITE.CREATE')->name('create');
                Route::get('{id}', [ApiCampsiteBlockSiteController::class, 'show'])->middleware('permission:CAMPSITE.READ')->name('read');
                Route::post('/update-map-coordinate', [ApiCampsiteBlockSiteController::class, 'updateMapCoordinate'])->middleware('permission:CAMPSITE.CREATE')->name('update-map');
                Route::put('{id}', [ApiCampsiteBlockSiteController::class, 'update'])->middleware('permission:CAMPSITE.UPDATE')->name('update');
                Route::delete('{id}', [ApiCampsiteBlockSiteController::class, 'destroy'])->middleware('permission:CAMPSITE.DELETE')->name('delete');
            });

            Route::prefix('images-block-sites')->name('images-block-sites.')->group(function () {
                Route::get('/', [ApiImageCampsiteController::class, 'index'])->middleware('permission:CAMPSITE.READ')->name('read');
            });

            Route::prefix('posts')->name('posts.')->group(function () {
                Route::get('/', [ApiPostController::class, 'index'])->middleware('permission:LANDING-PAGE-AND-NEWS-CIS.READ')->name('read');
                Route::post('/', [ApiPostController::class, 'store'])->middleware('permission:LANDING-PAGE-AND-NEWS-CIS.CREATE')->name('create');
                Route::put('{id}', [ApiPostController::class, 'update'])->middleware('permission:LANDING-PAGE-AND-NEWS-CIS.UPDATE')->name('update');
                Route::delete('{id}', [ApiPostController::class, 'destroy'])->middleware('permission:LANDING-PAGE-AND-NEWS-CIS.DELETE')->name('delete');
            });

            Route::prefix('banners')->name('banners.')->group(function () {
                Route::get('/', [ApiBannerController::class, 'index'])->middleware('permission:LANDING-PAGE-AND-NEWS-CIS.READ')->name('read');
                Route::post('/', [ApiBannerController::class, 'store'])->middleware('permission:LANDING-PAGE-AND-NEWS-CIS.CREATE')->name('create');
                Route::post('/{id}', [ApiBannerController::class, 'update'])->middleware('permission:LANDING-PAGE-AND-NEWS-CIS.UPDATE')->name('update');
                Route::delete('/{id}', [ApiBannerController::class, 'destroy'])->middleware('permission:LANDING-PAGE-AND-NEWS-CIS.DELETE')->name('delete');
            });

            Route::prefix('cost-categories')->name('cost-categories.')->group(function () {
                Route::get('/', [ApiCostCategoryController::class, 'index'])->middleware('permission:COST.READ')->name('read');
                Route::get('/list', [ApiCostCategoryController::class, 'list'])->middleware('permission:COST.READ')->name('list');
                Route::post('/', [ApiCostCategoryController::class, 'store'])->middleware('permission:COST.CREATE')->name('create');
                Route::put('/{id}', [ApiCostCategoryController::class, 'update'])->middleware('permission:COST.UPDATE')->name('update');
                Route::delete('/{id}', [ApiCostCategoryController::class, 'destroy'])->middleware('permission:COST.DELETE')->name('delete');
            });

            Route::prefix('costs')->name('costs.')->group(function () {
                Route::get('/', [ApiCostController::class, 'index'])->middleware('permission:COST.READ')->name('read');
                Route::post('/', [ApiCostController::class, 'store'])->middleware('permission:COST.CREATE')->name('create');
                Route::post('/filter', [ApiCostController::class, 'filter'])->middleware('permission:COST.READ')->name('filter');
                Route::put('/', [ApiCostController::class, 'update'])->middleware('permission:COST.UPDATE')->name('update');
                Route::delete('/{id}', [ApiCostController::class, 'destroy'])->middleware('permission:COST.DELETE')->name('delete');
            });
        });

        Route::name('api.admin.utility.')->group(function () {

            Route::prefix('map')->name('map.')->group(function () {
                Route::post('/update', [ApiMapConfigController::class, 'update'])->middleware('permission:USER.CREATE')->name('update');
            });

            Route::prefix('users')->name('users.')->group(function () {
                Route::get('/', [ApiUserController::class, 'index'])->middleware('permission:USER.READ')->name('read');
                Route::post('/', [ApiUserController::class, 'store'])->middleware('permission:USER.CREATE')->name('create');
                Route::put('{id}', [ApiUserController::class, 'update'])->middleware('permission:USER.UPDATE')->name('update');
                Route::delete('{id}', [ApiUserController::class, 'destroy'])->middleware('permission:USER.DELETE')->name('delete');
            });

            Route::prefix('roles')->name('roles.')->group(function () {
                Route::get('/', [ApiRoleController::class, 'index'])->middleware('permission:ROLE.READ')->name('read');
                Route::post('/', [ApiRoleController::class, 'store'])->middleware('permission:ROLE.CREATE')->name('create');
                Route::put('{id}', [ApiRoleController::class, 'update'])->middleware('permission:ROLE.UPDATE')->name('update');
                Route::delete('{id}', [ApiRoleController::class, 'destroy'])->middleware('permission:ROLE.DELETE')->name('delete');
            });
        });

        // finance
        Route::name('api.admin.finance.')->group(function () {
            Route::get('/finance-pembiayaan', [ApiFinanceController::class, 'pembiayaan'])->middleware('permission:CUSTOMER.READ')->name('finance-pembiayaan');
            Route::get('/finance-penjualan', [ApiFinanceController::class, 'penjualan'])->middleware('permission:CUSTOMER.READ')->name('finance-penjualan');
            Route::get('/finance-vendor', [ApiFinanceController::class, 'vendor'])->middleware('permission:CUSTOMER.READ')->name('finance-vendor');
            Route::get('/finance-vendor-detail', [ApiFinanceController::class, 'vendorDetail'])->middleware('permission:CUSTOMER.READ')->name('finance-vendor-detail');
            Route::get('/finance-a3', [ApiFinanceController::class, 'a3'])->middleware('permission:CUSTOMER.READ')->name('finance-a3');
        });

        Route::prefix('category-item')->group(function () {
            Route::get('/', [ApiKategoriItemController::class, 'index']);
            Route::post('/', [ApiKategoriItemController::class, 'store']);
            Route::put('{id}/update', [ApiKategoriItemController::class, 'update']);
            Route::delete('{id}/delete', [ApiKategoriItemController::class, 'destroy']);
        });

        Route::prefix('reservasi')->group(function () {
            Route::get('/', [ApiReservasiController::class, 'index']);
            Route::get('/status-list', [ApiReservasiController::class, 'listStatus']);
            Route::get('/select2-item', [ApiReservasiController::class, 'get_item']);
            Route::get('/select2-get-price/{id}', [ApiReservasiController::class, 'getPriceItem']);
            Route::get('/select2-get-include-item/{id}', [ApiReservasiController::class, 'itemInclude']);
            Route::get('/select2-get-not-include-item/{id}', [ApiReservasiController::class, 'itemNotInclude']);
            Route::get('/datatables-item/{id}', [ApiReservasiController::class, 'datatablesItem']);
            Route::get('/datatables-list-payment/{id}', [ApiReservasiController::class, 'datatablesPayment']);
            Route::delete('/delete-item/{id}', [ApiReservasiController::class, 'deleteItem']);
            Route::post('/store', [ApiReservasiController::class, 'storeReservasi']);
            Route::post('/store-new-item-reservation', [ApiReservasiController::class, 'storeItemReservasi']);
            Route::put('/{id}', [ApiReservasiController::class, 'updateReservasi']);
            Route::post('/add-new-item', [ApiReservasiController::class, 'addItem']);
            Route::post('/reservasi-payment/{id}', [ApiReservasiController::class, 'payment']);
            Route::post('/change-status-checkin/{id}', [ApiReservasiController::class, 'changeStatusCheckin']);
            Route::post('/change-status-checkout/{id}', [ApiReservasiController::class, 'changeStatusCheckout']);
            Route::post('/send-notification/{id}', [ApiReservasiController::class, 'sendNotification']);
        });

        Route::prefix('notification')->group(function () {
            Route::get('/', [ApiWhatsappController::class, 'index']);
        });

        Route::prefix('reservasi-online')->group(function () {
            Route::get('/', [ApiReservasiOnlineController::class, 'index']);
            Route::put('/{id}', [ApiReservasiOnlineController::class, 'updateReservasi']);
        });

        Route::prefix('campsite-image')->group(function () {
            Route::get('/datatables', [ApiCampsiteController::class, 'datatablesCampImage'])->name('camp-img.list')->middleware('permission:CAMPSITE-IMAGE.READ');
        });

        Route::prefix('tickets')->name('ticket-transaction.')->group(function () {
            Route::get('/transaction', [ApiTicketController::class, 'indexTicketTransantion'])->middleware('permission:TICKET.READ')->name('index');
            Route::post('/transaction', [ApiTicketController::class, 'storeTicketTransantion'])->middleware('permission:TICKET.CREATE')->name('create');
            Route::delete('/transaction/{id}', [ApiTicketController::class, 'destroyTicketTransantion'])->middleware('permission:TICKET.DELETE')->name('delete');
        });
    });
});
