<?php

use App\Http\Controllers\Web\WebAdminDashboardController;
use App\Http\Controllers\Web\WebAuthController;
use App\Http\Controllers\Web\WebKategoriItemController;
use App\Http\Controllers\Web\WebKategoryAttributItemController;
use App\Http\Controllers\Web\WebInvoiceController;
use App\Http\Controllers\Api\ApiReservasiController;
use App\Http\Controllers\Web\CampsiteImageController;
use App\Http\Controllers\Web\Master\WebAdminCustomerController;
use App\Http\Controllers\Web\Master\WebBannerController;
use App\Http\Controllers\Web\Master\WebBussinessController;
use App\Http\Controllers\Web\Master\WebCampsiteController;
use App\Http\Controllers\Web\Master\WebPackageController;
use App\Http\Controllers\Web\Master\WebPostController;
use App\Http\Controllers\Web\Master\WebVendorController;
use App\Http\Controllers\Web\Utility\WebAdminRoleController;
use App\Http\Controllers\Web\Utility\WebAdminUserController;
use App\Http\Controllers\Web\WebReservasiController;
use App\Http\Controllers\Web\WebCheckItemReservasiController;
use App\Http\Controllers\Web\WebLandingPageController;
use App\Http\Controllers\Web\WebCostController;
use App\Http\Controllers\Web\WebFinanceController;
use App\Http\Controllers\Web\WebReportController;
use App\Http\Controllers\Web\WebGeneralSettingController;
use App\Http\Controllers\Web\WebReservasiOnlineController;
use App\Http\Controllers\Web\WebTicketController;
use App\Http\Controllers\web\WebWhatsappController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('auth.login');
// });

Route::get('storage/background-ticket/{filename}', function ($filename) {
    $path = storage_path('app/public/ticket_background/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/media-bukti-transfer/{filename}', function ($filename) {
    $path = storage_path('app/public/request_order_image/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/campsite-image/{filename}', function ($filename) {
    $path = storage_path('app/public/campsite_image/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('storage/slider/{filename}', function ($filename) {
    $path = storage_path('app/public/slide-show/' . $filename);
    if (!File::exists($path)) {
        abort(404);
    }
    $file = File::get($path);
    $type = File::mimeType($path);
    $response = Response::make($file, 200);
    $response->header("Content-Type", $type);
    return $response;
});

Route::get('/login-form', [WebAuthController::class, 'loginView']);

Route::get('/', [WebLandingPageController::class, 'landingPage']);
Route::get('/detail/{id}', [WebLandingPageController::class, 'detailPage']);
Route::get('/reservasi-online/{id}', [WebLandingPageController::class, 'reservation']);
Route::get('/reservasi-success/{id}', [WebLandingPageController::class, 'successFrom']);

// Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/auth/logout', [WebAuthController::class, 'logout']);
Route::get('/auth/login', [WebAuthController::class, 'loginView'])->name('login');
Route::post('/auth/login-prosses', [WebAuthController::class, 'loginProsses']);
Route::get('map', [WebAdminDashboardController::class, 'map']);
Route::prefix('reservasi-post')->group(function () {
    Route::post('/', [ApiReservasiController::class, 'store']); //->middleware('permission:bussiness.show');
});

Route::middleware('auth')->group(function () {
    Route::prefix('admin')->group(function () {
        Route::get('dashboard', [WebAdminDashboardController::class, 'index']);
        Route::get('map-config', [WebAdminDashboardController::class, 'mapConfig']);

        // utility
        Route::name('web.admin.utility.')->group(function () {
            Route::get('/users', [WebAdminUserController::class, 'index'])->middleware('permission:USER.READ')->name('users');

            Route::prefix('roles')->name('roles.')->group(function () {
                Route::get('/', [WebAdminRoleController::class, 'index'])->middleware('permission:ROLE.READ')->name('read');
                Route::get('create', [WebAdminRoleController::class, 'create'])->middleware('permission:ROLE.CREATE')->name('create');
                Route::get('/{id}/edit', [WebAdminRoleController::class, 'edit'])->middleware('permission:ROLE.UPDATE')->name('update');
            });
        });

        // master
        Route::name('web.admin.master.')->group(function () {
            Route::get('/tickets', [WebTicketController::class, 'index'])->middleware('permission:TICKET.READ')->name('ticket');
            Route::get('/customers', [WebAdminCustomerController::class, 'index'])->middleware('permission:CUSTOMER.READ')->name('customer');
            Route::get('/bussiness', [WebBussinessController::class, 'index'])->middleware('permission:BUSSINESS.READ')->name('bussiness');
            Route::get('/vendors', [WebVendorController::class, 'index'])->middleware('permission:VENDOR.READ')->name('vendors');
            Route::get('/vendors-item', [WebVendorController::class, 'indexVendorItem'])->middleware('permission:VENDOR-ITEM.READ')->name('vendors-item');
            Route::get('/campsites', [WebCampsiteController::class, 'index'])->middleware('permission:CAMPSITE.READ')->name('campsites');
            Route::get('/campsites-block', [WebCampsiteController::class, 'campsiteBlock'])->middleware('permission:CAMPSITE.READ')->name('campsites-block');
            Route::get('/campsites-block-site', [WebCampsiteController::class, 'campsiteBlockSite'])->middleware('permission:CAMPSITE.READ')->name('campsites-block-site');
            Route::get('/campsites-block-site/create', [WebCampsiteController::class, 'campsiteBlockSiteCreate'])->middleware('permission:CAMPSITE.READ')->name('campsites-block-site-create');
            Route::get('/campsites-block-site/update/{id}', [WebCampsiteController::class, 'campsiteBlockSiteupdate'])->middleware('permission:CAMPSITE.READ')->name('campsites-block-site-update');
            // Route::get('/images-block-site', [WebCampsiteController::class, 'imagesBlockSite'])->middleware('permission:CAMPSITE.READ')->name('images-block-site');
            // Route::get('/images-block-site/create', [WebCampsiteController::class, 'imagesBlockSiteCreate'])->middleware('permission:CAMPSITE.READ')->name('images-block-site-create');
            // Route::get('/images-block-site/store', [WebCampsiteController::class, 'imagesBlockSiteStore'])->middleware('permission:CAMPSITE.READ')->name('images-block-site-store');
            Route::get('/inventory', [WebInventoryController::class, 'index'])->middleware('permission:INVENTORY.READ')->name('inventory');
            Route::get('/banners', [WebBannerController::class, 'index'])->middleware('permission:LANDING-PAGE-AND-NEWS-CIS.READ')->name('banners');
            Route::get('/cost-categories', [WebCostController::class, 'category'])->middleware('permission:COST.READ')->name('cost-categories');

            Route::prefix('costs')->name('costs.')->group(function () {
                Route::get('/', [WebCostController::class, 'index'])->middleware('permission:COST.READ')->name('read');
                Route::get('create', [WebCostController::class, 'create'])->middleware('permission:COST.CREATE')->name('create');
                Route::get('/{id}/edit', [WebCostController::class, 'edit'])->middleware('permission:COST.UPDATE')->name('update');
            });

            Route::prefix('posts')->name('posts.')->group(function () {
                Route::get('/', [WebPostController::class, 'index'])->middleware('permission:LANDING-PAGE-AND-NEWS-CIS.READ')->name('read');
                Route::get('create', [WebPostController::class, 'create'])->middleware('permission:LANDING-PAGE-AND-NEWS-CIS.CREATE')->name('create');
                Route::get('/{id}/edit', [WebPostController::class, 'edit'])->middleware('permission:LANDING-PAGE-AND-NEWS-CIS.UPDATE')->name('update');
            });

            Route::prefix('packages')->name('packages.')->group(function () {
                Route::get('/', [WebPackageController::class, 'index'])->middleware('permission:PACKAGE.READ')->name('read');
                Route::get('create', [WebPackageController::class, 'create'])->middleware('permission:PACKAGE.CREATE')->name('create');
                Route::get('/{id}/edit', [WebPackageController::class, 'edit'])->middleware('permission:PACKAGE.UPDATE')->name('update');
            });
        });

        // transaksi
        Route::name('web.admin.transaksi.')->group(function () {

            Route::prefix('reservasi')->name('reservasi.')->group(function () {
                Route::get('/', [WebReservasiController::class, 'index'])->middleware('permission:RESERVASI.READ')->name('read');
                Route::get('/ticket/{id}', [WebReservasiController::class, 'showTicket'])->middleware('permission:RESERVASI.READ')->name('read');
                Route::get('/print-ticket/{id}', [WebReservasiController::class, 'printTicket'])->middleware('permission:RESERVASI.READ')->name('read');
                Route::get('/batch-cencel', [WebReservasiController::class, 'cencelReservation'])->middleware('permission:RESERVASI.READ')->name('read');
                Route::get('/create', [WebReservasiController::class, 'create'])->middleware('permission:RESERVASI.READ')->name('create');
                Route::get('/detail/{id}', [WebReservasiController::class, 'detail'])->middleware('permission:RESERVASI.READ')->name('detail');
                Route::get('/edit/{id}', [WebReservasiController::class, 'edit'])->middleware('permission:RESERVASI.READ')->name('edit');
                Route::get('/view-invoice/{id}', [WebReservasiController::class, 'viewInvoice'])->middleware('permission:RESERVASI.READ')->name('view-invoice');
                Route::get('/payment-detail/{id}', [WebReservasiController::class, 'paymentDetail'])->middleware('permission:RESERVASI.READ')->name('payment-detail');
                Route::get('/send-notification/{id}', [WebReservasiController::class, 'sendNotification'])->middleware('permission:RESERVASI.READ')->name('payment-detail');
                Route::post('/reservasi-payment/{id}', [ApiReservasiController::class, 'payment'])->middleware('permission:RESERVASI.READ')->name('reservasi-payment');
            });

            Route::prefix('reservasi-online')->name('reservasi-online.')->group(function () {
                Route::get('/', [WebReservasiOnlineController::class, 'index'])->middleware('permission:RESERVASI.READ')->name('read');
                Route::get('/edit/{id}', [WebReservasiOnlineController::class, 'edit'])->middleware('permission:RESERVASI.READ')->name('edit');
                Route::get('/payment-detail/{id}', [WebReservasiController::class, 'paymentDetail'])->middleware('permission:RESERVASI.READ')->name('payment-detail');
            });

            Route::prefix('financing')->group(function () {
                Route::get('/', [WebReportPembiayaanController::class, 'index']); //->middleware('permission:bussiness.show');
            });

            Route::prefix('tickets')->group(function () {
                Route::get('/transaction', [WebTicketController::class, 'transactionIndex'])->middleware('permission:TICKET.READ');
                Route::get('/create-stand-alone', [WebTicketController::class, 'create'])->middleware('permission:TICKET.READ');
            });
        });

        // report
        Route::name('web.admin.report.')->group(function () {
            Route::get('/report-pembiayaan', [WebReportController::class, 'pembiayaan'])->middleware('permission:CUSTOMER.READ')->name('report-pembiayaan');
            Route::get('/report-pembiayaan-export', [WebReportController::class, 'pembiayaan_export'])->middleware('permission:CUSTOMER.READ')->name('report-pembiayaan-export');
            Route::get('/report-penjualan', [WebReportController::class, 'penjualan'])->middleware('permission:CUSTOMER.READ')->name('report-penjualan');
            Route::get('/report-penjualan-export', [WebReportController::class, 'penjualan_export'])->middleware('permission:CUSTOMER.READ')->name('report-penjualan-export');
        });

        // finance
        Route::name('web.admin.finance.')->group(function () {
            Route::get('/finance-pembiayaan', [WebFinanceController::class, 'pembiayaan'])->middleware('permission:CUSTOMER.READ')->name('finance-pembiayaan');
            Route::get('/finance-pembiayaan/detail/{id}', [WebFinanceController::class, 'detailPembiayaan'])->middleware('permission:CUSTOMER.READ')->name('finance-pembiayaan');
            Route::get('/finance-penjualan', [WebFinanceController::class, 'penjualan'])->middleware('permission:CUSTOMER.READ')->name('finance-penjualan');
            Route::get('/finance-vendor', [WebFinanceController::class, 'vendor'])->middleware('permission:CUSTOMER.READ')->name('finance-vendor');
            Route::get('/finance-vendor/detail/{id}', [WebFinanceController::class, 'detailVendor'])->middleware('permission:CUSTOMER.READ')->name('finance-vendor-detail');
            Route::get('/finance-a3', [WebFinanceController::class, 'a3'])->middleware('permission:CUSTOMER.READ')->name('finance-a3');
            Route::get('/finance-a3-print', [WebFinanceController::class, 'a3Print'])->middleware('permission:CUSTOMER.READ')->name('finance-a3-print');
        });

        Route::prefix('category-item')->group(function () {
            Route::get('/', [WebKategoriItemController::class, 'index']); //->middleware('permission:bussiness.show');
        });

        Route::prefix('kategory-attribute-item')->group(function () {
            Route::get('/', [WebKategoryAttributItemController::class, 'index']); //->middleware('permission:bussiness.show');
        });

        Route::prefix('invoice')->group(function () {
            Route::get('/', [WebInvoiceController::class, 'index']); //->middleware('permission:bussiness.show');
            Route::get('/show/{id}', [WebInvoiceController::class, 'invoice'])->name('reservasi_invoice');
        });

        Route::prefix('check-items')->group(function () {
            Route::get('/', [WebCheckItemReservasiController::class, 'index'])->name('check.list')->middleware('permission:HISTORY-PELAYANAN.READ');
            Route::get('/detail-item/{id}', [WebCheckItemReservasiController::class, 'detail'])->middleware('permission:HISTORY-PELAYANAN.UPDATE');
            Route::post('/status-update/{id}', [WebCheckItemReservasiController::class, 'saveChange'])->middleware('permission:ADDITIONAL-PAKET-OR-PRODUK.UPDATE');
        });

        Route::prefix('campsite-image')->group(function () {
            Route::get('/', [CampsiteImageController::class, 'index'])->name('camp-img.list')->middleware('permission:CAMPSITE-IMAGE.READ');
            Route::get('/create', [CampsiteImageController::class, 'create'])->name('camp-img.create')->middleware('permission:CAMPSITE-IMAGE.CREATE');
            Route::post('/store', [CampsiteImageController::class, 'store'])->name('camp-img.store')->middleware('permission:CAMPSITE-IMAGE.CREATE');
        });

        Route::prefix('general-setting-front-end')->group(function () {
            Route::get('/', [WebGeneralSettingController::class, 'index'])->name('general-setting.list')->middleware('permission:CAMPSITE-IMAGE.READ');
            Route::post('/update', [WebGeneralSettingController::class, 'update'])->name('general-setting.update')->middleware('permission:CAMPSITE-IMAGE.READ');
        });

        Route::prefix('general-setting-ticket')->group(function () {
            Route::get('/', [WebGeneralSettingController::class, 'indexSettingTicket'])->name('general-setting.list')->middleware('permission:CAMPSITE-IMAGE.READ');
            Route::post('/update-setting-ticket', [WebGeneralSettingController::class, 'updateSettingTicket'])->name('general-setting.update')->middleware('permission:CAMPSITE-IMAGE.READ');
        });

        Route::prefix('general-setting-whatsapp')->group(function () {
            Route::get('/', [WebGeneralSettingController::class, 'indexSettingWhatsapp'])->name('general-setting.list')->middleware('permission:CAMPSITE-IMAGE.READ');
            Route::post('/update-setting-whatsapp', [WebGeneralSettingController::class, 'updateSettingWhatsapp'])->name('general-setting.update')->middleware('permission:CAMPSITE-IMAGE.READ');
        });

        Route::prefix('whatsapp-template')->group(function () {
            Route::get('/', [WebWhatsappController::class, 'index'])->name('notif.list')->middleware('permission:CAMPSITE-IMAGE.READ');
            Route::get('/update/{id}', [WebWhatsappController::class, 'update'])->name('notif.show')->middleware('permission:CAMPSITE-IMAGE.READ');
            Route::get('/create', [WebWhatsappController::class, 'create'])->name('notif.create')->middleware('permission:CAMPSITE-IMAGE.READ');
            Route::post('/edit/{id}', [WebWhatsappController::class, 'edit'])->name('notif.edit')->middleware('permission:CAMPSITE-IMAGE.READ');
            Route::post('/store', [WebWhatsappController::class, 'store'])->name('notif.store')->middleware('permission:CAMPSITE-IMAGE.READ');
        });

        Route::prefix('banners')->group(function () {
            Route::get('/', [WebBannerController::class, 'index'])->middleware('permission:LANDING-PAGE-AND-NEWS-CIS.READ')->name('banners.list');
            Route::get('/create', [WebBannerController::class, 'create'])->middleware('permission:LANDING-PAGE-AND-NEWS-CIS.READ')->name('banners-create');
            Route::post('/store', [WebBannerController::class, 'store'])->middleware('permission:LANDING-PAGE-AND-NEWS-CIS.READ')->name('banners-store');
        });
    });
});
